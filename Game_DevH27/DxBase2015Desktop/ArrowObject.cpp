#include "stdafx.h"
#include "Project.h"

namespace basedx11{

	ArrowObject::ArrowObject(const shared_ptr<Stage>& StagePtr, const Vector3& i_Pos, const Vector2 ArrowId) :
		GameObject(StagePtr),
		m_Pos(i_Pos),
		m_Arrowid(ArrowId)
		
	{}

	//ArrowIdの値で画像リソースを変える関数
	void ArrowObject::ReturnResource(wstring& i_TexName ,Vector2 i_Arrow ){
		//wstring ArrowResorce(L"");
		if (i_Arrow == Vector2(1, 0))	i_TexName += L"ARROWRIGHT_TX";
		if (i_Arrow == Vector2(-1, 0))	i_TexName += L"ARROWLEFT_TX";
		if (i_Arrow == Vector2(0, 1))	i_TexName += L"ARROWUP_TX";
		if (i_Arrow == Vector2(0, -1))	i_TexName += L"ARROWDOWN_TX";
	}

	//初期化
	void ArrowObject::Create(){

		auto PtrTransform = GetComponent<Transform>();
		PtrTransform->SetPosition(m_Pos.x, 1.0f, m_Pos.z);
		PtrTransform->SetRotation(0.0f, 0.0f, 0.0f);
		PtrTransform->SetScale(0.8f, 0.05f, 0.8f);

		//衝突判定をつける
		auto PtrCol = AddComponent<CollisionObb>();
		PtrCol->SetUpdateActive(false);

		//描画コンポーネントの設定
		auto PtrDraw = AddComponent<BasicPNTDraw>();
		//描画するメッシュを設定
		PtrDraw->SetMeshResource(L"DEFAULT_CUBE");
		//テクスチャの動的変更
		wstring TexName = L"";
		ReturnResource(TexName, m_Arrowid);
		PtrDraw->SetTextureResource(TexName);

		//透明処理
		SetAlphaActive(true);
	}

	//向き変更と画像の差し替え
	void ArrowObject::ChangeArrow(Vector2& i_Arrow){
		auto PtrDraw = GetComponent<BasicPNTDraw>();
		wstring TexName = L"";
		ReturnResource(TexName, i_Arrow);
		PtrDraw->SetTextureResource(TexName);

		//新しい向きをメンバに入れて状態を変更
		m_Arrowid = i_Arrow;
	}

	//これがいらなくなったらUpdate & Drawをやめる
	void ArrowObject::Delete(){
		SetUpdateActive(false);
		SetDrawActive(false);
	}

}
//endof  basedx11