#pragma once

#include "stdafx.h"

namespace basedx11{

	/**
	* @class　ArrowObject
	* @brief　ColorCubeの進行方向を変更するClass.
	* @author yuya sike
	* @date 2015/10/21 Start
	*/
	class ArrowObject : public GameObject {

	public:	//構築と破棄
		ArrowObject(const shared_ptr<Stage>& StagePtr, const Vector3& i_Startpos, const Vector2 ArrowId);
		virtual ~ArrowObject(){}
		//初期化
		virtual void Create() override;

		//	---------- Method ----------.

		//画像リソース選択
		void ReturnResource(wstring& Arrow , Vector2 i_Arrow);

		//消去するとき
		void Delete();

		//画像と向きを変える
		void ChangeArrow(Vector2& Arrow);

		// ---------- Accessor ----------.

		//このマットはどちら向きなのか
		const Vector2 GetArrowid() const{
			return m_Arrowid;
		}

	private:
		// ---------- member ----------.
		Vector3 m_Pos;				//プレイヤーの位置
		Vector2 m_Arrowid;			//矢印の向き


	};
}
//endof  basedx11