#include "stdafx.h"
#include "Project.h"

/**
* @class BackGround
* @brief �w�i�̍쐬
* @author ���� �Č�
*/


namespace basedx11{
	//�\�z�Ɣj��
	BackGround::BackGround(shared_ptr<Stage>& StagePtr,
		const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position
		) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Rotation(Rotation),
		m_Position(Position)
	{
	}
	BackGround::~BackGround(){}

	//������
	void BackGround::Create(){
		auto PtrTransform = GetComponent<Transform>();

		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(m_Rotation);
		PtrTransform->SetPosition(m_Position);


		//��������
		SetAlphaActive(true);

		//�`�揇�̕ύX
		SetDrawLayer(-1);

		auto PtrAction = AddComponent<Action>();
		PtrAction->AddRotateBy(20.0f, Vector3(0, XM_PI, 0));
		//���[�v����
		PtrAction->SetLooped(true);

		PtrAction->Run();

		auto PtrDraw = AddComponent<BasicPNTDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_SPHERE");
		PtrDraw->SetTextureResource(L"BACKGROUND_TX");
		PtrDraw->SetOwnShadowActive(false);
	}

	//�X�V
	void BackGround::Update(){
	}

}
//endof  base