#pragma once

#include "stdafx.h"

namespace basedx11{

	class BackGround : public GameObject{
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;
	public:
		//�\�z�Ɣj��
		BackGround(shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position
			);
		virtual ~BackGround();
		//������
		virtual void Create() override;
		//�X�V
		virtual void Update() override;
		//����
	};
	class ActionBackGround : public ObjState < BackGround >
	{
		ActionBackGround(){}
	public:
	};
}
//endof  basedx11
