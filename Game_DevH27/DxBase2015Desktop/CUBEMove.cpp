#include "stdafx.h"
#include "Project.h"

namespace basedx11{
	/**
	* @class　CubeEffect_Move
	* @brief　Cubeが向きを変えたときにAddGameObjectをしてエフェクトを出す
	* @author yuyu sike
	* @date 2016/01/25 Start
	*/

	CubeEffect_Move::CubeEffect_Move(const shared_ptr<Stage>& StagePtr, const Vector3 i_FirstPos, const Vector2 Pre_direction, const size_t i_Name) :
		GameObject(StagePtr),
		m_Pos(i_FirstPos),
		m_Direction(Pre_direction),
		m_Key(i_Name)
	{}

	//作成
	void CubeEffect_Move::Create(){

		//実態は作成しない
		auto Trans = GetComponent<Transform>();
		Trans->SetPosition(m_Pos);
		Trans->SetRotation(Vector3(0, 0, 0));
		Trans->SetScale(Vector3(0, 0, 0));



	}

	//変化
	void CubeEffect_Move::Update(){
		const size_t sztInterval = 3;			//インターバル処理に使うパーティクルが出るまでの待ちフレーム

		////インターバル処理	3フレーム毎にエフェクトの呼び出し
		if (m_sztIntervalCount++ < sztInterval){

		return;

		}
		else {

		m_sztIntervalCount = 0;

		}


		//TODO : なにか消えたらエフェクトの表示
		m_wpGameEffectManager = GetStage()->GetSharedGameObject<GameEffectManager>(L"GameEffectManager", false);
		auto pGameEffectManeger = m_wpGameEffectManager.lock();
		////エフェクト
		Vector3 Pos{ GetComponent<Transform>()->GetPosition() + Vector3(0.0f, 0.2f, 0.0f) };
		pGameEffectManeger->InsertDirectionEffect(Pos, m_Key, Vector2(0.0f, 0.0f));

		auto Trans = GetComponent<Transform>();
		Vector3 Position = Trans->GetPosition();

		Position += Vector3(m_Direction.x, 0.0f, m_Direction.y);

		Trans->SetPosition(Position);

	}

	//反応
	void CubeEffect_Move::Update2(){

	}
}
//endof  basedx11
