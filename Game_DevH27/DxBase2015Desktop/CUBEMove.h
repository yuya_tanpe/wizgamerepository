#pragma once

#include "stdafx.h"

namespace basedx11{
	/**
	* @class　CubeEffect_Move
	* @brief　Cubeが向きを変えたときにAddGameObjectをしてエフェクトを出す
	* @author yuyu sike
	* @date 2016/01/25 Start
	*/
	class CubeEffect_Move : public GameObject{
		Vector3 m_Pos;
		Vector2 m_Direction;
		size_t m_Key;

		//インターバル
		size_t m_sztIntervalCount;

		//	: エフェクト管理システム.
		weak_ptr< GameEffectManager > m_wpGameEffectManager;	

	public:
		CubeEffect_Move(const shared_ptr<Stage>& StagePtr, const Vector3 i_FirstPos, const Vector2 Pre_direction, const size_t i_Name);
		virtual ~CubeEffect_Move(){}
		//初期化
		virtual void Create() override;
		virtual void Update() override;
		virtual void Update2() override;
		//イベント
		//virtual void OnEvent(const shared_ptr<Event>& event) override;

	};


}
//endof  basedx11
