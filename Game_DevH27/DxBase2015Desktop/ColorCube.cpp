#include "stdafx.h"
#include "Project.h"



namespace basedx11{

	//--------------------------------------------------------------------------------------
	//	class ChildBox : public GameObject;
	//	用途: 子供のCUBE
	//--------------------------------------------------------------------------------------
	//構築と破棄
	ChildBox::ChildBox(const shared_ptr<Stage>& StagePtr, const Vector3 i_FirstPos, const wstring i_TextureName, 
		const Vector2 i_Pre_Dir, const size_t i_VecNum, const wstring Key, const wstring i_mesh) :
		GameObject(StagePtr),
		m_FirstPos(i_FirstPos),
		m_TextureName(i_TextureName),
		Pre_direction(i_Pre_Dir),
		m_VecNum(i_VecNum),
		m_ChiledKey(Key),
		m_Mesh(i_mesh)
	{}

	//初期化
	void ChildBox::Create(){
		auto PtrTransform = GetComponent<Transform>();
		PtrTransform->SetScale(Vector3(1.0f, 1.0f, 1.0f));
		PtrTransform->SetRotation(Vector3(0.0f, 0, 0));
		PtrTransform->SetPosition(m_FirstPos);

		Matrix4X4 CharaScale;
		CharaScale.DefTransformation(
			Vector3(0.06f, 0.06f, 0.06f),
			Vector3(0.0f, 0.0f, 0.0f),
			Vector3(0.0f, -0.5f, 0.0f)
			);

		//描画コンポーネント
		auto PtrDraw = AddComponent<BasicFbxPNTDraw>();
		PtrDraw->SetMeshToTransform(CharaScale);
		PtrDraw->SetFbxMeshResource(m_Mesh);
		PtrDraw->SetTextureOnlyNoLight(true);
		//PtrDraw->SetOwnShadowActive(true);
		//auto PtrDraw = AddComponent<BasicPNTDraw>();
		//PtrDraw->SetMeshResource(L"DEFAULT_CUBE");
		//PtrDraw->SetTextureResource(m_TextureName);
		PtrDraw->SetDrawActive(true);

		//衝突判定
		auto PtrObb = AddComponent<CollisionObb>();
		PtrObb->SetFixed(false);
		PtrObb->SetMakedSize(0.6f);

		//親の生成
		auto GameStagePtr = dynamic_pointer_cast<GameStage>(GetStage());
		auto PtrParent = GameStagePtr->AddGameObject<ParentBox>((size_t)m_FirstPos.x, (size_t)m_FirstPos.z, Pre_direction, m_VecNum, m_TextureName, m_ChiledKey);
		
		//親CUBEのグループの取得
		auto Parent_Group = GameStagePtr->GetSharedObjectGroup(L"ParentGroup");
		//親CUBEをグループに登録
		Parent_Group->IntoGroup(PtrParent);

		//pauseなどで使うグループの取得
		auto Pause_Use_Group = GameStagePtr->GetSharedObjectGroup(L"PauseGroup");
		//PauseGloupにゲームオブジェクトを登録
		Pause_Use_Group->IntoGroup(PtrParent);
	}

	void ChildBox::Update2(){
		//Collisionを取得
		auto PtrCol = GetComponent<Collision>();
		auto GameStagePtr = dynamic_pointer_cast<GameStage>(GetStage());
		if (PtrCol->GetHitObject()){
			//CUBEとCUBE同士の当たり判定
			//SimpleEnemyは現在使っていないためあとで変更
			auto Wall = dynamic_pointer_cast<SimpleEnemy>(PtrCol->GetHitObject());
			auto Cube = dynamic_pointer_cast<ChildBox>(PtrCol->GetHitObject());
			if (Wall){
				SetUpdateActive(true);
			}
			else if (Cube){
				CUBEStop();
				auto PtrStageManager = GameStagePtr->GetSharedGameObject<StageManager>(L"StageManager");
				PostEvent(0.0f, GetThis<ChildBox>(), PtrStageManager, L"GoToGameOver");
			}
		}
	}

	//イベントの受け取り ポーズ画面等
	void ChildBox::OnEvent(const shared_ptr<Event>& event){
		//ポーズ画面のときGameStageからL"Pause"送られてくる
		//ゲームクリア
		if (event->m_MsgStr == L"GameClear"){
			auto PtrAction = AddComponent<Action>();
			PtrAction->AddRotateBy(2.0f, Vector3(0.0f, XM_PI, 0.0f));

			PtrAction->SetLooped(true);
			PtrAction->Run();
		}

		//ゲームオーバー
		if (event->m_MsgStr == L"GameOver"){
			auto PtrAction = AddComponent<Action>();
			PtrAction->AddRotateBy(2.0f, Vector3(0.0f, XM_PI, 0.0f));

			PtrAction->SetLooped(true);
			PtrAction->Run();
		}
	}

	//プレイヤーとCUBEの動きを止める
	void ChildBox::PlayerStop(){
		auto GameStagePtr = dynamic_pointer_cast<GameStage>(GetStage());
		auto PlayerPtr = GetStage()->GetSharedGameObject<Player>(L"Player");
		auto Parent_Group_Vec = GameStagePtr->GetSharedObjectGroup(L"ParentGroup")->GetGroupVector();
		for (auto wpParent : Parent_Group_Vec){
			auto pParent = dynamic_pointer_cast<ParentBox>(wpParent.lock());
			if ( pParent->Parent_to_Chiled_Connect() == this->Chiled_to_Parent_Connect() ){
				pParent->SetUpdateActive(false);
			}
		}
		PlayerPtr->SetUpdateActive(false);
	}

	//CUBEの動きを止める
	void ChildBox::CUBEStop(){
		auto GameStagePtr = dynamic_pointer_cast<GameStage>(GetStage());

		auto Parent_Group_Vec = GameStagePtr->GetSharedObjectGroup(L"ParentGroup")->GetGroupVector();
		for (auto wpParent : Parent_Group_Vec){
			auto pParent = dynamic_pointer_cast<ParentBox>(wpParent.lock());
			if (pParent->Parent_to_Chiled_Connect() == this->Chiled_to_Parent_Connect()){
				pParent->SetUpdateActive(false);
				SetUpdateActive(false);
			}
		}
	}

	//TODO : CUBE同士が当たったら反射するようにしてみる
	void ChildBox::CUBEReflection(){
		auto GameStagePtr = dynamic_pointer_cast<GameStage>(GetStage());
		auto Parent_Group_Vec = GameStagePtr->GetSharedObjectGroup(L"ParentGroup")->GetGroupVector();
		for (auto wpParent : Parent_Group_Vec){
			auto pParent = dynamic_pointer_cast<ParentBox>(wpParent.lock());
			if (pParent->Parent_to_Chiled_Connect() == this->Chiled_to_Parent_Connect()){
				Vector2 work = pParent->GetDirection();
				work* -1;	
				pParent->SetDirection(work);
			}
		}
	}

	//親子関係をつける
	void ChildBox::SetParent(const shared_ptr<ParentBox>& Par){
		auto Trans = GetComponent<Transform>();
		Trans->SetParent(Par);
	}

	//親子関係を外す
	void ChildBox::ResetParent(){
		auto Trans = GetComponent<Transform>();
		Trans->SetParent(nullptr);
	}

	//--------------------------------------------------------------------------------------
	//	class ParentBox : public GameObject;
	//	用途: 親のCUBE
	//--------------------------------------------------------------------------------------
	//構築と破棄

	ParentBox::ParentBox(const shared_ptr<Stage>& StagePtr,
		const size_t X,
		const size_t Z,
		Vector2 Direction,
		size_t i_VecNum,
		wstring i_TexName,
		wstring Key
	
		) :
		GameObject(StagePtr),
		m_MapX(X),						//マップからのX
		m_MapZ(Z),						//マップからのZ
		move_direction(Direction),		//進行方向
		m_timecounter(0.0f),			//0 ~ 1まで数える
		m_time(0),						//秒数カウント
		m_goal_value(i_VecNum),			//ゴールのとき
		m_TexName(i_TexName),			//テクスチャ	
		m_ParentKey(Key),				//親子の鍵
		m_SpinManager(1)				//Cubeのデフォルト回転速度		TODO : enumで名前付けたほうが分かりやすい
		{
			Craate_map();
		}

	//ユーティリティ関数
	void ParentBox::MapToTransform(size_t X, size_t Z, Vector3& TransPos){
		TransPos.x = (float)X;
		TransPos.y = 1.0f;
		TransPos.z = (float)Z;
		if (move_direction == Vector2(1, 0)){			//右
			TransPos.x -= 0.5f;
		}
		else if (move_direction == Vector2(-1, 0)){		//左
			TransPos.x += 0.5f;
		}
		else if (move_direction == Vector2(0, 1)){		//上
			TransPos.z -= 0.5f;
		}
		else if (move_direction == Vector2(0, -1)){		//下
			TransPos.z += 0.5f;
		}
	}

	//初期化
	void ParentBox::Create(){
		auto PtrTransform = GetComponent<Transform>();
		PtrTransform->SetScale(Vector3(0.5f, 0.5f, 0.5f));
		PtrTransform->SetRotation(Vector3(0, 0, 0));
		//ワーク用Vector3型
		Vector3 Pos;
		MapToTransform(m_MapX, m_MapZ, Pos);
		PtrTransform->SetPosition(Pos);

		//回転アクションをつける
		auto PtrRotateBy = AddComponent<RotateBy>();
		//アクションは無効にしておく
		PtrRotateBy->SetUpdateAllActive(false);

		//文字列をつける
		auto PtrString = AddComponent<StringSprite>();
		PtrString->SetText(L"");
		PtrString->SetTextRect(Rect2D<float>(16.0f, 120.0f, 640.0f, 240.0f));

		////描画コンポーネント
		auto PtrDraw = AddComponent<BasicPNTDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_CUBE");
		//PtrDraw->SetTextureResource(m_TexName);

		SetDrawActive(false);
		//親CUBEのデバッグが見たい場合trueにすると表示ONになります

		//SEの設定
		auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
		pMultiSoundEffect->AddAudioResource(L"Goal");
		pMultiSoundEffect->AddAudioResource(L"Cursor");
		pMultiSoundEffect->AddAudioResource(L"MatHit");
		pMultiSoundEffect->AddAudioResource(L"CUBEGoal");
		pMultiSoundEffect->AddAudioResource(L"REVERSE");
		pMultiSoundEffect->AddAudioResource(L"COLORSE");
		pMultiSoundEffect->AddAudioResource(L"Move");

		//マップに登録
		//func_Set();

		//ステートマシンの構築
		m_StateMachine = make_shared< StateMachine<ParentBox> >(GetThis<ParentBox>());
		//最初のステートをParentStartStateに設定
		m_StateMachine->SetCurrentState(TimeState::Instance());
		//ParentStartStateの初期化実行を行う
		m_StateMachine->GetCurrentState()->Enter(GetThis<ParentBox>());
	}

	//イベントの受け取り ポーズ画面等
	void ParentBox::OnEvent(const shared_ptr<Event>& event){	
		this->ExcuteFunc(event->m_MsgStr);
	}

	//ポーズ中に呼ぶ関数
	void ParentBox::PauseObject(){
		//アップデートをしなくする
		SetUpdateActive(false);
	}

	//ゲーム中はこれ呼ぶ
	void ParentBox::StartObject(){
		SetUpdateActive(true);
	}

	//回転速度を通常の速さにする
	void ParentBox::Spin_Default(){
		m_SpinManager = 1;
	}

	//回転速度を上げる
	void ParentBox::Spin_Up(){
		m_SpinManager = 0;
	}

	//回転速度を下げる
	void ParentBox::Spin_Down(){
		m_SpinManager = 2;
	}

	//効果音の再生
	void ParentBox::SoundSE(wstring SoundName, float volume){
		//SEを鳴らす
		auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
		pMultiSoundEffect->Start(SoundName, 0, volume);
	}

	//mapを作成
	void ParentBox::Craate_map(){
		m_ParentPtr[L"Pause"] = &ParentBox::PauseObject;
		m_ParentPtr[L"GamePlay"] = &ParentBox::StartObject;
		m_ParentPtr[L"DefaultSpeed"] = &ParentBox::Spin_Default;
		m_ParentPtr[L"RollUpSpeed"] = &ParentBox::Spin_Up;
		m_ParentPtr[L"RollDownSpeed"] = &ParentBox::Spin_Down;
	}

	//引数でマップから参照する関数を切り替える
	void ParentBox::ExcuteFunc(const wstring& str){
		(this->*m_ParentPtr[str])();
	}

	//マップ外 & 進めなくなったら小さくなる
	void ParentBox::DeadAction(){
		auto GameStagePtr = dynamic_pointer_cast<GameStage>(GetStage());
		auto Chiled_Group_Vec = GameStagePtr->GetSharedObjectGroup(L"ChiledGroup")->GetGroupVector();
		for (auto wpChiled : Chiled_Group_Vec){
			auto pChiled = dynamic_pointer_cast<ChildBox>(wpChiled.lock());
			//なかったら早期リターン	あってるけど書き方的に不安有
			if (!pChiled)	return;
			//Keyを親と子供から取得して同じCUBE同士で変化させる
			if (pChiled->Chiled_to_Parent_Connect() == this->Parent_to_Chiled_Connect()){

				//関連の持った子供のCubeにゴールしたイベントを送る
				PostEvent(0, GetThis<ParentBox>(), pChiled, L"GameOver");

				//ゲームオーバーに移動する
				GameStagePtr->GetStateMachine()->ChangeState(GameOverState::Instance());
			}
		}		
	}

	//Goalにたどり着いたら回る演出	仮で本来は落ちていく演出
	void ParentBox::GoalAction(){
		auto GameStagePtr = dynamic_pointer_cast<GameStage>(GetStage());
		auto Chiled_Group_Vec = GameStagePtr->GetSharedObjectGroup(L"ChiledGroup")->GetGroupVector();
		for (auto wpChiled : Chiled_Group_Vec){
			auto pChiled = dynamic_pointer_cast<ChildBox>(wpChiled.lock());
			//なかったら早期リターン	あってるけど書き方的に不安有
			if (!pChiled)	return;
			//Keyを親と子供から取得して同じCUBE同士で変化させる
			if (pChiled->Chiled_to_Parent_Connect() == this->Parent_to_Chiled_Connect()){
				
				//関連の持った子供のCubeにゴールしたイベントを送る
				PostEvent(0, GetThis<ParentBox>(), pChiled, L"GameClear");
				
				//SEを鳴らす
				auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
				pMultiSoundEffect->Start(L"Goal", 0, 0.3f);

				//ゴールマネージャーにゴールしたイベントを送る
				auto PtrStageManager = GameStagePtr->GetSharedGameObject<StageManager>(L"StageManager");
				PostEvent(0, GetThis<ParentBox>(), PtrStageManager, L"MathToClear");

				//エフェクト
				m_wpGameEffectManager = GetStage()->GetSharedGameObject<GameEffectManager>(L"GameEffectManager", false);
				auto pGameEffectManeger = m_wpGameEffectManager.lock();
				Vector2 Pos(static_cast<float>(this->GetMapX()) ,static_cast<float>(this->GetMapZ()) );
				pGameEffectManeger->InsertCubeGoalEffect(Pos);

				//エラー回避ができない
				//SetUpdateActive(false);

			}
		}
	}

	//テクスチャを変更する
	void ParentBox::ChangeColor(wstring MeshKey, DataID goalID){
		auto GameStagePtr = dynamic_pointer_cast<GameStage>(GetStage());
		auto Chiled_Group_Vec = GameStagePtr->GetSharedObjectGroup(L"ChiledGroup")->GetGroupVector();
		for (auto wpChiled : Chiled_Group_Vec){
			auto pChiled = dynamic_pointer_cast<ChildBox>(wpChiled.lock());
			//なかったら早期リターン 
			if (!pChiled)	return;
			//Keyを親と子供から取得して同じCUBE同士で変化させる
			if (pChiled->Chiled_to_Parent_Connect() == this->Parent_to_Chiled_Connect()){
				//メッシュを変更
				auto pd = pChiled->GetComponent<BasicFbxPNTDraw>();
				pChiled->SetMeshName(MeshKey);
				pd->SetFbxMeshResource(MeshKey); 
				//変えた色にあうようにColorCubeのゴールする色も変更
				Set_goal_value(goalID);
				//SEを鳴らす
				auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
				pMultiSoundEffect->Start(L"COLORSE", 0, 0.3f);
			}
		}
	}

	// ---------- Motion ----------.

	//一定の時間を計測する	2秒たったらtrue動く
	bool ParentBox::IsTimeCount(){
		//前回のターンからの時間
		float ElapsedTime = App::GetApp()->GetElapsedTime();

		m_timecounter += ElapsedTime;
		if (m_timecounter > 1.0f){
			m_time++;
			m_timecounter = 0;
		}

		//回転の値は入力で変更可能
		if (m_time >= m_SpinManager){
			m_time = 0;
			return true;
		}

		else if(m_time > 2){
			m_time = 0;
			return true;
		}
		return false;
	}

	//動き	
	//Todo : ここMotion関数での処理はできるだけ避けていきたい
	void ParentBox::MoveMotion(){

		auto GameStagePtr = dynamic_pointer_cast<GameStage>(GetStage());
		auto Chiled_Group_Vec = GameStagePtr->GetSharedObjectGroup(L"ChiledGroup")->GetGroupVector();
		for (auto wpChiled : Chiled_Group_Vec){
			auto pChiled = dynamic_pointer_cast<ChildBox>(wpChiled.lock());
			//なかったら早期リターン	あってるけど書き方的に不安有
			if (!pChiled)	return;
			//Keyを親と子供から取得して同じCUBE同士で変化させる
			if (pChiled->Chiled_to_Parent_Connect() == this->Parent_to_Chiled_Connect()){
				//親子関係を外す
				pChiled->ResetParent();

				//現在の位置をコピー
				size_t NextX = m_MapX;
				size_t NextZ = m_MapZ;

				if (move_direction == Vector2(0, 1)){			//上
					NextZ++;
				}
				else if (move_direction == Vector2(0, -1)){		//下
					NextZ--;
				}
				else if (move_direction == Vector2(1, 0)){		//右
					NextX++;
				}
				else if (move_direction == Vector2(-1, 0)){		//左
					NextX--;
				}
				//CUBEの向きに合わせて自分の位置メンバ変数を更新
				m_MapX = NextX;
				m_MapZ = NextZ;
				Vector3 Pos;
				MapToTransform(m_MapX, m_MapZ, Pos);
				//回転アクションを得る
				auto PtrRotateBy = GetComponent<RotateBy>();
				//アクション無効にしておく
				PtrRotateBy->SetUpdateAllActive(false);
				auto PtrTransform = GetComponent<Transform>();
				PtrTransform->SetPosition(Pos);
			}
		}
	}

	//回転
	void ParentBox::SpinMotion(){
		//回転アクションを得る
		auto PtrRotateBy = GetComponent<RotateBy>();
		//アクションは有効にしておく
		PtrRotateBy->SetUpdateAllActive(true);
		Vector3 Rot;
		if (move_direction == Vector2(0, 1)){			//上
			Rot = Vector3(XM_PIDIV2, 0, 0);
		}
		else if (move_direction == Vector2(0, -1)){		//下
			Rot = Vector3(-XM_PIDIV2, 0, 0);
		}
		else if (move_direction == Vector2(1, 0)){		//右
			Rot = Vector3(0, 0, -XM_PIDIV2);
		}
		else if (move_direction == Vector2(-1, 0)){		//左
			Rot = Vector3(0, 0, XM_PIDIV2);
		}
		//速さ
		PtrRotateBy->SetParams(0.42f, Rot);
		PtrRotateBy->Run(); 

		//回転を始めたら親子関係をつける
		auto GameStagePtr = dynamic_pointer_cast<GameStage>(GetStage());
		auto Chiled_Group_Vec = GameStagePtr->GetSharedObjectGroup(L"ChiledGroup")->GetGroupVector();
		for (auto wpChiled : Chiled_Group_Vec){
			auto pChiled = dynamic_pointer_cast<ChildBox>(wpChiled.lock());
			//なかったら早期リターン	あってるけど書き方的に不安有
			if (!pChiled)	return;
			//Keyを親と子供から取得して同じCUBE同士で変化させる
			if (pChiled->Chiled_to_Parent_Connect() == this->Parent_to_Chiled_Connect()){
				pChiled->SetParent(GetThis<ParentBox>());
			}
		}

	}

	//子供の位置からステージの最大最小を確認
	bool ParentBox::IsStageUpLowSerch(){
		auto PtrGameStage = dynamic_pointer_cast<GameStage>(GetStage());
		//親のメンバ変数(m_MapX,m_MapZ)に整数の位置座標が保持されている
		size_t Parent_To_CellX = m_MapX;	//現在の位置をコピー
		size_t Parent_To_CellZ = m_MapZ;	//現在の位置をコピー

		//次行くセルの位置をParent_To_Cellに入れる
		Parent_To_CellX += static_cast<size_t>(move_direction.x);
		Parent_To_CellZ += static_cast<size_t>(move_direction.y);

		//親の位置で上限下限を確認
		if (PtrGameStage->GetCheckToDataMap(Parent_To_CellX, Parent_To_CellZ)){
			//最大最小でなければ
			return true;
		}
		else{
			//ステージを越して動くことはできない
			GetStateMachine()->ChangeState(DeadState::Instance());
			return false;
		}
	}

	//次いくセルが通れるか確認
	bool ParentBox::IsNextMapDataCheck(){
		auto PtrGameStage = dynamic_pointer_cast<GameStage>(GetStage());

		size_t Parent_To_CellX = m_MapX;	//現在の位置をコピー
		size_t Parent_To_CellZ = m_MapZ;	//現在の位置をコピー

		//次行くセルの位置をParent_To_Cellに入れる
		Parent_To_CellX += static_cast<size_t>(move_direction.x);
		Parent_To_CellZ += static_cast<size_t>(move_direction.y);

		//親の位置で次進む方向に道があるか確認
		if (PtrGameStage->GetNextCheckMapData(Parent_To_CellX, Parent_To_CellZ)){
			//通れる道であれば
			return true;
		}
		else{
			//道ではないため 膨らむ　破裂
			GetStateMachine()->ChangeState(DeadState::Instance());
			return false;
		}
	}

	//固定マットでの変更
	void ParentBox::change_fixedmat(){
		//GetFixedMat();
		//ゲームステージの取得
		auto PtrGameStage = dynamic_pointer_cast<GameStage>(GetStage());

		//現在の子供の位置を保持S
		size_t Parent_To_CellX = m_MapX;	//現在の位置をコピー
		size_t Parent_To_CellZ = m_MapZ;	//現在の位置をコピー

	 	auto fixedmattype = PtrGameStage->GetFixedMat(Parent_To_CellZ, Parent_To_CellX);
		
		//これでEmpty以上になるはず
		/*if (fixedmattype > ItemCase::Empty){

		}*/
		switch (fixedmattype)
		{
		case ItemCase::Empty:
			//なにもしません
			break;
		case ItemCase::Reverse:
			move_direction *= -1.0f;
			//効果音の再生
			SoundSE(L"REVERSE", 0.1f);
			break;
		case ItemCase::ColorRed:
			ChangeColor(L"CUBERED_MESH", DataID::REDGOAL);
			break;
		case ItemCase::ColorBlue:
			ChangeColor(L"CUBEBLUE_MESH",DataID::BLUEGOAL);
			break;
		case ItemCase::ColorPurple:
			ChangeColor(L"CUBEPURPLE_MESH",DataID::PURPLEGOAL);
			break;
		case ItemCase::ColorGreen:
			ChangeColor(L"CUBEGREEN_MESH",DataID::YELLOGOAL);
			break;

		//スコア関連
		case ItemCase::ScoreBlue:
			break;
		case ItemCase::ScoreRed:
			break;
		case ItemCase::ScorePurple:
			break;
		case ItemCase::ScoreGreen:
			break;

		case ItemCase::MoveBox:
			break;
		default:
			assert(!"不正な値が入りました　Csvファイルを確認してください");
			break;
		}
	}

	//向き変更
	void ParentBox::change_direction(){

		//ゲームステージの取得
		auto PtrGameStage = dynamic_pointer_cast<GameStage>(GetStage());

		auto SceneBasePtr = App::GetApp()->GetSceneBase();
		auto ScenePtr = dynamic_pointer_cast<Scene>(SceneBasePtr);

		//現在の子供の位置を保持
		size_t Parent_To_CellX = m_MapX;	//現在の位置をコピー
		size_t Parent_To_CellZ = m_MapZ;	//現在の位置をコピー

		//GameStageから参照したものをコピー		vector< vector< weak_ptr<ArrowObject> > >
		auto& Vec = PtrGameStage->GetArrowVec();
		//悲しくもZ Xで動いた
		if (!Vec[Parent_To_CellZ][Parent_To_CellX].expired()){
			//そのセル座標にマットがあったら
			auto ParSh = Vec[Parent_To_CellZ][Parent_To_CellX].lock();
			if (ParSh){

				Vector2 ArrowId = ParSh->GetArrowid();
				//向きを変える
				move_direction = Vector2(ArrowId.x, ArrowId.y);

				Vector3 NewPos{ static_cast<float>(this->GetMapX()), 1.5f, static_cast<float>(this->GetMapZ()) };

				//エフェクトの作成
				PtrGameStage->AddGameObject<CubeEffect_Move>(NewPos, move_direction, m_goal_value);

				ParSh->Delete();	
				Vec[Parent_To_CellZ][Parent_To_CellX].reset();

				////エフェクト
				m_wpGameEffectManager = GetStage()->GetSharedGameObject<GameEffectManager>(L"GameEffectManager", false);
				auto pGameEffectManeger = m_wpGameEffectManager.lock();
				//SEの追加
				Vector2 Pos(static_cast<float>(this->GetMapX()), static_cast<float>(this->GetMapZ()));
				pGameEffectManeger->InsertArrowContactEffect(Pos, move_direction);

				//SE
				auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
				pMultiSoundEffect->Start(L"MatHit", 0, 0.3f);

				//マットを踏んだ数(スコアでやってます)適当でごめんなさい
				//ScenePtr->PlusUsemat();
				App::GetApp()->GetScene<Scene>()->PlusUsemat();
			}
		}

	}

	//ゴールにいるか
	bool ParentBox::goal_Position(){
		auto PtrGameStage = dynamic_pointer_cast<GameStage>(GetStage());
		if (PtrGameStage->GetMapData(m_MapX, m_MapZ) == m_goal_value){
			return true;
		}
		return false;
	}

	//移動できるかを管理する
	bool ParentBox::FlagMoveMethod(){
		//動くための条件がすべて揃えば動ける
		if (IsTimeCount() && IsStageUpLowSerch() && IsNextMapDataCheck()){
			return true;
		}
		//なにかしら妨げになっていれば動けない
		return false;
	}

	//Goal関連をまとめた関数
	void ParentBox::Goal_Method(){
		//自分の位置からゴールしているか確認
		if (goal_Position()){
			GetStateMachine()->ChangeState(GoalState::Instance());
		}
		else{
			//通常状態に戻る
			GetStateMachine()->ChangeState(TimeState::Instance());
		}
	}

	//Stateの更新
	void  ParentBox::Update(){

		m_StateMachine->Update();
		//ゲームステージの取得
		//--------------------------------------------------
		//TODO : イベントでUpdateを管理するようにする
		auto PtrGameStage = dynamic_pointer_cast<GameStage>(GetStage());

		if (PtrGameStage->GetStateMachine()->GetCurrentState() == GameOverState::Instance() ||
			PtrGameStage->GetStateMachine()->GetCurrentState() == GameClearState::Instance())
		{
			SetUpdateActive(false);
		}
		//----------------------------------------------------

	}

	//@Todo : Rotateして終わった時に綺麗な値にしてあげる
	void ParentBox::Update2(){

		//回転中なら	
		if (GetStateMachine()->GetCurrentState() == Parent_SpinState::Instance()){
			auto PtrRotateBy = GetComponent<RotateBy>();					//回転アクションを得る
			if (PtrRotateBy->IsArrived()){									//もし到着したら
				PtrRotateBy->SetUpdateAllActive(false);

				SoundSE(L"Move", 0.3f);
				//m_wpGameEffectManager = GetStage()->GetSharedGameObject<GameEffectManager>(L"GameEffectManager", false);
				//auto pGameEffectManeger = m_wpGameEffectManager.lock();
				//pGameEffectManeger->InsertMoveEffect(GetComponent<Transform>()->GetPosition());
				//エフェクト関連
				//auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
				//pMultiSoundEffect->Start(L"Cursor", 0, 0.3f);

				//固定マットがあったら変更
				change_fixedmat();

				//マットがあったら向き変更
				change_direction();

				//ゴールしたらステート変更
				Goal_Method();
			}
		}
	}

	//デバッグ用
	void ParentBox::Update3(){

		//State表示
		wstring StateStr;
		if (m_StateMachine->GetCurrentState() == TimeState::Instance()){
			StateStr = L"TimeState";
		}
		else if (m_StateMachine->GetCurrentState() == Parent_MoveState::Instance()){
			StateStr = L"Parent_MoveState";
		}
		else if (m_StateMachine->GetCurrentState() == Parent_SpinState::Instance()){
			StateStr = L"Parent_SpinState";
		}
		StateStr += L"\n";
		StateStr += L"\n";

		//メンバ変数の親の位置
		wstring Str_ParentPos(L"Member_Parent_Pos :\t");
		Str_ParentPos += L"X=" + Util::IntToWStr(m_MapX, Util::NumModify::Dec) + L",\t";
		Str_ParentPos += L"Y=" + Util::IntToWStr(m_MapZ, Util::NumModify::Dec) + L",\t";
		Str_ParentPos += L"\n";
		Str_ParentPos += L"\n";

		//メンバ変数の回転値
		wstring Str_Spin(L"Spin_Num :\t");
		Str_Spin += L"Spin =" + Util::IntToWStr(m_SpinManager, Util::NumModify::Dec) + L",\t";
		Str_Spin += L"\n";
		Str_Spin += L"\n";

		//メンバ変数の回転値
		wstring Str_RollTime(L"Spin_Num :\t");
		Str_Spin += L"Time =" + Util::IntToWStr(m_time, Util::NumModify::Dec) + L",\t";
		Str_RollTime += L"\n";
		Str_RollTime += L"\n";

		//現在の位置
		auto PtrTransform = GetComponent<Transform>();
		wstring Str_vCellPos(L"Parent_Pos :\t");
		Str_vCellPos += L"X=" + Util::FloatToWStr(PtrTransform->GetPosition().x, 6, Util::FloatModify::Fixed) + L",\t";
		Str_vCellPos += L"Y=" + Util::FloatToWStr(PtrTransform->GetPosition().y, 6, Util::FloatModify::Fixed) + L",\t";
		Str_vCellPos += L"Z=" + Util::FloatToWStr(PtrTransform->GetPosition().z, 6, Util::FloatModify::Fixed) + L",\t";
		Str_vCellPos += L"\n";

		//現在の角度
		wstring Str_Parent_Rot(L"Parent_Rot :\t");
		Str_Parent_Rot += L"X=" + Util::FloatToWStr(PtrTransform->GetRotation().x, 6, Util::FloatModify::Fixed) + L",\t";
		Str_Parent_Rot += L"Y=" + Util::FloatToWStr(PtrTransform->GetRotation().y, 6, Util::FloatModify::Fixed) + L",\t";
		Str_Parent_Rot += L"Z=" + Util::FloatToWStr(PtrTransform->GetRotation().z, 6, Util::FloatModify::Fixed) + L",\t";
		Str_Parent_Rot += L"\n";
		Str_Parent_Rot += L"\n";


		//子供の回転
		//auto PtrTransform = GetComponent<Transform>();
		//auto Chiled = GetStage()->GetSharedGameObject<ChildBox>(L"ChildBox");
		//auto ChiledTrans = Chiled->GetComponent<Transform>();
		//wstring Str_Chiled_Rot(L"Chiled_Rot :\t");
		//Str_Chiled_Rot += L"X=" + Util::FloatToWStr(ChiledTrans->GetRotation().x, 6, Util::FloatModify::Fixed) + L",\t";
		//Str_Chiled_Rot += L"Y=" + Util::FloatToWStr(ChiledTrans->GetRotation().y, 6, Util::FloatModify::Fixed) + L",\t";
		//Str_Chiled_Rot += L"Z=" + Util::FloatToWStr(ChiledTrans->GetRotation().z, 6, Util::FloatModify::Fixed) + L",\t";
		//Str_Chiled_Rot += L"\n";

		////子供の回転
		////auto PtrTransform = GetComponent<Transform>();
		//wstring Str_Chiled_Pos(L"Chiled_Pos :\t");
		//Str_Chiled_Pos += L"X=" + Util::FloatToWStr(ChiledTrans->GetPosition().x, 6, Util::FloatModify::Fixed) + L",\t";
		//Str_Chiled_Pos += L"Y=" + Util::FloatToWStr(ChiledTrans->GetPosition().y, 6, Util::FloatModify::Fixed) + L",\t";
		//Str_Chiled_Pos += L"Z=" + Util::FloatToWStr(ChiledTrans->GetPosition().z, 6, Util::FloatModify::Fixed) + L",\t";
		//Str_Chiled_Pos += L"\n";
		auto fps = App::GetApp()->GetStepTimer().GetFramesPerSecond();
		wstring FPS(L"FPS: ");
		FPS += Util::UintToWStr(fps);
		FPS += L"\n";

		//文字列をつける
		wstring str = Str_ParentPos + StateStr + Str_vCellPos + Str_Parent_Rot + Str_Spin + Str_RollTime + FPS; /*+Str_Chiled_Pos + Str_Chiled_Rot;*/
		auto PtrString = GetComponent<StringSprite>();
		PtrString->SetText(str);

	}

	//--------------------------------------------------------------------------------------
	//	class TimeState : public ObjState<ParentBox>;
	//	用途: 時間計測
	//--------------------------------------------------------------------------------------
	//ステートのインスタンス取得
	shared_ptr<TimeState> TimeState::Instance(){
		static shared_ptr<TimeState> instance;
		if (!instance){
			instance = shared_ptr<TimeState>(new TimeState);
		}
		return instance;
	}
	//ステートに入ったときに呼ばれる関数
	void TimeState::Enter(const shared_ptr<ParentBox>& Obj){
		//何もしない
	}
	//ステート実行中に毎ターン呼ばれる関数
	void TimeState::Execute(const shared_ptr<ParentBox>& Obj){
		//時間計測
		if (Obj->FlagMoveMethod()){
			//親の移動Stateへ移行
			Obj->GetStateMachine()->ChangeState(Parent_MoveState::Instance());
		}
	}
	//ステートから抜けるときに呼ばれる関数
	void TimeState::Exit(const shared_ptr<ParentBox>& Obj){
		//何もしない
	}

	//--------------------------------------------------------------------------------------
	//	class Parent_MoveState : public ObjState<ParentBox>;
	//	用途: 親の移動
	//--------------------------------------------------------------------------------------
	//ステートのインスタンス取得
	shared_ptr<Parent_MoveState> Parent_MoveState::Instance(){
		static shared_ptr<Parent_MoveState> instance;
		if (!instance){
			instance = shared_ptr<Parent_MoveState>(new Parent_MoveState);
		}
		return instance;
	}
	//ステートに入ったときに呼ばれる関数
	void Parent_MoveState::Enter(const shared_ptr<ParentBox>& Obj){
		Obj->MoveMotion();
	}
	//ステート実行中に毎ターン呼ばれる関数
	void Parent_MoveState::Execute(const shared_ptr<ParentBox>& Obj){
		//すぐに回転ステートに移動する
		Obj->GetStateMachine()->ChangeState(Parent_SpinState::Instance());
	}
	//ステートにから抜けるときに呼ばれる関数
	void Parent_MoveState::Exit(const shared_ptr<ParentBox>& Obj){
		//何もしない
	}

	//--------------------------------------------------------------------------------------
	//	class Parent_SpinState : public ObjState<ParentBox>;
	//	用途: 親の回転
	//--------------------------------------------------------------------------------------
	//ステートのインスタンス取得
	shared_ptr<Parent_SpinState> Parent_SpinState::Instance(){
		static shared_ptr<Parent_SpinState> instance;
		if (!instance){
			instance = shared_ptr<Parent_SpinState>(new Parent_SpinState);
		}
		return instance;
	}
	//ステートに入ったときに呼ばれる関数
	void Parent_SpinState::Enter(const shared_ptr<ParentBox>& Obj){
		Obj->SpinMotion();
	}
	//ステート実行中に毎ターン呼ばれる関数
	void Parent_SpinState::Execute(const shared_ptr<ParentBox>& Obj){

	}
	//ステートにから抜けるときに呼ばれる関数
	void Parent_SpinState::Exit(const shared_ptr<ParentBox>& Obj){
		//何もしない
	}

	//--------------------------------------------------------------------------------------
	//	class GoalState : public ObjState<ParentBox>;
	//	用途: ゴール処理
	//--------------------------------------------------------------------------------------
	//ステートのインスタンス取得
	shared_ptr<GoalState> GoalState::Instance(){
		static shared_ptr<GoalState> instance;
		if (!instance){
			instance = shared_ptr<GoalState>(new GoalState);
		}
		return instance;
	}
	//ステートに入ったときに呼ばれる関数
	void GoalState::Enter(const shared_ptr<ParentBox>& Obj){
		Obj->GoalAction();
	}
	//ステート実行中に毎ターン呼ばれる関数
	void GoalState::Execute(const shared_ptr<ParentBox>& Obj){

	}
	//ステートにから抜けるときに呼ばれる関数
	void GoalState::Exit(const shared_ptr<ParentBox>& Obj){
		//何もしない
	}


	//--------------------------------------------------------------------------------------
	//	class DeadState : public ObjState<ParentBox>;
	//	用途: ゴール処理
	//--------------------------------------------------------------------------------------
	//ステートのインスタンス取得
	shared_ptr<DeadState> DeadState::Instance(){
		static shared_ptr<DeadState> instance;
		if (!instance){
			instance = shared_ptr<DeadState>(new DeadState);
		}
		return instance;
	}
	//ステートに入ったときに呼ばれる関数
	void DeadState::Enter(const shared_ptr<ParentBox>& Obj){
		Obj->DeadAction();
	}
	//ステート実行中に毎ターン呼ばれる関数
	void DeadState::Execute(const shared_ptr<ParentBox>& Obj){

	}
	//ステートにから抜けるときに呼ばれる関数
	void DeadState::Exit(const shared_ptr<ParentBox>& Obj){
		//何もしない
	}

	

}
//endof  basedx11
