#pragma once

#include "stdafx.h"

namespace basedx11{

	class ParentBox;
	class GameEffectManager;
	/**
	* @class　ChildBox
	* @brief　ゲームで表示する転がるCUBE
	* @author yuyu sike
	* @date 2015/?/? Start
	*/
	class ChildBox : public GameObject{
	public:
		ChildBox(const shared_ptr<Stage>& StagePtr, const Vector3 i_FirstPos, const wstring i_TextureName, const Vector2 Pre_direction, const size_t i_VecNum, const wstring Key, const wstring i_mesh);
		virtual ~ChildBox(){}
		//初期化
		virtual void Create() override;
		virtual void Update2() override;
		//イベント
		virtual void OnEvent(const shared_ptr<Event>& event)override;


		//操作
		void SetParent(const shared_ptr<ParentBox>& Par);
		void ResetParent();
		void PlayerStop();
		void CUBEStop();
		//CUBE同士が当たったら反射するようにしてみる
		void CUBEReflection();	

		// ---------- Accessor ----------.

		//子供と親との関係を作る
		const wstring Chiled_to_Parent_Connect(){
			return m_ChiledKey;
		}

		//子供のテクスチャの名前を取得
		wstring Chiled_TextureName(){
			return m_TextureName;
		}

		//子供のテクスチャの名前を変更
		void SetChiled_TextureName(wstring SetName){
			m_TextureName = SetName;
		}
		//メッシュの名前を設定する
		void SetMeshName(wstring MeshName){
			m_Mesh = MeshName;
		}

		wstring GetMeshName(){
			return m_Mesh;
		}

	private:
		Vector3 m_FirstPos;			//初期位置
		wstring m_TextureName;		//テクスチャの名前
		Vector2 Pre_direction;		//ParentBoxに送る進行方向
		size_t m_VecNum;			//Goalするときにenumによって決めるときに使用
		wstring m_ChiledKey;		//ParentBoxとの強い関係を結ぶときに使用
		wstring m_Mesh;				//メッシュ読み込みに使用
	};

	/**
	* @class　ParentBox
	* @brief　ChiledBoxを回転させるためのCUBE
	* @author yuyu sike
	* @date 2015/?/? Start
	*/
	class ParentBox : public GameObject{
	private:

		//ParentBoxのポインタ取得
		using PtrParent = void(ParentBox::*)();

		shared_ptr<StateMachine<ParentBox>>  m_StateMachine;	//ステートマシーン
		Vector2 move_direction;		//進行方向
		size_t m_MapX;				//マップからXの摘出
		size_t m_MapZ;				//同上
		float m_timecounter;		//0.0 ~ 1.0まで格納するfloat型の箱
		size_t m_time;				//m_timecounterが１になったら1足していくsize_t型の箱
		size_t m_goal_value;		//ゴールするときのゴールのenumとm_VecNumを比較するときに必要
		wstring m_TexName;			//テクスチャの名前格納
		float m_Usemat;				//マットを踏んだ時にインクリメントしていくfloat型の箱
		wstring m_ParentKey;		//ChiledBoxとの強い関係を結ぶときに使用
		size_t m_SpinManager;		//Cubeの回転速度を管理する
		weak_ptr< GameEffectManager > m_wpGameEffectManager;	//エフェクト管理システム.
		
		//関数ポインタを格納したメンバ変数
		map<wstring, PtrParent> m_ParentPtr;
	

		//ユーティリティ関数
		void MapToTransform(size_t X, size_t Z, Vector3& TransPos);
		//デバッグログ
		void DebugLog();
		//効果音の再生
		void SoundSE(wstring SoundName , float volume);

		//map<wstring , PtrParent>に格納する
		void Craate_map();

		//文字列に合わせてマップを参照して関数を実行する関数。 参考あり
		void ExcuteFunc(const wstring& str);

		//(this->*m_FuncMap[str])();

	public:
		ParentBox(
			// ---------- member ----------.
			const shared_ptr<Stage>& StagePtr,
			const size_t X,
			const size_t Z,
			Vector2 Direction,
			size_t VecNum,
			wstring i_TexName,
			wstring Key
			);

		virtual ~ParentBox(){}

		//初期化
		virtual void Create() override;
		//イベント
		virtual void OnEvent(const shared_ptr<Event>& event)override;
		
		// ---------- Motion ----------.
		bool IsTimeCount();				//一定の時間を計測する	2秒たったらtrue動く
		bool IsStageUpLowSerch();		//ステージの最大最小を確認
		bool IsNextMapDataCheck();		//次いくセルが通れるか確認
		bool FlagMoveMethod();			//移動できるかを管理する
		void MoveMotion();				//ParentBoxの移動
		void SpinMotion();				//ParentBoxの回転
		void change_direction();		//ArrowObjectがあった時にCUBEの進行向きを変更する
		void change_fixedmat();			//ステージの固定マットによってCUBEの状態を変更する
		bool goal_Position();			//Cubeがゴールしたか確認する
		void Goal_Method();				//Cubeのゴール関連をまとめた関数

		// ---------- EventMotion ----------.
		void PauseObject();				//ポーズ関連		ポーズ中に呼ぶ関数
		void StartObject();				//ポーズ関連		ゲームプレイ中に移行したときに読んであげる
		void Spin_Up();					//回転速度を上げる
		void Spin_Down();				//回転速度を下げる
		void Spin_Default();			//回転を通常にする


		//class impl;		//実務クラス
		//unique_ptr<impl> ipm;
		//void Call_void(wstring var);
		//void func_Set();
		//mapで構成する
		//メンバへのポインタ型
		/*map<wstring, cube_ptr> m_ParentBoxPtr;
		typedef void(*PFUNC)(void);
		function<void(void)> Func;
		function<void(ParentBox*)> Func2;
		////メンバへのポインタ型
		//using cube_ptr = void(ParentBox::&)();
		////ParentBoxのポインタ
		//map<wstring, ParentBox*> m_Ptr;
		void Call_void(wstring var, wstring oper);*/
		//typedef void(*func_void)();
		//map<wstring, func_void> m_void_swich;
		//map<wstring, function<void()> > m_funcs;




		//操作
		virtual void Update()  override;
		virtual void Update2() override;
		virtual void Update3() override;


		//マップ外 & 進めなくなったら小さくなる
		void DeadAction();
		//Goalにたどり着いたら回る演出
		void GoalAction();

		//Cubeの色を変更する
		void ChangeColor(wstring MeshKey, DataID goalID);

		//アクセサ
		shared_ptr<StateMachine<ParentBox>> GetStateMachine() const{
			return m_StateMachine;
		}

		//現在のPlayerXの小数点切り上げの整数
		size_t GetMapX(){
			return m_MapX;
		}

		//現在のPlayerZの小数点切り上げの整数
		size_t GetMapZ(){
			return m_MapZ;
		}

		//進行する向きを取得
		Vector2 GetDirection(){
			return move_direction;
		}

		//進行する向きを設定
		void SetDirection(Vector2 work){
			move_direction = work;
		}

		//テクスチャの名前を取得
		const wstring Parent_TextureName(){
			return m_TexName;
		}

		//テクスチャの名前を変更
		void SetParent_TextureName(wstring SetName){
			m_TexName = SetName;
		}

		//子供と親との関係を作る
		const wstring Parent_to_Chiled_Connect(){
			return m_ParentKey;
		}

		//色変更時にCubeのゴール目標値を変更する関数　
		//m_Goal_valueの設定
		void Set_goal_value(size_t change_value){
			m_goal_value = change_value;
		}

		//かんの作
		void SetUsemat(){
			m_Usemat = 0;
		}

		//かんの作
		const float GetUsemat(){
			return m_Usemat;
		}

		//	: エフェクト管理システムを取得する.
		const weak_ptr< GameEffectManager >GetGameEffectManager() const
		{
			return m_wpGameEffectManager;
		}

		//	: エフェクト管理システムを設定する.
		void SetGameEffectManager(const weak_ptr< GameEffectManager > i_wpGameEffectManager)
		{
			m_wpGameEffectManager = i_wpGameEffectManager;
		}

	};

	//--------------------------------------------------------------------------------------
	//	class TimeState : public ObjState<ParentBox>;
	//	用途: 時間計測
	//--------------------------------------------------------------------------------------
	class TimeState : public ObjState<ParentBox>
	{
		TimeState(){}
	public:
		//ステートのインスタンス取得
		static shared_ptr<TimeState> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<ParentBox>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<ParentBox>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<ParentBox>& Obj)override;
	};

	//--------------------------------------------------------------------------------------
	//	class Parent_MoveState : public ObjState<ParentBox>;
	//	用途: 親の移動
	//--------------------------------------------------------------------------------------
	class Parent_MoveState : public ObjState<ParentBox>			//ステートにも多重継承可能	使い道は不明
	{
		Parent_MoveState(){}
	public:
		//ステートのインスタンス取得
		static shared_ptr<Parent_MoveState> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<ParentBox>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<ParentBox>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<ParentBox>& Obj)override;
	};

	//--------------------------------------------------------------------------------------
	//	class Parent_SpinState : public ObjState<ParentBox>;
	//	用途: 親の回転
	//--------------------------------------------------------------------------------------
	class Parent_SpinState : public ObjState<ParentBox>
	{
		Parent_SpinState(){}
	public:
		//ステートのインスタンス取得
		static shared_ptr<Parent_SpinState> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<ParentBox>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<ParentBox>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<ParentBox>& Obj)override;
	};

	//--------------------------------------------------------------------------------------
	//	class GoalState : public ObjState<ParentBox>;
	//	用途: ゴール処理
	//--------------------------------------------------------------------------------------
	class GoalState : public ObjState<ParentBox>			//ステートにも多重継承可能	使い道は不明
	{
		GoalState(){}
	public:
		//ステートのインスタンス取得
		static shared_ptr<GoalState> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<ParentBox>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<ParentBox>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<ParentBox>& Obj)override;
	};

	//--------------------------------------------------------------------------------------
	//	class DeadState : public ObjState<ParentBox>;
	//	用途: ダメだったとき処理
	//--------------------------------------------------------------------------------------
	class DeadState : public ObjState<ParentBox>			//ステートにも多重継承可能	使い道は不明
	{
		DeadState(){}
	public:
		//ステートのインスタンス取得
		static shared_ptr<DeadState> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<ParentBox>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<ParentBox>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<ParentBox>& Obj)override;
	};




}
//endof  basedx11
