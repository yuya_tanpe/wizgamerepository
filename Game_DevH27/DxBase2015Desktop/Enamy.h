#pragma once
#include "stdafx.h"

namespace basedx11{
	

	/**
	* @class　SimpleEnemy
	* @brief　動く敵
	* @author yuyu sike
	* @date 2015/10/29 Start
	*/
	class SimpleEnemy : public GameObject{

		// ---------- member ----------.

		Vector3 m_Pos;											//ポジション
		Vector3 m_VecMove;										//進む向き

		shared_ptr<StateMachine<SimpleEnemy>>  m_StateMachine;	//ステートマシーン
		shared_ptr<GameStage> m_GameStage;	//GameStageのポインタ保存

	

		//	---------- Method ----------.
	
	
		//向き変更するための関数 enum class を使用
		void SetMove_Direction(EnamyMoveRot EnumID, Quaternion& WorkQt);
		
		//回転を変更するための関数　enum classを使用
		void SetSpin_Action(EnamyMoveRot EnumId, Vector3& WorkVec);


	public:
		//構築と破棄
		SimpleEnemy(const shared_ptr<Stage>& StagePtr, const Vector3& Pos
			);
		virtual ~SimpleEnemy(){}
	
		//初期化
		virtual void Create() override;
		//操作
		virtual void Update() override;
		virtual void Update2() override;
		virtual void Update3() override;		//デバッグ用として使用

		vector< Vector2 > m_SortMap;		//上下左右を検索する動的配列

		// ---------- Accessor ----------.

		//アクセサ
		shared_ptr<StateMachine<SimpleEnemy>> GetStateMachine() const{
			return m_StateMachine;
		}

		// ---------- Motion ----------.
		
		//回転と向きのセット Motion関数
		void spin_and_direction(EnamyMoveRot Set_Spin_Ratate);
		//どちらに進むか確認する	Motion関数
		//void is_where_move();
		//ランダムに行先を決める Motion関数
		//void decide_at_random();
		//仕事用
		//void SimpleMoveMethod(Vector3& Work, EnamyMoveRot SetMoveDir, const float Move);

		//シンプルにルート進行 とりあえず一周するようにしてみる
		void SimpleMove();

		//移動するアクション
		void SimpleActionMove();

		


	};

	//--------------------------------------------------------------------------------------
	//	class MoveAIState : public ObjState<SimpleEnamy>;
	//	用途: 単純にまっすぐ動く
	//--------------------------------------------------------------------------------------
	class MoveAIState : public ObjState<SimpleEnemy>
	{
		MoveAIState(){}
	public:
		//ステートのインスタンス取得
		static shared_ptr<MoveAIState> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<SimpleEnemy>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<SimpleEnemy>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<SimpleEnemy>& Obj)override;
	};

	//--------------------------------------------------------------------------------------
	//	class RotateAIState : public ObjState<SimpleEnamy>;
	//	用途: AIの向きを変更する
	//--------------------------------------------------------------------------------------
	class RotateAIState : public ObjState<SimpleEnemy>
	{
		RotateAIState(){}
	public:
		//ステートのインスタンス取得
		static shared_ptr<RotateAIState> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<SimpleEnemy>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<SimpleEnemy>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<SimpleEnemy>& Obj)override;
	};



}
//end basedx11
