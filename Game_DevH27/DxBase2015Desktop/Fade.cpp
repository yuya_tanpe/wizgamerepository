#include "stdafx.h"
#include "Project.h"

/**
* @class FadePanel
* @brief フェードイン、アウト作成
* @author　菅野 翔吾
*/

namespace basedx11{
	FadePanel::FadePanel(shared_ptr<Stage>& StagePtr)
		:SpriteObject(StagePtr, Vector3(0, 0, -1), Vector3(60, 60, 0), L"BLOCKBLACK_TX"),
		m_FadeColor(0, 0, 0, 1.0f),
		m_MaxAlpha(1.0f),
		m_MinAlpha(0.0f),
		m_FadeSpeed(0.02f),
		m_FadeInActive(false),
		m_FadeOutActive(false),
		m_FadeInBeforeActive(false),
		m_FadeOutBeforeActive(false)
	{}
	FadePanel::FadePanel(shared_ptr<Stage>& StagePtr,
		const Vector3& position,
		const Vector3& scale,
		const wstring& TextureName
		) :
		SpriteObject(StagePtr, position, scale, TextureName),
		m_FadeColor(1, 1, 1, 1),
		m_MaxAlpha(1.0f),
		m_MinAlpha(0.0f),
		m_FadeSpeed(0.02f),
		m_FadeInActive(false),
		m_FadeOutActive(false),
		m_FadeInBeforeActive(false),
		m_FadeOutBeforeActive(false)
	{}


	FadePanel::~FadePanel(){}

	//初期化
	void FadePanel::Create(){
		SpriteObject::Create();

		//ステートマシンの構築
		//最初はフェードインの状態にします。
		m_StateMachine = make_shared< StateMachine<FadePanel> >(GetThis<FadePanel>());
		m_StateMachine->SetCurrentState(FadeInState::Instance());
		m_StateMachine->GetCurrentState()->Enter(GetThis<FadePanel>());

		//イベントマシンの構築
		//イベントキーとイベントステートを結び付ける
		m_EventMachine = make_shared< EventMachine<FadePanel> >(GetThis<FadePanel>());
		m_EventMachine->AddEventState(L"FadeIn", FadeInEventState::Instance());
		m_EventMachine->AddEventState(L"FadeOut", FadeOutEventState::Instance());
	}

	void FadePanel::Update(){
		//現在のフェード状態を取得
		bool NowFadeInActive = IsFadeInActive();
		bool NowFadeOutActive = IsFadeOutActive();

		//ステートマシンの更新
		m_StateMachine->Update();

		//スプライトコンポーネントを更新する
		auto PtrSprite = GetComponent<Sprite>();

		//４頂点の情報を作る
		vector < VertexPositionColorTexture > Virtex =
		{
			//左上頂点
			VertexPositionColorTexture(
			Vector3(-0.5f, 0.5f, 0),
			m_FadeColor,
			Vector2(0, 0)
			),
			//右上頂点
			VertexPositionColorTexture(
			Vector3(0.5f, 0.5f, 0),
			m_FadeColor,
			Vector2(1.0f, 0)
			),
			//左下頂点
			VertexPositionColorTexture(
			Vector3(-0.5f, -0.5f, 0),
			m_FadeColor,
			Vector2(0, 1.0f)
			),
			//右下頂点
			VertexPositionColorTexture(
			Vector3(0.5f, -0.5f, 0),
			m_FadeColor,
			Vector2(1.0f, 1.0f)
			),
		};

		//４頂点の更新
		PtrSprite->UpdateVirtexBuffer(Virtex);

		//１フレーム前のフェード状態の更新
		m_FadeInBeforeActive = NowFadeInActive;
		m_FadeOutBeforeActive = NowFadeOutActive;
	}


	void FadePanel::OnEvent(const shared_ptr<Event>& event){
		//ハンドラ関数呼び出し
		//これでイベントが振り分けられる
		m_EventMachine->HandleEvent(event);

		if (event->m_MsgStr == L"Flashing"){
			GetStateMachine()->ChangeState(FlashingState::Instance());
		}
	}



	//============================================
	//ステートマシンの設定
	//============================================

	//--------------------------------------------------------------------------------------
	//	class FadeInState : public ObjState<FadePanel>;
	//	用途: フェードインの状態（透明度が高くなり、ゲーム画面が見えてくる）
	//--------------------------------------------------------------------------------------
	shared_ptr<FadeInState> FadeInState::Instance(){
		static shared_ptr<FadeInState> instance(new FadeInState);
		return instance;
	}
	void FadeInState::Enter(const shared_ptr<FadePanel>& Obj){
		//フェードイン中であるフラグを真にする
		Obj->SetFadeInActive(true);
		Obj->SetFadeOutActive(false);
	}
	void FadeInState::Execute(const shared_ptr<FadePanel>& Obj){
		//現在の色を取得
		auto FadeColor = Obj->GetColor();

		//色の変化（アルファ値のみを変える）
		if (FadeColor.w > Obj->GetMinAlpha()){
			FadeColor.w -= Obj->GetFadeSpeed();
			if (FadeColor.w <= Obj->GetMinAlpha()){
				FadeColor.w = Obj->GetMinAlpha();
				Obj->SetFadeInActive(false);
			}
		}

		Obj->SetColor(FadeColor);
	}

	//--------------------------------------------------------------------------------------
	//	class FadeOutState : public ObjState<FadePanel>;
	//	用途: フェードアウトの状態（透明度が低くなり、ゲーム画面が見えなくなる）
	//--------------------------------------------------------------------------------------
	shared_ptr<FadeOutState> FadeOutState::Instance(){
		static shared_ptr<FadeOutState> instance(new FadeOutState);
		return instance;
	}
	void FadeOutState::Enter(const shared_ptr<FadePanel>& Obj){
		//フェードアウト中であるフラグを真にする
		Obj->SetFadeInActive(false);
		Obj->SetFadeOutActive(true);
	}
	void FadeOutState::Execute(const shared_ptr<FadePanel>& Obj){
		//現在の色を取得
		auto FadeColor = Obj->GetColor();

		//色の変化（アルファ値のみを変える）
		if (FadeColor.w < Obj->GetMaxAlpha()){
			FadeColor.w += Obj->GetFadeSpeed();
			if (FadeColor.w >= Obj->GetMaxAlpha()){
				FadeColor.w = Obj->GetMaxAlpha();
				Obj->SetFadeOutActive(false);
			}
		}

		Obj->SetColor(FadeColor);
	}

	//--------------------------------------------------------------------------------------
	//	class FlashingState : public ObjState<FadePanel>;
	//	用途: 点滅状態（透明度が変化し続ける）
	//--------------------------------------------------------------------------------------
	shared_ptr<FlashingState> FlashingState::Instance(){
		static shared_ptr<FlashingState> instance(new FlashingState);
		return instance;
	}
	void FlashingState::Enter(const shared_ptr<FadePanel>& Obj){
		//フェードアウト関係のフラグを偽にする
		Obj->SetFadeInActive(false);
		Obj->SetFadeOutActive(false);
	}
	void FlashingState::Execute(const shared_ptr<FadePanel>& Obj){
		//現在の色を取得
		auto FadeColor = Obj->GetColor();

		//色の変化（アルファ値のみを変える）
		//現在のアルファ値が最小値よりも大きいときは減算する
		if (FadeColor.w > Obj->GetMinAlpha()){
			FadeColor.w -= Obj->GetFadeSpeed();

			//アルファ値が最小値を下回ったら、最大値を代入する
			if (FadeColor.w < Obj->GetMinAlpha()){
				FadeColor.w = Obj->GetMaxAlpha();
			}
		}

		//色を更新する
		Obj->SetColor(FadeColor);
	}

	//============================================
	//イベントマシンの設定
	//============================================

	//--------------------------------------------------------------------------------------
	//	class FadeInEventState : public EventState<FadePanel>;
	//	用途: FadeInEventStateイベントステート(フェードインを開始する)
	//--------------------------------------------------------------------------------------
	shared_ptr<FadeInEventState> FadeInEventState::Instance(){
		static shared_ptr<FadeInEventState> instance(new FadeInEventState);
		return instance;
	}
	void FadeInEventState::Enter(const shared_ptr<FadePanel>& Obj, const shared_ptr<Event>& event){
		Obj->GetStateMachine()->ChangeState(FadeInState::Instance());
	}

	//--------------------------------------------------------------------------------------
	//	class FadeOutEventState : public EventState<FadePanel>;
	//	用途: FadeOutEventStateイベントステート(フェードアウト状態になる)
	//--------------------------------------------------------------------------------------
	shared_ptr<FadeOutEventState> FadeOutEventState::Instance(){
		static shared_ptr<FadeOutEventState> instance(new FadeOutEventState);
		return instance;
	}
	void FadeOutEventState::Enter(const shared_ptr<FadePanel>& Obj, const shared_ptr<Event>& event){
		Obj->GetStateMachine()->ChangeState(FadeOutState::Instance());
	}
}
//end basedx11
