#pragma once
#include "stdafx.h"

namespace basedx11{
	class FadePanel : public SpriteObject{
	protected:
		//色
		Color4 m_FadeColor;

		//フェードする時の最大、最小のアルファ値
		float m_MaxAlpha;
		float m_MinAlpha;

		//フェードの速度。デフォルト0.02
		float m_FadeSpeed;

		shared_ptr< StateMachine<FadePanel> >  m_StateMachine;	//ステートマシーン
		shared_ptr< EventMachine<FadePanel> >  m_EventMachine;	//イベントマシーン

		bool m_FadeInActive;	//フェードイン中であるときのみ真
		bool m_FadeOutActive;	//フェードアウト中であるときのみ真
		bool m_FadeInBeforeActive;	//１フレーム前の状態
		bool m_FadeOutBeforeActive;	//１フレーム前の状態

	public:
		//構築と破棄
		FadePanel(shared_ptr<Stage>& StagePtr);
		FadePanel(shared_ptr<Stage>& StagePtr,
			const Vector3& position,
			const Vector3& scale,
			const wstring& TextureName
			);

		virtual ~FadePanel();

		//初期化
		virtual void Create() override;
		//更新
		virtual void Update() override;
		virtual void OnEvent(const shared_ptr<Event>& event)override;


		//アクセサ：ステートマシンとイベントマシン
		virtual shared_ptr< StateMachine<FadePanel> > GetStateMachine() const{
			return m_StateMachine;
		}
		shared_ptr< EventMachine<FadePanel> > GetEventMachine() const{
			return m_EventMachine;
		}
		//アクセサ：フェードに使用する色関係
		Color4 GetColor() const{
			return m_FadeColor;
		}
		void SetColor(const Color4& color){
			m_FadeColor = color;
		}

		void SetMaxAlpha(float w){
			m_MaxAlpha = w;
		}
		void SetMinAlpha(float w){
			m_MinAlpha = w;
		}
		float GetMaxAlpha(){
			return m_MaxAlpha;
		}
		float GetMinAlpha(){
			return m_MinAlpha;
		}

		//アクセサ：フェード中であるかどうか関係
		//フェードイン中だと真を返す
		bool IsFadeInActive(){
			return m_FadeInActive;
		}
		//フェードアウト中だと真を返す
		bool IsFadeOutActive(){
			return m_FadeOutActive;
		}
		void SetFadeInActive(const bool flag){
			m_FadeInActive = flag;
		}
		void SetFadeOutActive(const bool flag){
			m_FadeOutActive = flag;
		}
		//１フレーム前のフェード状態を返す
		bool IsFadeInBeforeActive(){
			return m_FadeInBeforeActive;
		}
		bool IsFadeOutBeforeActive(){
			return m_FadeOutBeforeActive;
		}
		//アクセサ：フェードの速度関係
		float GetFadeSpeed(){
			return m_FadeSpeed;
		}
		void SetFadeSpeed(float speed){
			m_FadeSpeed = speed;
		}

	};


	//--------------------------------------------------------------------------------------
	//	class FadeInState : public ObjState<FadePanel>;
	//	用途: フェードインの状態（透明度が高くなり、ゲーム画面が見えてくる）
	//--------------------------------------------------------------------------------------
	class FadeInState : public ObjState<FadePanel>
	{
		FadeInState(){}
	public:
		static shared_ptr<FadeInState> Instance();
		virtual void Enter(const shared_ptr<FadePanel>& Obj);
		virtual void Execute(const shared_ptr<FadePanel>& Obj);
		virtual void Exit(const shared_ptr<FadePanel>& Obj){}
	};

	//--------------------------------------------------------------------------------------
	//	class FadeOutState : public ObjState<FadePanel>;
	//	用途: フェードアウトの状態（透明度が低くなり、ゲーム画面が見えなくなる）
	//--------------------------------------------------------------------------------------
	class FadeOutState : public ObjState<FadePanel>
	{
		FadeOutState(){}
	public:
		static shared_ptr<FadeOutState> Instance();
		virtual void Enter(const shared_ptr<FadePanel>& Obj);
		virtual void Execute(const shared_ptr<FadePanel>& Obj);
		virtual void Exit(const shared_ptr<FadePanel>& Obj){}
	};

	//--------------------------------------------------------------------------------------
	//	class FlashingState : public ObjState<FadePanel>;
	//	用途: 点滅状態（透明度が変化し続ける）
	//--------------------------------------------------------------------------------------
	class FlashingState : public ObjState<FadePanel>
	{
		FlashingState(){}
	public:
		static shared_ptr<FlashingState> Instance();
		virtual void Enter(const shared_ptr<FadePanel>& Obj);
		virtual void Execute(const shared_ptr<FadePanel>& Obj);
		virtual void Exit(const shared_ptr<FadePanel>& Obj){}
	};



	//--------------------------------------------------------------------------------------
	//	class FadeInEventState : public EventState<FadePanel>;
	//	用途: FadeInEventStateイベントステート(フェードインを開始する)
	//--------------------------------------------------------------------------------------
	class FadeInEventState : public EventState<FadePanel>
	{
		FadeInEventState(){}
	public:
		static shared_ptr<FadeInEventState> Instance();
		virtual void Enter(const shared_ptr<FadePanel>& Obj, const shared_ptr<Event>& event)override;
	};

	//--------------------------------------------------------------------------------------
	//	class FadeOutEventState : public EventState<FadePanel>;
	//	用途: FadeOutEventStateイベントステート(フェードアウト状態になる)
	//--------------------------------------------------------------------------------------
	class FadeOutEventState : public EventState<FadePanel>
	{
		FadeOutEventState(){}
	public:
		static shared_ptr<FadeOutEventState> Instance();
		virtual void Enter(const shared_ptr<FadePanel>& Obj, const shared_ptr<Event>& event)override;
	};
}
//end basedx11
