#include "stdafx.h"
#include "Project.h"

namespace basedx11{

	/**
	* @class　SuperMat
	* @brief　固定マットが複数できることを予想しての親クラス
	* @author yuya sike
	* @date 2015/11/30 Start
	*/
	SuperMat::SuperMat(const shared_ptr<Stage>& StagePtr, const Vector3& i_Start_Pos, const wstring& i_Texture) :
		GameObject(StagePtr),
		m_Start_Pos(i_Start_Pos),		//初期値
		m_Texture(i_Texture)			//生成時のテクスチャの名前
	{
	}

	//初期化
	void SuperMat::Create(){
		auto PtrTrans = GetComponent<Transform>();
		PtrTrans->SetPosition(m_Start_Pos);
		Vector3 v;
		v.Zero();
		PtrTrans->SetRotation(v);	//TODO : このXMVectorZero()の使い方の確認  全然違うことだったXMVector型で使うらしい
		PtrTrans->SetScale(0.8f, 0.05f, 0.8f);	//TODO : マジックナンバーの変更

		auto PtrDraw = AddComponent<BasicPNTDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_CUBE");		//描画するメッシュを設定
		PtrDraw->SetTextureResource(m_Texture);			//テクスチャの貼り付け
		SetAlphaActive(true);							//透明処理

	}

	/**
	* @class　ReverseMat
	* @brief　ColorCubeの進行方向を変更するClass.
	* @author yuya sike
	* @date 2015/10/21 Start
	*/
	ReverseMat::ReverseMat(const shared_ptr<Stage>& StagePtr, const Vector3& i_Pos, const wstring& i_Texture) :
		SuperMat(StagePtr, i_Pos, i_Texture)
	{}

	//純粋仮想関数 固定マット毎の効果
	void ReverseMat::special_effect(){

	}

	//初期化
	//void FixedMat::Create(){
	//	auto PtrTransform = GetComponent<Transform>();
	//	PtrTransform->SetPosition(m_Pos.x, 1.0f, m_Pos.z);
	//	PtrTransform->SetRotation(0.0f, 0.0f, 0.0f);
	//	PtrTransform->SetScale(0.8f, 0.05f, 0.8f);
	//	//描画コンポーネントの設定
	//	auto PtrDraw = AddComponent<BasicPNTDraw>();
	//	//描画するメッシュを設定
	//	PtrDraw->SetMeshResource(L"DEFAULT_CUBE");
	//	PtrDraw->SetTextureResource(L"REVERSE_TX");
	//	//透明処理
	//	SetAlphaActive(true);
	//}

	/**
	* @class　ChangeColorMat
	* @brief　ColorCubeの色を変更する有限Mat	一回使ってしまったらマットは削除する仕様？
	* @author yuya sike
	* @date 2015/11/26 Start
	*/
	ChangeColorMat::ChangeColorMat(const shared_ptr<Stage>& StagePtr, Vector3& Pos, const wstring& i_Texture, const wstring& TextureType) :
		SuperMat(StagePtr, Pos, i_Texture),
		m_TextureType(TextureType)			//色の種類を宣言
	{
	}

	//純粋仮想関数　固定マット毎の効果
	void ChangeColorMat::special_effect(){

	}

	//void ChangeColorMat::Create(){
	//	auto PtrTrans = GetComponent<Transform>();	//Transformの取得
	//	PtrTrans->SetPosition(m_Pos.x, 1.0f, m_Pos.z);
	//	PtrTrans->SetRotation(0, 0, 0);
	//	PtrTrans->SetScale(0.8f, 0.05f, 0.8f);			//TODO : 小さいCUBEでも表現するにはいいのかもしれない
	//	//描画コンポーネントの設定
	//	auto PtrDraw = AddComponent<BasicPNTDraw>();
	//	//描画するメッシュを設定
	//	PtrDraw->SetMeshResource(L"DEFAULT_CUBE");
	//	PtrDraw->SetTextureResource(L"CHANGERED_TX");
	//	//透明処理
	//	SetAlphaActive(true);
	//}
	
}
//endof  basedx11
