#pragma once

#include "stdafx.h"

namespace basedx11{

	/**
	* @class　SuperMat
	* @brief　固定マットが複数できることを予想しての親クラス
	* @author yuya sike
	* @date 2015/11/30 Start
	*/

	class SuperMat: public GameObject {

	private:
		// ---------- member ----------.
		Vector3 m_Start_Pos;				//プレイヤーの位置
		wstring m_Texture;					//生成時のテクスチャ
	
	public:
		//構築と生成
		SuperMat(const shared_ptr<Stage>& StagePtr, const Vector3& i_Start_Pos, const wstring& i_Texture);
		virtual ~SuperMat(){}
		//初期化
		virtual void Create() override;
		//TODO : 名前変更
		virtual void special_effect() = 0;

	};
	
	/**
	//TODO : 名前変更　リバースマット等
	* @class　ReverseMat
	* @brief　ColorCubeの進行方向を変更するClass.
	* @author yuya sike
	* @date 2015/10/21 Start
	*/
	//TODO : だだ
	class ReverseMat : public SuperMat {
	public:	
		//構築と破棄
		ReverseMat(const shared_ptr<Stage>& StagePtr, const Vector3& i_Start_Pos, const wstring& i_Texture);
		virtual ~ReverseMat(){}
		//初期化
		//virtual void Create() override;

		//	---------- Method ----------.
		virtual void special_effect() override;
	
		// ---------- Accessor ----------.
	private:
	
	};

	/**
	* @class　ChangeColorMat
	* @brief　ColorCubeの色を変更する有限Mat	一回使ってしまったらマットは削除する仕様？
	* @author yuya sike
	* @date 2015/11/26 Start
	*/
	class ChangeColorMat : public SuperMat {
	public:
		//コンストラクタ
		ChangeColorMat(const shared_ptr<Stage>& StagePtr, Vector3& Pos,const wstring& i_Texture, const wstring& TextureType);
		~ChangeColorMat(){}
		//初期化
		//virtual void Create() override;

		//	---------- Method ----------.
		virtual void special_effect() override;

		// ---------- Accessor ----------.

		//変更するテクスチャを取得
		const wstring GetTextureType() const{
			return m_TextureType;
		}

	private:
		// ---------- member ----------.
		wstring m_TextureType;	//自分のカラーを持っておく



	};
}
//endof  basedx11
