

#include "stdafx.h"
#include "Project.h"


namespace basedx11{
	/**
	* @class　GameClearStage
	* @brief クリアステージの分離化.
	* @author yuyu sike
	* @date 2015/10/06 Start
	*/
	//リソースの作成
	void GameClearStage::CreateResourses(){
		wstring strTexture = App::GetApp()->m_wstrRelativeDataPath + L"Scene\\Result.png";
		App::GetApp()->RegisterTexture(L"RESULT_TX", strTexture);

		wstring CursorWav = App::GetApp()->m_wstrRelativeDataPath + L"Sound\\Result.wav";
		App::GetApp()->RegisterWav(L"RESULT", CursorWav);


	}

	//ビューの作成
	void GameClearStage::CreateViews(){
		//最初にデフォルトのレンダリングターゲット類を作成する
		CreateDefaultRenderTargets();
		//マルチビューコンポーネントの取得
		auto PtrMultiView = GetComponent<MultiView>();
		//マルチビューにビューの追加
		auto PtrView = PtrMultiView->AddView();
		//ビューの矩形を設定（ゲームサイズ全体）
		Rect2D<float> rect(0, 0, (float)App::GetApp()->GetGameWidth(), (float)App::GetApp()->GetGameHeight());
		//最初のビューにパラメータの設定
		PtrView->ResetParamaters<LookAtCamera, MultiLight>(rect, Color4(0.0f, 0.01f, 0.01f, 1.0f), 1, 0.0f, 1.0f);

		//0番目のビューのカメラを得る
		auto PtrCamera = GetCamera(0);
		PtrCamera->SetEye(Vector3(0.0f, 0.0f, -5.0f));
		PtrCamera->SetAt(Vector3(0.0f, 0.0f, 0.1f));
	}
	//文字列の作成
	void GameClearStage::CreateString(){
		////文字列をつける
		//auto PtrRes = AddComponent<StringSprite>();
		//PtrRes->SetText(L"Yボタンでステージセレクトに戻る");
		//PtrRes->SetFont(L"DWRITE_FONT_WEIGHT_LIGHT", 50.0f);
		//auto strHeight2 = App::GetApp()->GetGameHeight();		//画面の高さの取得
		//auto strWidth2 = App::GetApp()->GetGameWidth();		//画面の幅の取得
		//Rect2D<float> rect2(0, 0, 400, 200);
		//rect2 += Point2D<float>(strWidth2 / 2.0f - 150, strHeight2 / 2.0f + 200);
		//PtrRes->SetTextRect(rect2);
	}
	void GameClearStage::ResultBgm(){
		//サウンドを登録.
		auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
		pMultiSoundEffect->AddAudioResource(L"RESULT");
		pMultiSoundEffect->Start(L"RESULT", 0, 0.3f);
	}
	//スコアの音
	void GameClearStage::ScoreCountSound(){
		//サウンドを登録.
		auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
		pMultiSoundEffect->AddAudioResource(L"SCORECOUNT");
		pMultiSoundEffect->Start(L"SCORECOUNT", 0, 0.2f);
	}


	void GameClearStage::CreatePlayGameTime(){
		auto SceneBasePtr = App::GetApp()->GetSceneBase();
		auto ScenePtr = dynamic_pointer_cast<Scene>(SceneBasePtr);

		Vector3 pos(5, 4, 0);
		Vector3 scale(2, 2, 0);
		Color4 color(1.0f, 1.0f, 1.0f, 1.0f);

		AddGameObject<GameTimeSprite>(scale, pos + Vector3(-2.3f, 1.9f, 0.0f), color, UINT(ScenePtr->GetPastTime() / 10.0f) % 10);
		AddGameObject<GameTimeSprite>(scale, pos + Vector3(-1.0f, 1.9f, 0.0f), color, UINT(ScenePtr->GetPastTime()) % 10);

		//画像の作成
		//AddGameObject<SpriteObject>(Vector3(0.0f, 9.0f, 0.0f), Vector3(10.0f, 5.0f, 1.0f), L"RESULT_TX");
		//AddGameObject<SpriteObject>(Vector3(8.0f, 4.0f, 0.0f), Vector3(5.0f, 3.0f, 1.0f), L"SECOND_TX");
		AddGameObject<SpriteObject>(Vector3(-12.7f, 6.0f, 0.0f), Vector3(12.0f, 5.5f, 1.0f), L"PLAYTIME_TX");
		AddGameObject<SpriteObject>(Vector3(5.5f, 6.0f, 0.0f), Vector3(2.0f, 2.0f, 1.0f), L"BATU_TX");

		AddGameObject<SpriteObject>(Vector3(-11.5f, 2.5f, 0.0f), Vector3(12.0f, 5.5f, 1.0f), L"PUTNUMBER_TX");
		AddGameObject<SpriteObject>(Vector3(5.5f, 2.6f, 0.0f), Vector3(2.0f, 2.0f, 1.0f), L"BATU_TX");
		AddGameObject<SpriteObject>(Vector3(6.5f, 2.5f, 0.0f), Vector3(3.0f, 2.0f, 1.0f), L"MAINASU_TX");

		AddGameObject<SpriteObject>(Vector3(-8.0f, -4.5f, 0.0f), Vector3(16.0f, 7.0f, 1.0f), L"TOTALSCORE_TX");

		AddGameObject<SpriteObject>(Vector3(16.0f, -7.3f, 0.0f), Vector3(15.0f, 5.0f, 1.0f), L"ABUTTON_TX");

		AddGameObject<SpriteObject>(Vector3(-14.0f, -7.3f, 0.0f), Vector3(15.0f, 4.0f, 1.0f), L"BBUTTON_TX");

		AddGameObject<SpriteObject>(Vector3(0.0f, 0.0f, 0.0f), Vector3(40.0f, 22.0f, 1.0f), L"FRAME_TX");

		AddGameObject<SpriteObject>(Vector3(-13.2f, -1.5f, 0.0f), Vector3(12.0f, 5.5f, 1.0f), L"SCOREMOJI");


		//タイムのスコア　1の位
	    AddGameObject<GameTimeSprite>(scale, pos + Vector3(2.0f, 1.9f, 0.0f), color, 5);

		//タイムのスコア　10の位
		AddGameObject<GameTimeSprite>(scale, pos + Vector3(3.5f, 1.9f, 0.0f), color, 0);


		//マットのスコア　1の位
		AddGameObject<GameTimeSprite>(scale, pos + Vector3(2.6f, -1.5f, 0.0f), color, 2);

		//マットのスコア　10の位
		AddGameObject<GameTimeSprite>(scale, pos + Vector3(4.1f, -1.5f, 0.0f), color, 0);



		//０の位
		AddGameObject<Score_UI>(Vector3(2.0f, 2.0f, 1.0f), Vector3(8.0f, -1.5f, 0.0f), 0);

		//10の位
		AddGameObject<Score_UI>(Vector3(2.0f, 2.0f, 1.0f), Vector3(6.5f, -1.5f, 0.0f), 0);

		//100の位
		auto Sh_100 = AddGameObject<Score_UI>(Vector3(2.0f, 2.0f, 1.0f), Vector3(5.0f, -1.5f, 0.0f), 0);
		SetSharedGameObject(L"100KURAI", Sh_100);

		//1000の位
		auto Sh_1000 = AddGameObject<Score_UI>(Vector3(2.0f, 2.0f, 1.0f), Vector3(3.5f, -1.5f, 0.0f), 0);
		SetSharedGameObject(L"1000KURAI", Sh_1000);

		//スコアの変化
		ScoreUp();
		//ボタン系の画像
		//AddGameObject<SpriteObject>(Vector3(12.0f, -6.0f, 0.0f), Vector3(15.0f, 5.0f, 1.0f), L"YBUTTONSELECT_TX");
		//AddGameObject<SpriteObject>(Vector3(-12.0f, -6.0f, 0.0f), Vector3(12.0f, 5.0f, 1.0f), L"ABUTTONTITLE_TX");
		//AddGameObject<SpriteObject>(Vector3(0.0f, -8.0f, 0.0f), Vector3(15.0f, 5.0f, 1.0f), L"NEXTSTAGE_TX");

	}
	//スコアアイテムの変更
	void GameClearStage::ScoreUp(){
		auto Sh_100 = GetSharedGameObject<Score_UI>(L"100KURAI", false);
		auto Sh_1000 = GetSharedGameObject<Score_UI>(L"1000KURAI", false);

		size_t Score_100 = App::GetApp()->GetScene<Scene>()->Get_100();
		size_t Score_1000 = App::GetApp()->GetScene<Scene>()->Get_1000();

	
		Sh_1000->ScorePlus(Score_1000);
		Sh_100->ScorePlus(Score_100);

	}
	//踏んだマットの回数のスプライト
	void GameClearStage::CreateUsemat(){
		auto SceneBasePtr = App::GetApp()->GetSceneBase();
		auto ScenePtr = dynamic_pointer_cast<Scene>(SceneBasePtr);

		Vector3 pos(5, 0, 0);
		Vector3 scale(2, 2, 0);
		Color4 color(1.0f, 1.0f, 1.0f, 1.0f);

		AddGameObject<GameTimeSprite>(scale, pos + Vector3(-2.3f, 2.5f, 0.0f), color, UINT(ScenePtr->GetUsemat() / 10.0f) % 10);
		AddGameObject<GameTimeSprite>(scale, pos + Vector3(-1.0f, 2.5f, 0.0f), color, UINT(ScenePtr->GetUsemat()) % 10);

	}
	//タイムだけのスコアの作成　使ってない
	void GameClearStage::TimeScore(){

		auto SceneBasePtr = App::GetApp()->GetSceneBase();
		auto ScenePtr = dynamic_pointer_cast<Scene>(SceneBasePtr);

		int TimeScore = 0;

		TimeScore += int(ScenePtr->GetPastTime() ) * 50;

		Vector3 pos(13, 6.0f, 0);
		Vector3 scale(2.0f, 2.0f, 1.0f);
		Color4 color(1.0f, 1.0f, 1.0f, 1.0f);

		UINT pow = 1;
		for (UINT i = 0; i < 4; i++){
			auto ScoreSprite = AddGameObject<GameTimeSprite>(scale, pos + Vector3(-2.0f * i, 0, 0), color, UINT(TimeScore / pow) % 10);
			pow *= 10;
		}

	}
	//マットだけのスコア作成 使ってない
	void GameClearStage::MatScore(){
		auto SceneBasePtr = App::GetApp()->GetSceneBase();
		auto ScenePtr = dynamic_pointer_cast<Scene>(SceneBasePtr);

		int MatScore = 0;

		MatScore += int(ScenePtr->GetUsemat()) * 20;

		Vector3 pos(13, 3.0f, 0);
		Vector3 scale(2.0f, 2.0f, 1.0f);
		Color4 color(1.0f, 1.0f, 1.0f, 1.0f);

		UINT pow = 1;
		for (UINT i = 0; i < 4; i++){
			auto ScoreSprite = AddGameObject<GameTimeSprite>(scale, pos + Vector3(-2.0f * i, 0, 0), color, UINT(MatScore / pow) % 10);
			pow *= 10;
		}

	}
	//トータルスコア作成
	void GameClearStage::CreateScore(){

		Vector3 pos(13, -3.5f, 0);
		Vector3 scale(3.0f, 3.0f, 1.0f);
		Color4 color(1.0f, 1.0f, 1.0f, 1.0f);

		UINT pow = 1;
		for (UINT i = 0; i < 6; i++){
			auto ScoreSprite = AddGameObject<GameTimeSprite>(scale, pos + Vector3(-2.0f * i, -1.0, 0), color, UINT((m_NowScore/ pow) % 10));
			SetSharedGameObject(L"NumberSprite" + to_wstring(pow), ScoreSprite);
			pow *= 10;
		}
	}

	//スコアアップアイテムの計算
	int GameClearStage::MathScoreItem(){

		size_t Score_100 = App::GetApp()->GetScene<Scene>()->Get_100();
		size_t Score_1000 = App::GetApp()->GetScene<Scene>()->Get_1000();

		int Change_100 = Score_100 * 100;
		int Change_1000 = Score_1000 * 1000;

		return Change_100 + Change_1000;
	}


	//初期化
	void GameClearStage::Create(){

		//リソースの作成
		CreateResourses();
		//ビューの作成
		CreateViews();
		//遊んだ時間
		CreatePlayGameTime();
		//使用したマットの数のUI
		CreateUsemat();
		//スコア
		CreateScore();

		CreateString();


		//画面のフェードイン用のオブジェクト作成
		auto FadePtr = AddGameObject<FadePanel>();
		SetSharedGameObject(L"FadePanel", FadePtr);



		m_StateMachine = make_shared< StateMachine<GameClearStage> >(GetThis<GameClearStage>());
		m_StateMachine->SetCurrentState(ResultStartState::Instance());
		m_StateMachine->GetCurrentState()->Enter(GetThis<GameClearStage>());


	}

	//操作
	void GameClearStage::Update(){

		//ステートマシンの更新
		m_StateMachine->Update();

		UINT pow = 1;
		for (UINT i = 0; i < 6; i++){
			auto NumberPtr = GetSharedGameObject<GameTimeSprite>(L"NumberSprite" + to_wstring(pow));
			NumberPtr->SetNumber(UINT((m_NowScore / pow) % 10));
			pow *= 10;
		}

	}

	bool GameClearStage::IsChengeSelectScene(){
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();

		if (CntlVec[0].bConnected){
			//Aボタンが押された瞬間ならScene移動
			if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_B){
				return true;
			}
		}
		return false;

	}

	//リザルト時にCsv読み込み、書き換えをする関数
	//@Name : yuya
	void GameClearStage::Csv_Read_Write(){
		//StageManager。Csvの作成
		wstring MediaPath = App::GetApp()->m_wstrRelativeDataPath;

		//シーンに保存していたワールド名を取得
		wstring WorldName = App::GetApp()->GetScene<Scene>()->Get_WorldSelect();
		wstring FileName_ = WorldName + L"\\";

		//wstring CsvFilename =L"GameStage\\";
		wstring name = L"HyScore.csv";
		//ローカル上にCSVファイルクラスの作成
		CsvFile GameStageCsv(MediaPath + FileName_ + name);
		if (!GameStageCsv.ReadCsv()){
			//ファイルが存在しなかった
			//初期化失敗
			throw BaseException(
				L"CSVファイルがありません。",
				MediaPath + FileName_ + name,
				L"変なとこ選択してんなよ"
				);
		}
		//ワーク用のベクター配列
		vector< wstring > StageManagerVec;
		//現在のステージ番号を設定する
		size_t i_StageNumber = App::GetApp()->GetScene<Scene>()->GetStageNumber();
		GameStageCsv.GetRowVec(i_StageNumber, StageManagerVec);
		//保存されていたトータルスコア（デフォは０）
		auto Point = static_cast<int>(_wtoi(StageManagerVec[0].c_str()));

		if (Point < m_TotalScore){
		wstring wstrTotalScore;
		//TODO : ここ少し理解不足 あとでもっかい　
		//トータルスコアをwstring型に変換し、保存する
		wchar_t Wstr[12];
		_itow_s(m_TotalScore, Wstr, 10);
		wstrTotalScore = Wstr;
		/*AddRowで下に追加
		//GameStageCsv.AddRow(StageName);*/

		//UpdateRowでそのマスの更新	RowでやるとおかしくなるのでCellで共通化した
		//GameStageCsv.UpdateRow(i_StageNumber, wstrTotalScore, false);
		GameStageCsv.UpdateCell(i_StageNumber, 0, wstrTotalScore, false);


		}

		//TODO : ここ関数にまとめたほうがいいかも
		//name yuya 2016/02/11
		auto ScenePtr = dynamic_pointer_cast<Scene>(App::GetApp()->GetSceneBase());
		wstring wstUseMat;
		wchar_t Wst[12];
	
		int UseMat = static_cast<int>(ScenePtr->GetUsemat());
		_itow_s(UseMat, Wst, 10);
		wstUseMat = Wst;

		size_t Col = 1;
		//UpdateRowでそのマスの更新
		GameStageCsv.UpdateCell(i_StageNumber, Col, wstUseMat, false);
		GameStageCsv.SaveCsv(false);

	
	}


	//リザルトに入った時の状態
	shared_ptr<ResultStartState> ResultStartState::Instance(){
		static shared_ptr<ResultStartState> instance(new ResultStartState);
		return instance;
	}
	void ResultStartState::Enter(const shared_ptr<GameClearStage>& Obj){
		Obj->ResultBgm();

	}
	void ResultStartState::Execute(const shared_ptr<GameClearStage>& Obj){
		//スコアのカウントアップを開始する。
		Obj->GetStateMachine()->ChangeState(ScoreeCountState::Instance());
	}
	void ResultStartState::Exit(const shared_ptr<GameClearStage>& Obj){
		//何もしない
	}


	//スコアがカウントされる状態
	shared_ptr<ScoreeCountState> ScoreeCountState::Instance(){
		static shared_ptr<ScoreeCountState> instance;
		if (!instance){
			instance = shared_ptr<ScoreeCountState>(new ScoreeCountState);
		}
		return instance;
	}
	//ステートに入ったときに呼ばれる関数
	void ScoreeCountState::Enter(const shared_ptr<GameClearStage>& Obj){
		//シーンの取得
		auto SceneBasePtr = App::GetApp()->GetSceneBase();
		auto ScenePtr = dynamic_pointer_cast<Scene>(SceneBasePtr);
		if (ScenePtr){

			//合計
			int Score = 0;

			//時間のスコア計算　制限時間ー過ぎた時間　*　10倍している
			Score += int(ScenePtr->GetPastTime() ) * 50;

			//使用したマットのスコア計算　スコアの合計　+　(使用したマットの数　＊　100倍している)
			Score = int(Score - (ScenePtr->GetUsemat() * 20));

			Score += Obj->GetNowScore();

			Score += Obj->MathScoreItem();

			Obj->SetTotalScore(Score);
		}
		//Obj->TimeScore();
		//Obj->MatScore();
	}
	//ステート実行中に毎ターン呼ばれる関数
	void ScoreeCountState::Execute(const shared_ptr<GameClearStage>& Obj){

		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();

		auto NowScore = Obj->GetNowScore();
		auto TotalScore = Obj->GetTotalScore();

		auto a = TotalScore - NowScore;

		if (a > 0){

			//加算
			Obj->SetNowScore(NowScore + 10);

			//音
			Obj->ScoreCountSound();

			if (CntlVec[0].bConnected){
				//Aボタンが押された瞬間なら、スコアを強制的に代入して計算を終わる
				if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_A){
					Obj->SetNowScore(TotalScore);
				}
			}
		//スコアの計算が終了したらステート変更
		}
		else{

			Obj->GetStateMachine()->ChangeState(ScoreeCountEndState::Instance());
		}
	}
	//ステートにから抜けるときに呼ばれる関数
	void ScoreeCountState::Exit(const shared_ptr<GameClearStage>& Obj){
		//何もしない
	}


	//スコアのカウントが終了したときの状態
	shared_ptr<ScoreeCountEndState> ScoreeCountEndState::Instance(){
		static shared_ptr<ScoreeCountEndState> instance(new ScoreeCountEndState);
		return instance;
	}
	void ScoreeCountEndState::Enter(const shared_ptr<GameClearStage>& Obj){
		//スコア完了次第、Csvに記録しておく
		Obj->Csv_Read_Write();

	}
	void ScoreeCountEndState::Execute(const shared_ptr<GameClearStage>& Obj){
		//コントローラ情報の取得
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();

		//Aボタンが押されたら
		if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_A) {
			//シーンから現在のゲームセレクトで選んだゲームステージの番号取得
			size_t i_StageNumber = App::GetApp()->GetScene<Scene>()->GetStageNumber();

			//TODO : ここ必ず直す いみふだから
			if (i_StageNumber == 25){
			
				Obj->PostEvent(0.0f, Obj->GetThis<GameClearStage>(), App::GetApp()->GetSceneBase(), L"ToStageSelect");
			}
			else{
				//次のステージの番号を設定
				App::GetApp()->GetScene<Scene>()->SetStageNumber(i_StageNumber + 1);
				Obj->PostEvent(0.0f, Obj->GetThis<GameClearStage>(), App::GetApp()->GetSceneBase(), L"ToGame");
			}
			
		}

		if (Obj->IsChengeSelectScene()){
			//フェードアウトさせる
			auto FadePtr = Obj->GetSharedGameObject<FadePanel>(L"FadePanel");
			if (FadePtr){
				Obj->PostEvent(0.0f, Obj->GetThis<GameClearStage>(), FadePtr, L"FadeOut");
			}

		}
		auto FadePtr = Obj->GetSharedGameObject<FadePanel>(L"FadePanel");
		if (FadePtr){
			if (FadePtr->IsFadeOutActive() == false && FadePtr->IsFadeOutBeforeActive() == true){
				Obj->PostEvent(0.0f, Obj->GetThis<GameClearStage>(), App::GetApp()->GetSceneBase(), L"ToStageSelect");
			}
		}		
	}
	void ScoreeCountEndState::Exit(const shared_ptr<GameClearStage>& Obj){
		//何もしない

	}




}

//endof  basedx11
