#pragma once

#include "stdafx.h"

namespace basedx11{

	/**
	* @class　GameClearStage
	* @brief クリアステージの分離化.
	* @author yuyu sike
	* @date 2015/10/06 Start
	*/


	class GameClearStage : public Stage{
		shared_ptr<StateMachine<GameClearStage>>  m_StateMachine;	//ステートマシーン

		//過ぎた時間
		float m_Timer;
		//踏んだマットの数
		float m_Usemat;
		//TODO : わかんないこれ
		int m_NowScore;
		//トータルのスコア
		int m_TotalScore;
		

		//リソースの作成
		void CreateResourses();
		//ビューの作成
		void CreateViews();
		//遊んだ時間のUI表示
		void CreatePlayGameTime();
		//使用したマットのUIの表示
		void CreateUsemat();
		//スコアの変化するやつ
		void ScoreUp();
		//スコアの計算
		void CreateScore();
		//文字列の作成
		void CreateString();


	public:
		//構築と破棄
		GameClearStage() :Stage(){}
		virtual ~GameClearStage(){}
		//初期化
		virtual void Create()override;
		//更新
		virtual void Update() override;
 
		void TimeScore();
		void MatScore();

		void ResultBgm();
		void ScoreCountSound();
		//スコアアップの合計を足して返す
		int MathScoreItem();
		bool IsChengeSelectScene();

		//リザルト時にCsv読み込み、書き換えをする関数
		//@Name : yuya
		void Csv_Read_Write();


		//アクセサ
		shared_ptr<StateMachine<GameClearStage>> GetStateMachine() const{
			return m_StateMachine;
		}
		//残り時間
		float GetTime(){
			return m_Timer;
		}
		//使用マットの数
		float GetUsemat(){
			return m_Usemat;
		}
		int GetNowScore(){
			return m_NowScore;
		}
		void SetNowScore(int score){
			m_NowScore = score;
		}
		//スコアの合計
		int GetTotalScore(){
			return m_TotalScore;
		}

		void SetTotalScore(int score){
			m_TotalScore = score;
		}

	};
	//リザルトに入った時の状態
	class ResultStartState : public ObjState<GameClearStage>
	{
		ResultStartState(){}
	public:
		static shared_ptr<ResultStartState> Instance();
		virtual void Enter(const shared_ptr<GameClearStage>& Obj);
		virtual void Execute(const shared_ptr<GameClearStage>& Obj);
		virtual void Exit(const shared_ptr<GameClearStage>& Obj);
	};

	//スコアがカウントされる状態
	class ScoreeCountState : public ObjState<GameClearStage>
	{
		ScoreeCountState(){}
	public:
		//ステートのインスタンス取得
		static shared_ptr<ScoreeCountState> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<GameClearStage>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<GameClearStage>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<GameClearStage>& Obj)override;
	};

	class ScoreeCountEndState : public ObjState<GameClearStage>
	{
		ScoreeCountEndState(){}
	public:
		//ステートのインスタンス取得
		static shared_ptr<ScoreeCountEndState> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<GameClearStage>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<GameClearStage>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<GameClearStage>& Obj)override;
	};


}
//endof  basedx11
