#include "stdafx.h"
#include "Project.h"

namespace basedx11{

	/*
	* @class Direction_Effect
	* @brief Cubeが向きを変えたときに前方に飛ばす四角いエフェクト
	* @author yuyu sike
	* @date 2016/01/21 Start
	*/
	Direction_Effect::Direction_Effect(shared_ptr<Stage>& StagePtr) :
		MultiParticle(StagePtr)
	{}

	//エフェクトの生成
	void Direction_Effect::InsertDirection_Effect(const Vector3& Pos, const size_t& TexNum, const Vector2& Dir){

		m_TextureMap[4] = L"BLUE_CUBE";
		m_TextureMap[6] = L"RED_CUBE";
		m_TextureMap[10] = L"GREEN_CUBE";
		m_TextureMap[8] = L"PURPLE_CUBE";
		
		//エフェクトの数
		auto ParticlePtr = InsertParticle(1);
		//エフェクトをどこから出すか
		ParticlePtr->SetEmitterPos(Pos);

		//透過処理
		//SetAlphaActive(true);
		//SetAlphaExActive(true);

		SetDrawLayer(1);

		//エフェクトのテクスチャ
		ParticlePtr->SetTextureResource(m_TextureMap[TexNum]);

		//たぶん生命時間					

		//ParticlePtr->SetMaxTime(0.3f);
		vector<ParticleSprite>& pSpriteVec = ParticlePtr->GetParticleSpriteVec();
		for (auto& rParticleSprite : ParticlePtr->GetParticleSpriteVec()){

			//パーティクルのサイズ
			rParticleSprite.m_LocalScale = Vector2(0.3f, 0.3f);

			rParticleSprite.m_Active = true;

			//パーティクルの位置
			rParticleSprite.m_LocalPos.x = 0.0f;
			rParticleSprite.m_LocalPos.y = 0.0f;
			rParticleSprite.m_LocalPos.z = 0.0f;

			//各パーティクルの移動速度を指定
			rParticleSprite.m_Velocity = Vector3(
				rParticleSprite.m_LocalPos.x * 0.0f,
				rParticleSprite.m_LocalPos.y * 0.0f,
				rParticleSprite.m_LocalPos.z * 0.0f
				);

			//色の指定
//			rParticleSprite.m_Color = Color4(Util::RandZeroToOne() * 1.0f, Util::RandZeroToOne() * 1.0f, Util::RandZeroToOne() * 1.0f, 1.0f);
			//rParticleSprite.m_Color = Color4(0.0f,0.0f,1.0f,1.0f);
		}


	}

	//変化
	void Direction_Effect::Update(){

		float fTimeSpan = App::GetApp()->GetElapsedTime();

		for (auto ParticlePtr : GetParticleVec())
		{
			ParticlePtr->AddTotalTime(fTimeSpan);
			for (auto& rParticleSprite : ParticlePtr->GetParticleSpriteVec())
			{
				if (rParticleSprite.m_Active)
				{
					rParticleSprite.m_LocalPos += rParticleSprite.m_Velocity * fTimeSpan;
					rParticleSprite.m_Color -= Color4(0.02f, 0.02f, 0.02f, 0.04f);			//カラー減る
					rParticleSprite.m_LocalScale -= Vector2(0.01f, 0.01f);

					if (ParticlePtr->GetTotalTime() > 1.0f) {
						rParticleSprite.m_Active = false;
					}
				}
			}
		}

	}

	/*
	* @class CubeGoalEffect
	* @brief ゴールしたときのエフェクト
	* @author yuyu sike
	* @date 2016/01/21 Start
	*/
	CubeGoalEffect::CubeGoalEffect(shared_ptr<Stage>& StagePtr) :
		MultiParticle(StagePtr),
		m_Time()
	{}
	CubeGoalEffect::~CubeGoalEffect(){}

	void CubeGoalEffect::InsertCubeGoalEffect(const Vector2& Pos){

		Vector3 V_Pos(Pos.x, 0.0f, Pos.y);

		//パーティクルの生成
		auto PtrParticle = InsertParticle(40);
		//基本座標の設定
		PtrParticle->SetEmitterPos(V_Pos);
		SetAlphaActive(true);
		SetAlphaExActive(true);		//透明処理

		PtrParticle->SetTextureResource(L"SPARK_TX"); //Lineテクスチャに変更　　　@yuya

		vector<ParticleSprite>& pSpriteVec = PtrParticle->GetParticleSpriteVec();

		for (auto& rParticleSprite : PtrParticle->GetParticleSpriteVec()){
			rParticleSprite.m_LocalPos.x = Util::RandZeroToOne() * 1.0f - 0.5f;
			rParticleSprite.m_LocalPos.y = Util::RandZeroToOne() * 2.0f;
			rParticleSprite.m_LocalPos.z = Util::RandZeroToOne() * 1.0f - 0.5f;

			rParticleSprite.m_LocalScale = Vector2(0.5f, 0.5f);

			rParticleSprite.m_Velocity = Vector3(
				0.0f,
				Util::RandZeroToOne() * 5.0f + 2.0f,
				0.0f
				);
		}
	}

	void CubeGoalEffect::Update(){

		//	: 事前準備
		static const float EndPos_Length = 5.0f;
		static const Color4 PullColor = Color4(Util::RandZeroToOne() * 0.1f, Util::RandZeroToOne() * 0.1f, Util::RandZeroToOne() * 0.1f, 0.0f);
		m_Time += 0.1f;

		//前回のターンからの時間
		float fTimeSpan = App::GetApp()->GetElapsedTime();

		for (auto ParticlePtr : GetParticleVec()){
			ParticlePtr->AddTotalTime(fTimeSpan);

			for (auto& rParticleSprite : ParticlePtr->GetParticleSpriteVec()){
				if (rParticleSprite.m_Active){
					rParticleSprite.m_LocalPos += rParticleSprite.m_Velocity * fTimeSpan;
					rParticleSprite.m_Color += PullColor;

					if (Vector3EX::Length(rParticleSprite.m_LocalPos) > EndPos_Length) {
						rParticleSprite.m_Active = false;
					}
				}
			}
		}
	}

	//スコアアップ時のエフェクトの構築と破棄
	ScoreUpEffect::ScoreUpEffect(shared_ptr<Stage>& StagePtr) :
		MultiParticle(StagePtr)
	{
	}

	void ScoreUpEffect::InsertScoreUpEffect(const Vector3& pos){
		//エフェクトの数
		auto ParticlePtr = InsertParticle(10);
		//エフェクトをどこから出すか
		ParticlePtr->SetEmitterPos(pos);

		//透過処理
		//SetAlphaActive(true);
		//SetAlphaExActive(true);

		//エフェクトのテクスチャ
		ParticlePtr->SetTextureResource(L"SPARK_TX");
	
		//たぶん生命時間
		//ParticlePtr->SetMaxTime(0.3f);
		vector<ParticleSprite>& pSpriteVec = ParticlePtr->GetParticleSpriteVec();
		for (auto& rParticleSprite : ParticlePtr->GetParticleSpriteVec()){

			rParticleSprite.m_LocalScale = Vector2(0.5f, 0.5f);

			rParticleSprite.m_LocalPos.x = Util::RandZeroToOne() * 0.1f - 0.05f;/*Util::RandZeroToOne() * 0.1f - 0.1f;*/
			rParticleSprite.m_LocalPos.y = Util::RandZeroToOne() * 0.1f ;/*Util::RandZeroToOne() * 0.1f - 0.05f;*/
			rParticleSprite.m_LocalPos.z = Util::RandZeroToOne() * 0.1f - 0.05f;/*Util::RandZeroToOne() * 0.1f - 0.1f;*/
			//各パーティクルの移動速度を指定

			rParticleSprite.m_Velocity = Vector3(
				rParticleSprite.m_LocalPos.x * 30.0f,
				rParticleSprite.m_LocalPos.y * 30.0f,
				rParticleSprite.m_LocalPos.z * 30.0f
				);
			//色の指定
			rParticleSprite.m_Color = Color4(Util::RandZeroToOne() * 1.0f, Util::RandZeroToOne() * 1.0f, Util::RandZeroToOne() * 1.0f, 1.0f);
		}
	}

	//変化
	void ScoreUpEffect::Update(){
		//事前準備
		static const float EndPos_Length = 3.0f;

		//前回のターンからの時間
		float fTimeSpan = App::GetApp()->GetElapsedTime();

		for (auto ParticlePtr : GetParticleVec()){
			ParticlePtr->AddTotalTime(fTimeSpan);

			for (auto& rParticleSprite : ParticlePtr->GetParticleSpriteVec()){
				if (rParticleSprite.m_Active){
					rParticleSprite.m_LocalPos += rParticleSprite.m_Velocity * fTimeSpan;

					if (Vector3EX::Length(rParticleSprite.m_LocalPos) > EndPos_Length) {
						rParticleSprite.m_Active = false;
					}
				}
			}
		}
	}

	/*
	* @class ScoreUp_Square_Effect
	* @brief スコアを表示した後のエフェクト
	* @author yuyu sike
	* @date 2016/01/18 Start
	*/

	ScoreUp_Square_Effect::ScoreUp_Square_Effect(shared_ptr<Stage>& StagePtr) :
		MultiParticle(StagePtr),
		m_EffectName(5),
		m_RapNum(0)
	{}

	//パーティクルの作成
	void ScoreUp_Square_Effect::InsertScoreUp_SquareEffect(const Vector3& Pos, const size_t& TexNum){
		//エフェクトの数
		auto ParticlePtr = InsertParticle(40);
		//エフェクトをどこから出すか
		ParticlePtr->SetEmitterPos(Pos);
		m_EffectName[0] = L"RED_CIRCLE";
		m_EffectName[1] = L"BLUE_CIRCLE";
		m_EffectName[2] = L"GREEN_CIRCLE";
		m_EffectName[3] = L"PURPLE_CIRCLE";
		m_EffectName[4] = L"STARBAN";

	
		//エフェクトのテクスチャ
		//ParticlePtr->SetTextureResource(L"SPARK_TX");
		ParticlePtr->SetTextureResource(m_EffectName[TexNum]);

		//たぶん生命時間
		ParticlePtr->SetMaxTime(1.0f);
		vector<ParticleSprite>& pSpriteVec = ParticlePtr->GetParticleSpriteVec();
		for (auto& rParticleSprite : ParticlePtr->GetParticleSpriteVec()){
			//ParticlePtr->SetTextureResource(m_EffectName[ static_cast<int>(Util::RandZeroToOne() * 3.0f) ] );

			rParticleSprite.m_LocalScale = Vector2(0.3f, 0.3f);

			rParticleSprite.m_LocalPos.x = Util::RandZeroToOne() * 0.1f - 0.05f;/*Util::RandZeroToOne() * 0.1f - 0.1f;*/
			rParticleSprite.m_LocalPos.y = Util::RandZeroToOne() * 0.1f;/*Util::RandZeroToOne() * 0.1f - 0.05f;*/
			rParticleSprite.m_LocalPos.z = Util::RandZeroToOne() * 0.1f - 0.05f; /*Util::RandZeroToOne() * 0.1f - 0.1f;*/
			//各パーティクルの移動速度を指定

			rParticleSprite.m_Velocity = Vector3(
				rParticleSprite.m_LocalPos.x * 20.0f,
				rParticleSprite.m_LocalPos.y * 20.0f,
				rParticleSprite.m_LocalPos.z * 20.0f
				);
			//色の指定
			if (m_EffectName[TexNum] == L"STARBAN"){
				rParticleSprite.m_Color = Color4(Util::RandZeroToOne() * 2.0f, Util::RandZeroToOne() * 2.0f, Util::RandZeroToOne() * 2.0f, 0.5f);

			}
		}
	}

	//変化
	void ScoreUp_Square_Effect::Update(){

		float fTimeSpan = App::GetApp()->GetElapsedTime();
		for (auto ParticlePtr : GetParticleVec()){
			//トータル時間に加算
			ParticlePtr->AddTotalTime(fTimeSpan);
			for (auto& rParticleSprite : ParticlePtr->GetParticleSpriteVec()){
				//サイズを小さくする
				rParticleSprite.m_LocalScale -= Vector2(0.006f,0.006f);
				rParticleSprite.m_LocalPos += rParticleSprite.m_Velocity * fTimeSpan;

				if (ParticlePtr->GetTotalTime() >= ParticlePtr->GetMaxTime()){
					//制限時間になったら
					rParticleSprite.m_Active = false;

				}
			}
		}
	}


	PlayerEffect::PlayerEffect(shared_ptr<Stage>& StagePtr) :
		MultiParticle(StagePtr)

	{}
	PlayerEffect::~PlayerEffect(){}

	void PlayerEffect::InsertPlayerEffect(const Vector3& Pos){
		auto ParticlePtr = InsertParticle(1);
		ParticlePtr->SetEmitterPos(Pos);
		ParticlePtr->SetTextureResource(L"SPARK_TX");
		ParticlePtr->SetMaxTime(0.3f);
		vector<ParticleSprite>& pSpriteVec = ParticlePtr->GetParticleSpriteVec();
		for (auto& rParticleSprite : ParticlePtr->GetParticleSpriteVec()){
			rParticleSprite.m_LocalPos.x = Util::RandZeroToOne() * 0.1f - 0.05f;
			rParticleSprite.m_LocalPos.y = -0.1f;
			rParticleSprite.m_LocalPos.z = Util::RandZeroToOne() * 0.1f - 0.05f;
			//各パーティクルの移動速度を指定
			rParticleSprite.m_Velocity = Vector3(
				rParticleSprite.m_LocalPos.x * 3.0f,
				rParticleSprite.m_LocalPos.y * 3.0f,
				rParticleSprite.m_LocalPos.z * 3.0f
				);
			//色の指定
			rParticleSprite.m_Color = Color4(1.0f, 1.0f, 0.0f, 1.0f);
		}
	}


	void PlayerEffect::Update(){

		float fTimeSpan = App::GetApp()->GetElapsedTime();
		for (auto ParticlePtr : GetParticleVec()){
			//トータル時間に加算
			ParticlePtr->AddTotalTime(fTimeSpan);
			
			for (auto& rParticleSprite : ParticlePtr->GetParticleSpriteVec()){
				if (ParticlePtr->GetTotalTime() >= ParticlePtr->GetMaxTime()){
					//制限時間になったら
					rParticleSprite.m_Active = false;
					
				}
			}
		}
	}

	/**
	* @class ArrowContactEffect
	* @brief 矢印表示のエフェクト作成
	* @author 菅野 翔吾
	*/

	ArrowContactEffect::ArrowContactEffect(shared_ptr<Stage>& StagePtr) :
		MultiParticle(StagePtr),
		m_Parentdirection()

	{}
	ArrowContactEffect::~ArrowContactEffect(){}

	void ArrowContactEffect::Create(){
		//ステートマシンの構築
		m_StateMachine = make_shared< StateMachine<ArrowContactEffect> >(GetThis<ArrowContactEffect>());
		//最初のステートをParentStartStateに設定
		m_StateMachine->SetCurrentState(ChangeResourceState::Instance());
		//ParentStartStateの初期化実行を行う
		m_StateMachine->GetCurrentState()->Enter(GetThis<ArrowContactEffect>());
	}

	void ArrowContactEffect::Update(){

		m_StateMachine->Update();

	}


	void ArrowContactEffect::InsertArrowContactEffect(const Vector2& Pos, const Vector2& Rot){
		//	: 事前準備
		static const Color4 YELLO = Color4(1.0f, 1.0f, 1.0f, 1.0f);

		Vector3 V_Pos(Pos.x, 0.0f, Pos.y);

		m_Parentdirection = Vector2(Rot);

		//	: パーティクルを生成.
		auto ptrParticle = InsertParticle(50);			//４０個
		//	: 基本座標を設定.
		ptrParticle->SetEmitterPos(V_Pos);
		ptrParticle->SetDrawOption(Particle::Normal);
		//	: テクスチャを設定.
		if (m_Parentdirection == Vector2(1.0f, 0.0f)){
			ptrParticle->SetTextureResource(L"ARROWRIGHTEFFECT_TX");

		}
		else if (m_Parentdirection == Vector2(-1.0f, 0.0f)){
			ptrParticle->SetTextureResource(L"ARROWLEFTEFFECT_TX");

		}
		else if (m_Parentdirection == Vector2(0.0f, 1.0f)){
			ptrParticle->SetTextureResource(L"ARROWUPEFFECT_TX");

		}
		else if (m_Parentdirection == Vector2(0.0f, -1.0f)){
			ptrParticle->SetTextureResource(L"ARROWDOWNEFFECT_TX");

		}

		SetAlphaActive(true);
		SetAlphaExActive(true);
		ptrParticle->SetMaxTime(0.5f);


		vector<ParticleSprite>& pSpriteVec = ptrParticle->GetParticleSpriteVec();
		for (auto& rParticleSprite : ptrParticle->GetParticleSpriteVec()){
			//サイズ
			rParticleSprite.m_LocalScale = Vector2(1.0f, 1.0f);

			Quaternion Qt;
			//Z回転を作成する
			Vector3 LocalZRot(1.5f, 0.0f, 0.0f);
			Qt.RotationRollPitchYawFromVector(LocalZRot);
			Qt.Normalize();
			rParticleSprite.m_LocalQt = Qt;
			rParticleSprite.m_LocalQt.Normalize();

			//ポジション
			rParticleSprite.m_LocalPos.x = 0.1f;
			rParticleSprite.m_LocalPos.y = 2.0f;
			rParticleSprite.m_LocalPos.z = 0.1f;
			//各パーティクルの移動速度を指定
			rParticleSprite.m_Velocity = Vector3(
				rParticleSprite.m_LocalPos.x * 20.0f,
				rParticleSprite.m_LocalPos.y * 0.0f,
				rParticleSprite.m_LocalPos.z * 20.0f
				);

			rParticleSprite.m_Color = YELLO;
		}
	}

	void ArrowContactEffect::ChangeResource(){
		//	: 事前準備
		static const float EndPos_Length = 2.0f;
		//前回のターンからの時間
		float fTimeSpan = App::GetApp()->GetElapsedTime();
		for (auto ParticlePtr : GetParticleVec())
		{
			ParticlePtr->AddTotalTime(fTimeSpan);
			for (auto& rParticleSprite : ParticlePtr->GetParticleSpriteVec())
			{
				if (rParticleSprite.m_Active)
				{


					//if (m_Parentdirection == Vector2(1, 0)){
					//	auto A = 0;
					//	rParticleSprite.m_LocalPos.x += rParticleSprite.m_Velocity.x * fTimeSpan;
					//}
					//else if (m_Parentdirection == Vector2(-1, 0)){
					//	rParticleSprite.m_LocalPos.x -= rParticleSprite.m_Velocity.x * fTimeSpan;
					//}
					//else if (m_Parentdirection == Vector2(0, 1)){
					//	rParticleSprite.m_LocalPos.z += rParticleSprite.m_Velocity.z * fTimeSpan;
					//}
					//else if (m_Parentdirection == Vector2(0, -1)){
					//	rParticleSprite.m_LocalPos.z -= rParticleSprite.m_Velocity.z * fTimeSpan;
					//}
					//一定時間で消滅させる
					if (ParticlePtr->GetTotalTime() > 0.5f){
						rParticleSprite.m_Active = false;
					}
				}
			}
		}
	}
	//ステートのインスタンス取得
	shared_ptr<ChangeResourceState> ChangeResourceState::Instance(){
		static shared_ptr<ChangeResourceState> instance;
		if (!instance){
			instance = shared_ptr<ChangeResourceState>(new ChangeResourceState);
		}
		return instance;
	}
	//ステートに入ったときに呼ばれる関数
	void ChangeResourceState::Enter(const shared_ptr<ArrowContactEffect>& Obj){
		//何もしない
	}
	//ステート実行中に毎ターン呼ばれる関数
	void ChangeResourceState::Execute(const shared_ptr<ArrowContactEffect>& Obj){
		Obj->ChangeResource();
	}

	//ステートから抜けるときに呼ばれる関数
	void ChangeResourceState::Exit(const shared_ptr<ArrowContactEffect>& Obj){
		//何もしない
	}


	/**
	* @class GameEffectManager
	* @brief エフェクトを管理するClass作成
	* @author 菅野 翔吾
	*/


	GameEffectManager::GameEffectManager(const shared_ptr<Stage>& StagePtr) :
		GameObject(StagePtr)
	{
	}
	GameEffectManager::~GameEffectManager(){}

	void GameEffectManager::InsertArrowContactEffect(const Vector2& pos, const Vector2& Rot)
	{
		auto pArrowContactEffect = m_ArrowContactEffect.lock();
		if (pArrowContactEffect)
		{
			pArrowContactEffect->InsertArrowContactEffect(pos, Rot);
		}

	}
	void GameEffectManager::InsertCubeGoalEffect(const Vector2& pos)
	{
		auto pMoveEffect = m_CubeGoalEffect.lock();
		if (pMoveEffect)
		{
			pMoveEffect->InsertCubeGoalEffect(pos);
		}

	}
	void GameEffectManager::InsertPlayerEffect(const Vector3& pos)
	{
		auto pPlayerEffect = m_PlayerEffect.lock();
		if (pPlayerEffect)
		{
			pPlayerEffect->InsertPlayerEffect(pos);
		}

	}

	//スコアアップ時のエフェクトの作成 @yua
	void GameEffectManager::InsertScoreUpEffect(const Vector3& pos){
		auto pScoreUpEffect = m_ScoreupEffect.lock();
		if (pScoreUpEffect){
			pScoreUpEffect->InsertScoreUpEffect(pos);
		}
	}

	//スコア取得時のSquareのエフェクト
	void GameEffectManager::InsertScoreUpSquareEffect(const Vector3& pos, const size_t& TexNum){
		auto pScoreUp_Square = m_ScoreUp_Square_Effect.lock();
		if (pScoreUp_Square){
			pScoreUp_Square->InsertScoreUp_SquareEffect(pos,TexNum);
		}
	}

	//向きの変更のエフェクト
	void GameEffectManager::InsertDirectionEffect(const Vector3& Pos, const size_t& TexNum, const Vector2& Dir){
		auto pDirectionEffect = m_DirectionEffect.lock();
		if (pDirectionEffect){
			pDirectionEffect->InsertDirection_Effect(Pos, TexNum, Dir);
		}
	}
}
//end basedx11
