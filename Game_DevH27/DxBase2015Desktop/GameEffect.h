#pragma once
#include "stdafx.h"

namespace basedx11{


	
	class CubeGoalEffect : public MultiParticle{
		float m_Time;
	public:
		CubeGoalEffect(shared_ptr<Stage>& StagePtr);
		virtual ~CubeGoalEffect();

		virtual void Update() override;
		void InsertCubeGoalEffect(const Vector2& Pos);
	};

	class ScoreUpEffect : public MultiParticle{
	public:
		ScoreUpEffect(shared_ptr<Stage>& StagePtr);
		virtual ~ScoreUpEffect(){}

		virtual void Update() override;
		void InsertScoreUpEffect(const Vector3& Pos);
	};

	/*
	* @class ScoreUp_Square_Effect
	* @brief スコアを表示した後のエフェクト
	* @author yuyu sike
	* @date 2016/01/18 Start
	*/

	class ScoreUp_Square_Effect : public MultiParticle{
	private:
		vector<wstring> m_EffectName;
		size_t m_RapNum;
	public:
		ScoreUp_Square_Effect(shared_ptr<Stage>& StagePtr);
		virtual ~ScoreUp_Square_Effect(){}

		virtual void Update();
		void InsertScoreUp_SquareEffect(const Vector3& Pos , const size_t& TexNum);
	};



	class PlayerEffect : public MultiParticle{
	public :
		PlayerEffect(shared_ptr<Stage>& StagePtr);
		virtual ~PlayerEffect();

		virtual void Update() override;
		void InsertPlayerEffect(const Vector3& Pos);

	};

	class ArrowContactEffect : public MultiParticle{
		Vector2 m_Parentdirection;
		shared_ptr<StateMachine<ArrowContactEffect>>  m_StateMachine;	//ステートマシーン

	public : 
		ArrowContactEffect(shared_ptr<Stage>& StagePtr);
		virtual~ArrowContactEffect();

		virtual void Create() override;
		virtual void Update() override;


		shared_ptr<StateMachine<ArrowContactEffect>> GetStateMachine() const{
			return m_StateMachine;
		}


		//	---------- Method ----------.


		void InsertArrowContactEffect(const Vector2& Pos, const Vector2& Rot);

		void ChangeResource();
	};

	/*
	* @class Direction_Effect
	* @brief Cubeが向きを変えたときに前方に飛ばす四角いエフェクト
	* @author yuyu sike
	* @date 2016/01/21 Start
	*/

	class Direction_Effect : public MultiParticle{
	private:
		//エフェクトの素材
		vector<wstring> m_EffectName;
	
		map<size_t, wstring> m_TextureMap;
	public:
		//構築と破棄
		Direction_Effect(shared_ptr<Stage>& StagePtr);
		~Direction_Effect(){}

		//変化と作成
		virtual void Update();
		void InsertDirection_Effect(const Vector3& Pos, const size_t& TexNum, const Vector2& Dir);

	};


	//--------------------------------------------------------------------------------------
	//	class TimeState : public ObjState<ParentBox>;
	//	用途: 時間計測
	//--------------------------------------------------------------------------------------
	class ChangeResourceState : public ObjState<ArrowContactEffect>
	{
		ChangeResourceState(){}
	public:
		//ステートのインスタンス取得
		static shared_ptr<ChangeResourceState> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<ArrowContactEffect>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<ArrowContactEffect>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<ArrowContactEffect>& Obj)override;
	};

	class GameEffectManager : public GameObject
	{
	public : 
		GameEffectManager(const shared_ptr<Stage>& StagePtr);
		virtual~GameEffectManager();

		//--------------Accessor----------------

		void SetArrowContactEffect(weak_ptr<ArrowContactEffect> i_ArrowContactEffect)
		{
			m_ArrowContactEffect = i_ArrowContactEffect;
		}
		const weak_ptr<ArrowContactEffect>GetArrowContactEffect() const
		{
			return m_ArrowContactEffect;
		}
		void SetMoveEffect(weak_ptr<CubeGoalEffect> i_CubeGoalEffect)
		{
			m_CubeGoalEffect = i_CubeGoalEffect;
		}
		const weak_ptr<CubeGoalEffect>GetMoveEffect() const
		{
			return m_CubeGoalEffect;
		}
		void SetPlayerEffect(weak_ptr<PlayerEffect> i_PlayerEffect)
		{
			m_PlayerEffect = i_PlayerEffect;
		}
		const weak_ptr<PlayerEffect>GetPlayerEffect() const
		{
			return m_PlayerEffect;
		}
		//スコアアップ時のエフェクトの設定
		void SetScoreUpEffect(weak_ptr<ScoreUpEffect> i_ScoreupEffect){
			m_ScoreupEffect = i_ScoreupEffect;
		}
		//スコアアップ時のエフェクトの取得
		const weak_ptr<ScoreUpEffect>GEtScoreupEffect() const{
			return m_ScoreupEffect;
		}
		//スクエア用のエフェクトの設定
		void SetScoreUp_Square_Effect(weak_ptr<ScoreUp_Square_Effect> i_ScoreUp_Square){
			m_ScoreUp_Square_Effect = i_ScoreUp_Square;
		}
		//スコアアップのスクエアのエフェクトの取得
		const weak_ptr<ScoreUp_Square_Effect>GetScoreUp_Square_Effect() const{
			return m_ScoreUp_Square_Effect;
		}
		//向きが変わった時のCubeから出てくるエフェクト
		void SetDirectionEffect(weak_ptr<Direction_Effect> i_DirectionEffect){
			m_DirectionEffect = i_DirectionEffect;
		}
		//DirectionEffectの取得
		const weak_ptr<Direction_Effect>GetDirectionEffect() const{
			return m_DirectionEffect;
		}
		//--------------------------------------

		//--------------Method----------------
		void InsertArrowContactEffect(const Vector2& Pos, const Vector2& Rot);
		void InsertCubeGoalEffect(const Vector2& Pos);
		void InsertPlayerEffect(const Vector3& Pos);
		void InsertScoreUpEffect(const Vector3& Pos);
		void InsertScoreUpSquareEffect(const Vector3& Pos, const size_t& TexNum);
		void InsertDirectionEffect(const Vector3& Pos, const size_t& TexNum, const Vector2& Dir);

	private:
		weak_ptr<ArrowContactEffect> m_ArrowContactEffect;
		weak_ptr<CubeGoalEffect> m_CubeGoalEffect;
		weak_ptr<PlayerEffect> m_PlayerEffect;
		weak_ptr<ScoreUpEffect> m_ScoreupEffect;
		weak_ptr<ScoreUp_Square_Effect> m_ScoreUp_Square_Effect;
		weak_ptr<Direction_Effect> m_DirectionEffect;


	};

}
//end basedx11
