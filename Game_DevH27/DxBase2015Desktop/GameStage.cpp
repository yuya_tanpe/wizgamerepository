
#include "stdafx.h"
#include "Project.h"


namespace basedx11{

	//--------------------------------------------------------------------------------------
	//	class GameStage : public Stage;
	//	用途: ゲームステージクラス
	//--------------------------------------------------------------------------------------
	//TODO : //型関数　検索
	//リソースの作成
	void GameStage::CreateResourses(){
		//リソースはSceneに置き換え
	}

	//ビュー類の作成
	void GameStage::CreateViews(){
		//最初にデフォルトのレンダリングターゲット類を作成する
		CreateDefaultRenderTargets();
		//マルチビューコンポーネントの取得
		auto PtrMultiView = GetComponent<MultiView>();
		//マルチビューにビューの追加
		auto PtrView = PtrMultiView->AddView();
		//ビューの矩形を設定（ゲームサイズ全体）
		Rect2D<float> rect(0, 0, (float)App::GetApp()->GetGameWidth(), (float)App::GetApp()->GetGameHeight());
		//最初のビューにパラメータの設定
		//PtrView->ResetParamaters<Camera, MultiLight>(rect, Color4(0.0f, 0.125f, 0.3f, 1.0f), 1, 0.0f, 1.0f);
		PtrView->ResetParamaters<LookAtCamera, MultiLight>(rect, Color4(0.25f, 0.25f, 0.25f, 1.0f), 1, 0.0f, 1.0f);
		
		//ゲームプレイ中のカメラ
		m_LookAtCamera = PtrView->GetCamera();
		m_LookAtCamera->SetEye(Vector3(0.0f, 7.5f, -6.5f));
		m_LookAtCamera->SetAt(Vector3(0.0f, 0.0f, 0.0f));


		//ゲームオーバー、ゲームクリア用のカメラ
		m_MoveCamera = Object::CreateObject<Camera>();
		m_MoveCamera->SetEye(Vector3(0.0f, 2.0f, -5.0f));
		m_MoveCamera->SetAt(m_CameraSetAt);
	
	}

	void GameStage::CreateUI(){
		AddGameObject<SpriteObject>(Vector3(-15.3f, 9.0f, 0.0f), Vector3(12.0f, 6.0f, 1.0f), L"PLAYTIME_TX");

		AddGameObject<SpriteObject>(Vector3(-12.0f, 9.0f, 0.0f), Vector3(14.0f, 4.0f, 1.0f), L"FRAME_TX");

	}

	//エフェクトの生成
	void GameStage::CreateEffect(){
		//エフェクトはZバッファを使用しない
		GetParticleManager()->SetZBufferUse(false);
		//Arrowマットを踏んだ時のエフェクト
		auto ArrowContactEffectPtr = AddGameObject<ArrowContactEffect>();
		//ColorCubeがゴールしたときのエフェクト
		auto CubeGoalEffectPtr = AddGameObject<CubeGoalEffect>();
		//Playerが動いたときのエフェクト　現在は未実装2015/11/26
		auto PlayerPtr = AddGameObject<PlayerEffect>();
		//ScoreUp時のエフェクト
		auto PtrScoreUpEffect = AddGameObject<ScoreUpEffect>();
		//ScoreUpSquareのエフェクト
		auto PtrScoreUp_Square = AddGameObject<ScoreUp_Square_Effect>();
		//向き変更時のエフェクト
		auto PtrDirection_Effect = AddGameObject<Direction_Effect>();
		//エフェクトマネージャーの追加
		auto GameEffectManagerPtr = AddGameObject<GameEffectManager>();
		//Shared配列に各エフェクトの追加
		SetSharedGameObject(L"GameEffectManager", GameEffectManagerPtr);
		GameEffectManagerPtr->SetArrowContactEffect(ArrowContactEffectPtr);
		GameEffectManagerPtr->SetMoveEffect(CubeGoalEffectPtr);
		GameEffectManagerPtr->SetPlayerEffect(PlayerPtr);
		GameEffectManagerPtr->SetScoreUpEffect(PtrScoreUpEffect);
		GameEffectManagerPtr->SetScoreUp_Square_Effect(PtrScoreUp_Square);
		GameEffectManagerPtr->SetDirectionEffect(PtrDirection_Effect);
	}

	//背景の作成
	void GameStage::CreateBackGround(){
		AddGameObject<BackGround>(
			Vector3(250.0f, 250.0f, 250.0f),
			Vector3(0.0f, XM_PIDIV2, 0.0f),
			Vector3(0.0f, 0.0f, 0.0f)
			);
	}

	//プレイヤーがスポーンするボックスの作成
	void GameStage::CreatePlayerBox(Vector3 i_Scale, Vector3 i_Pos){
		//ワーク用　Vector2型　ポジションXZを２次元にキャスト
		Vector2 CellPos(i_Pos.x, i_Pos.z);
		//プレイヤーを生成する際のポジションをメンバ変数に代入
		m_vPlayerSpawnCell = CellPos;
		//プレイヤースポーンボックスの生成
		AddGameObject<PlayerSpawnBox>(i_Scale, i_Pos);
	}

	void GameStage::SelectVector(int number, const Vector2 SelectVec){

		switch (number)
		{
		case 0:
			BlueVec = SelectVec;
			break;
		case 1:
			RedVec = SelectVec;
			break;
		case 2:
			PurpleVec = SelectVec;
			break;
		case 3:
			GreenVec = SelectVec;
			break;
		default:
			assert(!"不正な値が入りました。CsvでのCUBEの向きを確認してください");
			break;
		}
	}

	//動的にStageを変更する　引数のsize_t型でステージの名前を取得してステージ読み込み
	wstring GameStage::Change_Stage(size_t number){
		//StageManager。Csvの作成
		wstring MediaPath = App::GetApp()->m_wstrRelativeDataPath;
		 
		//シーンに保存していたワールド名を取得
		wstring WorldName = App::GetApp()->GetScene<Scene>()->Get_WorldSelect();
		wstring FileName_ = WorldName + L"\\";

		//wstring CsvFilename =L"GameStage\\";
		wstring name = L"Stage_Manager.csv";
		//ローカル上にCSVファイルクラスの作成
		CsvFile GameStageCsv(MediaPath + FileName_ + name);
		if (!GameStageCsv.ReadCsv()){
			//ファイルが存在しなかった
			//初期化失敗
			throw BaseException(
				L"CSVファイルがありません。",
				MediaPath + FileName_ + name,
				L"変なとこ選択してんなよ"
				);
		}
		//ワーク用のベクター配列
		vector< wstring > StageManagerVec;	
		//Change_Stageの引数から何列目の指定
		GameStageCsv.GetRowVec(number, StageManagerVec);
		//ワーク用　Wstring型　ステージ名を入れる
		wstring StageName = L"";
		//０行目からStageNameを取得
		StageName = StageManagerVec[0].c_str();
		//ステージのゴール数を取得	ステージ切り替え時に必要
		//TODO : もしかしたらエラーの原因になるかもしれないから再調整するかもしれない
		m_GoalSize =static_cast<size_t>( _wtoi(StageManagerVec[1].c_str()) );

		//ステージの色を取得する　７列目
		m_ChangeBoxNameMap[L"1"] = L"STAGEBOXBLUE_TX";
		m_ChangeBoxNameMap[L"2"] = L"STAGEBOXORANGE_TX";
		m_ChangeBoxNameMap[L"3"] = L"STAGEBOXRED_TX";
		m_StageColor = StageManagerVec[7].c_str();
		//自身のwstringをmapで変換し、テクスチャキーにする
		m_StageColor = m_ChangeBoxNameMap[m_StageColor];


		//Cubeの初動向きの取得
		vector< wstring > TokenVec;			//デリミタで区切る用のベクター配列
		for (auto i = 0; i < 4; i++){
			Util::WStrToTokenVector(TokenVec, StageManagerVec[i + 3].c_str(), L';');
			//初動の向きを取得
			float MoveX = static_cast<float>(_wtoi(TokenVec[0].c_str()));	//X座標
			float MoveZ = static_cast<float>(_wtoi(TokenVec[1].c_str()));	//Z座標
			//取得したものをまとめておく
			Vector2 CUBESelect{ MoveX, MoveZ };
			SelectVector(i, CUBESelect);
			TokenVec.clear();
		}
		
		//返却
		return StageName;
	}

	//CSVファイルを使ってゲームステージの作成
	void GameStage::CreateRoad(){
		//Csvファイルからゲームステージの選択
		wstring MediaPath = App::GetApp()->m_wstrRelativeDataPath;
		//シーンからゲームセレクトで選んだゲームステージの番号取得
		size_t i_StageNumber = App::GetApp()->GetScene<Scene>()->GetStageNumber();
		//ステージ番号からステージの名前を取得
		wstring i_StageName = Change_Stage(i_StageNumber);
		//ステージ名をメンバ変数に入れる
		m_StageName = i_StageName;

		//CSVファイルの選択			
		//wstring Math_wstirng(L"GameStage\\");	//事前準備

		//シーンに保存していたワールド名を取得
		wstring WorldName = App::GetApp()->GetScene<Scene>()->Get_WorldSelect();
		wstring FileName_ = WorldName + L"\\";

		wstring CsvText(L".csv");
		wstring StageName(FileName_ + i_StageName + CsvText);
		//ステージのCsvファイルを作成
		wstring CsvFilename = MediaPath + StageName;
		//ローカル上にCSVファイルクラスの作成
		CsvFile GameStageCsv(CsvFilename);
		if (!GameStageCsv.ReadCsv()){
			//ファイルが存在しなかった
			//初期化失敗
			throw BaseException(
				L"CSVファイルがありません。",
				CsvFilename,
				L"選択された場所にはアクセスできません"
				);
		}
		const int iDataSizeRow = 0;		//データが0行目から始まるようの定数
		vector< wstring > StageMapVec;	//ワーク用のベクター配列
		//iDataSizeRowのデータを抜き取りベクター配列に格納
		GameStageCsv.GetRowVec(iDataSizeRow, StageMapVec);
		//行、列の取得
		m_sRowSize = static_cast<size_t>( _wtoi(StageMapVec[0].c_str()) ); //Row  = 行
		m_sColSize = static_cast<size_t>( _wtoi(StageMapVec[1].c_str()) ); //Col　= 列
		m_GoalSize = App::GetApp()->GetScene<Scene>()->GetStageGoalSize(); //シーンからゴール数の取得
		//マップデータ配列作成
		m_MapDataVec = vector< vector<size_t> >(m_sRowSize, vector<size_t>(m_sColSize));	//列の数と行の数分作成
		//矢印マットおくためのベクター配列作成
		m_ArrowObjectVec = vector< vector< weak_ptr<ArrowObject> >>(m_sRowSize, vector< weak_ptr<ArrowObject> >(m_sColSize));
		//固定マットの配列作成
		//TODO : 2015/11/26現在size_t型で作成済みだがGameObjectで取得できるように変更
		m_FixedMatVec = vector< vector<ItemCase> >(m_sRowSize, vector<ItemCase>(m_sColSize));

		m_CameraSetAt = Vector3(m_sColSize / 2.0f, 1.5f, m_sRowSize / 2.0f);
		App::GetApp()->GetScene<Scene>()->SetHarfStageSize(m_CameraSetAt);


		//配列の初期化
		for (size_t i = 0; i < m_sRowSize; i++){
			for (size_t j = 0; j < m_sColSize; j++)
			{
				m_MapDataVec[i][j] = 0;								//size_t型のvector配列を０で初期化			
				m_ArrowObjectVec[i][j].reset();						//weak_ptr<ArrowObject>型のvector配列をreset()関数で初期化(nullptr)
				m_FixedMatVec[i][j] = ItemCase::Empty;				//size_t型のvector配列を０で初期化
			}
		}
		//ステージ毎の固定マットを生成
		//	CreateFixedMat(i_StageNumber);
		//1行目からステージが定義されている
		const int iDataStartRow = 1;
		assert(m_sRowSize > 0 && "行が０以下なのでゲームが開始できません");
		if (m_sRowSize > 0){
			for (size_t i = 0; i < m_sRowSize; i++){
				GameStageCsv.GetRowVec(i + iDataStartRow, StageMapVec);		//スタート + i　だから最初は 2が入る
				for (size_t j = 0; j < m_sColSize; j++){					//列分ループ処理
					const int iNum = _wtoi(StageMapVec[j].c_str());			//列から取得したwstring型をintに変換→格納	
					//マップデータ配列に格納
					m_MapDataVec[i][j] = iNum;
					//配置されるオブジェクトの基準スケール
					float ObjectScale = 0.99f;
					//基準Scale
					Vector3 Scale(ObjectScale, ObjectScale, ObjectScale);
					//基準Position
					Vector3 Pos(static_cast<float>(j), 0.5, static_cast<float>(i));
					switch (iNum){
					case DataID::NULLID:				//	Csv	:	0　空洞
							break;
					case DataID::ROAD:					//	Csv	:	1　道
						CreateBlock<FixedBox>(Scale, Pos, m_StageColor);
						break;
					case DataID::PLAYERSPAWN:			//	Csv	:	2　プレイヤー生成するブロック
						CreatePlayerBox(Scale, Pos);
						break;
					case DataID::BLUECUBE:				//	Csv	:	3　青色のCUBEを生成するブロック
						CreateBlock<ColorCubeSpawnBox>(Scale, Pos, L"BLUECUBE_TX", BlueVec, 4, L"BLUE", L"CUBEBLUE_MESH", m_StageColor);
						break;
					case DataID::BLUEGOAL:				//	Csv	:	4　青色のCubeがゴールするためのブロック
						CreateBlock<ColorCubeGoalBox>(Scale, Pos, L"BLUECUBE_TX");
						break;
					case DataID::REDCUBE:				//	Csv	:	5　赤色のCUBEを生成するブロック
						CreateBlock<ColorCubeSpawnBox>(Scale, Pos, L"REDCUBE_TX", RedVec, 6, L"RED", L"CUBERED_MESH", m_StageColor);
						break;
					case DataID::REDGOAL:				//	Csv	:	6 赤色のCubeがゴールするためのブロック
						CreateBlock<ColorCubeGoalBox>(Scale, Pos, L"REDCUBE_TX");
						break;
					case DataID::PURPLECUBE:			//	Csv	:	7 紫色のCUBEを生成するブロック
						CreateBlock<ColorCubeSpawnBox>(Scale, Pos, L"PURPLECUBE_TX", PurpleVec, 8, L"PURPLE", L"CUBEPURPLE_MESH", m_StageColor);
						break;
					case DataID::PURPLEGOAL:			//	Csv	:	8　紫色のCubeがゴールするためのブロック
						CreateBlock<ColorCubeGoalBox>(Scale, Pos, L"PURPLECUBE_TX");
						break;
					case DataID::YELLOCUBE:				//	Csv	:	9 緑色のCUBEを生成するブロック
						CreateBlock<ColorCubeSpawnBox>(Scale, Pos, L"GREENCUBE_TX", GreenVec, 10, L"YELLO", L"CUBEGREEN_MESH", m_StageColor);
						break;
					case DataID::YELLOGOAL:				//	Csv	:	10　緑色のCubeがゴールするためのブロック
						CreateBlock<ColorCubeGoalBox>(Scale, Pos, L"GREENCUBE_TX");
						break;
					default:
						assert(!"不正な値が入ってます。Csvマップの可能性が高いです");
						break;
					}

				}

			}

		}
	}

	//CSVからアイテムなどのオブジェクトの作成  主に固定マットやスコアアップアイテム、妨害アイテムなど
	void GameStage::CreateStageObject(){

		//Csvファイルからゲームステージの選択
		wstring MediaPath = App::GetApp()->m_wstrRelativeDataPath;
		//シーンからゲームセレクトで選んだゲームステージの番号取得
		size_t i_StageNumber = App::GetApp()->GetScene<Scene>()->GetStageNumber();
		//ステージ番号からステージの名前を取得
		wstring i_StageName = Change_Stage(i_StageNumber);
		//CSVファイルの選択			
		//wstring Math_wstirng(L"GameStage\\");	//事前準備
		//シーンに保存していたワールド名を取得
		wstring WorldName = App::GetApp()->GetScene<Scene>()->Get_WorldSelect();
		wstring FileName_ = WorldName + L"\\";

		wstring CsvText(L".csv");
		wstring StageName(FileName_ + i_StageName + CsvText);
		//ステージのCsvファイルを作成
		wstring CsvFilename = MediaPath + StageName;
		//ローカル上にCSVファイルクラスの作成
		CsvFile GameStageCsv(CsvFilename);
		if (!GameStageCsv.ReadCsv()){
			//ファイルが存在しなかった
			//初期化失敗
			throw BaseException(
				L"CSVファイルがありません。",
				CsvFilename,
				L"選択された場所にはアクセスできません"
				);
		}

		//Csvファイルから、string型でもらう物をあらかじめマップと関連付けしておく
		m_ItemMap[L"empty"] = ItemCase::Empty;
		//スコア関連
		m_ItemMap[L"ScoreRed"] = ItemCase::ScoreRed;
		m_ItemMap[L"ScoreBlue"] = ItemCase::ScoreBlue;
		m_ItemMap[L"ScoreGreen"] = ItemCase::ScoreGreen;
		m_ItemMap[L"ScorePurple"] = ItemCase::ScorePurple;
		//固定マップ関連
		m_ItemMap[L"Reverse"] = ItemCase::Reverse;
		m_ItemMap[L"ColorRed"] = ItemCase::ColorRed;
		m_ItemMap[L"ColorBlue"] = ItemCase::ColorBlue;
		m_ItemMap[L"ColorPurple"] = ItemCase::ColorPurple;
		m_ItemMap[L"ColorGreen"] = ItemCase::ColorGreen;
		m_ItemMap[L"MoveBox"] = ItemCase::MoveBox;

		vector< wstring > StageMapVec;	//ワーク用のベクター配列
		const int iDataStartRow = m_sRowSize + 3;		//どこから読み込むかを決める  + 1

		for (size_t i = 0; i < m_sRowSize; i++){
			GameStageCsv.GetRowVec(i + iDataStartRow, StageMapVec);		//スタート + i　だから最初は 2が入る
			for (size_t j = 0; j < m_sColSize; j++){					//列分ループ処理
				const wstring ObjectName = StageMapVec[j].c_str();		//列から取得したwstring型を格納

				float ObjectScale = 1.0f;
				//基準Scale
				Vector3 Scale(ObjectScale, ObjectScale, ObjectScale);
				//基準Position
				Vector3 Pos(static_cast<float>(j), 1.0f, static_cast<float>(i));

				m_FixedMatVec[static_cast<size_t>(Pos.z)][static_cast<size_t>(Pos.x)] = m_ItemMap[ObjectName];
				static const wstring REDTX{ L"REDCUBE_TX" };

				switch (m_ItemMap[ObjectName])
				{
				case ItemCase::Empty:
					//なにもおかない時はemptyで統一
					//assert(!"空洞です");
					break;

				//スコアアップ関連
				case ItemCase::ScoreRed:
					//スコアアップアイテムの生成
					AddGameObject<ScoreUp_Item>(Pos, L"RED");
					break;
				case ItemCase::ScoreBlue:
					//スコアアップアイテムの生成
					AddGameObject<ScoreUp_Item>(Pos, L"BLUE");
					break;	
				case ItemCase::ScoreGreen:
					//スコアアップアイテムの生成
					AddGameObject<ScoreUp_Item>(Pos, L"GREEN");
					break;	
				case ItemCase::ScorePurple:
					//スコアアップアイテムの生成
					AddGameObject<ScoreUp_Item>(Pos, L"PURPLE");
					break;

				//固定マット関連			
				case ItemCase::Reverse:
					//反転マット
					AddGameObject<ReverseMat>(Pos, L"REVERSE_TX");	
					break;
				case ItemCase::ColorRed:
					//色替えマット（赤）
					AddGameObject<ChangeColorMat>(Pos, L"CHANGERED_TX", REDTX);
					break;
				case ItemCase::ColorBlue:
					AddGameObject<ChangeColorMat>(Pos, L"CHANGEBLUE_TX", REDTX);
					break;
				case ItemCase::ColorPurple:
					AddGameObject<ChangeColorMat>(Pos, L"CHANGEPURPLE_TX", REDTX);
					break;
				case ItemCase::ColorGreen:
					AddGameObject<ChangeColorMat>(Pos, L"CHANGEGREEN_TX", REDTX);
					break;
				case ItemCase::MoveBox:
					//動くボックスはひとつしか作れない
					m_wpMove = AddGameObject<MoveBox>(Pos);
					break;
				default:
					assert(!"不正な値が入りました　Csvファイルを確認してください");
					break;
				}
			}
		}

	}

	//ステージマネージャの作成　ゴールや音楽等で実装
	void GameStage::CreateStageManager(){
		auto Ptr = AddGameObject<StageManager>(m_GoalSize);		//ここの引数はステージセレクト時に変更する
		SetSharedGameObject(L"StageManager", Ptr);
	}

	//固定マットの作成
	//void GameStage::CreateFixedMat(size_t number){
	//	//Stageの作成
	//	wstring MediaPath = App::GetApp()->m_wstrRelativeDataPath;
	//	wstring CsvFilename = MediaPath + L"GameStage\\Stage_Manager.csv";
	//	//ローカル上にCSVファイルクラスの作成
	//	CsvFile GameStageCsv(CsvFilename);
	//	if (!GameStageCsv.ReadCsv()){
	//		//ファイルが存在しなかった
	//		//初期化失敗
	//		throw BaseException(
	//			L"CSVファccfffffffイルがありません。",
	//			CsvFilename,
	//			L"選択している場所にアクセスできません"
	//			);
	//	}
	//	vector< wstring > StageManagerVec;	//ワーク用のベクター配列
	//	vector< wstring > TokenVec;			//デリミタで区切る用のベクター配列
	//	//Change_Stageの引数から何列目の指定
	//	GameStageCsv.GetRowVec(number, StageManagerVec);
	//	//固定マットがあるか 1 あり　0 なし
	//	size_t IsFixedMat = (size_t)_wtoi(StageManagerVec[2].c_str());	
	//	if (IsFixedMat){
	//		//固定マットの種類の取得		size_t型で種類作成 
	//		//TODO : 現在size_t型で生成しているがGameObjectで判断できるように改良していく
	//		size_t FixedMatType = static_cast<size_t>(_wtoi(StageManagerVec[7].c_str()));
	//		//L''で切り取る範囲の設定　第１引数のvector配列に格納していく
	//		Util::WStrToTokenVector(TokenVec, StageManagerVec[8].c_str(), L';');
	//		//座標の取得
	//		float FixedMatPosX = static_cast<float>(_wtoi(TokenVec[0].c_str()));	//X座標
	//		float FixedMatPosZ = static_cast<float>(_wtoi(TokenVec[1].c_str()));	//Z座標
	//		//座標の作成
	//		static const wstring REDTX{ L"REDCUBE_TX" };
	//		Vector3 MatPos{ FixedMatPosX, 1.0f, FixedMatPosZ };
	//		//固定マットの配列追加
	//		//m_FixedMatVec[static_cast<size_t>(FixedMatPosZ)][static_cast<size_t>(FixedMatPosX)] = FixedMatType;	
	//		//	: 事前準備
	//		//Typeによって作るオブジェクトを変える
	//		switch (FixedMatType)
	//		{
	//		case 1:
	//			AddGameObject<ReverseMat>(MatPos, L"REVERSE_TX");
	//			break;
	//		case 2:
	//			AddGameObject<ChangeColorMat>(MatPos, L"CHANGERED_TX", REDTX);
	//			break;
	//		default:
	//			assert(!"不正な値が入りました");
	//			break;
	//		}
	//	}
	//}

	//カメラの作成
	void GameStage::CreateCamera(){
		//半分のサイズの取得
		Vector3 Pos = App::GetApp()->GetScene<Scene>()->GetHarfStageSize();
		//ステージの真ん中に埋まっているオブジェクト
		auto Ptr = AddGameObject<LookAtObject>(Pos);
		SetSharedGameObject(L"LookAt", Ptr);
		//上空をまわっているカメラの生成	LookAtObjectを注視する
		AddGameObject<SpinObject>(Vector3(0, 8.5f, -6.0f));
	}
	
	//プレイヤーの作成
	void GameStage::CreatePlayer(Vector2 i_vCellPos){
		auto PtrPlayer = AddGameObject<Player>(i_vCellPos);
		//Sharedに登録
		SetSharedGameObject(L"Player", PtrPlayer);

		//pauseなどで使うグループの取得
		auto Pause_Use_Group = GetSharedObjectGroup(L"PauseGroup");

		//PauseGloupにゲームオブジェクトを登録
		Pause_Use_Group->IntoGroup(PtrPlayer);

	}

	//敵の生成	現在敵のアイデア出すためコメントアウトにしてある
	void GameStage::CreateEnamy(){
		//TODO : ここの敵の生成は今後Csvで出来るように設定する
		//AddGameObject<SimpleEnemy>(Vector3(0.0f,1.5f,0.0f));
		//AddGameObject<SimpleEnemy>(Vector3(0.0f, 1.5f, 2.0f));
		AddGameObject<SimpleEnemy>(Vector3(0.0f, 1.5, 4.0f));
		//AddGameObject<SimpleEnemy>(Vector3(2, 1.5, 0));
		AddGameObject<SimpleEnemy>(Vector3(2, 1.5, 2));
		//AddGameObject<SimpleEnemy>(Vector3(4, 1.5, 0));
		AddGameObject<SimpleEnemy>(Vector3(4, 1.5, 2));
		//AddGameObject<SimpleEnemy>(Vector3(6, 1.5, 0));
		//AddGameObject<SimpleEnemy>(Vector3(6, 1.5, 2));
		//AddGameObject<SimpleEnemy>(Vector3(6, 1.5, 4));
		//AddGameObject<SimpleEnemy>(Vector3(8, 1.5, 0));
		//AddGameObject<SimpleEnemy>(Vector3(8, 1.5, 2));
	}

	//オブジェクトグループを作成
	void GameStage::CreateGruop(){
		//親のCUBEを保管するグループ 
		auto pParentGroup = CreateSharedObjectGroup(L"ParentGroup");
		//子のCUBEを保管するグループ
		auto pChiledGroup = CreateSharedObjectGroup(L"ChiledGroup");
		//ポーズ画面等で生成したオブジェクトを止めるの使うグループ
		auto pPause_UsedGameObjectGloup = CreateSharedObjectGroup(L"PauseGroup");
	}

	//制限時間のスプライトの生成
	void GameStage::CreateTimeSprite(){

		Vector3 scale(2.0f, 1.5f, 0.0f);
		Vector3 Pos(0.0f, 9.0f, 0.0f);
		Color4 color(1.0f, 1.0f, 1.0f, 1.0f);


		auto TimePtr1 = AddGameObject<GameTimeSprite>(scale , Pos + Vector3(-12.0, 0.0f, 0.0f), color, 6);
		SetSharedGameObject(L"TimeSprite_Doubledigit", TimePtr1);

		auto TimePtr2 = AddGameObject<GameTimeSprite>(scale, Pos + Vector3(-10.5f, 0.0f, 0.0f), color, 0);
		SetSharedGameObject(L"TimeSprite_Onedigit", TimePtr2);

		auto TimePtr = AddGameObject<GameTimeSprite>(scale, Pos + Vector3(-9.0f, 0.0f, 0.0f), color, 10);
		SetSharedGameObject(L"TimeNumber_0", TimePtr2);

		auto TimePtr3 = AddGameObject<GameTimeSprite>(scale - Vector3(0.7f, 0.7f, 0.0f), Pos + Vector3(-8.7f, -0.25f, 0), color, 0);	
		SetSharedGameObject(L"TimeNumber_01", TimePtr3);

		auto TimePtr4 = AddGameObject<GameTimeSprite>(scale - Vector3(0.7f, 0.7f, 0.0f), Pos + Vector3(-7.5f, -0.25f, 0), color, 0);	
		SetSharedGameObject(L"TimeNumber_001", TimePtr4);

		//pauseなどで使うグループの取得
		auto Pause_Use_Group = GetSharedObjectGroup(L"PauseGroup");

		Pause_Use_Group->IntoGroup(TimePtr1);
		Pause_Use_Group->IntoGroup(TimePtr2);
		Pause_Use_Group->IntoGroup(TimePtr3);
		Pause_Use_Group->IntoGroup(TimePtr4);


	}

	//クリアした後のプレイした時間のスプライト
	void GameStage::TimeNumber(float time, Color4 color, bool IsShow){

		auto TimePtr = GetSharedGameObject<GameTimeSprite>(L"TimeSprite_Doubledigit");
		TimePtr->SetDrawActive(IsShow);
		TimePtr->SetColor(color);
		TimePtr->SetNumber(UINT(time / 10.0f) % 10);
		TimePtr = GetSharedGameObject<GameTimeSprite>(L"TimeSprite_Onedigit");
		TimePtr->SetDrawActive(IsShow);
		TimePtr->SetNumber(UINT(time) % 10);
		TimePtr->SetColor(color);
		TimePtr = GetSharedGameObject<GameTimeSprite>(L"TimeNumber_0");
		TimePtr->SetDrawActive(IsShow);
		TimePtr->SetColor(color);
		TimePtr->SetNumber(UINT(time) % 10);
		TimePtr = GetSharedGameObject<GameTimeSprite>(L"TimeNumber_01");
		TimePtr->SetDrawActive(IsShow);
		TimePtr->SetColor(color);
		TimePtr->SetNumber(UINT(time * 10.0f) % 10);
		TimePtr = GetSharedGameObject<GameTimeSprite>(L"TimeNumber_001");
		TimePtr->SetDrawActive(IsShow);
		TimePtr->SetColor(color);
		TimePtr->SetNumber(UINT(time * 100.0f) % 10);
	}

	void GameStage::Time(){
		App::GetApp()->GetElapsedTime();
		//-------------------Time----------------------------
		auto time = GetTimer();
		auto timeleft = GetTimeleft();
		time = UpdateTimer();
		timeleft = UpdateTimerleft();

		//残り時間
		//時間の速さを変えるときは下を変更する
		float TimeLeft = 60.0f - time;

		Color4 color(1, 1, 1, 1);

		if (TimeLeft < 30.0f){
			color = Color4(1, 1, 0, 1);
		}

		if (TimeLeft < 10.0f){
			//color = Color4(1, 0, 0, 1);
			color = Color4(1, 0.5f + 0.25f*cos(TimeLeft * 10.0f), 0.5f + 0.25f*cos(TimeLeft * 10.0f), 1.0f);
		}

		if (time >= 60.0f){
			GetStateMachine()->ChangeState(GameOverState::Instance());
		}
		//スプライト更新
		TimeNumber(TimeLeft, color, true);

	}

	//Sprite
	void GameStage::CreateSprite(){

		auto Sh_Sprite = AddGameObject<PauseSprite>(Vector3(0.0f, 0.0f, 0.0f));
		//Sh_Sprite->SetDrawActive(false);
		//Shared_ptrとして他で使えるようにしておく
		SetSharedGameObject(L"SH_PAUSE", Sh_Sprite);

		//ポーズ画面のアイコン作成
		auto Sh_PauseIcon = AddGameObject<PauseArrow>(Vector3(-10.0f, 5.0f, 0.0f));
		SetSharedGameObject(L"SH_ICON", Sh_PauseIcon);

		//チュートリアル画像の作成
		auto Sh_Tutorial = AddGameObject<Tutorial_UI>();
		SetSharedGameObject(L"TUTORIAL", Sh_Tutorial);

		auto AButonn = AddGameObject<AButton_UI>();
		SetSharedGameObject(L"AButton", AButonn);

		//ロールアップ
		auto RollUp = AddGameObject<Roll_UI>(L"ROLLUP_NOT", L"ROLLUP",Vector3(14.3f,-9.0f,0.0f));
		SetSharedGameObject(L"ROLLUP_SP", RollUp);

		//ロールダウン
		auto RollDown = AddGameObject<Roll_UI>(L"ROLLDOWN_NOT", L"ROLLDOWN",Vector3(-14.0f, -9.0f, 0.0f));
		SetSharedGameObject(L"ROLLDOWN_SP", RollDown);


		
	}

	//目標マット数のスプライトの作成			名前変えたほうがいいかも
	void GameStage::Create_MarkMatSprite(){

		//StageManager。Csvの作成
		wstring MediaPath = App::GetApp()->m_wstrRelativeDataPath;

		//シーンに保存していたワールド名を取得
		wstring WorldName = App::GetApp()->GetScene<Scene>()->Get_WorldSelect();
		wstring FileName_ = WorldName + L"\\";
		
		//ステージ全般を管理しているCsv名		File : Stage_Manager.csv
		wstring name = L"Stage_Manager.csv";
		//ローカル上にCSVファイルクラスの作成
		CsvFile GameStageCsv(MediaPath + FileName_ + name);
		if (!GameStageCsv.ReadCsv()){
			//ファイルが存在しなかった
			//初期化失敗
			throw BaseException(
				L"CSVファイルがありません。",
				MediaPath + FileName_ + name,
				L"変なとこ選択してんなよ"
				);
		}

		//ワーク用のベクター配列
		vector< wstring > StageManagerVec;
		//Change_Stageの引数から7行目の指定		文字列をStageManagerVecに格納
		GameStageCsv.GetRowVec( 7, StageManagerVec);
		//ワーク用　Wstring型　ステージ名を入れる
		wstring StageName = L"";
		//０行目からStageNameを取得
		StageName = StageManagerVec[0].c_str();
		//０番目に格納されている数字（目標の数）を取得
		size_t MarkMat = static_cast<size_t>( _wtoi(StageManagerVec[0].c_str()) );

		//AddGameObjectで目標マット数のスプライトを作る際、引数MarkMatで数字が変更できるように作る
		//TODO : GameObjectの作成
	}

	//スコアの作成
	void GameStage::CreateScoreUI(){
		//０の位
		AddGameObject<Score_UI>(Vector3(2.0f, 2.5f, 1.0f), Vector3(18.0f, 9.0f, 0.0f), 0);

		//10の位
		AddGameObject<Score_UI>(Vector3(2.0f, 2.5f, 1.0f), Vector3(16.5f, 9.0f, 0.0f), 0);
		
		//100の位
		auto Sh_100 = AddGameObject<Score_UI>(Vector3(2.0f, 2.5f, 1.0f), Vector3(15.0f, 9.0f, 0.0f), 0);
		SetSharedGameObject(L"100KURAI", Sh_100);

		//1000の位
		auto Sh_1000 = AddGameObject<Score_UI>(Vector3(2.0f, 2.5f, 1.0f), Vector3(13.5f, 9.0f, 0.0f), 0);
		SetSharedGameObject(L"1000KURAI", Sh_1000);
		//Scoreの文字
		AddGameObject<SpriteObject>(Vector3(9.0f, 9.0f, 0.0f), Vector3(12.0f, 7.0f, 0.0f), L"SCOREMOJI");

		auto LB = AddGameObject<LB_UI>(Vector3(4.7, 5.5, 0));
		SetSharedGameObject(L"LB", LB);
		auto RB = AddGameObject<RB_UI>(Vector3(15.3, 5.3, 0));
		SetSharedGameObject(L"RB", RB);
	}

	//スコアアイテムの変更
	void GameStage::ScoreUp(size_t Score){
		auto Sh_100 = GetSharedGameObject<Score_UI>(L"100KURAI", false);
		auto Sh_1000 = GetSharedGameObject<Score_UI>(L"1000KURAI", false);
		m_TotalScoreItem += Score;			//合計もScoreで入った分あげる
		if (m_TotalScoreItem >= 10){
			size_t nokori = m_TotalScoreItem - 10;
			//１０００以上になったから1000の位を繰り上げする		m_1000Scoreはデフォで0である
			Sh_1000->ScorePlus(++m_1000Score);
			//残りの数を代入
			m_TotalScoreItem = nokori;		
		}
		
		Sh_100->ScorePlus(m_TotalScoreItem);
		
	}
	//デバッグ表示　
	void GameStage::DebugLog(){

		auto fps = App::GetApp()->GetStepTimer().GetFramesPerSecond();
		wstring FPS(L"FPS: ");
		FPS += Util::UintToWStr(fps);
		FPS += L"\n";

		wstring StateStr;
		if (m_StateMachine->GetCurrentState() == GameStartState::Instance()){
			StateStr = L"GameStartState";
		}
		else if (m_StateMachine->GetCurrentState() == GamePlayState::Instance()){
			StateStr = L"GamePlayState";
		}
		else if (m_StateMachine->GetCurrentState() == GameClearState::Instance()){
			StateStr = L"GameClearState";
		}
		else if (m_StateMachine->GetCurrentState() == GameOverState::Instance()){
			StateStr = L"GameOverState";
		}
		else if (m_StateMachine->GetCurrentState() == GamePauseState::Instance()){
			StateStr = L"GamePauseState";
		}

		Vector3 CameraPos =  m_MoveCamera->GetEye();
		//Vector3 CameraPos = m_LookAtCamera->GetEye();

		wstring Str_CameraPos(L"Camera_Pos : \t");
		Str_CameraPos += L"X=" + Util::FloatToWStr(CameraPos.x, 6, Util::FloatModify::Fixed) + L",\t";
		Str_CameraPos += L"Y=" + Util::FloatToWStr(CameraPos.y, 6, Util::FloatModify::Fixed) + L",\t";
		Str_CameraPos += L"Z=" + Util::FloatToWStr(CameraPos.z, 6, Util::FloatModify::Fixed) + L",\t";
		Str_CameraPos += L"\n";


		wstring str = FPS + Str_CameraPos + StateStr;
		//文字列をつける
		auto PtrString = GetComponent<StringSprite>();
		PtrString->SetText(str);
	}

	//TODO : デバッグでUpdateいれてるが、ゲームの流れ次第でコメント処理
	void GameStage::Update(){
		//ステートマシンの更新
		m_StateMachine->Update();
		//デバッグログで確認
		//DebugLog();
	}

	//初期化
	void GameStage::Create(){
		//文字列をつける
		auto PtrString = AddComponent<StringSprite>();
		PtrString->SetText(L"");
		PtrString->SetTextRect(Rect2D<float>(16.0f, 120.0f, 640.0f, 240.0f));
			try{

			//リソースの作成
			CreateResourses();
			//ビュー類を作成する
			CreateViews();
			//グループの作成
			CreateGruop();
			//背景
			CreateBackGround();
			//スプライトの生成
			CreateSprite();
			//壁の作成
			CreateRoad();
			//アイテム等のオブジェクトの生成
			CreateStageObject();
			//カメラの動き
			CreateCamera();
			//StageManagerの作成
			CreateStageManager();
			//エフェクト
			CreateEffect();
			//制限時間のスプライトの作成
			CreateTimeSprite();
			//プレーヤーの作成
			CreatePlayer(Vector2 (m_vPlayerSpawnCell));
			//ゲームプレイ中表示するUI
			CreateUI();
			//スコアの作成
			CreateScoreUI();
			//エネミーの作成
			//CreateEnamy();

			//画面のフェードイン用のオブジェクト作成
			auto FadePtr = AddGameObject<FadePanel>();
			SetSharedGameObject(L"FadePanel", FadePtr);


			//ステートマシンの構築
			m_StateMachine = make_shared< StateMachine<GameStage> >(GetThis<GameStage>());
			m_StateMachine->SetCurrentState(GameStartState::Instance());
			m_StateMachine->GetCurrentState()->Enter(GetThis<GameStage>());

			}
			catch (...){
				throw;
			}
	}
	
	//ゲーム初めのSEを鳴らすMotion関数
	void GameStage::GamePlaySEMotion(){
		auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
		pMultiSoundEffect->AddAudioResource(L"BGM");
		pMultiSoundEffect->Start(L"BGM", XAUDIO2_LOOP_INFINITE, 0.3f);
	}

	//CUBEが接触したときに流すMotion関数
	void GameStage::GameOverSEMotion(){
		auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
		pMultiSoundEffect->AddAudioResource(L"GameOver");
		pMultiSoundEffect->Start(L"GameOver", 0, 0.5f);
	}

	//ゲームをクリアした際にSEを鳴らすMotion関数
	void GameStage::GameClaerSEMotion(){
		auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
		pMultiSoundEffect->AddAudioResource(L"CUBEALLGOAL");
		pMultiSoundEffect->Start(L"CUBEALLGOAL", 0, 0.4f);
	}

	//ゲーム中のLookAtCameraに設定しておくMotion関数
	void GameStage::ToPlayerCameraMotion(){
		//マルチビューコンポーネントの取得
		auto PtrMultiView = GetComponent<MultiView>();
		auto TgtView = PtrMultiView->GetView(0);
		TgtView->SetCamera(m_LookAtCamera);
	}

	//ゲームをクリアした時、ゲームオーバーの時のカメラ切り替えるMotion関数
	void GameStage::ToMoveCameraMotion(){
		//マルチビューコンポーネントの取得
		auto PtrMultiView = GetComponent<MultiView>();
		auto TgtView = PtrMultiView->GetView(0);
		TgtView->SetCamera(m_MoveCamera);
		//これじゃま

	}

	//GameStageからResultへ移動するときに使うMotion関数
	bool GameStage::IsChangeScene(){
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();

			if (CntlVec[0].bConnected){
			//Aボタンが押された瞬間ならScene移動
			if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_A){
				//制限時間の更新
				App::GetApp()->GetScene<Scene>()->SetPastTime(m_Timeleft );
				//スコアの位の受け渡し
				App::GetApp()->GetScene<Scene>()->Set_100(m_TotalScoreItem);
				App::GetApp()->GetScene<Scene>()->Set_1000(m_1000Score);

				return true;
			}
		}
		return false;
	}

	//テスト
	bool GameStage::TestPush(){
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		if (CntlVec[0].bConnected){
			if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_A){
				return true;
			}
		}
		return false;
	}

	//ポーズ画面に行くときに使う
	bool GameStage::IsPushPause(){
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		if (CntlVec[0].bConnected){
			if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_START){
				PauseSprite_Active_true();
				m_Pause = true;
				Is_TimeStop = true;
				return true;
			}
		}
		return false;
	}

	//ポーズ画面の際、生成したオブジェクトで動かしたくない物にイベントでL"Pause"を送る
	void GameStage::PauseEvent(){
		//ポーズ等で使うオブジェクトの取得
		auto Pause_Group_Vec = GetSharedObjectGroup(L"PauseGroup")->GetGroupVector();
		for (auto wpPause : Pause_Group_Vec){
			auto Sh_Object = wpPause.lock();
			if (Sh_Object){
				PostEvent(0.0f, GetThis<GameStage>(), Sh_Object, L"Pause");		//ゲームオブジェクトグループ
			}
		}
	}

	//ポーズ画面の画像を表示する
	void GameStage::PauseSprite_Active_true(){
		//Shared_ptrからPauseSpriteを取得する
		auto Sh_PauseSprite = GetSharedGameObject<PauseSprite>(L"SH_PAUSE", false);
		//ポーズ画面の画像をActiveにする
		PostEvent(0.0f, GetThis<GameStage>(), Sh_PauseSprite, L"StartPause");

		//Shared_ptrからPauseSpriteを取得する
		auto Sh_PauseArrow = GetSharedGameObject<PauseArrow>(L"SH_ICON", false);
		//IDの初期化
		Sh_PauseArrow->Set_SelectID(PauseCase::BacktoGame);
		//位置の初期化
		Sh_PauseArrow->Back_base();
		//ポーズ画面の画像をActiveにする
		PostEvent(0.0f, GetThis<GameStage>(), Sh_PauseArrow, L"StartPause");
	}

	void GameStage::PauseSprite_Active_false(){
		//Shared_ptrからPauseSpriteを取得する
		auto Sh_PauseSprite = GetSharedGameObject<PauseSprite>(L"SH_PAUSE", false);
		//ポーズ画面の画像をfalseにする
		PostEvent(0.0f, GetThis<GameStage>(), Sh_PauseSprite, L"BackGame");

		//Shared_ptrからPauseSpriteを取得する
		auto Sh_PauseArrow = GetSharedGameObject<PauseArrow>(L"SH_ICON", false);
		//ポーズ画面の画像をActiveにする
		PostEvent(0.0f, GetThis<GameStage>(), Sh_PauseArrow, L"BackGame");
	}

	//ゲームスタート時に送る
	void GameStage::GamePlayEvent(){
		//ポーズ等で使うオブジェクトの取得
		auto Pause_Group_Vec = GetSharedObjectGroup(L"PauseGroup")->GetGroupVector();
		for (auto wpPause : Pause_Group_Vec){
			auto Sh_Object = wpPause.lock();
			if (Sh_Object){
				PostEvent(0.0f, GetThis<GameStage>(), Sh_Object, L"GamePlay");		//ゲームオブジェクトグループ
				Is_TimeStop = false;

			}
		}
	}

	//引数のイベントの名前でイベントを変える
	void GameStage::SomeEvent_Select(wstring EventName){
		auto Pause_Group_Vec = GetSharedObjectGroup(L"PauseGroup")->GetGroupVector();
		for (auto wpPause : Pause_Group_Vec){
			auto Sh_Object = wpPause.lock();
			if (Sh_Object){
				PostEvent(0.0f, GetThis<GameStage>(), Sh_Object, EventName);		//ゲームオブジェクトグループに送る
			}
		}
	}

	//チュートリアルをみたか、現在のステージがStage01か
	bool GameStage::IsTutorial_1(){
		//現在のステージが L" Stage01 "であるか
		bool IsStage01 = m_StageName == L"Stage_1";
		//チュートリアルを見たか
		//初期はFalseなので、見た後、Aボタンを押されたらTrueに変化する
		//m_TutorialLook;
		if (IsStage01 && !m_TutorialLook){
			return true;
		}
		else{
			return false;
		}
	}

	//コントローラーのLTRTを監視し、入力によってCubeの速度を変えるイベントを送る関数
	void GameStage::Spin_Controller_LTRT(){
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		if (CntlVec[0].bConnected){
			//トリガー系の入力
			BYTE LT = CntlVec[0].bLeftTrigger;
			BYTE RT = CntlVec[0].bRightTrigger;

			//右トリガー
			if (RT > 0 && LT == 0){
				SomeEvent_Select(L"RollUpSpeed");		//回転を上げるイベント
				Event_UP(L"YES");
				
			}

			//左トリガー
			if (RT == 0 && LT > 0){
				SomeEvent_Select(L"RollDownSpeed");		//回転を下げるイベント
				Event_DOWN(L"YES");
			}

			if (RT > 0 && LT > 0){
				SomeEvent_Select(L"DefaultSpeed");		//どちらも入力の場合、デフォルト
				Event_UP(L"NO");
				Event_DOWN(L"NO");

			}

			if (RT == 0 && LT == 0){
				SomeEvent_Select(L"DefaultSpeed");		//入力されてない場合、デフォルト
				Event_UP(L"NO");
				Event_DOWN(L"NO");
			}
		}
	}

	void GameStage::AButton_false(){
		auto Sh = GetSharedGameObject<AButton_UI>(L"AButton", false);
		PostEvent(0.0f, GetThis<GameStage>(), Sh, L"AButton_false");		
	}

	void GameStage::AButton_true(){
		auto Sh = GetSharedGameObject<AButton_UI>(L"AButton", false);
		PostEvent(0.0f, GetThis<GameStage>(), Sh, L"AButton_true");		

	}
	//イベントちゅるー
	void GameStage::Event_true(){
		auto Sh = GetSharedGameObject<Tutorial_UI>(L"TUTORIAL", false);
		PostEvent(0.0f, GetThis<GameStage>(), Sh, L"DrawUp_true");	

		auto LB = GetSharedGameObject<LB_UI>(L"LB", false);
		PostEvent(0.0f, GetThis<GameStage>(), LB, L"DrawUp_true");
		auto RB = GetSharedGameObject<RB_UI>(L"RB", false);
		PostEvent(0.0f, GetThis<GameStage>(), RB, L"DrawUp_true");
	}

	//イベントファルス
	void GameStage::Event_false(){
		auto Sh = GetSharedGameObject<Tutorial_UI>(L"TUTORIAL", false);
		PostEvent(0.0f, GetThis<GameStage>(), Sh, L"DrawUp_false");	

		auto LB = GetSharedGameObject<LB_UI>(L"LB", false);
		PostEvent(0.0f, GetThis<GameStage>(), LB, L"DrawUp_false");
		auto RB = GetSharedGameObject<RB_UI>(L"RB", false);
		PostEvent(0.0f, GetThis<GameStage>(), RB, L"DrawUp_false");
	}

	void GameStage::Tutorial_Sp(bool Look){
		//チュートリアルを見たことにする
		m_TutorialLook = Look;
	}

	void GameStage::Roll_false(){
		auto Sh_Up = GetSharedGameObject<Roll_UI>(L"ROLLUP_SP", false);
		PostEvent(0.0f, GetThis<GameStage>(), Sh_Up, L"DrawUp_false");		

		auto Sh_Down = GetSharedGameObject<Roll_UI>(L"ROLLDOWN_SP", false);
		PostEvent(0.0f, GetThis<GameStage>(), Sh_Down, L"DrawUp_false");		
	}

	void GameStage::Roll_true(){
		auto Sh_Up = GetSharedGameObject<Roll_UI>(L"ROLLUP_SP", false);
		PostEvent(0.0f, GetThis<GameStage>(), Sh_Up, L"DrawUp_true");		

		auto Sh_Down = GetSharedGameObject<Roll_UI>(L"ROLLDOWN_SP", false);
		PostEvent(0.0f, GetThis<GameStage>(), Sh_Down, L"DrawUp_true");	
	}

	void GameStage::Event_UP(wstring event){
		auto Sh = GetSharedGameObject<Roll_UI>(L"ROLLUP_SP", false);
		PostEvent(0.0f, GetThis<GameStage>(), Sh, event );	
	}

	void GameStage::Event_DOWN(wstring event){
		auto Sh = GetSharedGameObject<Roll_UI>(L"ROLLDOWN_SP", false);
		PostEvent(0.0f, GetThis<GameStage>(), Sh, event );		
	}

	bool GameStage::Is_Pause(){
		if (m_Pause){
			return true;
		}
		else
			return false;
	}

	//チュートリアル01を見ているときの処理をまとめる
	void GameStage::Tutorial_01(){
		//プレイヤーなどを動かなくする
		PauseEvent();
		//ゲーム画面下方にあるSpeedUp　Down関連を非表示にする
		Roll_false();

		//画像のオブジェクトはTutorial_UIのUpdateでLB RBで反応して動くようにする
		Event_true();
	}

	//チュートリアル01を見た後の処理をまとめる
	void GameStage::Tutorial_01_Look(){
		//ゲーム画面下方にあるSpeedUp　Down関連を表示する
		Roll_true();
		//チュートリアルの非表示
		Event_false();
		//GameStageのプレイヤーなどを動かせるようにイベント送信
		GamePlayEvent();
		//チュートリアルを再度表示しないようにBool値を変更する
		Tutorial_Sp(true);
	}

	//--------------------------------------------------------------------------------------
	//	class GameStartState : public ObjState<GameStage>;
	//	用途: PlayerやCUBEが動いてるときのステート
	//--------------------------------------------------------------------------------------
	//ステートのインスタンス取得
	shared_ptr<GameStartState> GameStartState::Instance(){
		static shared_ptr<GameStartState> instance;
		if (!instance){
			instance = shared_ptr<GameStartState>(new GameStartState);
		}
		return instance;
	}
	//ステートに入ったときに呼ばれる関数
	void GameStartState::Enter(const shared_ptr<GameStage>& Obj){
		Obj->ToMoveCameraMotion();
		Obj->Event_false();
		Obj->Roll_false();
	}
	//ステート実行中に毎ターン呼ばれる関数
	void GameStartState::Execute(const shared_ptr<GameStage>& Obj){
		Obj->PauseEvent();
		Obj->AButton_true();
		if (Obj->TestPush()){
			Obj->AButton_false();
			Obj->GamePlayEvent();
			Obj->GetStateMachine()->ChangeState(GamePlayState::Instance());
		}
	}
	//ステートにから抜けるときに呼ばれる関数
	void GameStartState::Exit(const shared_ptr<GameStage>& Obj){
		//何もしない
	}


	//--------------------------------------------------------------------------------------
	//	class GamePlayState : public ObjState<GameStage>;
	//	用途: PlayerやCUBEが動いてるときのステート
	//--------------------------------------------------------------------------------------
	//ステートのインスタンス取得
	shared_ptr<GamePlayState> GamePlayState::Instance(){
		static shared_ptr<GamePlayState> instance;
		if (!instance){
			instance = shared_ptr<GamePlayState>(new GamePlayState);
		}
		return instance;
	}

	//ステートに入ったときに呼ばれる関数
	void GamePlayState::Enter(const shared_ptr<GameStage>& Obj){

		//フェードインさせる
		auto FadePtr = Obj->GetSharedGameObject<FadePanel>(L"FadePanel");
		if (FadePtr){
			Obj->PostEvent(0.0f, Obj->GetThis<GameStage>(), FadePtr, L"FadeIn");
		}

		Obj->GamePlaySEMotion();
		Obj->ToPlayerCameraMotion();
		Obj->StartTimer();
		Obj->Timeleft();
	
		//TODO : これよくわかんない
		Color4 color(1, 1, 1, 1);
		Obj->TimeNumber(60.0f , color, true);
	}

	//ステート実行中に毎ターン呼ばれる関数
	void GamePlayState::Execute(const shared_ptr<GameStage>& Obj){

		//初期はfalseだから下が呼ばれる ステージ１なら基本操作の画像
		if (Obj->IsTutorial_1()){
			//チュートリアル０１の画像の表示
			Obj->Tutorial_01();
			//Aボタンを押されたらチュートリアルの画像を非表示にして　m_TutorialLookをtrueにする
			if (Obj->TestPush()){
				Obj->Tutorial_01_Look();
			}
		}
		//ここにチュートリアルの画像をいれる
		else if (0){

		}
		else if (0){

		}
		else if (0){
			
		}
		else{

			//ポーズにしている
			if (Obj->Is_Pause()){
				Obj->Roll_false();
			}
			else{
				//ポーズにしていない
				Obj->Roll_true();
			}

			//Pause画面へいくButtonの検出
			if (Obj->IsPushPause()){
				Obj->PauseEvent();
			}

			//ポーズ画面に移行したとき制限時間のストップ
			auto TimeStop = Obj->GetTimeStop();
			if (!TimeStop){
				Obj->Time();
			}

			//コントローラーのLTRTを取得し、振る舞いを変化させるイベントを送る
			Obj->Spin_Controller_LTRT();
		}
	}
	//ステートにから抜けるときに呼ばれる関数
	void GamePlayState::Exit(const shared_ptr<GameStage>& Obj){

	}

	//--------------------------------------------------------------------------------------
	//	class GameClearState : public ObjState<GameStage>;
	//	用途: ゲームクリアでCUBEが綺麗にゴールしたことをカメラで伝える
	//--------------------------------------------------------------------------------------
	//ステートのインスタンス取得
	shared_ptr<GameClearState> GameClearState::Instance(){
		static shared_ptr<GameClearState> instance;
		if (!instance){
			instance = shared_ptr<GameClearState>(new GameClearState);
		}
		return instance;
	}
	//ステートに入ったときに呼ばれる関数
	void GameClearState::Enter(const shared_ptr<GameStage>& Obj){
		Obj->GameClaerSEMotion();
		Obj->Roll_false();

		

	}
	//ステート実行中に毎ターン呼ばれる関数
	void GameClearState::Execute(const shared_ptr<GameStage>& Obj){
		Obj->ToMoveCameraMotion();
		//直に書いてますごめんなさい
		Obj->AddGameObject<SpriteObject>(Vector3(18.0f, -10.0f, 0.0f), Vector3(15.0f, 5.0f, 5.0f), L"ABUTTON_TX");


		if (Obj->IsChangeScene()){
			//フェードアウトさせる
			auto FadePtr = Obj->GetSharedGameObject<FadePanel>(L"FadePanel");
			if (FadePtr){
				Obj->PostEvent(0.0f, Obj->GetThis<GameStage>(), FadePtr, L"FadeOut");
			}


			//Obj->PostEvent(0.0f, Obj->GetThis<GameStage>(), App::GetApp()->GetSceneBase(), L"ToGameClear");
		}
		auto FadePtr = Obj->GetSharedGameObject<FadePanel>(L"FadePanel");
		if (FadePtr){
			if (FadePtr->IsFadeOutActive() == false && FadePtr->IsFadeOutBeforeActive() == true){
				Obj->PostEvent(0.0f, Obj->GetThis<GameStage>(), App::GetApp()->GetSceneBase(), L"ToGameClear");
				auto pMultiSoundEffect = Obj->GetComponent<MultiSoundEffect>();
				pMultiSoundEffect->Stop(L"BGM");
			}
		}


	}
	//ステートにから抜けるときに呼ばれる関数
	void GameClearState::Exit(const shared_ptr<GameStage>& Obj){
		//何もしない
	}

	//--------------------------------------------------------------------------------------
	//	class GameOverState : public ObjState<GameStage>;
	//	用途: ゲームオーバーになりカメラでどこがダメだったかを見せる
	//--------------------------------------------------------------------------------------
	//ステートのインスタンス取得
	shared_ptr<GameOverState> GameOverState::Instance(){
		static shared_ptr<GameOverState> instance;
		if (!instance){
			instance = shared_ptr<GameOverState>(new GameOverState);
		}
		return instance;
	}
	//ステートに入ったときに呼ばれる関数
	void GameOverState::Enter(const shared_ptr<GameStage>& Obj){
		Obj->GameOverSEMotion();
		//直に書いてますごめんなさい
		Obj->AddGameObject<SpriteObject>(Vector3(18.0f, -10.0f, 0.0f), Vector3(15.0f, 5.0f, 5.0f), L"ARETRY_TX");
		Obj->Roll_false();
	}
	//ステート実行中に毎ターン呼ばれる関数
	void GameOverState::Execute(const shared_ptr<GameStage>& Obj){
		Color4 color(1, 1, 1, 1);
		Obj->TimeNumber(0.0f, color, true);

		//リザルトに行く処理やる(｀・ω・´)
		if (Obj->IsChangeScene()){
			//フェードアウトさせる
			auto FadePtr = Obj->GetSharedGameObject<FadePanel>(L"FadePanel");
			if (FadePtr){
				Obj->PostEvent(0.0f, Obj->GetThis<GameStage>(), FadePtr, L"FadeOut");
			}
		}
		auto FadePtr = Obj->GetSharedGameObject<FadePanel>(L"FadePanel");
		if (FadePtr){
			if (FadePtr->IsFadeOutActive() == false && FadePtr->IsFadeOutBeforeActive() == true){
				Obj->PostEvent(0.0f, Obj->GetThis<GameStage>(), App::GetApp()->GetSceneBase(), L"ToGame");
				auto pMultiSoundEffect = Obj->GetComponent<MultiSoundEffect>();
				pMultiSoundEffect->Stop(L"BGM");
			}
		}
	}
	//ステートにから抜けるときに呼ばれる関数
	void GameOverState::Exit(const shared_ptr<GameStage>& Obj){
		//何もしない
	}

	
	//--------------------------------------------------------------------------------------
	//	class GamePauseState : public ObjState<GameStage>;
	//	用途: リトライやセレクトに戻るときに使うState
	//--------------------------------------------------------------------------------------

	//ステートのインスタンス取得
	shared_ptr<GamePauseState> GamePauseState::Instance(){
		static shared_ptr<GamePauseState> instance;
		if (!instance){
			instance = shared_ptr<GamePauseState>(new GamePauseState);
		}
		return instance;
	}
	//ステートに入ったときに呼ばれる関数
	void GamePauseState::Enter(const shared_ptr<GameStage>& Obj){
		//ポーズ画面中の画像は配置

	}
	//ステート実行中に毎ターン呼ばれる関数
	void GamePauseState::Execute(const shared_ptr<GameStage>& Obj){
		//コントローラーの入力を取得する
		//0 ~ 2までの値でスティックで反応
		//0 ~ 2でアイコンを動かして	0ならゲームに戻る、　1ならリトライ、 2はステージセレクトに戻る

	}
	//ステートにから抜けるときに呼ばれる関数
	void GamePauseState::Exit(const shared_ptr<GameStage>& Obj){
		//生成した４つのゲームオブジェクトを非表示にしてUpdateをfalseにする
		//ポーズ中に使っていた値を初期化しておく
	}

}
//endof  basedx11
