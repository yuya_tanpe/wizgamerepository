#pragma once

#include "stdafx.h"

namespace basedx11{

	
	class ArrowObject;
	class MoveBox;

	//--------------------------------------------------------------------------------------
	//	class GameStage : public Stage;
	//	用途: ゲームステージクラス
	//--------------------------------------------------------------------------------------
	class GameStage : public Stage{

		//ブロック作成
		template<typename T, typename... Ts>
		shared_ptr<T> CreateBlock(Ts&&... params){
			shared_ptr<T> Ptr = AddGameObject<T>(params...);
			//呼び出したい関数
			//Ptr->SetStage(GetThis<GameStage>());
			return Ptr;
		}

	public:
		//構築と破棄
		GameStage() :Stage(), m_TutorialLook(false), m_Pause(false), m_TotalScoreItem(0), m_1000Score(0){}
		virtual ~GameStage(){}

		//初期化
		virtual void Create()override;
		//ステート切り替えでUpdate使用中
		virtual void Update()override;





		// ---------- Accessor ----------.

		//Csvの行を取得
		const size_t GetRow() const{
			return m_sRowSize;
		}

		//Csvの列を取得
		const size_t GetCol() const{
			return m_sColSize;
		}

		//引数から選択したセルのデータを取得する
		const int GetMapData(size_t row, size_t col) const{
			return m_MapDataVec[col][row];
		}

		//マップデータを設定する 2015/12/05作成
		void SetMapData(size_t row, size_t col, size_t enumMapDate){
			m_MapDataVec[col][row] = enumMapDate;
		}

		//マップデータサイズを取得する
		const Vector2 GetMapSize() const{
			return Vector2((float)m_sRowSize, (float)m_sColSize);
		}

		//プレイヤーを生成するCell座標を取得する
		const Vector2 GetPlayerSpawnCell() const{
			return m_vPlayerSpawnCell;
		}

		//マップデータから通れるか
		const bool GetNextCheckMapData(const size_t row, const size_t col) const {
			if (GetMapData(row, col) >= DataID::ROAD){
				return true;
			}
			return false;
		}

		//マップデータの最大最低からいかないようにする
		const bool GetCheckToDataMap(const int i_PlayerPosX, const int i_PlayerPosZ) const{
			bool OverCol = GetCol() > (size_t)i_PlayerPosX;
			bool OverRow = GetRow() > (size_t)i_PlayerPosZ;
			bool LowRow = i_PlayerPosX >= 0;
			bool LowCol = i_PlayerPosZ >= 0;

			if (OverRow && OverCol && LowRow && LowCol){
				return true;
			}

			return false;
		}

		//ゴールしたか調べる
		const bool CheckGoalMapData(const size_t row, const size_t col, const size_t i_enamGoalName){
			if (GetMapData(col, row) == i_enamGoalName){
				return true;
			}
			return false;
		}

		//ArrowObjectVecを返す
		vector< vector< weak_ptr<ArrowObject> > >& GetArrowVec(){
			return m_ArrowObjectVec;
		}
		//アクセサ：フェイズごとのタイマー関係
		void StartTimer(){
			m_Timer = 0;
		}
		float UpdateTimer(){
			m_Timer += App::GetApp()->GetElapsedTime();
			return m_Timer;
		}
		//時間の取得
		const float GetTimer(){
			return m_Timer;
		}

		void Timeleft(){
			m_Timeleft = 60.0f;
		}
		float UpdateTimerleft(){
			m_Timeleft -= App::GetApp()->GetElapsedTime();
			return m_Timeleft;
		}
		const float GetTimeleft(){
			return m_Timeleft;
		}
		const bool GetTimeStop(){
			return Is_TimeStop;
		}
		const bool GetUIDelet(){
			return Is_UIDelet;
		}

		//フィール上のアイテムなどの取得
		const ItemCase GetFixedMat(size_t row, size_t col){
			return m_FixedMatVec[row][col];
		}
		//フィールド上のアイテムの設定
		void SetItemObjsize_t (size_t row, size_t col, ItemCase Type){
			m_FixedMatVec[row][col] = Type;
		}

		//設定
		void SetPause(bool Pause){
			m_Pause = Pause;
		}

		//ステートの取得
		shared_ptr< StateMachine<GameStage> > GetStateMachine() const{
			return m_StateMachine;
		}

		//テスト
		const weak_ptr<MoveBox> GetMoveBox(){
			return m_wpMove;
		}



		// ---------- Motion ----------.	
		/* ※ステートは多くなることを考えMotionを作る際、どこのStateで、どんなMotion関数になるかの記載を義務付け */

		void TimeNumber(float time,Color4 color, bool IsShow);
		void Time();

		void GameUI();

		void GamePlaySEMotion();
		void GameOverSEMotion();		//ゲームオーバー時に流すSE	
		void GameClaerSEMotion();		//ゲームクリア時に流すSE
		void ToPlayerCameraMotion();	//ゲーム中のカメラ
		void ToMoveCameraMotion();		//ゲーム以外の時
		void Fade();
		bool IsChangeScene();			//ぷっす
		bool TestPush();				//なんだっけ

		void SomeEvent_Select(wstring EventName);	//引数のイベントの名前でイベントを変える

		//---　ポーズ画面等で使用 ---
		bool IsPushPause();				//ポーズ画面にいくときに使うStartButton押されたら
		void PauseEvent();				//ポーズ画面の際、生成したオブジェクトで動かしたくない物にイベントでL"Pause"を送る
		
		void GamePlayEvent();			//ポーズ画面の際、生成したオブジェクトで動かしたい物にイベントでL"GamePlay"を送る
		void PauseSprite_Active_true();	//ポーズ画面を表示する
		void PauseSprite_Active_false();//ポーズ画面の非表示にする

		//--- Cubeの回転速度を変更するのに仕様
		void Spin_Controller_LTRT();

		//デバッグ表示
		void DebugLog();

		//チュートリアルを見たか、Stage01かを識別する
		bool IsTutorial_1();
		//チュートリアル01を見ているときの処理をまとめる
		void Tutorial_01();
		//チュートリアル01を見た後の処理をまとめる
		void Tutorial_01_Look();


		void Tutorial_Sp(bool look);

		//イベント
		void Event_false();
		void Event_true();

		void AButton_false();
		void AButton_true();

		//回転画像	 描画関係
		void Roll_false();
		void Roll_true();

		void Event_UP(wstring event);
		void Event_DOWN(wstring event);

		//ポーズであるか、ステージ１移行に必要
		bool Is_Pause();

		//スコアの数字を変える
		void ScoreUp(size_t Score);

	


	private:
		// ---------- Method ----------.
		//リソースの作成
		void CreateResourses();
		//ビューの作成
		void CreateViews();
		//グループの作成
		void CreateGruop();
		//背景
		void CreateBackGround();
		//CSVからボックス作成　どうせ処理重くなる
		void CreateRoad();
		//CSVからアイテムなどのオブジェクトの作成
		void CreateStageObject();
		//Stageの名前をCsvから取得
		wstring Change_Stage(size_t number);
		//プレイヤーの作成
		void CreatePlayer(Vector2 i_vCellPos);
		//Playerスポーンボックスの生成
		void CreatePlayerBox(const Vector3 i_Scale, const Vector3 i_Pos);
		//敵の追加
		void CreateEnamy();
		//カメラの作成
		void CreateCamera();
		//エフェクト
		void CreateEffect();

		void CreateTimeSprite();

		void CreateFixedMat(size_t number);

		void SelectVector(int Nember, const Vector2 selectvec);

		void CreateScoreUI();


		//void CreateMoveEffect();
		//void CreatePlayerEffect();
		//void CreateArrowContactEffect();

		void Create_MarkMatSprite();

		void CreateStageManager();		//完成はCsvからステージのゴール数をもらってくる
		//テスト
		void CreateSprite();

		void CreateUI();
		

		// ---------- member ----------.

		//プレイヤーを生成するCell座標
		Vector2 m_vPlayerSpawnCell;		

		//ステージ生成に必要なメンバ変数
		size_t m_sColSize;
		size_t m_sRowSize;
		//ワーク用のベクター配列のメンバ変数
		vector< vector <size_t> > m_MapDataVec;

		//クリアするためのゴール数
		size_t m_GoalSize;
		//ワーク用ArrowObject配列のメンバ変数
		vector< vector< weak_ptr<ArrowObject> > > m_ArrowObjectVec;
		//固定マット用のObject配列
		vector< vector<ItemCase> > m_FixedMatVec;

		//wstringと enum classを結びつける
		map<wstring, ItemCase> m_ItemMap;

		//ステート
		shared_ptr< StateMachine<GameStage> > m_StateMachine;
		
		//プレイヤーを追うカメラの格納場所
		shared_ptr<Camera> m_LookAtCamera;
		//ゲームオーバー、ゲームクリアにつかう動くカメラ
		shared_ptr<Camera> m_MoveCamera;

		//TODO : これ使い道あるの？
		//一度GameStartStateが開かれたか
		bool Is_GameStart = false;
		//一度ModeStateを開いていたか
		bool Is_ModeSelect = false;

		bool Is_TimeStop = false;

		bool Is_UIDelet = false;
		

		//過ぎた時間
		float m_Timer;
		//残り時間
		float m_Timeleft;

		//Manager.csvからそれぞれのCOlorCUBEの向きの取得	

		Vector2 BlueVec;
		Vector2 RedVec;
		Vector2 PurpleVec;
		Vector2 GreenVec;
		
		//テスト
		weak_ptr<MoveBox> m_wpMove;

		//カメラのなんか
		Vector3 m_CameraSetAt;

		//現在のステージ名
		wstring m_StageName;

		//チュートリアルを見たか
		bool m_TutorialLook;

		//バグよけ
		bool m_Pause;

		//スコアアイテムの合計
		size_t m_TotalScoreItem;

		//100の値
	//	size_t m_100Score;
		//1000の値
		size_t m_1000Score;

		//ステージの色を格納する
		wstring m_StageColor;
		map<wstring, wstring>m_ChangeBoxNameMap;
		
	};

	//--------------------------------------------------------------------------------------
	//	class GameStartState : public ObjState<GameStage>;
	//	用途: PlayerやCUBEが動いてるときのステート
	//--------------------------------------------------------------------------------------
	class GameStartState : public ObjState<GameStage>
	{
		GameStartState(){}
	public:
		//ステートのインスタンス取得
		static shared_ptr<GameStartState> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<GameStage>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<GameStage>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<GameStage>& Obj)override;
	};

	//--------------------------------------------------------------------------------------
	//	class GamePlayState : public ObjState<GameStage>;
	//	用途: PlayerやCUBEが動いてるときのステート
	//--------------------------------------------------------------------------------------
	class GamePlayState : public ObjState<GameStage>
	{
		GamePlayState(){}
	public:
		//ステートのインスタンス取得
		static shared_ptr<GamePlayState> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<GameStage>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<GameStage>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<GameStage>& Obj)override;
	};

	//--------------------------------------------------------------------------------------
	//	class GameClearState : public ObjState<GameStage>;
	//	用途: ゲームクリアでCUBEが綺麗にゴールしたことをカメラで伝える
	//--------------------------------------------------------------------------------------
	class GameClearState : public ObjState<GameStage>
	{
		GameClearState(){}
	public:
		//ステートのインスタンス取得
		static shared_ptr<GameClearState> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<GameStage>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<GameStage>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<GameStage>& Obj)override;
	};
	
	//--------------------------------------------------------------------------------------
	//	class GameOverState : public ObjState<GameStage>;
	//	用途: ゲームオーバーになりカメラでどこがダメだったかを見せる
	//--------------------------------------------------------------------------------------
	class GameOverState : public ObjState<GameStage>
	{
		GameOverState(){}
	public:
		//ステートのインスタンス取得
		static shared_ptr<GameOverState> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<GameStage>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<GameStage>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<GameStage>& Obj)override;
	};

	//--------------------------------------------------------------------------------------
	//	class GamePauseState : public ObjState<GameStage>;
	//	用途: リトライやセレクトに戻るときに使うState
	//--------------------------------------------------------------------------------------
	class GamePauseState : public ObjState<GameStage>
	{
		GamePauseState(){}
	public:
		//ステートのインスタンス取得
		static shared_ptr<GamePauseState> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<GameStage>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<GameStage>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<GameStage>& Obj)override;
	};


	
}
//endof  basedx11
