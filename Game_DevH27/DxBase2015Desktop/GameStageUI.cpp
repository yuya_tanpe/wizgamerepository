
#include "stdafx.h"
#include "Project.h"


namespace basedx11{


	/**
	* @class　Pause画面のSprite　親クラス
	* @brief　GamePauseStateで表示
	* @author yuya sike
	* @date 2015/12/08 Start
	*/
	PauseSprite::PauseSprite(const shared_ptr<Stage>& StagePtr, const Vector3& i_Start_Pos) :
		GameObject(StagePtr),
		m_StartPos(i_Start_Pos)
	{}

	//生成
	void PauseSprite::Create(){
		//Transformの取得
		auto PtrTrans = GetComponent<Transform>();
		PtrTrans->SetPosition(m_StartPos);
		PtrTrans->SetRotation(0, 0, 0);
		PtrTrans->SetScale(28.0f, 16.0f, 1.0f);

		auto PtrSprite = AddComponent<Sprite>(Color4(1.0f, 1.0f, 1.0f, 1.0f));
		PtrSprite->SetTextureResource(L"PAUSE_TX");
		PtrSprite->SetPixelParMeter(32.0f);
		PtrSprite->SetCoordinate(Sprite::Coordinate::m_CenterZeroPlusUpY);
		PtrSprite->SetDrawActive(false);

		SetAlphaActive(true);
		SetAlphaExActive(true);

		
	}

	void PauseSprite::OnEvent(const shared_ptr<Event>& event){

			//ポーズ画面を呼ばれたとき
			if (event->m_MsgStr == L"StartPause"){

				auto PtrSprite = GetComponent<Sprite>();
				PtrSprite->SetDrawActive(true);
			}

			//ゲーム画面に戻るとき
			else if (event->m_MsgStr == L"BackGame"){
				auto PtrSprite = GetComponent<Sprite>();

				PtrSprite->SetDrawActive(false);
			}
	}

	/**
	* @class　Pause画面のSprite　矢印
	* @author yuya sike
	* @date 2015/12/20 Start
	*/
	PauseArrow::PauseArrow(const shared_ptr<Stage>& StagePtr, const Vector3& i_Start_Pos) : 
		GameObject(StagePtr),
		m_Arrow_Position(i_Start_Pos),
		m_Stick(false),
		m_SelectUI(PauseCase::None)
	{}

	//生成
	void PauseArrow::Create(){
		//Transformの取得
		auto PtrTrans = GetComponent<Transform>();
		PtrTrans->SetPosition(m_Arrow_Position);
		PtrTrans->SetRotation(0, 0, 0);
		PtrTrans->SetScale(3.0f, 3.0f, 1.0f);
		auto PtrSprite = AddComponent<Sprite>(Color4(1.0f, 1.0f, 1.0f, 1.0f));
		PtrSprite->SetTextureResource(L"PAUSEICON_TX");
		PtrSprite->SetPixelParMeter(32.0f);
		PtrSprite->SetCoordinate(Sprite::Coordinate::m_CenterZeroPlusUpY);
		PtrSprite->SetDrawActive(false);

		//エフェクトの追加
		auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
		pMultiSoundEffect->AddAudioResource(L"PAUSESE");
		pMultiSoundEffect->AddAudioResource(L"PAUSEOK");

		SetAlphaActive(true);
		SetAlphaExActive(true);

		//m_SelectUI = PauseCase::BacktoGame;
		SetUpdateActive(false);

	}

	//変化
	void PauseArrow::Update(){
		//ゲームステージの取得
		auto GamePtr = dynamic_pointer_cast<GameStage>(GetStage());
		//コントローラの取得
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		//接続されているか
		if (CntlVec[0].bConnected){

			if (GamePtr->GetStateMachine()->GetCurrentState() == GamePlayState::Instance()){

				//事前準備
				bool Min = m_SelectUI > PauseCase::BacktoGame;
				bool Max = m_SelectUI < PauseCase::MovetoSelectStage;
				//スティックが傾いていない時など
				if (CntlVec[0].fThumbLX == 0.0f && CntlVec[0].fThumbLY == 0.0f){
					//スティック操作をまだしてない
					m_Stick = false;
				}

				//まだ入力されてない場合、通る
				else if (m_Stick == false){
					//上入力
					if (CntlVec[0].fThumbLY > 0.85f){
						if (Min){

							m_SelectUI--;	//選択数を引く
							m_Stick = true;	//スティック入力を受け付けた

							//SEを鳴らす
							auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
							pMultiSoundEffect->Start(L"PAUSESE", 0, 0.1f);

							//アイコンを１項目上に移動させる
							auto Trans = GetComponent<Transform>();
							auto Pos = Trans->GetPosition();
							Pos.y += 4.5f;
							Trans->SetPosition(Pos);
						}
					}

					//下入力
					else if (CntlVec[0].fThumbLY < -0.85f){
						if (Max){
							m_SelectUI++;	//選択数を引く
							m_Stick = true;	//スティック入力を受け付けた


							//SEを鳴らす
							auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
							pMultiSoundEffect->Start(L"PAUSESE", 0, 0.1f);

							//アイコンを１項目下に移動させる
							auto Trans = GetComponent<Transform>();
							auto Pos = Trans->GetPosition();
							Pos.y -= 4.5f;
							Trans->SetPosition(Pos);
						}
					}

				}

				//Aボタンを押されたとき
				if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_A){
					switch (m_SelectUI)
					{
					case PauseCase::BacktoGame:
						//SEを鳴らす
						SE_OK();
						//ゲームを再開する
						Back_to_Game();
						break;
					case PauseCase::RestartGame:
						//SEを鳴らす
						SE_OK();
						//ステージを再読み込みする
						BGMStop();
						PostEvent(0.0f, GetThis<PauseArrow>(), App::GetApp()->GetSceneBase(), L"ToGame");
						break;
					case PauseCase::MovetoSelectStage:
						//SEを鳴らす
						SE_OK();
						//モードセレクトに移動する
						BGMStop();
						PostEvent(0.0f, GetThis<PauseArrow>(), App::GetApp()->GetSceneBase(), L"ToStageSelect");
						//BGMのスタート		
						App::GetApp()->GetScene<Scene>()->StartBGM();
						break;
					case PauseCase::None:
						//なにもにゅうろくない
						break;
					default:
						assert(!"不正な値が入りました。m_SelectUIの値を確認してください。");
						break;
					}
				}
			}

		}
	}

	//SEを鳴らす
	void PauseArrow::SE_OK(){
		//SEを鳴らす
		auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
		pMultiSoundEffect->Start(L"PAUSEOK", 0, 0.3f);
	}

	//アイコンの位置を元に戻す
	void PauseArrow::Back_base(){
		m_SelectUI = PauseCase::BacktoGame;
		auto Trans = GetComponent<Transform>();
		//初期位置
		Vector3 BasePos{ -10.0f, 5.0f, 0.0f };
		Trans->SetPosition(BasePos);
	}

	//BGMを止める
	void PauseArrow::BGMStop(){
		auto PtrGameStage = dynamic_pointer_cast<GameStage>(GetStage());
		auto PtrSE = PtrGameStage->GetComponent<MultiSoundEffect>();
		PtrSE->Stop(L"BGM");
	}

	//ゲームに戻る
	void PauseArrow::Back_to_Game(){
		auto GameStagePtr = dynamic_pointer_cast<GameStage>(GetStage());
		//ポーズ画面の非表示
		GameStagePtr->PauseSprite_Active_false();
		//オブジェクトのUpdateをActiveに
		GameStagePtr->GamePlayEvent();
		GameStagePtr->Roll_true();
		GameStagePtr->SetPause(false);
	}

	void PauseArrow::OnEvent(const shared_ptr<Event>& event){

		//ポーズ画面を呼ばれたとき
		if (event->m_MsgStr == L"StartPause"){
			auto PtrSprite = GetComponent<Sprite>();
			PtrSprite->SetDrawActive(true);
			//Updateの更新を開始
			SetUpdateActive(true);
		}

		//ゲーム画面に戻るとき
		else if (event->m_MsgStr == L"BackGame"){
			auto PtrSprite = GetComponent<Sprite>();
			PtrSprite->SetDrawActive(false);
			//Updateの更新を停止
			SetUpdateActive(false);
			Back_base();
		}
	}

	/**
	* @class　Tutorial_UI　
	* @author yuya sike
	* @date 2016/01/21 Start
	*/

	Tutorial_UI::Tutorial_UI(const shared_ptr<Stage>& StagePtr) :
		GameObject(StagePtr),
		m_Texture(4),
		m_point(0)
	{
	}
	
	//作成
	void Tutorial_UI::Create(){

		//テクスチャの名前
		m_Texture[0] = L"TUTORIAL01";
		m_Texture[1] = L"TUTORIAL02";
		m_Texture[2] = L"TUTORIAL03";
		m_Texture[3] = L"TUTORIAL04";

		//Transformの取得
		auto PtrTrans = GetComponent<Transform>();
		PtrTrans->SetPosition(0.0f, -2.0f, 0);
		PtrTrans->SetRotation(0,0,0);
		PtrTrans->SetScale(32.0f,19.0f, 1.0f);
		auto PtrSprite = AddComponent<Sprite>(Color4(1.0f, 1.0f, 1.0f, 1.0f));
		PtrSprite->SetTextureResource(m_Texture[m_point]);
		PtrSprite->SetPixelParMeter(32.0f);
		PtrSprite->SetCoordinate(Sprite::Coordinate::m_CenterZeroPlusUpY);
		SetAlphaActive(true);							//透明処理

		/*SetDrawActive(false);
		SetUpdateActive(false);*/

		
	}

	//変化
	void Tutorial_UI::Update(){
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		if (CntlVec[0].bConnected){
			if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_RIGHT_SHOULDER){
				//右に移動
				m_point++;
				if (m_point == 4){
					m_point = 0;
				}
				//スプライトの変更
				auto PtrSprite = GetComponent<Sprite>();
				PtrSprite->SetTextureResource(m_Texture[m_point]);
			}
			if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_LEFT_SHOULDER){
				//右に移動
				m_point--;
				if (m_point == -1){
					m_point = 3;
				}
				//スプライトの変更
				auto PtrSprite = GetComponent<Sprite>();
				PtrSprite->SetTextureResource(m_Texture[m_point]);
			}
		}
	}

	void Tutorial_UI::OnEvent(const shared_ptr<Event>& event){

		if (event->m_MsgStr == L"DrawUp_false"){
			//非表示
			auto PtrSprite = GetComponent<Sprite>();
			PtrSprite->SetDrawActive(false);
			SetUpdateActive(false);

		}
		else if (event->m_MsgStr == L"DrawUp_true"){
			//表示
			auto PtrSprite = GetComponent<Sprite>();
			PtrSprite->SetDrawActive(true);
			SetUpdateActive(true);

		}
	}

	/**
	* @class　RollUp_UI　
	* @author yuya sike
	* @date 2016/01/21 Start
	//@yuya明日締め切りだから汚いよ
	*/

	Roll_UI::Roll_UI(const shared_ptr<Stage>& StagePtr, const wstring& TextureNOT, const wstring& TextureYES , const Vector3 Pos):
		GameObject(StagePtr),
		m_Texture_NOT(TextureNOT),
		m_Texture_YES(TextureYES),
		m_Pos(Pos)
	{}


	void Roll_UI::Create(){

		//Transformの取得
		auto PtrTrans = GetComponent<Transform>();
		PtrTrans->SetPosition(m_Pos);
		PtrTrans->SetRotation(0, 0, 0);
		PtrTrans->SetScale(12.0f, 5.5f, 1.0f);
		auto PtrSprite = AddComponent<Sprite>(Color4(1.0f, 1.0f, 1.0f, 1.0f));
		PtrSprite->SetTextureResource(m_Texture_NOT);
		PtrSprite->SetPixelParMeter(32.0f);
		PtrSprite->SetCoordinate(Sprite::Coordinate::m_CenterZeroPlusUpY);
		SetAlphaActive(true);							//透明処理

	}

	void Roll_UI::OnEvent(const shared_ptr<Event>& event){

		if (event->m_MsgStr == L"DrawUp_false"){
			//非表示
			auto PtrSprite = GetComponent<Sprite>();
			PtrSprite->SetDrawActive(false);
			SetUpdateActive(false);

		}

		if (event->m_MsgStr == L"DrawUp_true"){
			//表示
			auto PtrSprite = GetComponent<Sprite>();
			PtrSprite->SetDrawActive(true);
			SetUpdateActive(true);

		}

		//色を強くした画像にする
		if (event->m_MsgStr == L"YES"){
			//スプライトの変更
			auto PtrSprite = GetComponent<Sprite>();
			PtrSprite->SetTextureResource(m_Texture_YES);
		}
		//元の画像にする
		if (event->m_MsgStr == L"NO"){
			//スプライトの変更
			auto PtrSprite = GetComponent<Sprite>();
			PtrSprite->SetTextureResource(m_Texture_NOT);
		}
		
	}
	/**
	* @class　AButton_UI　
	* @author 菅野　翔吾
	*/
	AButton_UI::AButton_UI(const shared_ptr<Stage>& StagePtr):
		GameObject(StagePtr)
		{}

	void AButton_UI::Create(){
		auto PtrTrans = GetComponent<Transform>();
		PtrTrans->SetPosition(18.0f,-10.0f,0.0f);
		PtrTrans->SetRotation(0, 0, 0);
		PtrTrans->SetScale(15.0f, 5.0f, 1.0f);
		auto PtrSprite = AddComponent<Sprite>(Color4(1.0f, 1.0f, 1.0f, 1.0f));
		PtrSprite->SetTextureResource(L"ABUTTON_TX");
		PtrSprite->SetPixelParMeter(32.0f);
		PtrSprite->SetCoordinate(Sprite::Coordinate::m_CenterZeroPlusUpY);
		SetAlphaActive(true);							//透明処理

	}
	void AButton_UI::OnEvent(const shared_ptr<Event>& event){

		if (event->m_MsgStr == L"AButton_false"){
			//非表示
			auto PtrSprite = GetComponent<Sprite>();
			PtrSprite->SetDrawActive(false);
			SetUpdateActive(false);

		}
		else if (event->m_MsgStr == L"AButton_true"){
			//表示
			auto PtrSprite = GetComponent<Sprite>();
			PtrSprite->SetDrawActive(true);
			SetUpdateActive(true);


		}

	}


	/**
	* @class　Score_UI　
	* @author yuya sike
	* @date 2016/01/21 Start
	*/

	Score_UI::Score_UI(const shared_ptr<Stage>& StagePtr, Vector3 Scale, Vector3 Pos, size_t number) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Pos(Pos),
		m_Number(number)
	{}

	void Score_UI::Create(){
		auto PtrTransform = GetComponent<Transform>();

		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetPosition(m_Pos);

		auto PtrSprite = AddComponent<Sprite>(Color4(1.0f, 1.0f, 1.0f, 1.0f));
		PtrSprite->SetTextureResource(L"SCORENUMBER");

		PtrSprite->SetPixelParMeter(32.0f);
		SetAlphaActive(true);
		SetAlphaExActive(true);


		PtrSprite->SetCoordinate(Sprite::Coordinate::m_CenterZeroPlusUpY);


		for (size_t i = 0; i < 11; i++){
			float from = ((float)i) / 11.0f;
			float to = from + (1.0f / 11.0f);
			vector<VertexPositionColorTexture> NumVirtex =
			{
				//左上頂点
				VertexPositionColorTexture(
				Vector3(-0.5f, 0.5, 0),
				Color4(1.0f, 1.0f, 1.0f, 1.0f),
				Vector2(from, 0)
				),

				//右上頂点
				VertexPositionColorTexture(
				Vector3(0.5f, 0.5, 0),
				Color4(1.0f, 1.0f, 1.0f, 1.0f),
				Vector2(to, 0)
				),

				//左下頂点
				VertexPositionColorTexture(
				Vector3(-0.5f, -0.5, 0),
				Color4(1.0f, 1.0f, 1.0f, 1.0f),
				Vector2(from, 1.0f)
				),

				//右上頂点
				VertexPositionColorTexture(
				Vector3(0.5f, -0.5, 0),
				Color4(1.0f, 1.0f, 1.0f, 1.0f),
				Vector2(from, 1.0f)
				),
			};
			m_NumberBurtexVec.push_back(NumVirtex);


			ScorePlus(m_Number);

		}

	}

	void Score_UI::Update(){


	}

	//数変更
	void Score_UI::ScorePlus(size_t socre){
		//size_t Num = (size_t)m_Number;

		size_t Num = socre % 10;

		auto PtrSprite = GetComponent<Sprite>();
		PtrSprite->UpdateVirtexBuffer(m_NumberBurtexVec[Num]);

		//４頂点の情報を作る
		float from = ((float)Num) / 11.0f;
		float to = from + (1.0f / 11.0f);
		vector < VertexPositionColorTexture > Virtex =
		{
			//左上頂点
			VertexPositionColorTexture(
			Vector3(-0.5f, 0.5f, 0),
			Color4(1.0f, 1.0f, 1.0f, 1.0f),
			Vector2(from, 0)
			),
			//右上頂点
			VertexPositionColorTexture(
			Vector3(0.5f, 0.5f, 0),
			Color4(1.0f, 1.0f, 1.0f, 1.0f),
			Vector2(to, 0)
			),
			//左下頂点
			VertexPositionColorTexture(
			Vector3(-0.5f, -0.5f, 0),
			Color4(1.0f, 1.0f, 1.0f, 1.0f),
			Vector2(from, 1.0f)
			),
			//右下頂点
			VertexPositionColorTexture(
			Vector3(0.5f, -0.5f, 0),
			Color4(1.0f, 1.0f, 1.0f, 1.0f),
			Vector2(to, 1.0f)
			),
		};
		//４頂点の更新
		PtrSprite->UpdateVirtexBuffer(Virtex);
	}
	

	void Score_UI::OnEvent(const shared_ptr<Event>& event){

		if (event->m_MsgStr == L"DrawUp_false"){
			//非表示
			auto PtrSprite = GetComponent<Sprite>();
			PtrSprite->SetDrawActive(false);
			SetUpdateActive(false);

		}

		if (event->m_MsgStr == L"DrawUp_true"){
			//表示
			auto PtrSprite = GetComponent<Sprite>();
			PtrSprite->SetDrawActive(true);
			SetUpdateActive(true);

		}
	}

	/**
	* @class　LB_UI　
	* @brief　チュートリアルで表示するボタン表記
	* @author yuya sike
	* @date 2016/01/28 Start
	* @TODO : ここの二つのUIは基底クラス作って最適化できる　今度そのうちやる
	*/
	LB_UI::LB_UI(const shared_ptr<Stage>& StagePtr, const Vector3& Pos) :
		GameObject(StagePtr),
		m_Position(Pos),
		m_Time(0),
		m_TimeCounter(0.0f)
	{}

	void LB_UI::Create(){

		//テクスチャの登録 もしかしたらいらなかったかもしれない
		m_Texture[L"LB_WHITE"] = L"LB_L";
		m_Texture[L"LB_DARK"] = L"LB_D";

		//Transformの取得
		auto PtrTrans = GetComponent<Transform>();
		PtrTrans->SetPosition(m_Position);
		PtrTrans->SetRotation(0, 0, 0);
		PtrTrans->SetScale(2.0f, 1.0f, 1.0f);
		auto PtrSprite = AddComponent<Sprite>(Color4(1.0f, 1.0f, 1.0f, 1.0f));
		PtrSprite->SetTextureResource(m_Texture[L"LB_DARK"]);
		PtrSprite->SetPixelParMeter(32.0f);
		PtrSprite->SetCoordinate(Sprite::Coordinate::m_CenterZeroPlusUpY);
		SetAlphaActive(true);
	}

	void LB_UI::Update(){
		auto Elp = App::GetApp()->GetElapsedTime();
		m_TimeCounter += Elp;
		if (m_TimeCounter > 1.0f){
			m_Time++;
			m_TimeCounter = 0.0f;
		}
		
		else if (m_Time == 3){
			//タイムが３になったら0に戻す
			m_Time = 1;
		}

		if (m_Time == 1){
			auto PtrSprite = GetComponent<Sprite>();
			PtrSprite->SetTextureResource(m_Texture[L"LB_WHITE"]);
		}

		if (m_Time == 2){
			auto PtrSprite = GetComponent<Sprite>();
			PtrSprite->SetTextureResource(m_Texture[L"LB_DARK"]);
		}
	}

	void LB_UI::OnEvent(const shared_ptr<Event>& event){

		if (event->m_MsgStr == L"DrawUp_false"){
			//非表示
			auto PtrSprite = GetComponent<Sprite>();
			PtrSprite->SetDrawActive(false);
			SetUpdateActive(false);

		}
		else if (event->m_MsgStr == L"DrawUp_true"){
			//表示
			auto PtrSprite = GetComponent<Sprite>();
			PtrSprite->SetDrawActive(true);
			SetUpdateActive(true);

		}
	}

	/**
	* @class　RB_UI　
	* @brief　チュートリアルで表示するボタン表記
	* @author yuya sike
	* @date 2016/01/28 Start
	* @TODO : ここの二つのUIは基底クラス作って最適化できる　今度そのうちやる
	*/
	RB_UI::RB_UI(const shared_ptr<Stage>& StagePtr, const Vector3& Pos) :
		GameObject(StagePtr),
		m_Position(Pos),
		m_Time(0),
		m_TimeCounter(0.0f)
	{}

	void RB_UI::Create(){

		//テクスチャの登録 もしかしたらいらなかったかもしれない
		m_Texture[L"RB_WHITE"] = L"RB_L";
		m_Texture[L"RB_DARK"] = L"RB_D";

		//Transformの取得
		auto PtrTrans = GetComponent<Transform>();
		PtrTrans->SetPosition(m_Position);
		PtrTrans->SetRotation(0, 0, 0);
		PtrTrans->SetScale(2.0f, 1.0f, 1.0f);
		auto PtrSprite = AddComponent<Sprite>(Color4(1.0f, 1.0f, 1.0f, 1.0f));
		PtrSprite->SetTextureResource(m_Texture[L"RB_DARK"]);
		PtrSprite->SetPixelParMeter(32.0f);
		PtrSprite->SetCoordinate(Sprite::Coordinate::m_CenterZeroPlusUpY);
		SetAlphaActive(true);
	}

	void RB_UI::Update(){
		auto Elp = App::GetApp()->GetElapsedTime();
		m_TimeCounter += Elp;
		if (m_TimeCounter > 1.0f){
			m_Time++;
			m_TimeCounter = 0.0f;
		}

		else if (m_Time == 3){
			//タイムが３になったら0に戻す
			m_Time = 1;
		}

		if (m_Time == 1){
			auto PtrSprite = GetComponent<Sprite>();
			PtrSprite->SetTextureResource(m_Texture[L"RB_WHITE"]);
		}

		if (m_Time == 2){
			auto PtrSprite = GetComponent<Sprite>();
			PtrSprite->SetTextureResource(m_Texture[L"RB_DARK"]);
		}
	}

	void RB_UI::OnEvent(const shared_ptr<Event>& event){

		if (event->m_MsgStr == L"DrawUp_false"){
			//非表示
			auto PtrSprite = GetComponent<Sprite>();
			PtrSprite->SetDrawActive(false);
			SetUpdateActive(false);

		}
		else if (event->m_MsgStr == L"DrawUp_true"){
			//表示
			auto PtrSprite = GetComponent<Sprite>();
			PtrSprite->SetDrawActive(true);
			SetUpdateActive(true);

		}
	}


}
//end basedx11