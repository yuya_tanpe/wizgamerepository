#pragma once

#include "stdafx.h"

namespace basedx11{
	/**
	* @class　Pause画面のSprite　親クラス
	* @brief　GamePauseStateで表示
	* @author yuya sike
	* @date 2015/12/08 Start
	*/

	class PauseSprite : public GameObject {
	private:
		Vector3 m_StartPos;
	public:
		//構築と生成
		PauseSprite(const shared_ptr<Stage>& StagePtr, const Vector3& i_Start_Pos);
		virtual ~PauseSprite(){}
		//初期化
		virtual void Create() override;

		//変化


		//継承したObjectは必ずイベントを持つようにする
		
		virtual void OnEvent(const shared_ptr<Event>& event) override;
	
	};

	/**
	* @class　Pause画面のSprite　矢印
	* @author yuya sike
	* @date 2015/12/20 Start
	*/

	class PauseArrow : public GameObject {
	private:
		Vector3 m_Arrow_Position;			//アイコンの位置
		bool m_Stick;				//スティックが何度も取得されるのを防ぐ物
		size_t m_SelectUI;			//0~2までで使用		デフォは０

	public:
		PauseArrow(const shared_ptr<Stage>& StagePtr, const Vector3& i_Start_Pos);
		~PauseArrow(){}
	
		//初期化
		virtual void Create() override;
		//変化
		virtual void Update() override;
		
		virtual void OnEvent(const shared_ptr<Event>& event) override;

		void Back_base();	//アイコンの位置を元に戻す
		void Back_to_Game();	//ゲームに戻る　
		void BGMStop();			//BGMをストップ
		void SE_OK();			//SEを鳴らす

		//SelectIDの設定
		void Set_SelectID(size_t SelectID){
			m_SelectUI = SelectID;
		}
	};

	/**
	* @class　Tutorial_UI　星の名前　切り替え可能にする
	* @author yuya sike
	* @date 2016/01/04 Start
	*/

	class Tutorial_UI : public GameObject {
	private:
		vector<wstring> m_Texture;
		int m_point;
	public:
		Tutorial_UI(const shared_ptr<Stage>& StagePtr);
		~Tutorial_UI(){}

		//初期化
		virtual void Create() override;
		//変化
		virtual void Update() override;

		virtual void OnEvent(const shared_ptr<Event>& event) override;

	};

	/**
	* @class　RollUp_UI　
	* @author yuya sike
	* @date 2016/01/21 Start
	//@yuya明日締め切りだから汚いよ
	*/
	class Roll_UI : public GameObject {
	private:
		//テクスチャの名前
		wstring m_Texture_NOT;
		wstring m_Texture_YES;
		Vector3 m_Pos;

	public:
		Roll_UI(const shared_ptr<Stage>& StagePtr, const wstring& TextureNOT,const wstring& TextureYES , const Vector3 Pos);
		~Roll_UI(){}

		//初期化
		virtual void Create() override;

		//イベント
		virtual void OnEvent(const shared_ptr<Event>& event) override;

	};

	class AButton_UI : public GameObject {
	private :

	public : 
		AButton_UI(const shared_ptr<Stage>& StagePtr);
		~AButton_UI(){}

		//初期化
		virtual void Create() override;

		//イベント
		virtual void OnEvent(const shared_ptr<Event>& event) override;

	};

	/**
	* @class　Score_UI　
	* @author yuya sike
	* @date 2016/01/21 Start
	*/
	class Score_UI : public GameObject {
	private:
		vector< vector<VertexPositionColorTexture> > m_NumberBurtexVec;
		//表示したい数字
		size_t m_Number;
		Vector3 m_Scale;
		Vector3 m_Pos;


	public:
		Score_UI(const shared_ptr<Stage>& StagePtr, Vector3 Scale, Vector3 Pos, size_t number);
		~Score_UI(){}

		//初期化
		virtual void Create() override;
		//変化
		virtual void Update() override;

		virtual void OnEvent(const shared_ptr<Event>& event) override;

		void ScorePlus(size_t socre);

	};

	/**
	* @class　LB_UI　
	* @brief　チュートリアルで表示するボタン表記
	* @author yuya sike
	* @date 2016/01/28 Start
	* @TODO : ここの二つのUIは基底クラス作って最適化できる　今度そのうちやる
	*/

	class LB_UI : public GameObject {
	private:
		//画像二種類を格納
		map<wstring, wstring> m_Texture;
		
		//自身の位置
		Vector3 m_Position;

		float m_TimeCounter;
		size_t m_Time;

	public:
		LB_UI(const shared_ptr<Stage>& StagePtr, const Vector3& Pos);
		~LB_UI(){}

		//作成
		virtual void Create() override;
		//変化
		virtual void Update() override;

		virtual void OnEvent(const shared_ptr<Event>& event) override;
	};

	/**
	* @class　RB_UI　
	* @brief　チュートリアルで表示するボタン表記
	* @author yuya sike
	* @date 2016/01/28 Start
	*/

	class RB_UI : public GameObject {
	private:
		//画像二種類を格納
		map<wstring, wstring> m_Texture;

		//自身の位置
		Vector3 m_Position;

		float m_TimeCounter;
		size_t m_Time;

	public:
		RB_UI(const shared_ptr<Stage>& StagePtr, const Vector3& Pos);
		~RB_UI(){}

		//作成
		virtual void Create() override;
		//変化
		virtual void Update() override;

		virtual void OnEvent(const shared_ptr<Event>& event) override;
	};





}
//endof  basedx11