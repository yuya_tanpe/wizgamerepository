#include "stdafx.h"
#include "Project.h"

namespace basedx11{

	GameTimeSprite::GameTimeSprite(shared_ptr<Stage>& StagePtr,
											Vector3 Scale,
											Vector3 Pos,
											Color4 color,
											const UINT number) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Pos(Pos),
		m_Color(color),
		m_Number(number)
	{}
	GameTimeSprite::~GameTimeSprite(){}

	void GameTimeSprite::Create(){
		auto PtrTransform = GetComponent<Transform>();

		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetPosition(m_Pos);

		auto PtrSprite = AddComponent<Sprite>(Color4(1.0f, 1.0f, 1.0f, 1.0f));
		PtrSprite->SetTextureResource(L"SCORENUMBER");

		PtrSprite->SetPixelParMeter(32.0f);
		SetAlphaActive(true);
		SetAlphaExActive(true);

		PtrSprite->SetCoordinate(Sprite::Coordinate::m_CenterZeroPlusUpY);

		for (size_t i = 0; i < 11; i++){
			float from = ((float)i) / 11.0f;
			float to = from + (1.0f / 11.0f);
			vector<VertexPositionColorTexture> NumVirtex =
			{
				//左上頂点
				VertexPositionColorTexture(
				Vector3(-0.5f, 0.5, 0),
				m_Color,
				Vector2(from, 0)
				),

				//右上頂点
				VertexPositionColorTexture(
				Vector3(0.5f, 0.5, 0),
				m_Color, 
				Vector2(to, 0)
				),

				//左下頂点
				VertexPositionColorTexture(
				Vector3(-0.5f, -0.5, 0),
				m_Color, 
				Vector2(from, 1.0f)
				),

				//右上頂点
				VertexPositionColorTexture(
				Vector3(0.5f, -0.5, 0),
				m_Color, 
				Vector2(from, 1.0f)
				),
			};
			m_NumberBurtexVec.push_back(NumVirtex);

		}
	}

	//イベントの受け取り ポーズ画面等
	void GameTimeSprite::OnEvent(const shared_ptr<Event>& event){
		//ポーズ画面のときGameStageからL"Pause"送られてくる
		if (event->m_MsgStr == L"Pause"){
			//ポーズ中はSetUpdateActive(false)にする
			PauseObject();
		}
		//TODO : もうひとつポーズ解除のイベント作成予定
		else if (event->m_MsgStr == L"GamePlay"){
			//ポーズ中はSetUpdateActive(false)にする
			StartObject();
		}
	}

	//ポーズ中に呼ぶ関数
	void GameTimeSprite::PauseObject(){
		//アップデートをしなくする
		SetUpdateActive(false);
	}
	//ゲーム中はこれ呼ぶ
	void GameTimeSprite::StartObject(){
		SetUpdateActive(true);
	}

	void GameTimeSprite::Update(){

		size_t Num = (size_t)m_Number;
		Num = Num % 11;

		auto PtrSprite = GetComponent<Sprite>();
		PtrSprite->UpdateVirtexBuffer(m_NumberBurtexVec[Num]);

		//４頂点の情報を作る
		float from = ((float)Num) / 11.0f;
		float to = from + (1.0f / 11.0f);
		vector < VertexPositionColorTexture > Virtex =
		{
			//左上頂点
			VertexPositionColorTexture(
			Vector3(-0.5f, 0.5f, 0),
			m_Color, 
			Vector2(from, 0)
			),
			//右上頂点
			VertexPositionColorTexture(
			Vector3(0.5f, 0.5f, 0),
			m_Color, 
			Vector2(to, 0)
			),
			//左下頂点
			VertexPositionColorTexture(
			Vector3(-0.5f, -0.5f, 0),
			m_Color, 
			Vector2(from, 1.0f)
			),
			//右下頂点
			VertexPositionColorTexture(
			Vector3(0.5f, -0.5f, 0),
			m_Color, 
			Vector2(to, 1.0f)
			),
		};
		//４頂点の更新
		PtrSprite->UpdateVirtexBuffer(Virtex);
	}
}
//end basedx11
