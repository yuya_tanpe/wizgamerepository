#pragma once
#include "stdafx.h"

namespace basedx11{

	class GameTimeSprite : public GameObject{
		vector< vector<VertexPositionColorTexture> > m_NumberBurtexVec;
		Vector3 m_Scale;
		Vector3 m_Pos;
		//表示したい数字
		size_t m_Number;
		Color4 m_Color;
		
	public :
		GameTimeSprite(shared_ptr<Stage>& StagePtr, Vector3 Scale ,Vector3 Pos,Color4 color,UINT number);
		virtual~GameTimeSprite();

		//ポーズ中に呼ぶ関数
		void PauseObject();
		//ゲームプレイ中に移行したときに読んであげる
		void StartObject();

		virtual void Create() override;
		virtual void Update() override;

		virtual void OnEvent(const shared_ptr<Event>& event)override;


		//アクセサ：表示する数字関連
		void SetNumber(UINT number){
			m_Number = number;
		}
		UINT GetNumber(){
			return m_Number;
		}
		//アクセサ：表示する色関係
		void SetColor(const Color4& color){
			m_Color = color;
		}


	};
}
//end basedx11
