
#include "stdafx.h"
#include "Project.h"


namespace basedx11{

	//--------------------------------------------------------------------------------------
	//	struct LookAtCamera::Impl;
	//	用途: Implクラス
	//--------------------------------------------------------------------------------------
	struct GazeLookCamera::Impl{
		weak_ptr<GameObject> m_TargetObject;	//目標となるオブジェクト
		float m_ToTargetLerp;					//目標を追いかける際の補間値
		Impl() :
			m_ToTargetLerp(1.0f)
		{}
		~Impl(){}
	};

	//--------------------------------------------------------------------------------------
	//	class GazeLookCamera : public Camera ;
	//	用途: いろんなものを注視するカメラ（コンポーネントではない）
	//--------------------------------------------------------------------------------------
	GazeLookCamera::GazeLookCamera() :
		Camera(),
		pImpl(new Impl())
	{
	}
	GazeLookCamera::~GazeLookCamera(){}

	//アクセサ
	shared_ptr<GameObject> GazeLookCamera::GetTargetObject() const{
		if (!pImpl->m_TargetObject.expired()){
			return pImpl->m_TargetObject.lock();
		}
		return nullptr;
	}
	
	void GazeLookCamera::SetTargetObject(const shared_ptr<GameObject>& Obj){
		pImpl->m_TargetObject = Obj;
	}

	float GazeLookCamera::GetToTargetLerp() const{
		return pImpl->m_ToTargetLerp;
	}

	void GazeLookCamera::SetToTargetLerp(float f){
		pImpl->m_ToTargetLerp = f;
	}

	void GazeLookCamera::Update(){
		//auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		////前回のターンからの時間
		//float ElapsedTime = App::GetApp()->GetElapsedTime();
		//if (CntlVec[0].bConnected){
		//	Vector3 NewAt(2, 0, 0);
		//	auto TargetPtr = GetTargetObject();
		//	if (TargetPtr){
		//		//目指したい場所
		//		Vector3 ToAt = TargetPtr->GetComponent<Transform>()->GetPosition();
		//		NewAt = Lerp::CalculateLerp(GetAt(), ToAt, 0, 1.0f, pImpl->m_ToTargetLerp, Lerp::Linear);
		//	}
		//	//ステップ1、注視点と位置の変更
		//	Vector3 Span = GetAt() - GetEye();
		//	Vector3 NewEye = NewAt - Span;
		//	SetAt(NewAt);
		//	SetEye(NewEye);
		//	//ステップ２、ズームの変更
		//	//カメラ位置と注視点の間のベクトルを算出
		//	Span = GetAt() - GetEye();
		//	//正規化
		//	Span.Normalize();
		//	//変化値の決定
		//	Span = Span * ElapsedTime * 10.0f;

		//	Vector3 NewArm = GetAt() - GetEye();
		//	//Dパッド下
		//	//カメラを引く
		//	if (CntlVec[0].wButtons & XINPUT_GAMEPAD_DPAD_DOWN){
		//		//カメラ位置を引く
		//		NewEye = NewEye - Span;
		//		NewArm = NewAt - NewEye;
		//		if (NewArm.Length() > (GetFar() * 0.1f)){
		//			NewEye = NewEye + Span;
		//			NewArm = NewAt - NewEye;
		//		}
		//	}
		//	//Dパッド上
		//	//カメラを寄る
		//	if (CntlVec[0].wButtons & XINPUT_GAMEPAD_DPAD_UP){
		//		//カメラ位置を寄る
		//		NewEye = NewEye + Span;
		//		NewArm = NewAt - NewEye;
		//		if (NewArm.Length() < GetNear() * 2.0f){
		//			NewEye = NewEye - Span;
		//			NewArm = NewAt - NewEye;
		//		}
		//	}
		//	SetAt(NewAt);
		//	SetEye(NewEye);
		//	//ステップ3角度の変更
		//	//現在のAtとEyeの角度を得る
		//	Vector3 ArmInv = GetEye() - GetAt();
		//	//右スティックX方向
		//	FLOAT AngleY = 0;
		//	//右スティックY方向
		//	FLOAT AngleX = 0;
		//	FLOAT AngleZ = 0;
		//	if (CntlVec[0].fThumbRX != 0){
		//		AngleY = -CntlVec[0].fThumbRX * ElapsedTime;
		//	}
		//	if (CntlVec[0].fThumbRY != 0){
		//		AngleX = CntlVec[0].fThumbRY * ElapsedTime;
		//		AngleZ = CntlVec[0].fThumbRY * ElapsedTime;
		//	}
		//	if (ArmInv.z > 0){
		//		AngleX *= -1.0f;
		//	}
		//	if (ArmInv.x < 0){
		//		AngleZ *= -1.0f;
		//	}
		//	Quaternion QtSpan(AngleX, AngleY, AngleZ);
		//	QtSpan.Normalize();
		//	//回転先計算の行列を作成
		//	Matrix4X4 Mat, Mat2;
		//	Mat.STRTransformation(
		//		Vector3(1.0f, 1.0f, 1.0f),
		//		ArmInv,
		//		QtSpan);
		//	Mat2.TranslationFromVector(GetAt());
		//	Mat *= Mat2;
		//	NewEye = Mat.PosInMatrix();
		//	if (NewEye.y < 0.5f){
		//		NewEye.y = 0.5f;
		//	}
		//	//カメラが一定以上、上から視線にならなように調整
		//	ArmInv = NewEye - GetAt();
		//	ArmInv.Normalize();
		//	float y2 = ArmInv.y * ArmInv.y;
		//	float x2 = ArmInv.x * ArmInv.x;
		//	float z2 = ArmInv.z * ArmInv.z;
		//	if (y2 <= (x2 + z2)){
		//		SetEye(NewEye);
		//	}

		}
	}











//end basedx11
