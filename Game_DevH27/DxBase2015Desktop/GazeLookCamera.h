#pragma once

#include "stdafx.h"

namespace basedx11{

		//--------------------------------------------------------------------------------------
		//	class GazeLookCamera : public Camera ;
		//	用途: いろんなものを注視するカメラ（コンポーネントではない）
		//--------------------------------------------------------------------------------------
	class GazeLookCamera : public Camera {
	public:
		//構築と破棄
		explicit GazeLookCamera();
		virtual ~GazeLookCamera();

		//アクセサ
		shared_ptr<GameObject> GetTargetObject() const;
		void SetTargetObject(const shared_ptr<GameObject>& Obj);

		float GetToTargetLerp() const;
		void SetToTargetLerp(float f);

		//操作
		virtual void Update() override;

	private:
		// pImplイディオム
		struct Impl;
		unique_ptr<Impl> pImpl;
	};


}
//endof  basedx11
