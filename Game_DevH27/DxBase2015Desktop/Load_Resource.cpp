#include "stdafx.h"
#include "Project.h"

namespace basedx11{
	/*
	* @class　Load_Resource_Scene
	* @brief ゲーム立ち上げ最初に読み込むScene リソースを読み込むなど
	* @author yuyu sike
	* @date 2016/01/01 Start
	*/

	////画像リソースの読み込み
	//void Load_Resource_Scene::Create_Texture_Resourses(){
	//	//ワーク用配列
	//	vector< vector<wstring> > wsTextureTable;

	//	struct NameTable
	//	{
	//		wstring texture;
	//		wstring resource;
	//		NameTable(const wstring& str1, const wstring& str2) :
	//			texture(str1),
	//			resource(str2){}
	//	};


	//	//ネームテーブルの作成	Textureの追加はここで
	//	vector<NameTable> nameTable = {
	//		{ L"Scene\\Start.png", L"START_TX" },
	//		{ L"Scene\\PlayGame.png", L"PLAYGAME_TX" },
	//		{ L"GameStateUI\\A_Button.png", L"ABUTTON_TX" },
	//		{ L"GameStateUI\\SelectIcon.png", L"PAUSEICON_TX" },
	//		{ L"GameStateUI\\Pazzule_Stage1.png", L"PAZZULE_STAGE1_TX" },
	//		{ L"GameStateUI\\PauseTex.png", L"PAUSE_TX" },
	//		{ L"Player\\PlayerColor.png", L"PLAYERCOLOR_TX" },
	//		{ L"Stage\\orange.png", L"STAGEBOXORANGE_TX" },
	//		{ L"Stage\\alfa.png", L"ALFA_TX" },
	//		{ L"Enemy\\green.png", L"GREENENEMY_TX" },
	//		{ L"Enemy\\RedEnamy.png", L"REDENEMY_TX" },
	//		{ L"CubeGoal\\purpleGoal.png", L"PURPLEGOAL_TX" },
	//		{ L"CubeGoal\\WhiteGoal.png", L"WHITEGOAL_TX" },
	//		{ L"CubeGoal\\BlueGoal.png", L"BLUEGOAL_TX" },
	//		{ L"CubeGoal\\RedGoal.png", L"REDGOAL_TX" },
	//		{ L"CubeGoal\\GreenGoal.png", L"GREENGOAL_TX" },
	//		{ L"Cube\\Blue.png", L"BLUECUBE_TX" },
	//		{ L"Cube\\Purple.png", L"PURPLECUBE_TX" },
	//		{ L"Cube\\white.png", L"WHITECUBE_TX" },
	//		{ L"Cube\\red.png", L"REDCUBE_TX" },
	//		{ L"Cube\\green.png", L"GREENCUBE_TX" },
	//		{ L"Arrow\\ArrowUp.png", L"ARROWUP_TX" },
	//		{ L"Arrow\\ArrowDown.png", L"ARROWDOWN_TX" },
	//		{ L"Arrow\\ArrowRight.png", L"ARROWRIGHT_TX" },
	//		{ L"Arrow\\ArrowLeft.png", L"ARROWLEFT_TX" },
	//		{ L"Arrow\\reverse.png", L"REVERSE_TX" },
	//		{ L"Arrow\\ChangeColorRed.png", L"CHANGERED_TX" },
	//		{ L"Effect\\spark.png", L"SPARK_TX" },
	//		{ L"Effect\\ArrowUpEffect.png", L"ARROWUPEFFECT_TX" },
	//		{ L"Effect\\ArrowDownEffect.png", L"ARROWDOWNEFFECT_TX" },
	//		{ L"Effect\\ArrowRightEffect.png", L"ARROWRIGHTEFFECT_TX" },
	//		{ L"Effect\\ArrowLeftEffect.png", L"ARROWLEFTEFFECT_TX" },
	//		{ L"BackGround\\gias.jpg", L"BACKGROUND_TX" },
	//		{ L"TimeSprite\\number.png", L"NUMBER_TX" },
	//		{ L"TimeSprite\\PlayTime.png", L"PLAYTIME_TX" },
	//		{ L"TimeSprite\\Second.png", L"SECOND_TX" },
	//		{ L"TimeSprite\\YButtonSelect.png", L"YBUTTONSELECT_TX" },
	//		{ L"TimeSprite\\AButtonTitle.png", L"ABUTTONTITLE_TX" },
	//		{ L"TimeSprite\\PutNumber.png", L"PUTNUMBER_TX" },
	//		{ L"TimeSprite\\AButtonRiselt.png", L"ABUTTONRISELT_TX" },
	//		{ L"TimeSprite\\Time.png", L"TIME_TX" },
	//		{ L"TimeSprite\\TotalScore.png", L"TOTALSCORE_TX" },
	//		{ L"TimeSprite\\NextStage.png", L"NEXTSTAGE_TX" },
	//		{ L"Fade\\Black.png", L"BLOCKBLACK_TX" },
	//		{ L"ColorCUBEIcon\\BlueBox.png", L"BLUEICON_NOT" },
	//		{ L"ColorCUBEIcon\\GreenBox.png", L"GREENICON_NOT" },
	//		{ L"ColorCUBEIcon\\PurpleBox.png", L"PURPLEICON_NOT" },
	//		{ L"ColorCUBEIcon\\RedBox.png", L"REDICON_NOT" },
	//		{ L"ColorCUBEIcon\\BlueBoxCheck.png", L"BLUEICON_CHECK" },
	//		{ L"ColorCUBEIcon\\GreenBoxCheck.png", L"GREENICON_CHECK" },
	//		{ L"ColorCUBEIcon\\PurpleBoxCheck.png", L"PURPLEICON_CHECK" },
	//		{ L"ColorCUBEIcon\\RedBoxCheck.png", L"REDICON_CHECK" },
	//	};

	//	for (auto& nt : nameTable){
	//		wsTextureTable.push_back({ nt.texture, nt.resource });
	//	}

	//	//	: テクスチャを登録.
	//	for (auto& wsTexture : wsTextureTable) {
	//		const wstring wsTexturePath = App::GetApp()->m_wstrRelativeDataPath + wsTexture.at(0);
	//		const wstring wsKeyName = wsTexture.at(1);
	//		App::GetApp()->RegisterTexture(wsKeyName, wsTexturePath);
	//	}

	//}

	////サウンドリソースの読み込み
	//void Load_Resource_Scene::Create_Sound_Resourses(){
	//	//ワーク用配列
	//	vector< vector<wstring> > wsWavTable;

	//	struct WavTable
	//	{
	//		wstring music;
	//		wstring resource;
	//		WavTable(const wstring& str1, const wstring& str2) :
	//			music(str1),
	//			resource(str2){}
	//	};

	//	//ネームテーブルの作成	BGM SEの追加はここで
	//	vector<WavTable> WavTable = {
	//		{ L"Sound\\cursor.wav", L"Cursor" },
	//		{ L"Sound\\MatDelete.wav", L"MatDelete" },
	//		{ L"Sound\\Move.wav", L"Move" },
	//		{ L"Sound\\Goal.wav", L"Move" },
	//		{ L"Sound\\Goal.wav", L"Goal" },
	//		{ L"Sound\\bubu.wav", L"BUBU" },
	//		{ L"Sound\\MatPut.wav", L"MatPut" },
	//		{ L"Sound\\MatHit.wav", L"MatHit" },
	//		{ L"Sound\\GameOver.wav", L"GameOver" },
	//		{ L"Sound\\EnamyHit.wav", L"EnamyHit" },
	//		{ L"Sound\\CUBEGoal.wav", L"CUBEGoal" },
	//		{ L"Sound\\CUBEALLGOAL.wav", L"CUBEALLGOAL" },
	//		{ L"Sound\\BGM.wav", L"BGM" },
	//		{ L"Sound\\MatChange.wav", L"MatChange" },
	//		{ L"Sound\\ScoreCount.wav", L"SCORECOUNT" },
	//		{ L"Sound\\Start.wav", L"START" }
	//		//{ L"Sound\\Title.wav",          L"TITLE"},	

	//	};

	//	for (auto& nt : WavTable){
	//		wsWavTable.push_back({ nt.music, nt.resource });
	//	}

	//	//BGM SEの登録.
	//	for (auto& wsWav : wsWavTable) {
	//		const wstring wsWavPath = App::GetApp()->m_wstrRelativeDataPath + wsWav.at(0);
	//		const wstring wsKeyName = wsWav.at(1);
	//		App::GetApp()->RegisterWav(wsKeyName, wsWavPath);
	//	}


	//}

	//ビューの作成
	void Load_Resource_Scene::CreateViews(){
		//最初にデフォルトのレンダリングターゲット類を作成する
		CreateDefaultRenderTargets();
		//マルチビューコンポーネントの取得
		auto PtrMultiView = GetComponent<MultiView>();
		//マルチビューにビューの追加
		auto PtrView = PtrMultiView->AddView();
		//ビューの矩形を設定（ゲームサイズ全体）
		Rect2D<float> rect(0, 0, (float)App::GetApp()->GetGameWidth(), (float)App::GetApp()->GetGameHeight());
		//最初のビューにパラメータの設定
		PtrView->ResetParamaters<Camera, MultiLight>(rect, Color4(0.0f, 0.01f, 0.01f, 1.0f), 1, 0.0f, 1.0f);

		//0番目のビューのカメラを得る
		auto PtrCamera = GetCamera(0);
		PtrCamera->SetEye(Vector3(0.0f, 0.0f, -5.0f));
		PtrCamera->SetAt(Vector3(0.0f, 0.0f, 0.0f));
	}

	//画像の作成
	//void Load_Resource_Scene::CreateSprite_Wiz(){
	//	//（仮）	画像１
	//	auto WizTitle = AddGameObject<SpriteObject>(Vector3(0, 3, 0), Vector3(5, 5, 1), L"PURPLEICON_NOT");
	//	SetSharedGameObject(L"WIZ", WizTitle);
	//	
	//	auto TeamTitle = AddGameObject<SpriteObject>(Vector3(0, -3, 0), Vector3(5, 5, 1), L"PURPLEICON_NOT");
	//	SetSharedGameObject(L"TEAM", TeamTitle);
	//}

	//構築
	void Load_Resource_Scene::Create(){
		//画像リソースの読み込み
		//void Create_Texture_Resourses();
		//サウンドリソースの読み込み
		//void Create_Sound_Resourses();
		//ビューの作成
		void CreateViews();


		//state関連
	/*	m_StateMachine = make_shared< StateMachine<Load_Resource_Scene> >(GetThis<Load_Resource_Scene>());
		m_StateMachine->SetCurrentState(Load_Create_State::Instance());
		m_StateMachine->GetCurrentState()->Enter(GetThis<Load_Resource_Scene>());*/
	}

	//変化
	void Load_Resource_Scene::Update(){
		//ステートマシンの更新
		//m_StateMachine->Update();
	}

	//テクスチャとサウンドをロードしながら、画像表示するState
	//shared_ptr<Load_Create_State> Load_Create_State::Instance(){
	//	static shared_ptr<Load_Create_State> instance(new Load_Create_State);
	//	return instance;
	//}

	//void Load_Create_State::Enter(const shared_ptr<Load_Resource_Scene>& Obj){
	//	//画像の生成
	//	//Obj->CreateSprite_Wiz();
	//}
	//void Load_Create_State::Execute(const shared_ptr<Load_Resource_Scene>& Obj){
	//}
	//void Load_Create_State::Exit(const shared_ptr<Load_Resource_Scene>& Obj){
	//}



}
//endof  basedx11
