#pragma once

#include "stdafx.h"

namespace basedx11{
	/*
	* @class　Load_Resource_Scene
	* @brief ゲーム立ち上げ最初に読み込むScene リソースを読み込むなど
	* @author yuyu sike
	* @date 2016/01/01 Start
	*/
	class Load_Resource_Scene : public Stage {
	private:
		// ---------- member ----------.
		//shared_ptr<StateMachine<Load_Resource_Scene>>  m_StateMachine;	//ステートマシーン
		

		//	---------- Method ----------.

		//TODO : もし時間があったら画像とサウンドを一緒に取り込めるような物を作っておく
		//	//画像リソースの読み込み
		//void Create_Texture_Resourses();
		////サウンドリソースの読み込み
		//void Create_Sound_Resourses();

		//ビューの作成
		void CreateViews();

		

	public:
		//構築と破棄	
		Load_Resource_Scene() : Stage(){}
		virtual ~Load_Resource_Scene(){}

		//初期化
		virtual void Create()override;
		//更新
		virtual void Update() override;

		// ---------- Accessor ----------.

		//ステートマシンの取得
		/*shared_ptr<StateMachine<Load_Resource_Scene>> GetStateMachine() const{
			return m_StateMachine;
		}*/

		// ---------- Motion ----------.

		//なんか名前出したオープニング作りたいから作る
		//void CreateSprite_Wiz();	
	
	};

	//テクスチャとサウンドをロードしながら、画像表示するState
	//class Load_Create_State : public ObjState<Load_Resource_Scene>
	//{
	//	Load_Create_State(){}
	//public:
	//	static shared_ptr<Load_Create_State> Instance();
	//	virtual void Enter(const shared_ptr<Load_Resource_Scene>& Obj);		//ロード
	//	virtual void Execute(const shared_ptr<Load_Resource_Scene>& Obj);	//画像表示して切り替える		
	//	virtual void Exit(const shared_ptr<Load_Resource_Scene>& Obj);		//時間がたったら自動でタイトル画面に
	//};



}
//endof  basedx11
