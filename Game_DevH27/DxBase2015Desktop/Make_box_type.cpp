#include "stdafx.h"
#include "Project.h"

namespace basedx11{


	//--------------------------------------------------------------------------------------
	//	class FixedBox : public GameObject;
	//	用途: 固定のボックス
	//--------------------------------------------------------------------------------------
	//構築と破棄
	FixedBox::FixedBox(const shared_ptr<Stage>& StagePtr,
		const Vector3& Scale,
		const Vector3& Position,
		const wstring& BoxName
		) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Position(Position),
		m_BoxTextureName(BoxName)
	{
	}
	FixedBox::~FixedBox(){}

	//初期化
	void FixedBox::Create(){
		auto PtrTransform = GetComponent<Transform>();

		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(Vector3(0.0f, 0.0f, 0.0f));
		PtrTransform->SetPosition(m_Position);

		//影をつける
		auto ShadowPtr = AddComponent<Shadowmap>();
		ShadowPtr->SetMeshResource(L"DEFAULT_CUBE");

		auto PtrDraw = AddComponent<BasicPNTDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_CUBE");
		PtrDraw->SetTextureResource(m_BoxTextureName);
		PtrDraw->SetOwnShadowActive(true);
		SetAlphaActive(true);
		SetAlphaExActive(true);
	}

	//--------------------------------------------------------------------------------------
	//	class ColorCubeSpawnBox : public GameObject;
	//	用途: プレイヤーが生成されるボックス
	//--------------------------------------------------------------------------------------
	//構築と破棄
	ColorCubeSpawnBox::ColorCubeSpawnBox(const shared_ptr<Stage>& StagePtr,
		const Vector3& Scale,
		const Vector3& Position,
		const wstring& t_name,
		const Vector2& i_CUBERot,
		const size_t& i_VecNum,
		const wstring& i_Key,
		const wstring i_mesh,
		const wstring& BoxName
		) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Position(Position),
		m_texture(t_name),
		m_CUBERot(i_CUBERot),
		m_VecNum(i_VecNum),
		m_Parent_Child_Key(i_Key),
		m_mesh(i_mesh),
		m_BoxTextureName(BoxName)

	{
	}


	void ColorCubeSpawnBox::Create(){
		auto PtrTransform = GetComponent<Transform>();
		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(Vector3(0.0f, 0.0f, 0.0f));
		PtrTransform->SetPosition(m_Position);
		//影をつける
		auto ShadowPtr = AddComponent<Shadowmap>();
		ShadowPtr->SetMeshResource(L"DEFAULT_CUBE");

		auto PtrDraw = AddComponent<BasicPNTDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_CUBE");
		PtrDraw->SetTextureResource(m_BoxTextureName);
		PtrDraw->SetOwnShadowActive(true);
		SetAlphaActive(true);
		SetAlphaExActive(true);


		auto GameStagePtr = dynamic_pointer_cast<GameStage>(GetStage());
		Vector3 Pos(m_Position + Vector3(0.0f, 1.0f, 0.0f));

		//子のオブジェクト生成
		/*
			親のオブジェクトはChiledBoxのCreate()内で生成している
			そろそろ構造体を渡して生成するようにして引数を減らすこと
		*/
		auto PtrChild = GameStagePtr->AddGameObject<ChildBox>(Pos, m_texture, m_CUBERot, m_VecNum, m_Parent_Child_Key, m_mesh);
		//子CUBEのグループの取得
		auto Chiled_Group = GameStagePtr->GetSharedObjectGroup(L"ChiledGroup");
		//青の子CUBEをグループに登録
		Chiled_Group->IntoGroup(PtrChild);
	}

	//--------------------------------------------------------------------------------------
	//	class ColorCubeGoalBox : public GameObject;
	//	用途: 動くColorCubeが生成されるボックス
	//--------------------------------------------------------------------------------------
	//構築と破棄
	ColorCubeGoalBox::ColorCubeGoalBox(const shared_ptr<Stage>& StagePtr,
		const Vector3& Scale,
		const Vector3& Position,
		const wstring& t_name
		) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Position(Position),
		m_texture(t_name)
	{
	}

	void ColorCubeGoalBox::Create(){

		//textureの変化


		auto PtrTransform = GetComponent<Transform>();
		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(Vector3(0.0f, 0.0f, 0.0f));
		PtrTransform->SetPosition(m_Position);

		auto PtrDraw = AddComponent<BasicPNTDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_CUBE");
		PtrDraw->SetTextureResource(m_texture);
		PtrDraw->SetOwnShadowActive(false);
	}

	//--------------------------------------------------------------------------------------
	//	class PlayeSpwnParentBox : public GameObject;
	//	用途: プレイヤーが生成されるボックス
	//--------------------------------------------------------------------------------------
	//構築と破棄
	//構築と破棄
	PlayerSpawnBox::PlayerSpawnBox(const shared_ptr<Stage>& StagePtr,
		const Vector3& Scale,
		const Vector3& Position
		) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Position(Position)
	{
	}

	void PlayerSpawnBox::Create(){
		auto PtrTransform = GetComponent<Transform>();
		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(Vector3(0.0f, 0.0f, 0.0f));
		PtrTransform->SetPosition(m_Position);

		auto PtrDraw = AddComponent<BasicPNTDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_CUBE");
		PtrDraw->SetTextureResource(L"PLAYERCOLOR_TX");
		PtrDraw->SetOwnShadowActive(false);

	}
	//変化
	void PlayerSpawnBox::Update(){

	}


	//--------------------------------------------------------------------------------------
	//	class ColorCube_Lair : public GameObject;
	//	用途: 動くColorCubeが生成されるボックス
	//--------------------------------------------------------------------------------------
	//構築と破棄
	ColorCube_Lair::ColorCube_Lair(const shared_ptr<Stage>& StagePtr,
		const Vector3& Scale,
		const Vector3& Position,
		const wstring& t_name
		) :
		GameObject(StagePtr),
		m_Scale(Scale),
		m_Position(Position),
		m_texture(t_name)
	{
	}

	void ColorCube_Lair::Create(){

		auto PtrTransform = GetComponent<Transform>();
		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(Vector3(0.0f, 0.0f, 0.0f));
		PtrTransform->SetPosition(m_Position);

		Matrix4X4 CharaScale;
		CharaScale.DefTransformation(
			Vector3(0.06f, 0.06f, 0.06f),
			Vector3(0.0f, 0.0f, 0.0f),
			Vector3(0.0f, -0.5f, 0.0f)
			);

		/*auto PtrDraw = AddComponent<BasicPNTDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_CUBE");
		PtrDraw->SetTextureResource(m_texture);
		PtrDraw->SetOwnShadowActive(false);
*/
		//描画コンポーネント
		auto PtrDraw = AddComponent<BasicFbxPNTDraw>();
		PtrDraw->SetMeshToTransform(CharaScale);
		PtrDraw->SetFbxMeshResource(m_texture);
		PtrDraw->SetTextureOnlyNoLight(true);
	}
	//変化
	void ColorCube_Lair::Update(){

	}


	//--------------------------------------------------------------------------------------
	//	class MoveBox : public GameObject;
	//	用途: 動かせるBox　行ける道によってアクションの変化
	//--------------------------------------------------------------------------------------

	MoveBox::MoveBox(const shared_ptr<Stage>& StagePtr,
		const Vector3& Position
		) :
		GameObject(StagePtr),
		m_Pos(Position),
		m_SaveItemData(ItemCase::Empty)			//初期はEmptyで登録
	{

	}

	void MoveBox::Create(){
		auto PtrTransform = GetComponent<Transform>();
		PtrTransform->SetScale(Vector3(1.0f, 1.0f, 1.0f));
		PtrTransform->SetRotation(Vector3(0.0f, 0.0f, 0.0f));
		PtrTransform->SetPosition(m_Pos.x,1.5f,m_Pos.z);

		auto PtrDraw = AddComponent<BasicPNTDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_CUBE");
		PtrDraw->SetTextureResource(L"ALFA_TX");
		PtrDraw->SetOwnShadowActive(true);
		SetAlphaActive(true);
		SetAlphaExActive(true);

		//文字列をつける			デバッグ用Update3で使用中
		auto PtrString = AddComponent<StringSprite>();
		PtrString->SetText(L"");
		PtrString->SetTextRect(Rect2D<float>(16.0f, 16.0f, 640.0f, 480.0f));


		//アクションの設定
		auto PtrMoveTo = AddComponent<MoveTo>();
		//初期時は無効に
		PtrMoveTo->SetUpdateAllActive(false);

		//ステートマシンの構築
		m_StateMachine = make_shared< StateMachine<MoveBox> >(GetThis<MoveBox>());
		//最初のステートをStopStateに設定
		m_StateMachine->SetCurrentState(StopState::Instance());
		//ParentStartStateの初期化実行を行う
		m_StateMachine->GetCurrentState()->Enter(GetThis<MoveBox>());
	}

	void MoveBox::Update(){
		//ステートの更新
		m_StateMachine->Update();
		wstring StateStr;
		if (m_StateMachine->GetCurrentState() == StopState::Instance()){
			StateStr = L"StopState";
		}
		else if (m_StateMachine->GetCurrentState() == NextSerchState::Instance()){
			StateStr = L"NextSerchState";
		}
		else if (m_StateMachine->GetCurrentState() == PushToMoveState::Instance()){
			StateStr = L"PushToMoveState";
		}
		else if (m_StateMachine->GetCurrentState() == PushToFollState::Instance()){
			StateStr = L"PushToFollState";
		}
		wstring str =  StateStr ;
		//文字列をつける
		auto PtrString = GetComponent<StringSprite>();
		//PtrString->SetText(str);
	}
	//Actionがおわったらここで処理
	void MoveBox::Update2(){
		if (m_StateMachine->GetCurrentState() == PushToMoveState::Instance()){
			//ステートがMoveStateなら
			auto PtrMoveTo = GetComponent<MoveTo>();
			//アクションが終わったらtrueになる
			if (PtrMoveTo->IsArrived()){
				//MoveToを無効にする
				PtrMoveTo->SetUpdateAllActive(false);
				//自分の位置のItemCaseを保存しておく
				SaveItemCaseVec();
				//自分の位置のItemCaseにMoveBoxを設定する
				AddItemCaseVec();
				//移動終了なら
				m_StateMachine->ChangeState(StopState::Instance());
			}
		}
		else if (m_StateMachine->GetCurrentState() == PushToFollState::Instance()){
			//ステートがMoveStateなら
			auto PtrMoveTo = GetComponent<MoveTo>();
			//アクションが終わったらtrueになる
			if (PtrMoveTo->IsArrived()){
				//MoveToを無効にする
				PtrMoveTo->SetUpdateAllActive(false);

				PostEvent(0.0f, GetThis<MoveBox>(), GetThis<MoveBox>(), L"Foll");

				//自分の位置のItemCaseを保存しておく
				//SaveItemCaseVec();
				//自分の位置のItemCaseにMoveBoxを設定する
				//AddItemCaseVec();
				//移動終了なら
				m_StateMachine->ChangeState(StopState::Instance());
			}
		}
	}



	//イベント受け取り
	void MoveBox::OnEvent(const shared_ptr<Event>& event){
		if (event->m_MsgStr == L"Move"){
			//イベントが送られたらステートを切り替え
			GetStateMachine()->ChangeState(NextSerchState::Instance());
		}
		else if (event->m_MsgStr == L"Foll"){
			//関数呼びたし
			EventFoll();
			//ゲームマップの設定
			AddDataMap();
		}
	}

	//下に落ちるイベント
	void MoveBox::EventFoll(){
		//ゲームの取得
		auto PtrGameStage = dynamic_pointer_cast<GameStage>(GetStage());
		//アクションの追加
		auto PtrAction = AddComponent<Action>();
		auto PtrTrans = GetComponent<Transform>();
		
		int ToCelX = (int)PtrTrans->GetPosition().x;		//行		Row
		int ToCelZ = (int)PtrTrans->GetPosition().z;		//列		Col

		//PtrGameStage->SetMapData(ToCelX, ToCelZ,)
		PtrAction->AddMoveBy(0.4f, Vector3(0.0f, -1.0f, 0.0f));
		//ループしない
		PtrAction->SetLooped(false);
		//アクション開始
		PtrAction->Run();
	}

	//ステージの設定
	void MoveBox::AddDataMap(){
		//ゲームの取得
		auto PtrGameStage = dynamic_pointer_cast<GameStage>(GetStage());
		auto PtrTrans = GetComponent<Transform>();

		int ToCelX = (int)PtrTrans->GetPosition().x;		//行		Row
		int ToCelZ = (int)PtrTrans->GetPosition().z;		//列		Col
		PtrGameStage->SetMapData(ToCelX, ToCelZ, DataID::ROAD);

	}

	//進行する方向に道があるか確認
	bool MoveBox::NextCheckOverMap(){
		//ゲームステージの取得
		auto PtrGameStage = dynamic_pointer_cast<GameStage>(GetStage());
		auto PtrTrans = GetComponent<Transform>();
		int ToCelX = (int)PtrTrans->GetPosition().x;		//行		Row
		int ToCelZ = (int)PtrTrans->GetPosition().z;		//列		Col
		ToCelX += (int)m_PushVec.x;
		ToCelZ += (int)m_PushVec.y;
		//m_PushVec; //進む進行　Vector2型
		//これ使う 最大最小　穴があるか		TODO : 穴があったら違う動きになる
		if (PtrGameStage->GetCheckToDataMap(ToCelX, ToCelZ)){
			//if (PtrGameStage->GetNextCheckMapData(ToCelX, ToCelZ)){
				return true;
			//}
		}
		//もし道じゃない、穴があったら動けない
		return false;
	}

	//道の判定　０なら空洞
	bool MoveBox::NextCheck_0_Map(){
		//ゲームステージの取得
		auto PtrGameStage = dynamic_pointer_cast<GameStage>(GetStage());
		auto PtrTrans = GetComponent<Transform>();
		int ToCelX = (int)PtrTrans->GetPosition().x;		//行		Row
		int ToCelZ = (int)PtrTrans->GetPosition().z;		//列		Col
		ToCelX += (int)m_PushVec.x;
		ToCelZ += (int)m_PushVec.y;
		size_t CheckMapType = PtrGameStage->GetNextCheckMapData(ToCelX, ToCelZ);
		if (CheckMapType == DataID::NULLID){
			return true;
		}
		//もし道じゃない、穴があったら動けない
		return false;
	}

	//vector配列のMoveBoxをEmptyに変える
	void MoveBox::ResetItemCaseVec(){
		auto GameStagePtr = dynamic_pointer_cast<GameStage>(GetStage());
		auto PtrTransform = GetComponent<Transform>();
		int ToCelX = (int)PtrTransform->GetPosition().x;
		int ToCelZ = (int)PtrTransform->GetPosition().z;
		//保存していたItemCaseのデータをセットする
		GameStagePtr->SetItemObjsize_t(ToCelZ, ToCelX, m_SaveItemData);
	}

	//vector配列の自分の前をMoveBoxにする
	void MoveBox::AddItemCaseVec(){
		auto GameStagePtr = dynamic_pointer_cast<GameStage>(GetStage());
		auto PtrTransform = GetComponent<Transform>();
		int ToCelX = (int)PtrTransform->GetPosition().x;
		int ToCelZ = (int)PtrTransform->GetPosition().z;
		GameStagePtr->SetItemObjsize_t(ToCelZ, ToCelX, ItemCase::MoveBox);
	}

	//自分の位置のItemCaseを保存する
	void MoveBox::SaveItemCaseVec(){
		auto GameStagePtr = dynamic_pointer_cast<GameStage>(GetStage());
		auto PtrTransform = GetComponent<Transform>();
		int ToCelX = (int)PtrTransform->GetPosition().x;
		int ToCelZ = (int)PtrTransform->GetPosition().z;
		//自分の位置のItemCaseを取得
		ItemCase SaveData = GameStagePtr->GetFixedMat(ToCelZ, ToCelX);
		//今いるItemCaseの情報を格納することで次移動するときに消さないようにする
		m_SaveItemData = SaveData;
	}

	//アクションを使った動き
	void MoveBox::ActionMove(){
		//ゲームステージの取得
		auto GameStagePtr = dynamic_pointer_cast<GameStage>(GetStage());
		auto PtrTransform = GetComponent<Transform>();
		//MoveToを取得してUpdateをtrueにしておく
		auto PtrMoveTo = GetComponent<MoveTo>();
		PtrMoveTo->SetUpdateAllActive(true);
		//自分の位置を取得
		int ToCelX = (int)PtrTransform->GetPosition().x;
		int ToCelZ = (int)PtrTransform->GetPosition().z;

		ResetItemCaseVec();			
		//押された方向に移動するため自分の位置に押された方向を足す
		ToCelX += (int)m_PushVec.x;
		ToCelZ += (int)m_PushVec.y;
		//次のマップに移動するためにアクションをする
		Vector3 NextVec((float)ToCelX, 1.5f, (float)ToCelZ);
		PtrMoveTo->SetParams(0.2f, NextVec);
		PtrMoveTo->Run();
	}

	//アクションを使った動き( 1マス動いて下に1マス落ちる )
	//void MoveBox::ActionMove_Foll(){
	//	//ゲームステージの取得
	//	auto GameStagePtr = dynamic_pointer_cast<GameStage>(GetStage());
	//	auto PtrTransform = GetComponent<Transform>();
	//	//MoveToを取得してUpdateをtrueにしておく
	//	auto PtrMoveTo = GetComponent<MoveTo>();
	//	PtrMoveTo->SetUpdateAllActive(true);

	//	//自分の位置を取得
	//	int ToCelX = (int)PtrTransform->GetPosition().x;
	//	int ToCelZ = (int)PtrTransform->GetPosition().z;

	//	ResetItemCaseVec();
	//	//押された方向に移動するため自分の位置に押された方向を足す
	//	ToCelX += (int)m_PushVec.x;
	//	ToCelZ += (int)m_PushVec.y;

	//	//次のマップに移動するためにアクションをする
	//	Vector3 NextVec((float)ToCelX, 1.5f, (float)ToCelZ);
	//	Vector3 FollVec((float)ToCelX, 0.5f, (float)ToCelZ);
	//	//PtrMoveTo->SetParams(0.2f, NextVec);
	//	//PtrMoveTo->SetParams(0.2f, FollVec);
	//	PtrMoveTo->Run();
	//}

	//--------------------------------------------------------------------------------------
	//	class StopState : public ObjState<MoveBox>;
	//	用途: なにもしない　動かないステート
	//--------------------------------------------------------------------------------------
	//ステートのインスタンス取得
	shared_ptr<StopState> StopState::Instance(){
		static shared_ptr<StopState> instance;
		if (!instance){
			instance = shared_ptr<StopState>(new StopState);
		}
		return instance;
	}
	//ステートに入ったときに呼ばれる関数
	void StopState::Enter(const shared_ptr<MoveBox>& Obj){
		//何もしない
	}
	//ステート実行中に毎ターン呼ばれる関数
	void StopState::Execute(const shared_ptr<MoveBox>& Obj){

	}
	//ステートから抜けるときに呼ばれる関数
	void StopState::Exit(const shared_ptr<MoveBox>& Obj){
		//何もしない
	}


	//--------------------------------------------------------------------------------------
	//	class NextSerchState : public ObjState<MoveBox>;
	//	用途: 次いく場所を確認する
	//--------------------------------------------------------------------------------------
	//ステートのインスタンス取得
	shared_ptr<NextSerchState> NextSerchState::Instance(){
		static shared_ptr<NextSerchState> instance;
		if (!instance){
			instance = shared_ptr<NextSerchState>(new NextSerchState);
		}
		return instance;
	}
	//ステートに入ったときに呼ばれる関数
	void NextSerchState::Enter(const shared_ptr<MoveBox>& Obj){
		//進行する方向を確認
		//進行できなかったらできないよってSEならしてStopStateに戻してあげる
		if (Obj->NextCheckOverMap()){
			if(Obj->NextCheck_0_Map()){
				//動いた後下に落ちるStateに切り替え
				Obj->GetStateMachine()->ChangeState(PushToFollState::Instance());
			}
			//動けることが確認できたので動かすStateに移行
			else{
				Obj->GetStateMachine()->ChangeState(PushToMoveState::Instance());
			}
		}
		else{
			//動けないためStopStateに移行
			//TODO : 動けないことを伝えるためにSE鳴らしてあげる
			Obj->GetStateMachine()->ChangeState(StopState::Instance());
		}
	}
	//ステート実行中に毎ターン呼ばれる関数
	void NextSerchState::Execute(const shared_ptr<MoveBox>& Obj){
	
	}
	//ステートから抜けるときに呼ばれる関数
	void NextSerchState::Exit(const shared_ptr<MoveBox>& Obj){
		//何もしない
	}

	//--------------------------------------------------------------------------------------
	//	class PushToMoveState : public ObjState<MoveBox>;
	//	用途: とりあえず移動先に動かすState
	//--------------------------------------------------------------------------------------
	//ステートのインスタンス取得
	shared_ptr<PushToMoveState> PushToMoveState::Instance(){
		static shared_ptr<PushToMoveState> instance;
		if (!instance){
			instance = shared_ptr<PushToMoveState>(new PushToMoveState);
		}
		return instance;
	}
	//ステートに入ったときに呼ばれる関数
	void PushToMoveState::Enter(const shared_ptr<MoveBox>& Obj){
		//進行方向にアクション
		//Update2で動き終わったらvector<Iconcase>に現在の位置に書き換え
		Obj->ActionMove();
		
	}
	//ステート実行中に毎ターン呼ばれる関数
	void PushToMoveState::Execute(const shared_ptr<MoveBox>& Obj){
		//毎ターン呼ぶ物がないため使わないと思われる
	}
	//ステートから抜けるときに呼ばれる関数
	void PushToMoveState::Exit(const shared_ptr<MoveBox>& Obj){
		//何もしない
	}

	//--------------------------------------------------------------------------------------
	//	class PushToFollState : public ObjState<MoveBox>;
	//	用途: 移動先で落ちるState
	//--------------------------------------------------------------------------------------
	//ステートのインスタンス取得
	shared_ptr<PushToFollState> PushToFollState::Instance(){
		static shared_ptr<PushToFollState> instance;
		if (!instance){
			instance = shared_ptr<PushToFollState>(new PushToFollState);
		}
		return instance;
	}
	//ステートに入ったときに呼ばれる関数
	void PushToFollState::Enter(const shared_ptr<MoveBox>& Obj){
		//Obj->ActionMove_Foll();
		Obj->ActionMove();
	}
	//ステート実行中に毎ターン呼ばれる関数
	void PushToFollState::Execute(const shared_ptr<MoveBox>& Obj){

	}
	//ステートから抜けるときに呼ばれる関数
	void PushToFollState::Exit(const shared_ptr<MoveBox>& Obj){
		//何もしない
	}




}
//endof  basedx11
