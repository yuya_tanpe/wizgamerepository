#pragma once

#include "stdafx.h"

namespace basedx11{

	/**
	* @class　FixedBox
	* @brief　なにもない道.
	* @author yuyu sike
	* @date 2015/10/31 Start
	*/
	class FixedBox : public GameObject{
		Vector3 m_Position;
		Vector3 m_Scale;
		wstring m_BoxTextureName;
	public:
		//構築と破棄
		FixedBox(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Position,
			const wstring& TextureName
			);
		virtual ~FixedBox();
		//初期化
		virtual void Create() override;
	};


	/*
	* @class　ColorCubeSpawnBox
	* @brief　動くColorCubeがこのボックスの上から生成させる.
	* @author yuyu sike
	* @date 2015/10/08 Start
	*/
	class ColorCubeSpawnBox : public GameObject{
		Vector3 m_Position;
		Vector3 m_Scale;
		wstring m_texture;
		Vector2 m_CUBERot;
		size_t m_VecNum;
		wstring m_Parent_Child_Key;
		wstring m_mesh;
		wstring m_BoxTextureName;

	public:
		//構築と破棄
		ColorCubeSpawnBox(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Position,
			const wstring& t_name,
			const Vector2& i_CUBERot,
			const size_t& i_VecNum,
			const wstring& i_Key,
			const wstring i_mesh,
			const wstring& TextureName
			);
		virtual ~ColorCubeSpawnBox(){}

		//初期化
		virtual void Create() override;
	
		//アクセサ
		const Vector3 SpawnPos() const{
			Vector3 HalfPos(0.0f, 1.0f, 0.0f);
			return m_Position + HalfPos;
		}

	};


	/**
	* @class　ColorCubeGoalBox
	* @brief　動くColorCubeのゴールするボックス.
	* @author yuyu sike
	* @date 2015/10/08 Start
	*/
	class ColorCubeGoalBox : public GameObject{
		Vector3 m_Position;
		Vector3 m_Scale;
		wstring m_texture;
	public:
		//構築と破棄
		ColorCubeGoalBox(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Position,
			const wstring& t_name
			);
		virtual ~ColorCubeGoalBox(){}

		//初期化
		virtual void Create() override;
	};


	/**
	* @class　PlayerSpawnBox
	* @brief　Playerが生成されるボックス.
	* @author yuyu sike
	* @date 2015/10/08 Start
	*/
	class PlayerSpawnBox : public GameObject {
		Vector3 m_Position;
		Vector3 m_Scale;
	public:
		//構築と破棄
		PlayerSpawnBox(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Position
			);
		virtual ~PlayerSpawnBox(){}

		//初期化
		virtual void Create() override;
		//操作
		virtual void Update() override;

		//アクセサ
		const Vector3 SpawnPos() const{
			Vector3 HalfPos(0.0f, 1.0f, 0.0f);
			return m_Position + HalfPos;
		}

	};

	/**
	* @class　ColorCube_Lair
	* @brief　動くColorCubeのゴールするボックス.
	* @author yuyu sike
	* @date 2015/10/08 Start
	*/
	class ColorCube_Lair : public GameObject{
		Vector3 m_Position;
		Vector3 m_Scale;
		wstring m_texture;
	public:
		//構築と破棄
		ColorCube_Lair(const shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Position,
			const wstring& t_name
			);
		virtual ~ColorCube_Lair(){}

		//初期化
		virtual void Create() override;
		//操作
		virtual void Update() override;
	};

	/*
	* @class　MoveBox
	* @brief　動かせるBox　行ける道によってアクションの変化
	* @author yuyu sike
	* @date 2015/12/04 Start
	*/

	class MoveBox :  public GameObject {
	private:
		// ---------- member ----------.

		Vector3 m_Pos;		//初期位置
		Vector2 m_PushVec;	//押される方向
		shared_ptr<StateMachine<MoveBox>>  m_StateMachine;	//ステートマシーン
		ItemCase m_SaveItemData;							//書き換えないように移動先のItemCase情報を保存する	初期はEmptyで統一


	public:
		//構築と破棄
		MoveBox(const shared_ptr<Stage>& StagePtr,
			const Vector3& Position
			);
		virtual ~MoveBox(){}

		//初期化
		virtual void Create() override;
		//操作
		virtual void Update() override;
		virtual void Update2() override;
		virtual void OnEvent(const shared_ptr<Event>& event)override;
		// ---------- Motion ----------.
		//進行する方向に道があるか確認　あったらステート切り替え　なかったら動けないからStopStateに移行
		bool NextCheckOverMap();
		//道の判定　０なら空洞
		bool NextCheck_0_Map();
		//アクションを使った動き(　1マス動く )
		void ActionMove();
		//アクションを使った動き( 1マス動いて下に1マス落ちる )
		void ActionMove_Foll();
		//vector配列のMoveBoxをEmptyに変える
		void ResetItemCaseVec();
		//vector配列の自分の前をMoveBoxにする
		void AddItemCaseVec();
		//自分の位置のItemCaseを保存する
		void SaveItemCaseVec();
		//下に落ちるイベント
		void EventFoll();
		//ステージの設定
		void AddDataMap();
	

		// ---------- Method ----------.

		// ---------- Accessor ----------.

		shared_ptr<StateMachine<MoveBox>> GetStateMachine() const{
			return m_StateMachine;
		}

		//現在のポジションをVector2型で取得
		const Vector2 GetPosition(){
			return Vector2{ m_Pos.x, m_Pos.z };
		}

		//押される方向を設定
		void SetPushVec(Vector2 Vec){
			m_PushVec = Vec;
		}
	};

	//--------------------------------------------------------------------------------------
	//	class StopState : public ObjState<Player>;
	//	用途: なにもしない　動かないステート
	//--------------------------------------------------------------------------------------
	class StopState : public ObjState<MoveBox>
	{
		StopState(){}
	public:
		//ステートのインスタンス取得
		static shared_ptr<StopState> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<MoveBox>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<MoveBox>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<MoveBox>& Obj)override;
	};



	//--------------------------------------------------------------------------------------
	//	class NextSerchState : public ObjState<MoveBox>;
	//	用途: 次いく場所を確認する
	//--------------------------------------------------------------------------------------
	class NextSerchState : public ObjState<MoveBox>
	{
		NextSerchState(){}
	public:
		//ステートのインスタンス取得
		static shared_ptr<NextSerchState> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<MoveBox>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<MoveBox>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<MoveBox>& Obj)override;
	};
	
	//--------------------------------------------------------------------------------------
	//	class PushToMoveState : public ObjState<MoveBox>;
	//	用途: とりあえず移動先に動かすState
	//--------------------------------------------------------------------------------------
	class PushToMoveState : public ObjState<MoveBox>
	{
		PushToMoveState(){}
	public:
		//ステートのインスタンス取得
		static shared_ptr<PushToMoveState> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<MoveBox>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<MoveBox>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<MoveBox>& Obj)override;
	};

	//--------------------------------------------------------------------------------------
	//	class PushToFollState : public ObjState<MoveBox>;
	//	用途: 移動先で落ちるState
	//--------------------------------------------------------------------------------------
	class PushToFollState : public ObjState<MoveBox>
	{
		PushToFollState(){}
	public:
		//ステートのインスタンス取得
		static shared_ptr<PushToFollState> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<MoveBox>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<MoveBox>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<MoveBox>& Obj)override;
	};


}
//endof  basedx11
