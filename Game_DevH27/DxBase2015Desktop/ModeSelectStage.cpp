#include "stdafx.h"
#include "Project.h"

namespace basedx11{
	/**
	* @class ModeSelectStage
	* @brief モードセレクト、ステージセレクトの実装.
	* @author yuyu sike
	* @date 2015/11/09 Start
	*/
	void ModeSelectStage::CreateResourses(){

		//Spriteの画像リソース
		wstring strTexture = App::GetApp()->m_wstrRelativeDataPath + L"Player\\PlayerColor.png";
		App::GetApp()->RegisterTexture(L"PLAYERCOLOR_TX", strTexture);

		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"GameStateUI\\AButtonSelect.png";
		App::GetApp()->RegisterTexture(L"ABUTTONSELECT_TX", strTexture);

		//ステージのBoxのテクスチャ
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"Stage\\orange.png";
		App::GetApp()->RegisterTexture(L"STAGEBOXORANGE_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"Stage\\alfa.png";
		App::GetApp()->RegisterTexture(L"ALFA_TX", strTexture);
		//{ L"Stage\\alfa.png", L"ALFA_TX" },

		//エネミーのテクスチャ
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"Enemy\\green.png";
		App::GetApp()->RegisterTexture(L"GREENENEMY_TX", strTexture);

		//CubeGoalのテクスチャ
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"CubeGoal\\purpleGoal.png";
		App::GetApp()->RegisterTexture(L"PURPLEGOAL_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"CubeGoal\\WhiteGoal.png";
		App::GetApp()->RegisterTexture(L"WHITEGOAL_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"CubeGoal\\BlueGoal.png";
		App::GetApp()->RegisterTexture(L"BLUEGOAL_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"CubeGoal\\RedGoal.png";
		App::GetApp()->RegisterTexture(L"REDGOAL_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"CubeGoal\\GreenGoal.png";
		App::GetApp()->RegisterTexture(L"GREENGOAL_TX", strTexture);

		//Cubeのテクスチャ
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"Cube\\Blue.png";
		App::GetApp()->RegisterTexture(L"BLUECUBE_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"Cube\\Purple.png";
		App::GetApp()->RegisterTexture(L"PURPLECUBE_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"Cube\\white.png";
		App::GetApp()->RegisterTexture(L"WHITECUBE_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"Cube\\red.png";
		App::GetApp()->RegisterTexture(L"REDCUBE_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"Cube\\green.png";
		App::GetApp()->RegisterTexture(L"GREENCUBE_TX", strTexture);

		//矢印
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"Arrow\\ArrowUp.png";
		App::GetApp()->RegisterTexture(L"ARROWUP_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"Arrow\\ArrowDown.png";
		App::GetApp()->RegisterTexture(L"ARROWDOWN_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"Arrow\\ArrowRight.png";
		App::GetApp()->RegisterTexture(L"ARROWRIGHT_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"Arrow\\ArrowLeft.png";
		App::GetApp()->RegisterTexture(L"ARROWLEFT_TX", strTexture);

		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"GameStateUI\\number.png";
		App::GetApp()->RegisterTexture(L"NUMBER_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"GameStateUI\\Number1.png";
		App::GetApp()->RegisterTexture(L"NUMBER1_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"GameStateUI\\Number2.png";
		App::GetApp()->RegisterTexture(L"NUMBER2_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"GameStateUI\\Number3.png";
		App::GetApp()->RegisterTexture(L"NUMBER3_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"GameStateUI\\Number4.png";
		App::GetApp()->RegisterTexture(L"NUMBER4_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"GameStateUI\\Number5.png";
		App::GetApp()->RegisterTexture(L"NUMBER5_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"GameStateUI\\Number6.png";
		App::GetApp()->RegisterTexture(L"NUMBER6_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"GameStateUI\\Number7.png";
		App::GetApp()->RegisterTexture(L"NUMBER7_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"GameStateUI\\Number8.png";
		App::GetApp()->RegisterTexture(L"NUMBER8_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"GameStateUI\\Number9.png";
		App::GetApp()->RegisterTexture(L"NUMBER9_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"GameStateUI\\Number10.png";
		App::GetApp()->RegisterTexture(L"NUMBER10_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"GameStateUI\\Number11.png";
		App::GetApp()->RegisterTexture(L"NUMBER11_TX", strTexture);

		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"GameStateUI\\Number12.png";
		App::GetApp()->RegisterTexture(L"NUMBER12_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"GameStateUI\\Number13.png";
		App::GetApp()->RegisterTexture(L"NUMBER13_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"GameStateUI\\Number14.png";
		App::GetApp()->RegisterTexture(L"NUMBER14_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"GameStateUI\\Number15.png";
		App::GetApp()->RegisterTexture(L"NUMBER15_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"GameStateUI\\Number16.png";
		App::GetApp()->RegisterTexture(L"NUMBER16_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"GameStateUI\\Number17.png";
		App::GetApp()->RegisterTexture(L"NUMBER17_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"GameStateUI\\Number18.png";
		App::GetApp()->RegisterTexture(L"NUMBER18_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"GameStateUI\\Number19.png";
		App::GetApp()->RegisterTexture(L"NUMBER19_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"GameStateUI\\Number20.png";
		App::GetApp()->RegisterTexture(L"NUMBER20_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"GameStateUI\\Number21.png";
		App::GetApp()->RegisterTexture(L"NUMBER21_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"GameStateUI\\Number22.png";
		App::GetApp()->RegisterTexture(L"NUMBER22_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"GameStateUI\\Number23.png";
		App::GetApp()->RegisterTexture(L"NUMBER23_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"GameStateUI\\Number24.png";
		App::GetApp()->RegisterTexture(L"NUMBER24_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"GameStateUI\\Number25.png";
		App::GetApp()->RegisterTexture(L"NUMBER25_TX", strTexture);


		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"GameStateUI\\SelectArrowLeft.png";
		App::GetApp()->RegisterTexture(L"SELECTARROWLEFT_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"GameStateUI\\SelectArrowRight.png";
		App::GetApp()->RegisterTexture(L"SELECTARROWRIGHT_TX", strTexture);

		wstring CursorWav = App::GetApp()->m_wstrRelativeDataPath + L"Sound\\Start.wav";
		App::GetApp()->RegisterWav(L"Start", CursorWav);
	/*	CursorWav = App::GetApp()->m_wstrRelativeDataPath + L"Sound\\Title.wav";
		App::GetApp()->RegisterWav(L"TITLE", CursorWav);*/
		




	}

	//ビューの作成
	void ModeSelectStage::CreateViews(){
		//最初にデフォルトのレンダリングターゲット類を作成する
		CreateDefaultRenderTargets();
		//マルチビューコンポーネントの取得
		auto PtrMultiView = GetComponent<MultiView>();
		//マルチビューにビューの追加
		auto PtrView = PtrMultiView->AddView();
		//ビューの矩形を設定（ゲームサイズ全体）
		Rect2D<float> rect(0, 0, (float)App::GetApp()->GetGameWidth(), (float)App::GetApp()->GetGameHeight());
		//最初のビューにパラメータの設定
		PtrView->ResetParamaters<Camera, MultiLight>(rect, Color4(0.1f, 0.1f, 0.2f, 0.0f), 1, 0.0f, 1.0f);
		//PtrView->ResetParamaters<GazeLookCamera, MultiLight>(rect, Color4(0.25f, 0.25f, 0.25f, 1.0f), 1, 0.0f, 1.0f);
		//0番目のビューのカメラを得る
		PtrView->SetCamera(dynamic_pointer_cast<Camera>(GetCamera(0)));
		auto PtrCamera = GetCamera(0);

		//横にずらすのでfloat値で少し修正
		static const float XPos = 6.0f;
		//カメラのポジション
		Vector3 CameraPosition{ half_size + Vector3(XPos, 0.0f, 0.0f) };
		PtrCamera->SetEye(Vector3(half_size.x + XPos, 20.0f, -10.0f));			
		PtrCamera->SetAt(CameraPosition );

	}

	//書き直し　 菅野
	void ModeSelectStage::CreateStegeNumber(){
		if (m_StageNumber == 0){
			AddGameObject<SpriteObject>(Vector3(11.0f, 8.0f, 0.0f), Vector3(12.0f, 3.0f, 5.0f), L"NUMBER1_TX");
		}
		else if (m_StageNumber == 1){
			AddGameObject<SpriteObject>(Vector3(11.0f, 8.0f, 0.0f), Vector3(12.0f, 3.0f, 5.0f), L"NUMBER2_TX");
		}
		else if (m_StageNumber == 2){
			AddGameObject<SpriteObject>(Vector3(11.0f, 8.0f, 0.0f), Vector3(12.0f, 3.0f, 5.0f), L"NUMBER3_TX");
		}
		else if (m_StageNumber == 3){
			AddGameObject<SpriteObject>(Vector3(11.0f, 8.0f, 0.0f), Vector3(12.0f, 3.0f, 5.0f), L"NUMBER4_TX");
		}
		else if (m_StageNumber == 4){
			AddGameObject<SpriteObject>(Vector3(11.0f, 8.0f, 0.0f), Vector3(12.0f, 3.0f, 5.0f), L"NUMBER5_TX");
		}
		else if (m_StageNumber == 5){
			AddGameObject<SpriteObject>(Vector3(11.0f, 8.0f, 0.0f), Vector3(12.0f, 3.0f, 5.0f), L"NUMBER6_TX");
		}
		else if (m_StageNumber == 6){
			AddGameObject<SpriteObject>(Vector3(11.0f, 8.0f, 0.0f), Vector3(12.0f, 3.0f, 5.0f), L"NUMBER7_TX");
		}
		else if (m_StageNumber == 7){
			AddGameObject<SpriteObject>(Vector3(11.0f, 8.0f, 0.0f), Vector3(12.0f, 3.0f, 5.0f), L"NUMBER8_TX");
		}
		else if (m_StageNumber == 8){
			AddGameObject<SpriteObject>(Vector3(11.0f, 8.0f, 0.0f), Vector3(12.0f, 3.0f, 5.0f), L"NUMBER9_TX");
		}
		else if (m_StageNumber == 9){
			AddGameObject<SpriteObject>(Vector3(11.0f, 8.0f, 0.0f), Vector3(12.0f, 3.0f, 5.0f), L"NUMBER10_TX");
		}
		else if (m_StageNumber == 10){
			AddGameObject<SpriteObject>(Vector3(11.0f, 8.0f, 0.0f), Vector3(12.0f, 3.0f, 5.0f), L"NUMBER11_TX");
		}
		else if (m_StageNumber == 11){
			AddGameObject<SpriteObject>(Vector3(11.0f, 8.0f, 0.0f), Vector3(12.0f, 3.0f, 5.0f), L"NUMBER12_TX");
		}
		else if (m_StageNumber == 12){
			AddGameObject<SpriteObject>(Vector3(11.0f, 8.0f, 0.0f), Vector3(12.0f, 3.0f, 5.0f), L"NUMBER13_TX");
		}
		else if (m_StageNumber == 13){
			AddGameObject<SpriteObject>(Vector3(11.0f, 8.0f, 0.0f), Vector3(12.0f, 3.0f, 5.0f), L"NUMBER14_TX");
		}
		else if (m_StageNumber == 14){
			AddGameObject<SpriteObject>(Vector3(11.0f, 8.0f, 0.0f), Vector3(12.0f, 3.0f, 5.0f), L"NUMBER15_TX");
		}
		else if (m_StageNumber == 15){
			AddGameObject<SpriteObject>(Vector3(11.0f, 8.0f, 0.0f), Vector3(12.0f, 3.0f, 5.0f), L"NUMBER16_TX");
		}
		else if (m_StageNumber == 16){
			AddGameObject<SpriteObject>(Vector3(11.0f, 8.0f, 0.0f), Vector3(12.0f, 3.0f, 5.0f), L"NUMBER17_TX");
		}
		else if (m_StageNumber == 17){
			AddGameObject<SpriteObject>(Vector3(11.0f, 8.0f, 0.0f), Vector3(12.0f, 3.0f, 5.0f), L"NUMBER18_TX");
		}
		else if (m_StageNumber == 18){
			AddGameObject<SpriteObject>(Vector3(11.0f, 8.0f, 0.0f), Vector3(12.0f, 3.0f, 5.0f), L"NUMBER19_TX");
		}
		else if (m_StageNumber == 19){
			AddGameObject<SpriteObject>(Vector3(11.0f, 8.0f, 0.0f), Vector3(12.0f, 3.0f, 5.0f), L"NUMBER20_TX");
		}
		else if (m_StageNumber == 20){
			AddGameObject<SpriteObject>(Vector3(11.0f, 8.0f, 0.0f), Vector3(12.0f, 3.0f, 5.0f), L"NUMBER21_TX");
		}
		else if (m_StageNumber == 21){
			AddGameObject<SpriteObject>(Vector3(11.0f, 8.0f, 0.0f), Vector3(12.0f, 3.0f, 5.0f), L"NUMBER22_TX");
		}
		else if (m_StageNumber == 22){
			AddGameObject<SpriteObject>(Vector3(11.0f, 8.0f, 0.0f), Vector3(12.0f, 3.0f, 5.0f), L"NUMBER23_TX");
		}
		else if (m_StageNumber == 23){
			AddGameObject<SpriteObject>(Vector3(11.0f, 8.0f, 0.0f), Vector3(12.0f, 3.0f, 5.0f), L"NUMBER24_TX");
		}
		else if (m_StageNumber == 24){
			AddGameObject<SpriteObject>(Vector3(11.0f, 8.0f, 0.0f), Vector3(12.0f, 3.0f, 5.0f), L"NUMBER25_TX");
		}


		if (m_StageNumber >= 1){
			AddGameObject<SpriteObject>(Vector3(4.0f, 8.1f, 0.0f), Vector3(3.0f, 3.0f, 5.0f), L"SELECTARROWLEFT_TX");
		}
		if (m_StageNumber < 24){
			AddGameObject<SpriteObject>(Vector3(18.0f, 8.0f, 0.0f), Vector3(3.0f, 3.0f, 5.0f), L"SELECTARROWRIGHT_TX");
		}

		//AボタンのＵＩ作成
		AddGameObject<SpriteObject>(Vector3(19.0f, -10.3f, 0.0f), Vector3(15.0f, 4.5f, 5.0f), L"ABUTTON_TX");
		//BボタンのＵＩ作成
		AddGameObject<SpriteObject>(Vector3(-17.0f, -10.3f, 0.0f), Vector3(13.0f, 3.5f, 5.0f), L"BBUTTON_TX");


		//auto StageNumberPtr = AddGameObject<GameTimeSprite>(Vector3(2.0f, 2.0f, 0.0f), Vector3(2.0f, 5.0f, 0.0f), Color4(1.0f, 1.0f, 1.0f, 1.0f), 1);
		//SetSharedGameObject(L"StageNumber_01", StageNumberPtr);

		//StageNumberPtr = AddGameObject<GameTimeSprite>(Vector3(2.0f, 2.0f, 0.0f), Vector3(0.0f, 5.0f, 0.0f), Color4(1.0f, 1.0f, 1.0f, 1.0f), 0);
		//SetSharedGameObject(L"StageNumber_10", StageNumberPtr);

		/*
		auto LevelSprite = AddGameObject<WorldSelect_UI>();
		SetSharedGameObject(L"LevelSprite", LevelSprite);
		*/
	}

	//変化がないようなUIなど
	void ModeSelectStage::CreateUI(){
		//ステージセレクトの作成
		AddGameObject<SpriteObject>(Vector3(-8.0f, 8.0f, 0.0f), Vector3(23.0f, 12.5f, 1.0f), L"STAGESELECT_TX");
		//ハイスコア等の枠作成
		AddGameObject<SpriteObject>(Vector3(11.5f, -1.0f, 0.0f), Vector3(19.0f, 18.5f, 1.0f), L"FRAME");
		//目標マットの数
		AddGameObject<SpriteObject>(Vector3(11.5f, 3.0f, 0.0f), Vector3(15.0f, 7.5f, 1.0f), L"MARKMAT");
		//使用したマットの数
		AddGameObject<SpriteObject>(Vector3(11.5f, -0.5f, 0.0f), Vector3(15.0f, 7.5f, 1.0f), L"USEDMAT");
		//ハイスコア
		AddGameObject<SpriteObject>(Vector3(13.8f, -5.0f, 0.0f), Vector3(23.0f, 13.0f, 1.0f), L"HYSCORE");


		//０の位
		auto Score_1 = AddGameObject<Score_UI>(Vector3(1.5f, 2.2f, 1.0f), Vector3(18.0f, -4.5f, 0.0f), 0);

		//10の位
		auto Score_10 = AddGameObject<Score_UI>(Vector3(1.5f, 2.2f, 1.0f), Vector3(16.5f, -4.5f, 0.0f), 0);

		//100の位
		auto Score_100 = AddGameObject<Score_UI>(Vector3(1.5f, 2.2f, 1.0f), Vector3(15.0f, -4.5f, 0.0f), 0);
		
		//1000の位
		auto Score_1000 = AddGameObject<Score_UI>(Vector3(1.5f, 2.2f, 1.0f), Vector3(13.5f, -4.5f, 0.0f), 0);

		auto High = HyScore();
		//1000の位の変更
		auto S_1000 = (High / 1000) % 10;
		Score_1000->ScorePlus(S_1000);

		auto S_100 = (High / 100) % 10;
		Score_100->ScorePlus(S_100);

		auto S_10 = (High / 10) % 10;
		Score_10->ScorePlus(S_10);

		auto S_1 = (High / 1) % 10;
		Score_1->ScorePlus(S_1);

		//目標マット１の位
		auto Mark_1 = AddGameObject<Score_UI>(Vector3(1.5f, 2.2f, 1.0f), Vector3(18.0f, 3.8f, 0.0f), 0);
		//目標マット１０の位
		auto Mark_10 = AddGameObject<Score_UI>(Vector3(1.5f, 2.2f, 1.0f), Vector3(16.5f, 3.8f, 0.0f), 0);
		size_t ReturnMark_Mat = MarkMat();
		//１０以上だったら
		if (ReturnMark_Mat >= 10){
			size_t nokori = ReturnMark_Mat - 10;
			Mark_10->ScorePlus(1);
			Mark_1->ScorePlus(nokori);
		}
		else{
			//目標マットが１０以下だったらそのまま代入
			Mark_1->ScorePlus(ReturnMark_Mat);
		}

		//使ったマット1の位
		auto UseMat_1 = AddGameObject<Score_UI>(Vector3(1.5f, 2.2f, 1.0f), Vector3(18.0f, 0.2f, 0.0f), 0);
		//使ったマット10の位
		auto UseMat_10 = AddGameObject<Score_UI>(Vector3(1.5f, 2.2f, 1.0f), Vector3(16.5f, 0.2f, 0.0f), 0);
		size_t ReturnUse_Mat = UseMat();
		//１０以上だったら
		if (ReturnUse_Mat >= 10){
			size_t Used = ReturnUse_Mat - 10;
			UseMat_10->ScorePlus(1);
			UseMat_1->ScorePlus(Used);
		}
		else{
			//目標マットが１０以下だったらそのまま代入
			UseMat_1->ScorePlus(ReturnUse_Mat);
		}

	}

	//BGMの再生
	void ModeSelectStage::PlaySE(){
		auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
		pMultiSoundEffect->AddAudioResource(L"STAGESELECT");
		pMultiSoundEffect->Start(L"STAGESELECT", XAUDIO2_LOOP_INFINITE, 0.1f);
	}

	//BGMの停止
	void ModeSelectStage::StopSE(){
		auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
		pMultiSoundEffect->AddAudioResource(L"STAGESELECT");
		pMultiSoundEffect->Stop(L"STAGESELECT");
	}

	//目標マットを返す
	size_t ModeSelectStage::MarkMat(){

		//StageManager。Csvの作成
		wstring MediaPath = App::GetApp()->m_wstrRelativeDataPath;

		//シーンに保存していたワールド名を取得
		wstring WorldName = App::GetApp()->GetScene<Scene>()->Get_WorldSelect();
		wstring FileName_ = WorldName + L"\\";

		//ステージ全般を管理しているCsv名		File : Stage_Manager.csv
		wstring name = L"Stage_Manager.csv";
		//ローカル上にCSVファイルクラスの作成
		CsvFile GameStageCsv(MediaPath + FileName_ + name);
		if (!GameStageCsv.ReadCsv()){
			//ファイルが存在しなかった
			//初期化失敗
			throw BaseException(
				L"CSVファイルがありません。",
				MediaPath + FileName_ + name,
				L"変なとこ選択してんなよ"
				);
		}

		//ワーク用のベクター配列
		vector< wstring > StageManagerVec;
		//ステージ番号の取得
		size_t i_StageNumber = App::GetApp()->GetScene<Scene>()->GetStageNumber();

		//Change_Stageの引数から7行目の指定		文字列をStageManagerVecに格納
		GameStageCsv.GetRowVec(i_StageNumber, StageManagerVec);

		//０番目に格納されている数字（目標の数）を取得
		size_t MarkMat = static_cast<size_t>(_wtoi(StageManagerVec[8].c_str()));
		return MarkMat;
	}

	//自分が使ったマットの数を取得する
	size_t ModeSelectStage::UseMat(){
		//StageManager。Csvの作成
		wstring MediaPath = App::GetApp()->m_wstrRelativeDataPath;

		//シーンに保存していたワールド名を取得
		wstring WorldName = App::GetApp()->GetScene<Scene>()->Get_WorldSelect();
		wstring FileName_ = WorldName + L"\\";

		//wstring CsvFilename =L"GameStage\\";
		wstring name = L"HyScore.csv";
		//ローカル上にCSVファイルクラスの作成
		CsvFile GameStageCsv(MediaPath + FileName_ + name);
		if (!GameStageCsv.ReadCsv()){
			//ファイルが存在しなかった
			//初期化失敗
			throw BaseException(
				L"CSVファイルがありません。",
				MediaPath + FileName_ + name,
				L"変なとこ選択してんなよ"
				);
		}
		//ワーク用のベクター配列
		vector< wstring > StageManagerVec;
		//現在のステージ番号を設定する
		size_t i_StageNumber = App::GetApp()->GetScene<Scene>()->GetStageNumber();
		GameStageCsv.GetRowVec(i_StageNumber, StageManagerVec);

		auto UseMat = static_cast<size_t>(_wtoi(StageManagerVec[1].c_str()));
		return UseMat;

	}

	//TODO : ハイスコアと使ったマットを同じ関数にまとめれる
	//ハイスコアの取得
	size_t ModeSelectStage::HyScore(){
		//StageManager。Csvの作成
		wstring MediaPath = App::GetApp()->m_wstrRelativeDataPath;

		//シーンに保存していたワールド名を取得
		wstring WorldName = App::GetApp()->GetScene<Scene>()->Get_WorldSelect();
		wstring FileName_ = WorldName + L"\\";

		//wstring CsvFilename =L"GameStage\\";
		wstring name = L"HyScore.csv";
		//ローカル上にCSVファイルクラスの作成
		CsvFile GameStageCsv(MediaPath + FileName_ + name);
		if (!GameStageCsv.ReadCsv()){
			//ファイルが存在しなかった
			//初期化失敗
			throw BaseException(
				L"CSVファイルがありません。",
				MediaPath + FileName_ + name,
				L"変なとこ選択してんなよ"
				);
		}
		//ワーク用のベクター配列
		vector< wstring > StageManagerVec;
		//現在のステージ番号を設定する
		size_t i_StageNumber = App::GetApp()->GetScene<Scene>()->GetStageNumber();
		GameStageCsv.GetRowVec(i_StageNumber, StageManagerVec);
		auto UseMat = static_cast<size_t>(_wtoi(StageManagerVec[0].c_str()));
		return UseMat;
	}
	
	//道の生成
	void ModeSelectStage::CreateBox(Vector3 i_Scale, Vector3 i_Pos, wstring name){
		AddGameObject<FixedBox>(i_Scale, i_Pos, name);
	}

	//動くCubeがスポーンするボックス	
	void ModeSelectStage::CreateSpawnBox(Vector3 i_Scale, Vector3 i_Pos, wstring i_tex){
		AddGameObject<ColorCube_Lair>(i_Scale, i_Pos, i_tex);
	}

	//ゴール地点のボックスの作成		
	void ModeSelectStage::CreateGoalBox(Vector3 i_Scale, Vector3 i_Pos, wstring i_tex){
		AddGameObject<ColorCubeGoalBox>(i_Scale, i_Pos, i_tex);
	}

	//プレイヤーがスポーンするボックスの作成
	void ModeSelectStage::CreatePlayerBox(Vector3 i_Scale, Vector3 i_Pos){
		Vector2 CellPos(i_Pos.x, i_Pos.z);
		m_vPlayerSpawnCell = CellPos;
		auto PtrPlayerBox = AddGameObject<PlayerSpawnBox>(i_Scale, i_Pos);
	}

	void ModeSelectStage::CreateStage(){

		// @Todo : ここ関数に包める
		//CSVファイルの選択	

		//シーンに保存していたワールド名を取得		2016/01/13	@仕様変更により難易度になる
		wstring WorldName = App::GetApp()->GetScene<Scene>()->Get_WorldSelect();
		//ステージ番号の取得
		size_t i_StageNumber = App::GetApp()->GetScene<Scene>()->GetStageNumber();		//初期は１　（配列で０）
		//ステージ変更　番号により名前を変える
		wstring StageNameCopy = Change_Stage(i_StageNumber, WorldName);
		//Stageの作成  前処理
		wstring MediaPath = App::GetApp()->m_wstrRelativeDataPath;

		wstring FileName_ = WorldName + L"\\";
		//wstring Math_wstirng(L"GameStage\\");

		wstring CsvText(L".csv");

		wstring StageName(FileName_ + StageNameCopy + CsvText);

		wstring CsvFilename = MediaPath + StageName;
		//ローカル上にCSVファイルクラスの作成
		CsvFile GameStageCsv(CsvFilename);
		if (!GameStageCsv.ReadCsv()){
			//ファイルが存在しなかった
			//初期化失敗
			throw BaseException(
				L"CSVファイルがありません。",
				CsvFilename,
				L"変なとこ選択してんなよ"
				);
		}
		const int iDataSizeRow = 0;		//データが0行目から始まるようの定数

		vector< wstring > StageMapVec;	//ワーク用のベクター配列

		GameStageCsv.GetRowVec(iDataSizeRow, StageMapVec);

		//行、列の取得
		//wstring Stage = StageMapVec[0].c_str();
		m_sRowSize = (size_t)_wtoi(StageMapVec[0].c_str()); //Row  = 行
		m_sColSize = (size_t)_wtoi(StageMapVec[1].c_str()); //Col　= 列
		m_MapDataVec = vector< vector<size_t> >(m_sRowSize, vector<size_t>(m_sColSize));	//列の数と行の数分作成
		//真ん中ポジション
		half_size = Vector3(m_sColSize / 2.0f, 1.5f, m_sRowSize / 2.0f);
		//Scene上にStageの半分を設定
		App::GetApp()->GetScene<Scene>()->SetHarfStageSize(half_size);

		//配列の初期化
		for (size_t i = 0; i < m_sRowSize; i++){
			for (size_t j = 0; j < m_sColSize; j++)
			{
				m_MapDataVec[i][j] = 0;
			}
		}
		
		//1行目からマップが定義されている
		const int iDataStartRow = 1;

		if (m_sRowSize > 0){

			for (size_t i = 0; i < m_sRowSize; i++){
				GameStageCsv.GetRowVec(i + iDataStartRow, StageMapVec);		//スタート + i　だから最初は 2が入る

				for (size_t j = 0; j < m_sColSize; j++){					//列分ループ処理
					const int iNum = _wtoi(StageMapVec[j].c_str());		//列から取得したwstring型をintに変換→格納	

					//マップデータ配列に格納
					m_MapDataVec[i][j] = iNum;
					//配置されるオブジェクトの基準スケール
					float ObjectScale = 1.0f;
					float ObjectScaleHalf = ObjectScale / 2.0f;
					//基準Scale
					Vector3 Scale(ObjectScale, ObjectScale, ObjectScale);
					//基準position
					//Vector3 Pos((-ColCenter * ObjectScale) + ((float)j * ObjectScale + ObjectScaleHalf), 0.5f, (RowCenter * ObjectScale) - ((float)i *ObjectScale + ObjectScaleHalf));
					//Vector3 Pos((float)j, 0.5f,((float)-i));
					float s_fPosX = (float)j;
					float s_fPosZ = (float)i;
					Vector3 Pos(s_fPosX, 0.5, s_fPosZ);
					Vector3 Demo_Pos(s_fPosX, 1.5f, s_fPosZ);
					switch (iNum){
					case DataID::NULLID:				//	Csv	:	0
						//assert(!"不正な値が入りました");
						break;
					case DataID::ROAD:					//	Csv	:	1
						//ここに生成するCreate関数を入れる
						CreateBox(Scale, Pos, m_StageColor);
						break;
					case DataID::PLAYERSPAWN:			//	Csv	:	2
						//ここに生成するCreate関数を入れる
						CreatePlayerBox(Scale, Pos);
						break;
					case DataID::BLUECUBE:				//	Csv	:	3
						//ここに生成するCreate関数を入れる	( 引数	　大きさ　位置　テクスチャ　進む向き )
						CreateBox(Scale, Pos, m_StageColor);
						CreateSpawnBox(Scale, Demo_Pos, L"CUBEBLUE_MESH");
						break;
					case DataID::BLUEGOAL:				//	Csv	:	4
						//ここに生成するCreate関数を入れる	( 引数	　大きさ　位置　テクスチャ )
						CreateGoalBox(Scale, Pos, L"BLUECUBE_TX");
						break;
					case DataID::REDCUBE:				//	Csv	:	5
						//ここに生成するCreate関数を入れる	( 引数	　大きさ　位置　テクスチャ　進む向き )
						CreateBox(Scale, Pos, m_StageColor);
						CreateSpawnBox(Scale, Demo_Pos, L"CUBERED_MESH");
						break;
					case DataID::REDGOAL:				//	Csv	:	6
						//ここに生成するCreate関数を入れる	( 引数	　大きさ　位置　テクスチャ )
						CreateGoalBox(Scale, Pos, L"REDCUBE_TX");
						break;
					case DataID::PURPLECUBE:			//	Csv	:	7
						//ここに生成するCreate関数を入れる	( 引数	　大きさ　位置　テクスチャ　進む向き )
						CreateBox(Scale, Pos,m_StageColor);
						CreateSpawnBox(Scale, Demo_Pos, L"CUBEPURPLE_MESH");

						break;
					case DataID::PURPLEGOAL:			//	Csv	:	8
						//ここに生成するCreate関数を入れる	( 引数	　大きさ　位置　テクスチャ )
						CreateGoalBox(Scale, Pos, L"PURPLECUBE_TX");
						break;
					case DataID::YELLOCUBE:			//	Csv	:	7
						CreateBox(Scale, Pos, m_StageColor);
						CreateSpawnBox(Scale, Demo_Pos, L"CUBEGREEN_MESH");

						break;
					case DataID::YELLOGOAL:			//	Csv	:	8
						CreateGoalBox(Scale, Pos, L"GREENCUBE_TX");
						break;
					default:
						assert(!"不正な値が入ってます。Csvマップの可能性が高いです");
						break;
					}

				}

			}

		}
	}

	//動的にStageを変更する	スティックでもボタンでもとりあえず変更させる
	wstring ModeSelectStage::Change_Stage(size_t number , wstring worldname){
		//Stageの名前を作成
		wstring MediaPath = App::GetApp()->m_wstrRelativeDataPath;
		//↓これを動的に変更できるようにする	引数と\\を足して作ることにする
		//これで引数で持ってきたワールド名とスラッシュを合わせることでファイル名を作ることが可能になる
		wstring CsvFilename = worldname + L"\\";	
		//StageManagerは各ファイル共通に用意
		wstring StageManager = 	L"Stage_Manager.csv";
		//ローカル上にCSVファイルクラスの作成
		CsvFile GameStageCsv(MediaPath + CsvFilename + StageManager);
		if (!GameStageCsv.ReadCsv()){
			//ファイルが存在しなかった
			//初期化失敗
			throw BaseException(
				L"CSVファイルがありません。",
				CsvFilename,
				L"変なとこ選択してんなよ"
				);

		}

		vector< wstring > StageManagerVec;	//ワーク用のベクター配列
		
		//Change_Stageの引数から何列目の指定
		GameStageCsv.GetRowVec(number, StageManagerVec);

		wstring StageName = L"";
		//０番目からStageNameを取得
		StageName = StageManagerVec[0].c_str();
		//TODO : ゴール数はゲームステージで取得でもいいと思う
		m_StageGoalSize = (size_t)_wtoi(StageManagerVec[1].c_str());
		//ステージの色を取得する　７列目
		m_ChangeBoxNameMap[L"1"] = L"STAGEBOXBLUE_TX";
		m_ChangeBoxNameMap[L"2"] = L"STAGEBOXORANGE_TX";
		m_ChangeBoxNameMap[L"3"] = L"STAGEBOXRED_TX";

		m_StageColor = StageManagerVec[7].c_str();

		//自身のwstringをmapで変換し、テクスチャキーにする
		m_StageColor = m_ChangeBoxNameMap[m_StageColor];

		return StageName;
	}

	//Sceneからスティック情報を取得
	void ModeSelectStage::GetScene_Stickbrief(){
		//前回のスティック情報を今の情報と同期する
		m_Stick = App::GetApp()->GetScene<Scene>()->Get_Stickbrief();


	}

	//コントローラーの取得 スティック操作
	bool ModeSelectStage::Is_StickSelect(){
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		
		if (!CntlVec[0].bConnected)	return false;
		bool Max = m_StageNumber < 24;
		bool Min = m_StageNumber > 0;


		//事前準備
		int StageNumber_YY = m_StageNumber + 10;
		int StageNumber_XX = m_StageNumber - 10;
			
		//スティック上下入力のときに使う
		bool TenMax = StageNumber_YY < 25;
		bool TenMin = StageNumber_XX > -1;

		//両方のスティックが傾いてない
		bool StickLap_X = CntlVec[0].fThumbLX < 0.2f && CntlVec[0].fThumbLX > -0.2f;
		bool StickLap_Y = CntlVec[0].fThumbLY < 0.2f && CntlVec[0].fThumbLY > -0.2f;

		//スティックが傾いていない時
		if (StickLap_X && StickLap_Y){
			//スティック操作をまだしてない
			m_Stick = false;
			return false;
		}

		else if (m_Stick == false){
			//スティックが左に傾いたとき　
			if (CntlVec[0].fThumbLX < -0.75f){
				//数字を引く
				if (Min){
					m_Stick = true;
					m_StageNumber--;
					App::GetApp()->GetScene<Scene>()->SetStageNumber(m_StageNumber);	
					//スティック情報をSceneに保存
					App::GetApp()->GetScene<Scene>()->Set_Stickbrief(m_Stick);
					return true;
				}
			}
			//スティックが右に傾いたとき
			if (CntlVec[0].fThumbLX > 0.75f){
				//数字を足す
				if (Max){
					m_Stick = true;
					m_StageNumber++;

					App::GetApp()->GetScene<Scene>()->SetStageNumber(m_StageNumber);
					//スティック情報をSceneに保存
					App::GetApp()->GetScene<Scene>()->Set_Stickbrief(m_Stick);
					return true;
				}
			}

			//スティックが上に傾いたとき
			if (CntlVec[0].fThumbLY > 0.75f){
				//数字を足す
				if (TenMin){
					m_Stick = true;
					m_StageNumber -= 10;
					App::GetApp()->GetScene<Scene>()->SetStageNumber(m_StageNumber);
					//スティック情報をSceneに保存
					App::GetApp()->GetScene<Scene>()->Set_Stickbrief(m_Stick);
					return true;
				}
			}

			//スティックが下に傾いたとき
			if (CntlVec[0].fThumbLY < -0.75f){
				//数字を足す
				if (TenMax){
					m_Stick = true;
					m_StageNumber += 10;
					App::GetApp()->GetScene<Scene>()->SetStageNumber(m_StageNumber);
					//スティック情報をSceneに保存
					App::GetApp()->GetScene<Scene>()->Set_Stickbrief(m_Stick);
					return true;
				}
			}

			//何も入力を受け取ってない場合
			return false;
		}

		//m_Stickがtrueだった場合
		return false;
	}


	

	//Aボタンを押したら開始
	bool ModeSelectStage::Is_pushStage(){
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();

		if (!CntlVec[0].bConnected)	return false;
		if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_A){
			return true;
		}
		return false;
	}

	//デバッグ関係の処理
	void ModeSelectStage::Debug_wstring(){

		//メンバ変数の親の位置
		wstring Str_Number(L" StageNumber :\t");
		Str_Number += L"StageNumber = " + Util::IntToWStr(m_StageNumber, Util::NumModify::Dec) + L",\t";
		Str_Number += L"\n";

		wstring Str_GoalSize(L" StageGoalSize :\t");
		Str_GoalSize += L"StageGoalSizer = " + Util::IntToWStr(m_StageGoalSize, Util::NumModify::Dec) + L",\t";
		Str_GoalSize += L"\n";

		//文字列をつける
		wstring str = Str_Number + Str_GoalSize; 
		auto PtrString = GetComponent<StringSprite>();
		//表示しない場合は↓をコメントで対応
		PtrString->SetText(str);
	}

	void ModeSelectStage::Create(){
		//ステージの番号の取得　デフォは１
		m_StageNumber = App::GetApp()->GetScene<Scene>()->GetStageNumber();
		//TODO : ゴール数の取得	いらないと思う
		m_StageGoalSize = App::GetApp()->GetScene<Scene>()->GetStageGoalSize();


		//BGMのスタート		
		//App::GetApp()->GetScene<Scene>()->StartBGM();

		//エフェクトの追加
		auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
		pMultiSoundEffect->AddAudioResource(L"KETTEI");
		pMultiSoundEffect->AddAudioResource(L"PAUSEOK");
		pMultiSoundEffect->AddAudioResource(L"Start");

		//文字列をつける
		auto PtrString = AddComponent<StringSprite>();
		PtrString->SetText(L"");
		PtrString->SetTextRect(Rect2D<float>(16.0f, 120.0f, 640.0f, 240.0f));
		//テクスチャリソースの作成
		CreateResourses();
		//ステージナンバー表示
		CreateStegeNumber();
		//ステージ作成
		CreateStage();
		//カメラ作成
		CreateViews();
		//スティック操作を同期する	
		//TODO : もしかしたら他にいい方法あるかもしれない
		GetScene_Stickbrief();
		//タイトルの作成
		CreateUI();
		
	}

	void ModeSelectStage::Update(){

		//デバッグの表示
		//Debug_wstring();

		//ステージ切り替え時に左スティックを使用したとき
		if (Is_StickSelect()){
			//SEを鳴らす
			auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
			pMultiSoundEffect->Start(L"KETTEI", 0, 0.2f);
			//ボタン入力があったらStageの再読み込み
			PostEvent(0.0f, GetThis<ModeSelectStage>(), App::GetApp()->GetSceneBase(), L"ToStageSelect");
		}
		//ステージを決定してAボタンを押したとき
		if(Is_pushStage()){
			//BGMのストップ
			App::GetApp()->GetScene<Scene>()->StopBGM();
			//ゴールサイズを設定
			App::GetApp()->GetScene<Scene>()->SetStageGoalSize(m_StageGoalSize);
			PostEvent(0.0f, GetThis<ModeSelectStage>(), App::GetApp()->GetSceneBase(), L"ToGame");
			//SEの追加
			auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
			pMultiSoundEffect->Start(L"Start", 0, 0.1f);
		}

		//コントローラ情報の取得
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		//Bボタンが押されたら
		if (CntlVec[0].bConnected){
			if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_B) {
				//BGMのストップ
				App::GetApp()->GetScene<Scene>()->StopBGM();
				//タイトルに戻ったらステージの番号を初期化する
				App::GetApp()->GetScene<Scene>()->SetStageNumber(0);
				PostEvent(0.0f, GetThis<ModeSelectStage>(), App::GetApp()->GetSceneBase(), L"ToMenu");
			}
		}
	}
}
//endof  basedx11
