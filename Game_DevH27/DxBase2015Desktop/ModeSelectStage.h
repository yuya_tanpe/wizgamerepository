#pragma once

#include "stdafx.h"

namespace basedx11{

	/**
	* @class ModeSelectStage
	* @brief モードセレクト、ステージセレクトの実装.
	* @author yuyu sike
	* @date 2015/11/09 Start
	*/

	class ModeSelectStage : public Stage{

		//	---------- Method ----------.

		void PlaySE();
		void StopSE();
		size_t MarkMat();
		size_t UseMat();
		size_t HyScore();

		//プレイヤーを生成するCell座標
		Vector2 m_vPlayerSpawnCell;
		//ステージ生成に必要なメンバ変数
		size_t m_sColSize;
		size_t m_sRowSize;
		//ステージの真ん中を格納
		Vector3 half_size;		
		//ワーク用のベクター配列のメンバ変数
		vector< vector <size_t> > m_MapDataVec;
		//テストで作るもの
		//vector< vector<weak_ptr<FixedBox> > > m_BlockVec;

		//ステージの番号
		size_t m_StageNumber;

		//難易度のサイズ
		int m_Level;
		//ステージゴールサイズをSceneに持っていく
		size_t m_StageGoalSize;

		//スティック操作で選択するために使用　再度操作されないように
		bool m_Stick;

		//ステージの色を格納する
		wstring m_StageColor;
		map<wstring, wstring>m_ChangeBoxNameMap;

	public:
		//構築と破棄
		ModeSelectStage() :Stage(), m_Stick(false), m_Level(0), m_StageColor(L"Orange"){}
		virtual ~ModeSelectStage(){}
		//初期化
		virtual void Create()override;
		//更新
		virtual void Update() override;



		// ---------- Accessor ----------.


		//プレイヤーを生成するCell座標を取得する
		const Vector2 GetPlayerSpawnCell() const{
			return m_vPlayerSpawnCell;
		}

		//Csvの行を取得
		const size_t GetRow() const{
			return m_sRowSize;
		}

		//Csvの列を取得
		const size_t GetCol() const{
			return m_sColSize;
		}

		//引数から選択したセルのデータを取得する
		const int GetMapData(size_t row, size_t col) const{
			return m_MapDataVec[col][row];
		}

		//マップデータサイズを取得する
		const Vector2 GetMapSize() const{
			return Vector2((float)m_sRowSize, (float)m_sColSize);
		}


		//	---------- Method ----------.

		//ビューの作成
		void CreateViews();
		//リソースの作成
		void CreateResourses();
		//ステージナンバー表示
		void CreateStegeNumber();
		//Stageの作成
		void CreateStage();
		//UI関係
		void CreateUI();
		//Stageの名前をCsvから取得
		wstring Change_Stage(size_t number , wstring worldname);
		//移動するためのボックス
		void CreateBox(Vector3 i_Scale, Vector3 i_Pos, wstring name);
		//BlueCubeスポーンボックスの生成
		void CreateSpawnBox(Vector3 i_Scale, Vector3 i_Pos, wstring i_tex);
		//Blueゴールボックスの生成
		void CreateGoalBox(Vector3 i_Scale, Vector3 i_Pos, wstring i_tex);
		//Playerスポーンボックスの生成
		void CreatePlayerBox(Vector3 i_Scale, Vector3 i_Pos);


		/*
		* @brief スティックによるステージセレクト.
		* @author yuyu sike
		* @date 2015/12/18 Start
		*/
		bool Is_StickSelect();
		void Is_StickSelectNumber();
		
		//ステージの決定
		bool Is_pushStage();
		//BGMの作成
		void aSound();
		//デバッグ文字の作成
		void Debug_wstring();

		//Sceneからスティック情報を取得する
		void GetScene_Stickbrief();




		//ステージの決定２
		//イベントでステージの動的変更を実装予定
	};




}
//endof  basedx11
