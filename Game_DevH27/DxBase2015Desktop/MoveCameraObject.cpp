#include "stdafx.h"
#include "Project.h"

namespace basedx11{


	//--------------------------------------------------------------------------------------
	//	class LookAtObject : public GameObject;
	//	用途: ボックス
	//--------------------------------------------------------------------------------------
	//構築と破棄
	LookAtObject::LookAtObject(const shared_ptr<Stage>& StagePtr, const Vector3& StartPos) :
		GameObject(StagePtr), m_StartPos(StartPos)
	{}
	LookAtObject::~LookAtObject(){}

	void LookAtObject::Create(){
		//Transformだけは追加しなくても取得できる
		auto Ptr = GetComponent<Transform>();
		Ptr->SetPosition(m_StartPos);
		Ptr->SetScale(0.25f, 0.25f, 0.25f);	//直径25センチの球体
		Ptr->SetRotation(0.0f, 0.0f, 0.0f);

	}

	void LookAtObject::Update(){

		//トランスフォームからクォータニオンを取得
		auto Ptr = GetComponent<Transform>();
		auto Qt = Ptr->GetQuaternion();

		////回転の計算
		Quaternion QtSpan;
		QtSpan.RotationRollPitchYawFromVector(Vector3(0, 0.02f, 0.0f));
		Qt *= QtSpan;

		////自分の回転量を更新する
		Ptr->SetQuaternion(Qt);


		
	}

	//--------------------------------------------------------------------------------------
	//	class SpinObject : public GameObject;
	//	用途: 回るオブジェクト
	//--------------------------------------------------------------------------------------
	//構築と破棄
	SpinObject::SpinObject(const shared_ptr<Stage>& StagePtr, const Vector3& StartPos) :
		GameObject(StagePtr), m_StartPos(StartPos)
	{}
	SpinObject::~SpinObject(){}

	void SpinObject::Create(){
		//Transformだけは追加しなくても取得できる
		auto Ptr = GetComponent<Transform>();
		Ptr->SetPosition(m_StartPos);
		Ptr->SetScale(0.25f, 0.25f, 0.25f);	//直径25センチの球体
		Ptr->SetRotation(0.0f, 0.0f, 0.0f);
		
		auto Parent = GetStage()->GetSharedGameObject<LookAtObject>(L"LookAt", false);
		Ptr->SetParent(Parent);
	}

	void SpinObject::Update(){
		auto Ptr = GetComponent<Transform>();
		auto GamePtr = dynamic_pointer_cast<GameStage>(GetStage());
		Vector3 LookAtStage = App::GetApp()->GetScene<Scene>()->GetHarfStageSize();

		if (GamePtr->GetStateMachine()->GetCurrentState() == GameClearState::Instance() ||
			GamePtr->GetStateMachine()->GetCurrentState() == GameOverState::Instance() ||
			GamePtr->GetStateMachine()->GetCurrentState() == GameStartState::Instance()){
			//カメラの座標を自分に追従させる
			auto PtrCamera = dynamic_pointer_cast<Camera>(GetStage()->GetCamera(0));
			if (PtrCamera){
				PtrCamera->SetEye(Ptr->GetPosition());
				PtrCamera->SetAt(LookAtStage);
			}
		}
		////カメラの座標を自分に追従させる
		/*auto PtrCamera = dynamic_pointer_cast<Camera>(GetStage()->GetCamera(0));
		if (PtrCamera){
			PtrCamera->SetEye(Ptr->GetPosition());
			PtrCamera->SetAt(0, 0, 0);

		}*/
	}






}
//endof  basedx11
