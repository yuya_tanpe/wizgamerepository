#pragma once

#include "stdafx.h"

namespace basedx11{



	//注視するオブジェクト
	class LookAtObject : public GameObject{
		Vector3 m_StartPos;
	public:
		//構築と破棄
		LookAtObject(const shared_ptr<Stage>& StagePtr, const Vector3& StartPos);
		virtual ~LookAtObject();
		//初期化
		virtual void Create() override;
		//変化
		virtual void Update() override;
	};

	//回るオブジェクト
	class SpinObject : public GameObject{
		Vector3 m_StartPos;
	public:
		//構築と破棄
		SpinObject(const shared_ptr<Stage>& StagePtr, const Vector3& StartPos);
		virtual ~SpinObject();
		//初期化
		virtual void Create() override;

		virtual void Update() override;

	};
}
//endof  basedx11
