
#include "stdafx.h"
#include "Project.h"


namespace basedx11{
	//--------------------------------------------------------------------------------------
	//	class MovePlate : public GameObject;
	//	用途: プレイヤーが置く進行方向を変えるプレート
	//--------------------------------------------------------------------------------------
	MovePlate::MovePlate(const shared_ptr<Stage>& StagePtr , const Vector3 i_vPlayerPos) :
		GameObject(StagePtr),
		m_vPlayerPos(i_vPlayerPos),		//プレイヤーのポジション保持
		m_vPlayerCellPos(0,0)		//プレイヤーがいるCellのポジションの保持
	{}

	//初期化
	void MovePlate::Create(){

		auto PtrTransform = GetComponent<Transform>();
		PtrTransform->SetPosition(m_vPlayerPos - Vector3(0.0f,0.5f,0.0f));
		PtrTransform->SetRotation(0.0f,0.0f,0.0f);
		PtrTransform->SetScale(0.8f,0.1f,0.8f);

		//衝突判定をつける
		auto PtrCol = AddComponent<CollisionObb>();
		PtrCol->SetUpdateActive(false);

		//描画コンポーネントの設定
		auto PtrDraw = AddComponent<BasicPNTDraw>();
		//描画するメッシュを設定
		PtrDraw->SetMeshResource(L"DEFAULT_CUBE");
		//描画するテクスチャを設定
		PtrDraw->SetTextureResource(L"PURI_TX");


		//透明処理
		SetAlphaActive(true);




	}

	//--------------------------------------------------------------------------------------
	//	class Defult : public ObjState<MovePlate>;
	//	用途: プレイヤーからよばれたら実行
	//--------------------------------------------------------------------------------------
	//ステートのインスタンス取得
	shared_ptr<Defult> Defult::Instance(){
		static shared_ptr<Defult> instance;
		if (!instance){
			instance = shared_ptr<Defult>(new Defult);
		}
		return instance;
	}
	//ステートに入ったときに呼ばれる関数
	void Defult::Enter(const shared_ptr<MovePlate>& Obj){
		//何もしない
	}
	//ステート実行中に毎ターン呼ばれる関数
	void Defult::Execute(const shared_ptr<MovePlate>& Obj){
		//何もしない
	}
	//ステートにから抜けるときに呼ばれる関数
	void Defult::Exit(const shared_ptr<MovePlate>& Obj){
		//何もしない
	}




}
//endof  basedx11
