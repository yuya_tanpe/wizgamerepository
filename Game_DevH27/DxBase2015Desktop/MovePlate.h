#pragma once

#include "stdafx.h"

namespace basedx11{

	/**
	* @class　MovePlate
	* @brief　ColorCubeの進行方向を変更するClass.
	* @author yuya sike
	* @date 2015/10/08 Start
	*/
	class MovePlate : public GameObject {

	public:	//構築と破棄
		MovePlate(const shared_ptr<Stage>& StagePtr, const Vector3 i_vPlayerPos);
		virtual ~MovePlate(){}
		//初期化
		virtual void Create() override;
		
		// ---------- Accessor ----------.
		shared_ptr< StateMachine<MovePlate> > GetStateMachine() const{
			return m_StateMachine;
		}

	private:
		//	---------- Method ----------.

		shared_ptr< StateMachine<MovePlate> >  m_StateMachine;

		Vector3 m_vPlayerPos;			//プレイヤーのポジション保持
		Vector2 m_vPlayerCellPos;		//プレイヤーがいるCellのポジションの保持





	};



	//--------------------------------------------------------------------------------------
	//	class Defult : public ObjState<ColorCube>;
	//	用途: プレイヤーからの呼び出しで反応　
	//--------------------------------------------------------------------------------------
	class Defult : public ObjState<MovePlate>
	{
		Defult(){}
	public:
		//ステートのインスタンス取得
		static shared_ptr<Defult> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<MovePlate>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<MovePlate>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<MovePlate>& Obj)override;
	};


}
//endof  basedx11
