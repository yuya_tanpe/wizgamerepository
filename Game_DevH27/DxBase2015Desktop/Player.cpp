
#include "stdafx.h"
#include "Project.h"

namespace basedx11{
	//--------------------------------------------------------------------------------------
	//	class Player : public GameObject;
	//	用途: プレイヤー
	//--------------------------------------------------------------------------------------
	//構築と破棄
	Player::Player(const shared_ptr<Stage>& StagePtr, const Vector2 iPos) :
		GameObject(StagePtr),
		m_vNextMove(0, 0),							//次いくセル座標
		m_Arrow(0, 0),								//選択した矢印を保存
		FirstPos(iPos),								//初期位置
		m_Stick(false),								//スティックでのマット配置に使用
		m_Player_Rot(PlayerRot::Front),		//初期のプレイヤーの向き
		m_RightStick(false)
	{
	}



	//初期化
	void Player::Create(){

		//初期位置などの設定
		auto Ptr = GetComponent<Transform>();
		Ptr->SetScale(0.5f, 0.5f, 0.5f);
		//初期のプレイヤーの向きを関数で制御　修正↓

		Vector3 WorkVec;		//作業用
		SetPlayer_Rotation(m_Player_Rot, WorkVec);
		Ptr->SetRotation(WorkVec);
		Ptr->SetPosition(FirstPos.x, 1.25f, FirstPos.y);
		
		auto PtrCol = AddComponent<CollisionSphere>();
		PtrCol->SetFixed(false);
		PtrCol->SetDrawActive(false);

		//アクションの設定
		auto PtrMoveTo = AddComponent<MoveTo>();
		//初期時は無効に
		PtrMoveTo->SetUpdateAllActive(false);

		//文字列をつける			デバッグ用Update3で使用中
		auto PtrString = AddComponent<StringSprite>();
		PtrString->SetText(L"");
		PtrString->SetTextRect(Rect2D<float>(16.0f, 16.0f, 640.0f, 480.0f));

		//モデルのデフォルトサイズの設定
		Matrix4X4 CharaScale;
		CharaScale.DefTransformation(
			Vector3(0.03f, 0.03f, 0.03f),
			Vector3(0.0f, 0.0f, 0.0f),
			Vector3(0.0f, -0.5f, 0.0f)
			);


		//描画コンポーネントの設定
		auto PtrDraw = AddComponent<BasicFbxPNTBoneDraw>();
		PtrDraw->SetMeshToTransform(CharaScale);
		PtrDraw->SetFbxMeshResource(L"HUMAN_MESH");
		PtrDraw->SetTextureOnlyNoLight(true);

		PtrDraw->SetCurrentAnimation("walk");
		
		//0番目のビューのカメラを得る
		auto PtrCamera = dynamic_pointer_cast<LookAtCamera>(GetStage()->GetCamera(0));
		if (PtrCamera){
			//LookAtCameraに注目するオブジェクト（プレイヤー）の設定
			PtrCamera->SetTargetObject(GetThis<GameObject>());
		}

		auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
		pMultiSoundEffect->AddAudioResource(L"MatPut");
		pMultiSoundEffect->AddAudioResource(L"MatDelete");
		pMultiSoundEffect->AddAudioResource(L"MatChange");

		//ステートマシンの構築PpPP
		m_StateMachine = make_shared< StateMachine<Player> >(GetThis<Player>());
		//最初のステートをDefaultStateに設定
		m_StateMachine->SetCurrentState(DefaultState::Instance());
		//DefaultStateの初期化実行を行う
		m_StateMachine->GetCurrentState()->Enter(GetThis<Player>());
		//ゲームエフェクト
		m_wpGameEffectManager = GetStage()->GetSharedGameObject<GameEffectManager>(L"GameEffectManager", false);
	}

	//更新
	void Player::Update(){
		m_StateMachine->Update();
		auto GameStagePtr = dynamic_pointer_cast<GameStage>(GetStage());
		if (GameStagePtr->GetStateMachine()->GetCurrentState() == GameOverState::Instance() ||
			GameStagePtr->GetStateMachine()->GetCurrentState() == GameClearState::Instance())
			{
				SetUpdateActive(false);
			}

	}

	//MoveToでState切り替えするためUpdate2使用中
	void Player::Update2(){
		auto GameStagePtr = dynamic_pointer_cast<GameStage>(GetStage());
		if (m_StateMachine->GetCurrentState() == MoveState::Instance()){
			//ステートがMoveStateなら
			auto PtrMoveTo = GetComponent<MoveTo>();
			if (PtrMoveTo->IsArrived()){
				//無効にする
				PtrMoveTo->SetUpdateAllActive(false);
				//移動終了なら
				m_StateMachine->ChangeState(DefaultState::Instance());
			}
		}

		auto PtrCol = GetComponent<CollisionSphere>();
		if (PtrCol->GetHitObject()){
			auto Enemy = dynamic_pointer_cast<SimpleEnemy>(PtrCol->GetHitObject());
			auto Box = dynamic_pointer_cast<ChildBox>(PtrCol->GetHitObject());
			if (Enemy){
				//PostEvent(1.0f, GetThis<Player>(), App::GetApp()->GetSceneBase(), L"ToGameClear");
				auto PtrStageManager = GameStagePtr->GetSharedGameObject<StageManager>(L"StageManager");
				PostEvent(0.0f, GetThis<Player>(), PtrStageManager, L"GoToGameOver");
			}
			else if (Box){
				Box->PlayerStop();
				auto PtrStageManager = GameStagePtr->GetSharedGameObject<StageManager>(L"StageManager");
				PostEvent(0.0f, GetThis<Player>(), PtrStageManager, L"GoToGameOver");
			}
		}
	}

	//デバッグ文字列表示するためにUpdate3使用中
	void Player::Update3(){

		auto fps = App::GetApp()->GetStepTimer().GetFramesPerSecond();
		wstring FPS(L"FPS: ");
		FPS += Util::UintToWStr(fps);
		FPS += L"\n";

		wstring StateStr;
		if (m_StateMachine->GetCurrentState() == DefaultState::Instance()){
			StateStr = L"DefaultState";
		}
		else if (m_StateMachine->GetCurrentState() == MoveState::Instance()){
			StateStr = L"MoveState";
		}
		else if (m_StateMachine->GetCurrentState() == MatPlantState::Instance()){
			StateStr = L"MatPlantState";
		}
		else if (m_StateMachine->GetCurrentState() == PushState::Instance()){
			StateStr = L"PushState";
		}
		StateStr += L"\n"; 
		auto PtrTransform = GetComponent<Transform>();
		wstring Str_vCellPos(L"Player_Cell:\t");
		Str_vCellPos += L"X=" + Util::FloatToWStr(PtrTransform->GetPosition().x, 6, Util::FloatModify::Fixed) + L",\t";
		Str_vCellPos += L"Y=" + Util::FloatToWStr(PtrTransform->GetPosition().z, 6, Util::FloatModify::Fixed) + L",\t";
		Str_vCellPos += L"\n";


		wstring str = FPS + StateStr + Str_vCellPos;
		//文字列をつける
		auto PtrString = GetComponent<StringSprite>();
		//PtrString->SetText(str);

	}

	//イベントの受け取り ポーズ画面等
	void Player::OnEvent(const shared_ptr<Event>& event){
		//ポーズ画面のときGameStageからL"Pause"送られてくる
		if (event->m_MsgStr == L"Pause"){
			//ポーズ中はSetUpdateActive(false)にする
			PauseObject();
		}
		//TODO : もうひとつポーズ解除のイベント作成予定
		else if (event->m_MsgStr == L"GamePlay"){
			//ポーズ中はSetUpdateActive(false)にする
			StartObject();
		}
	}

	//ポーズ中に呼ぶ関数
	void Player::PauseObject(){
		//アップデートをしなくする
		SetUpdateActive(false);
	}
	//ゲーム中はこれ呼ぶ
	void Player::StartObject(){
		SetUpdateActive(true);
	}

	//	---------- Method ----------.

	//プレイヤーの向きをQuatanionで変更する 
	void Player::SetPlayer_Rotation(PlayerRot EnumClass, Vector3& SetVec){
		switch (EnumClass)
		{
		case basedx11::PlayerRot::Back:
			//上向き
			SetVec = Vector3( 0, 0.0f * XM_PI / 180.0f, 0 );
			break;
		case basedx11::PlayerRot::Front:
			//下向き
			SetVec = Vector3(0, 180.0f * XM_PI / 180.0f, 0);
			break;
		case basedx11::PlayerRot::Left:
			//左向き
			SetVec = Vector3(0, 90.0f * XM_PI / 180.0f, 0);
			break;
		case basedx11::PlayerRot::Right:
			//右向き
			SetVec = Vector3(0, 270.0f * XM_PI / 180.0f, 0);
			break;
		default:
			assert(!"不正な値が入りました。PlayerRot　enumClassを確認してください");
			break;
		}
	}

	//@breif : これヘッダーにも同じ奴書けるんじゃね疑惑浮上
	//自分の座標のセルを返す
	size_t Player::ReturnColPos(){
		auto PtrGameStage = dynamic_pointer_cast<GameStage>(GetStage());
		assert(PtrGameStage);								//もしここでポインタが取れなかったらエラーまたはバグになる
		auto PtrTrans = GetComponent<Transform>();
		Vector3 PtrPlayerPos = PtrTrans->GetPosition();		//プレイヤーの位置取得			
		auto Col = (size_t)PtrPlayerPos.z;					//プレイヤーの位置からセル座標を特定
		return Col;
	}
	//	同上
	size_t Player::ReturnRowPos(){
		auto PtrGameStage = dynamic_pointer_cast<GameStage>(GetStage());
		assert(PtrGameStage);								//もしここでポインタが取れなかったらエラーまたはバグになる
		auto PtrTrans = GetComponent<Transform>();
		Vector3 PtrPlayerPos = PtrTrans->GetPosition();		//プレイヤーの位置取得			
		auto Row = (size_t)PtrPlayerPos.x;					//プレイヤーの位置からセル座標を特定
		return Row;
	}


	//プレイヤーが向く方向と進むVector3の情報
	void Player::Set_NextMove_PlayerRot(Vector2 Next, PlayerRot Rot){
		m_vNextMove = Next; 
		m_Player_Rot = Rot;
	}

	//プレイヤーが次行くポジションの情報をまとめる
	Vector3 Player::NextVec_Cell(){
		
		//Transformの取得
		auto PtrTransform = GetComponent<Transform>();

		int ToCelX = static_cast<int>(PtrTransform->GetPosition().x);
		int ToCelZ = static_cast<int>(PtrTransform->GetPosition().z);
		ToCelX += static_cast<int>(m_vNextMove.x);
		ToCelZ += static_cast<int>(m_vNextMove.y);

		return Vector3((float)ToCelX, 1.25f, (float)ToCelZ);
	}

	//マットがあるかないかでStateの変更
	void Player::MatDataCheck(bool cool){
		if (!cool){
			//自分の位置になにもなかったら
			m_StateMachine->ChangeState(MatPlantState::Instance());
		}
		else{
			//自分の位置にマットがあったら
			m_StateMachine->ChangeState(MatDeleteorChangeState::Instance());
		}
	}

	//矢印の情報とスティック操作の変更
	void Player::Set_Mat_Stick(Vector2 matrot, bool _true){
		//引数付きMotion関数で配置していく
		m_Arrow = matrot;
		//スティック操作をした
		m_Stick = _true;
		//マットがあるかないかでStateの変更
		MatDataCheck(IsCheckMatDataMotion());
	}

	// ---------- Motion ----------.

	//スティックが押されたらの取得　プレイヤーの向きを変更できるようにメンバ変数を変更する　修正↓
	bool Player::IsKeyDownMotion(){
		//コントローラーの取得
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();

		//説明用変数		スティックが操作されているか
		const bool StickLap_RX = CntlVec[0].fThumbRX < 0.2f && CntlVec[0].fThumbRX > -0.2f;
		const bool StickLap_RY = CntlVec[0].fThumbRY < 0.2f && CntlVec[0].fThumbRY > -0.2f;
		const bool Is_Stick_NoMove = StickLap_RX && StickLap_RY;

		//次に行く方向の初期化（0にしておく） 
		m_vNextMove = Vector2(0.0f, 0.0f);
		if (CntlVec[0].bConnected){
			if (Is_Stick_NoMove){
				//上入力
				if (CntlVec[0].fThumbLY > 0.8f){
					Set_NextMove_PlayerRot(Vector2(0.0f, 1.0f), PlayerRot::Front);
					return true;
				}
				//下入力
				else if (CntlVec[0].fThumbLY < -0.8f){
					Set_NextMove_PlayerRot(Vector2(0.0f, -1.0f), PlayerRot::Back);
					return true;
				}
				//右入力
				else if (CntlVec[0].fThumbLX > 0.8f){
					Set_NextMove_PlayerRot(Vector2(1.0f, 0.0f), PlayerRot::Left);
					return true;
				}
				//左入力
				else if (CntlVec[0].fThumbLX < -0.8f){
					Set_NextMove_PlayerRot(Vector2(-1.0f, 0.0f), PlayerRot::Right);
					return true;
				}
			}
		}
		return false;
	}

	//入力した場所に動かせるBoxがあるかチェックする
	bool Player::IsCheckMoveBox(){
		auto PtrGameStage = dynamic_pointer_cast<GameStage>(GetStage());
		auto PtrTransform = GetComponent<Transform>();
		size_t ToCelX = static_cast<size_t>(PtrTransform->GetPosition().x);
		size_t ToCelZ = static_cast<size_t>(PtrTransform->GetPosition().z);
		//次いく場所を足しておく
		ToCelX += static_cast<size_t>(m_vNextMove.x);
		ToCelZ += static_cast<size_t>(m_vNextMove.y);
		//次いく場所にMoveBoxがあるか取得
		auto TypeItem = PtrGameStage->GetFixedMat(ToCelZ, ToCelX);
		if (TypeItem == ItemCase::MoveBox ){
			//もしMoveBoxだったらステート切り替え
			return true;
		}
		return false;
	}

	//設定された移動方向に行けるかどうかチェックする
	bool Player::IsCheckNextMove(){
		//ここで必要であればステージからMAPを取得し、範囲チェックを行う
		auto PtrTransform = GetComponent<Transform>();
		int ToCelX = (int)PtrTransform->GetPosition().x;		//行		Row
		int ToCelZ = (int)PtrTransform->GetPosition().z;		//列		Col
		ToCelX += (int)m_vNextMove.x;
		ToCelZ += (int)m_vNextMove.y;
		//以下のように呼べるようにGameStageにチェック関数を作成するとよい
		auto GameStagePtr = dynamic_pointer_cast<GameStage>(GetStage());
		//クライアント(プレイヤー)　は結果だけを聞く
		//サーバーはGameStage　処理はそっちで
		if (GameStagePtr->GetCheckToDataMap(ToCelX, ToCelZ)){
			if (GameStagePtr->GetNextCheckMapData(ToCelX, ToCelZ)){
				//行ける
				return true;
			}
		}
		return false;
	}

	//セル移動
	void Player::CellMoveMotion(){
		//プレイヤーの移動速度
		const float PlayerMove_param = 0.23f;
		//MoveToの取得
		auto PtrMoveTo = GetComponent<MoveTo>();
		//MoveToを有効にする
		PtrMoveTo->SetUpdateAllActive(true);
		PtrMoveTo->SetParams(PlayerMove_param, NextVec_Cell());
		//実行
		PtrMoveTo->Run();
	}

	//プレイヤーの進む向きに合わせてモデルの向きを変更する
	void Player::Rotate_Change(){
		//回転
		auto PtrTrans = GetComponent<Transform>();
		Vector3 WorkVec;
		SetPlayer_Rotation(m_Player_Rot, WorkVec);
		PtrTrans->SetRotation(WorkVec);
	}

	//自分の位置にマットがあるか確認
	bool Player::IsCheckMatDataMotion(){
		auto PtrTrans = GetComponent<Transform>();
		auto PtrPlayerPos = PtrTrans->GetPosition();
		auto GameStagePtr = dynamic_pointer_cast<GameStage>(GetStage());

		//自分のポジションを四捨五入して代入
		int Row = static_cast<int>(PtrPlayerPos.x);
		int Col = static_cast<int>(PtrPlayerPos.z);

		//GameStageから参照したものをコピー
		auto&  Vec = GameStagePtr->GetArrowVec();

		if (!Vec[Col][Row].expired()){
			//そのセル座標にマットがあったら
			return true;
		}
		//なにもなかったら
		return false;
	}

	//マット配置するときの右スティックで取得
	void Player::MatSelectMethod(){
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		
		//説明用変数 スティックの設定
		bool StickLap_X = CntlVec[0].fThumbRX < 0.2f && CntlVec[0].fThumbRX > -0.2f;
		bool StickLap_Y = CntlVec[0].fThumbRY < 0.2f && CntlVec[0].fThumbRY > -0.2f;

		//接続されているか
		if (CntlVec[0].bConnected){
			//スティックがあまり傾いていない時など
			if (StickLap_X && StickLap_Y){
				//スティック操作をまだしてない
				m_Stick = false;
			}

			else if (!m_Stick){
					//右スティックの傾きが0.85以上になったら		上向きのマット配置
					if (CntlVec[0].fThumbRY > 0.85f){
						Set_Mat_Stick(Vector2(0, 1), true);
					}

					//右スティックが-0.85f以下になったら		下向きのマット配置
					else if (CntlVec[0].fThumbRY < -0.85f){
						Set_Mat_Stick(Vector2(0, -1), true);
					}

					//右スティックが-0.85f以下になったら		左向きのマット配置
					else if (CntlVec[0].fThumbRX < -0.85f){
						Set_Mat_Stick(Vector2(-1, 0), true);
					}

					//右スティックが0.85f以上になったら		右向きのマット配置
					else if (CntlVec[0].fThumbRX > 0.85f){
						Set_Mat_Stick(Vector2(1, 0), true);
					}
				}

		}
	}

	/*
	@MatSelectMethod()で呼ばれる	i_Arrowで矢印の種類を決める		MatChangeMotion & MatDeleteMotion　改良済み
	@breif : ArrowObjectCreateMotion()を参考に
	@name : yuya
	@day : 2015/10/24
	*/
	void Player::MatCreateMotion(){
		auto PtrGameStage = dynamic_pointer_cast<GameStage>(GetStage());
		assert(PtrGameStage);								//もしここでポインタが取れなかったらエラーまたはバグになる 

		auto PtrTrans = GetComponent<Transform>();
		Vector3 PtrPlayerPos = PtrTrans->GetPosition();		//プレイヤーの位置取得
		auto Row = (size_t)PtrPlayerPos.x;					//プレイヤーの位置からセル座標を特定
		auto Col = (size_t)PtrPlayerPos.z;					//同上

		auto PtrArrowObj = PtrGameStage->AddGameObject<ArrowObject>(PtrPlayerPos, m_Arrow);
		vector< vector< weak_ptr<ArrowObject> > >& Vec = PtrGameStage->GetArrowVec();		//vector配列の参照したものをコピーする
		Vec[Col][Row] = PtrArrowObj;

		//マット作成後、State切り替え
		m_StateMachine->ChangeState(DefaultState::Instance());

		auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
		pMultiSoundEffect->Start(L"MatPut", 0, 0.3f);

	}

	//マットの向きと画像を変更する
	void Player::MatChangeMotion(){
		auto PtrTrans = GetComponent<Transform>();
		auto PtrPlayerPos = PtrTrans->GetPosition();
		auto GameStagePtr = dynamic_pointer_cast<GameStage>(GetStage());
		//-----------------------------------

		auto Row = (int)PtrPlayerPos.x;
		auto Col = (int)PtrPlayerPos.z;

		//GameStageから参照したものをコピー
		vector< vector< weak_ptr<ArrowObject> > >&  Vec = GameStagePtr->GetArrowVec();

		if (!Vec[Col][Row].expired()){
			//そのセル座標にマットがあったら
			auto ParSh = Vec[Col][Row].lock();
			if (ParSh){
				ParSh->ChangeArrow(m_Arrow);
			}
		}
		//変更後、State切り替え
		m_StateMachine->ChangeState(DefaultState::Instance());

		auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
		pMultiSoundEffect->Start(L"MatChange", 0, 0.2f);

	}

	//マットを削除
	void Player::MatDeleteMotion(){
		auto PtrTrans = GetComponent<Transform>();
		auto PtrPlayerPos = PtrTrans->GetPosition();

		auto GameStagePtr = dynamic_pointer_cast<GameStage>(GetStage());
		
		//GameStageから参照したものをコピー
		vector< vector< weak_ptr<ArrowObject> > >&  Vec = GameStagePtr->GetArrowVec();

		auto Row = (int)PtrPlayerPos.x;
		auto Col = (int)PtrPlayerPos.z;
		
		if (!Vec[Col][Row].expired()){
			auto ParSh = Vec[Col][Row].lock();
			if (ParSh){
				ParSh->Delete();
				Vec[Col][Row].reset();

			}
		}
		//マット削除後、State切り替え
		m_StateMachine->ChangeState(DefaultState::Instance());
		//SEの再生
		auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
		pMultiSoundEffect->Start(L"MatDelete", 0, 0.3f);

	}

	//現在押したときのm_ArrowとArrowObjectが保持している向きの情報と同じか
	bool Player::IsArrowToArrowid(){
		auto PtrTrans = GetComponent<Transform>();
		auto PtrPlayerPos = PtrTrans->GetPosition();
		auto GameStagePtr = dynamic_pointer_cast<GameStage>(GetStage());
		//-----------------------------------

		int Row = static_cast<int>(PtrPlayerPos.x);
		int Col = static_cast<int>(PtrPlayerPos.z);

		//GameStageから参照したものをコピー
		auto&  Vec = GameStagePtr->GetArrowVec();
		Vector2 ArrowId(0, 0);
		if (!Vec[Col][Row].expired()){
			auto ParSh = Vec[Col][Row].lock();
			if (ParSh){
				ArrowId = ParSh->GetArrowid();
			}
		}
		//判別しているとこ
		if (m_Arrow == ArrowId){
			//同じならMat削除する
			return true;
		}
		else{
			//違うならMatChangeをする
			return false;
		}
	}

	//イベント送信
	void Player::EventMove(){
		auto GameStagePtr = dynamic_pointer_cast<GameStage>(GetStage());
		auto& wpMove = GameStagePtr->GetMoveBox();
		auto ShMove = wpMove.lock();
		//現在進行しようとしている方向をBoxに伝える
		ShMove->SetPushVec(m_vNextMove);
		//イベントを送りMoveBoxを動かせるかあっちで操作する
		PostEvent(0.0f, GetThis<Player>(), ShMove, L"Move");
		GetStateMachine()->ChangeState(DefaultState::Instance());
	}

	//プレイヤーが固定マットの上にいるか
	bool Player::IsCheckFixedMat(){
		//Transformと自分の位置の取得
		//TODO : なんども同じことしてるから、なにかにまとめれないか
		auto PtrTrans = GetComponent<Transform>();
		auto PtrPlayerPos = PtrTrans->GetPosition();
		auto GameStagePtr = dynamic_pointer_cast<GameStage>(GetStage());
		//-----------------------------------

		int Row = static_cast<int>(PtrPlayerPos.x);
		int Col = static_cast<int>(PtrPlayerPos.z);
		auto fixedmattype = GameStagePtr->GetFixedMat(Col, Row);
		//固定マットがないところでしか、マットを配置することが出来ないようにする
		if (fixedmattype == ItemCase::Empty){
			return true;
		}
		else if (fixedmattype > ItemCase::Empty && IsRightStick()){
			//SEの再生
			//TODO : 音の種類を変える
			auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
			pMultiSoundEffect->Start(L"MatDelete", 0, 0.3f);

			return false;
		}
		return false;
	}
	
	//右スティックの操作を取得
	bool Player::IsRightStick(){
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		//説明用変数 スティックの設定
		bool StickLap_X = CntlVec[0].fThumbRX < 0.2f && CntlVec[0].fThumbRX > -0.2f;
		bool StickLap_Y = CntlVec[0].fThumbRY < 0.2f && CntlVec[0].fThumbRY > -0.2f;

		//接続されているか
		if (CntlVec[0].bConnected){
			//スティックがあまり傾いていない時など
			if (StickLap_X && StickLap_Y){
				//スティック操作をまだしてない
				m_RightStick = false;
			}
			else if (!m_RightStick){
				//右スティックの傾きが0.85以上になったら		上向きのマット配置
				if (CntlVec[0].fThumbRY > 0.85f){
					m_RightStick = true;
					return true;
				}

				//右スティックが-0.85f以下になったら		下向きのマット配置
				else if (CntlVec[0].fThumbRY < -0.85f){
					m_RightStick = true;
					return true;
				}
				//右スティックが-0.85f以下になったら		左向きのマット配置
				else if (CntlVec[0].fThumbRX < -0.85f){
					m_RightStick = true;
					return true;
				}
				//右スティックが0.85f以上になったら		右向きのマット配置
				else if (CntlVec[0].fThumbRX > 0.85f){
					m_RightStick = true;
					return true;
				}
				return false;
			}
			//なにも入力されてない
			return false;
		}
		return false;
	}
	//--------------------------------------------------------------------------------------
	//	class DefaultState : public ObjState<Player>;
	//	用途: 待機
	//--------------------------------------------------------------------------------------
	//ステートのインスタンス取得
	shared_ptr<DefaultState> DefaultState::Instance(){
		static shared_ptr<DefaultState> instance;
		if (!instance){
			instance = shared_ptr<DefaultState>(new DefaultState);
		}
		return instance;
	}
	//ステートに入ったときに呼ばれる関数
	void DefaultState::Enter(const shared_ptr<Player>& Obj){
		//何もしない
	}
	//ステート実行中に毎ターン呼ばれる関数
	void DefaultState::Execute(const shared_ptr<Player>& Obj){
		/*if (Obj->IsKeyDownMotion() &&  && Obj->IsCheckNextMove()){
			Obj->GetStateMachine()->ChangeState(PushState::Instance());
		}*/
		
		//スティックが倒されて、移動方向が有効ならステート変更
		if (Obj->IsKeyDownMotion() && Obj->IsCheckNextMove()){
			if (Obj->IsCheckMoveBox()){
				//もしMoveBoxがあったら
				Obj->GetStateMachine()->ChangeState(PushState::Instance());
			}
			else{
				//Boxがなく、道のみの場合移動する
				Obj->GetStateMachine()->ChangeState(MoveState::Instance());
			}
		}
	
		//マットを置く操作
		if (Obj->IsCheckFixedMat()){
			Obj->MatSelectMethod();
		}
	}

	//--------------------------------------------------------------------------------------
	//	class PushState : public ObjState<Player>;
	//	用途: 物を動かす（ボックス）
	//--------------------------------------------------------------------------------------
	//ステートのインスタンス取得
	shared_ptr<PushState> PushState::Instance(){
		static shared_ptr<PushState> instance;
		if (!instance){
			instance = shared_ptr<PushState>(new PushState);
		}
		return instance;
	}
	//ステートに入ったときに呼ばれる関数
	void PushState::Enter(const shared_ptr<Player>& Obj){
		Obj->EventMove();
	}
	
	//--------------------------------------------------------------------------------------
	//	class MoveState : public ObjState<Player>;
	//	用途: 移動
	//--------------------------------------------------------------------------------------
	//ステートのインスタンス取得
	shared_ptr<MoveState> MoveState::Instance(){
		static shared_ptr<MoveState> instance;
		if (!instance){
			instance = shared_ptr<MoveState>(new MoveState);
		}
		return instance;
	}
	//ステートに入ったときに呼ばれる関数
	void MoveState::Enter(const shared_ptr<Player>& Obj){
		//MoveTo開始
		Obj->CellMoveMotion();
		//プレイヤーの向きをスティック入力に合わせて変更
		Obj->Rotate_Change();
		
	}
	//ステート実行中に毎ターン呼ばれる関数
	void MoveState::Execute(const shared_ptr<Player>& Obj){

	}
	//ステートにから抜けるときに呼ばれる関数
	void MoveState::Exit(const shared_ptr<Player>& Obj){
		//何もしない
	}


	//--------------------------------------------------------------------------------------
	//	class MatPlantState : public ObjState<Player>;
	//	用途: マット設置
	//--------------------------------------------------------------------------------------
	//ステートのインスタンス取得
	shared_ptr<MatPlantState> MatPlantState::Instance(){
		static shared_ptr<MatPlantState> instance;
		if (!instance){
			instance = shared_ptr<MatPlantState>(new MatPlantState);
		}
		return instance;
	}
	//ステートに入ったときに呼ばれる関数
	void MatPlantState::Enter(const shared_ptr<Player>& Obj){
		//マット設置
		Obj->MatCreateMotion();
	}
	//ステート実行中に毎ターン呼ばれる関数
	void MatPlantState::Execute(const shared_ptr<Player>& Obj){

	}
	//ステートにから抜けるときに呼ばれる関数
	void MatPlantState::Exit(const shared_ptr<Player>& Obj){
		//何もしない
	}



	//--------------------------------------------------------------------------------------
	//	class MatDeleteorChangeState : public ObjState<Player>;
	//	用途: マット削除
	//--------------------------------------------------------------------------------------
	//ステートのインスタンス取得
	shared_ptr<MatDeleteorChangeState> MatDeleteorChangeState::Instance(){
		static shared_ptr<MatDeleteorChangeState> instance;
		if (!instance){
			instance = shared_ptr<MatDeleteorChangeState>(new MatDeleteorChangeState);
		}
		return instance;
	}
	//ステートに入ったときに呼ばれる関数
	void MatDeleteorChangeState::Enter(const shared_ptr<Player>& Obj){
		//ここで同じ入力がきたか調べる
		//bool型　m_ArrowとArrowObjectのm_Arrowidが同じなら再度同じ入力
		if (Obj->IsArrowToArrowid()){
			//マット削除
			Obj->MatDeleteMotion();
		}
		else{
			//マットの向きと画像を変える
			Obj->MatChangeMotion();
		}
	}
	//ステート実行中に毎ターン呼ばれる関数
	void MatDeleteorChangeState::Execute(const shared_ptr<Player>& Obj){
	}
	//ステートにから抜けるときに呼ばれる関数
	void MatDeleteorChangeState::Exit(const shared_ptr<Player>& Obj){
		//何もしない
	}
}
//endof  basedx11
