#pragma once

#include "stdafx.h"

namespace basedx11{


	class GameEffectManager;

	//--------------------------------------------------------------------------------------
	//	class Player : public GameObject;
	//	用途: プレイヤー
	//--------------------------------------------------------------------------------------
	class Player : public GameObject{
	public:
		//構築と破棄
		Player(const shared_ptr<Stage>& StagePtr, const Vector2 Pos);
		virtual ~Player(){}

		// ---------- Accessor ----------.
		shared_ptr< StateMachine<Player> > GetStateMachine() const{
			return m_StateMachine;
		}
		//	: エフェクト管理システムを取得する.
		const weak_ptr< GameEffectManager >GetGameEffectManager() const
		{
			return m_wpGameEffectManager;
		}

		//	: エフェクト管理システムを設定する.
		void SetGameEffectManager(const weak_ptr< GameEffectManager > i_wpGameEffectManager)
		{
			m_wpGameEffectManager = i_wpGameEffectManager;
		}

		// ---------- Motion ----------.

		//スティックの傾きでbool型を返す
		bool IsKeyDownMotion();
		//設定された移動方向に行けるかどうかチェックする
		bool IsCheckNextMove();
		//セル移動
		void CellMoveMotion();
		//プレイヤーの進む向きに合わせてモデルの向きを変更する
		void Rotate_Change();
		//プレイヤーが固定マットの上にいるか
		bool IsCheckFixedMat();
		//右スティックの操作を取得
		bool IsRightStick();

		//@day : 2015/12/04
		//入力した場所に動かせるBoxがあるか
		bool IsCheckMoveBox();
		//イベント送る
		void EventMove();
		
		//@brief : ABXYでマット配置	SelectArrowMortion()を参考に
		//@name : yuya
		//@day : 2015/10/24
		void MatSelectMethod();
		//MatSelectMethodで選択したボタンによって矢印の種類を変えるMotion関数
		void MatCreateMotion();
		//Matがあった場合、画像と向きを変えるだけ
		void MatChangeMotion();
		//自分の位置にマットがあるかチェックする
		bool IsCheckMatDataMotion();
		//マット削除
		void MatDeleteMotion();
		//現在押したときのm_ArrowとArrowObjectが保持している向きの情報と同じか
		bool IsArrowToArrowid();
		//ポーズ中に呼ぶ関数
		void PauseObject();
		//ゲームプレイ中に移行したときに読んであげる
		void StartObject();

		//初期化
		virtual void Create() override;
		//更新
		virtual void Update() override;
		virtual void Update2() override;
		virtual void Update3() override;
		//イベント
		virtual void OnEvent(const shared_ptr<Event>& event)override;

	private:
		//	---------- Method ----------.
		size_t ReturnRowPos();
		size_t ReturnColPos();
		void SetPlayer_Rotation(PlayerRot nam, Vector3& SetVec);		//プレイヤーの向きをQuatanionで変更する
		//プレイヤーが向く方向と進むVector3の情報
		void Set_NextMove_PlayerRot(Vector2 Next, PlayerRot Rot);
		//プレイヤーが次行くポジションの情報をまとめる
		Vector3 NextVec_Cell();
		//マットがあるかないかでStateの変更
		void MatDataCheck(bool cool);
		//矢印の情報とスティック操作の変更
		void Set_Mat_Stick(Vector2 matrot, bool _true);

		// ---------- member ----------.
		shared_ptr< StateMachine<Player> >  m_StateMachine;	//ステートマシーン
		Vector2 m_vNextMove;		//移動方向
		Vector2 m_Arrow;			//選択した矢印を保存する
		Vector2 FirstPos;			//初期位置
		PlayerRot m_Player_Rot;		//プレイヤーの向き
		weak_ptr< GameEffectManager > m_wpGameEffectManager;	//	: エフェクト管理システム.

		bool m_Stick;				//スティックが何度も取得されるのを防ぐ物
		bool m_RightStick;


		/**
		* @class StartStage                //ここにクラスの名前
		* @brief スタートステージ作成.	       //主な説明
		* @author yuyatanpe                //これの作成者
		* @date 2015/10/06 Start           //始めた日付
		*/

		//// ---------- Method ----------.
		//class InputKey {
		//	InputKey(InputKey& ik);
		//	virtual ~InputKey(){}
		//	bool Button_A();
		//	bool Button_B();
		//	bool Button_X();
		//	bool Button_Y();
		//	InputKey* i_key;

		//};
	};

	//--------------------------------------------------------------------------------------
	//	class DefaultState : public ObjState<Player>;
	//	用途: 通常移動
	//--------------------------------------------------------------------------------------
	class DefaultState : public ObjState<Player>
	{
		DefaultState(){}
	public:
		//ステートのインスタンス取得
		static shared_ptr<DefaultState> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<Player>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<Player>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<Player>& Obj)override{}
	};

		//--------------------------------------------------------------------------------------
		//	class PushState : public ObjState<Player>;
		//	用途: 物を動かす（ボックス）
		//--------------------------------------------------------------------------------------
	class PushState : public ObjState<Player>
	{
		PushState(){}
	public:
		//ステートのインスタンス取得
		static shared_ptr<PushState> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<Player>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<Player>& Obj)override{}
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<Player>& Obj)override{}
	};


	//--------------------------------------------------------------------------------------
	//	class MoveState : public ObjState<Player>;
	//	用途: 通常移動
	//--------------------------------------------------------------------------------------
	class MoveState : public ObjState<Player>
	{
		MoveState(){}
	public:
		//ステートのインスタンス取得
		static shared_ptr<MoveState> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<Player>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<Player>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<Player>& Obj)override;
	};

	//--------------------------------------------------------------------------------------
	//	class MatPlantState : public ObjState<Player>;
	//	用途: マット設置
	//--------------------------------------------------------------------------------------
	class MatPlantState : public ObjState<Player>
	{
		MatPlantState(){}
	public:
		//ステートのインスタンス取得
		static shared_ptr<MatPlantState> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<Player>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<Player>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<Player>& Obj)override;
	};


	//--------------------------------------------------------------------------------------
	//	class MatDeleteorChangeState : public ObjState<Player>;
	//	用途: マット削除
	//--------------------------------------------------------------------------------------
	class MatDeleteorChangeState : public ObjState<Player>
	{
		MatDeleteorChangeState(){}
	public:
		//ステートのインスタンス取得
		static shared_ptr<MatDeleteorChangeState> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<Player>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<Player>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<Player>& Obj)override;
	};

}
//endof  basedx11
