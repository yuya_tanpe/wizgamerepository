#pragma once

namespace basedx11 {

	//enum　class に対応させる
	// @Todo : 内部的での使い方

	//オブジェクトのデータID　修正
	enum DataID {
		NULLID = 0,
		ROAD,
		PLAYERSPAWN,
		BLUECUBE,		//青色CUBE		3
		BLUEGOAL,		//青色のGOAL		4
		REDCUBE,		//赤色CUBE		5
		REDGOAL,		//赤色のGOAL		6
		PURPLECUBE,		//紫色			7
		PURPLEGOAL,		//紫色			8
		YELLOCUBE,		//			9
		YELLOGOAL		//黄色			10
	};

	

	
	//矢印のデータID
	enum ArrowID {
		NULLARROW = 0,
		RIGHT,			//右
		DOWN,			//下
		LEFT,			//左
		UP				//上
	};

	//向き変えるときに必要	メモリ領域を1バイト？にしてみた
	enum class EnamyMoveRot{
		Front,
		Back,
		Right,
		Left,
	};

	enum class ItemCase : char{
		Empty,
		ScoreRed,
		ScoreBlue,
		ScoreGreen,
		ScorePurple,
		Reverse,
		ColorRed,
		ColorBlue,
		ColorPurple,
		ColorGreen,
		MoveBox,
	};

	//プレイヤーの向き　
	enum class PlayerRot {
		Front,
		Back,
		Left,
		Right,
	};
	//ポーズ画面でのアイコンに使用	0 ~ 2まで
	enum PauseCase{
		None,				//うんこ
		BacktoGame,			//ゲームに戻る
		RestartGame,		//ゲームをリスタートする
		MovetoSelectStage,	//ステージセレクトに移動する
	};
}


#include "resource.h"
#include "Scene.h"
#include "GameStage.h"
#include "Character.h"
#include "Player.h"
#include "Enamy.h"
#include "StartStage.h"
#include "GameClearStage.h"
#include "StartStageUi.h"
#include "SpriteObject.h"
#include "GazeLookCamera.h"
#include "ColorCube.h"
#include "GameStageUI.h"
#include "ArrowObject.h"
#include "CUBEMove.h"
#include "Make_box_type.h"
#include "MoveEffect.h"
#include "GameEffect.h"
#include "StageManager.h"
#include "ModeSelectStage.h"
#include "MoveCameraObject.h"
#include "Background.h"
#include "GameTimeSprite.h"
#include "FixedMat.h"
#include "Fade.h"
#include "Various_Items.h"
#include "WorldSelectScene.h"
#include "Load_Resource.h"
#include "Star_Object.h"
#include "ScoreUp_Item.h"
