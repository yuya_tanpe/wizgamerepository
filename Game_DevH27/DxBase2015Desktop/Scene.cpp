#include "stdafx.h"
#include "Project.h"

namespace basedx11{

	//リソース作成
	void Scene::CreateResorce(){

		//ワーク用配列
		vector< vector<wstring> > wsTextureTable;

		struct NameTable
		{
			wstring texture;
			wstring resource;
			NameTable(const wstring& str1, const wstring& str2) :
				texture(str1),
				resource(str2){}
		};

		//ネームテーブルの作成	Textureの追加はここで
		vector<NameTable> nameTable = {
			{ L"Scene\\Title.png", L"START_TX" },
			{ L"Scene\\PlayGame.png", L"PLAYGAME_TX" },
			{ L"GameStateUI\\AButtonSelect.png", L"ABUTTON_TX" },
			{ L"GameStateUI\\BButtonSelect.png", L"BBUTTON_TX" },
			{ L"GameStateUI\\Apng.png", L"ARETRY_TX" },
			{ L"GameStateUI\\SelectIcon.png", L"PAUSEICON_TX" },
			{ L"GameStateUI\\Pazzule_Stage1.png", L"PAZZULE_STAGE1_TX" },
			{ L"GameStateUI\\PauseTex.png", L"PAUSE_TX" },
			{ L"GameStateUI\\frame.png", L"FRAME_TX" },
			{ L"Player\\PlayerColor.png", L"PLAYERCOLOR_TX" },

			//ステージの色関連
			{ L"Stage\\orange_ds.png", L"STAGEBOXORANGE_TX" },
			{ L"Stage\\Blue_ds.png", L"STAGEBOXBLUE_TX" },
			{ L"Stage\\red.png", L"STAGEBOXRED_TX" },

			//動かせるCUBEのテクスチャ
			{ L"Stage\\alfa.png", L"ALFA_TX" },

			{ L"Enemy\\green.png", L"GREENENEMY_TX" },
			{ L"Enemy\\RedEnamy.png", L"REDENEMY_TX" },
			{ L"CubeGoal\\purpleGoal.png", L"PURPLEGOAL_TX" },
			{ L"CubeGoal\\WhiteGoal.png", L"WHITEGOAL_TX" },
			{ L"CubeGoal\\BlueGoal.png", L"BLUEGOAL_TX" },
			{ L"CubeGoal\\RedGoal.png", L"REDGOAL_TX" },
			{ L"CubeGoal\\GreenGoal.png", L"GREENGOAL_TX" },
			{ L"Cube\\Blue.png", L"BLUECUBE_TX" },
			{ L"Cube\\Purple.png", L"PURPLECUBE_TX" },
			{ L"Cube\\white.png", L"WHITECUBE_TX" },
			{ L"Cube\\red.png", L"REDCUBE_TX" },
			{ L"Cube\\green.png", L"GREENCUBE_TX" },
			{ L"Arrow\\ArrowUp.png", L"ARROWUP_TX" },
			{ L"Arrow\\ArrowDown.png", L"ARROWDOWN_TX" },
			{ L"Arrow\\ArrowRight.png", L"ARROWRIGHT_TX" },
			{ L"Arrow\\ArrowLeft.png", L"ARROWLEFT_TX" },
			{ L"Arrow\\reverse.png", L"REVERSE_TX" },
			{ L"Arrow\\ChangeColorRed.png", L"CHANGERED_TX" },
			{ L"Arrow\\ChangeColorBlue.png",L"CHANGEBLUE_TX" },
			{ L"Arrow\\ChangeColorPurple.png",L"CHANGEPURPLE_TX"},
			{ L"Arrow\\ChangeColorGreen.png", L"CHANGEGREEN_TX" },
			{ L"Effect\\spark.png", L"SPARK_TX" },
			{ L"Effect\\ArrowUpEffect.png", L"ARROWUPEFFECT_TX" },
			{ L"Effect\\ArrowDownEffect.png", L"ARROWDOWNEFFECT_TX" },
			{ L"Effect\\ArrowRightEffect.png", L"ARROWRIGHTEFFECT_TX" },
			{ L"Effect\\ArrowLeftEffect.png", L"ARROWLEFTEFFECT_TX" },
			{ L"Effect\\light.bmp",			  L"LIGHT_TX" },
			{ L"Effect\\lineFX.bmp",		  L"LINEFX_TX" },
			{ L"Effect\\circle01.png",  	  L"RED_CIRCLE" },
			{ L"Effect\\circle02.png",		  L"BLUE_CIRCLE" },
			{ L"Effect\\circle03.png",		  L"PURPLE_CIRCLE" },
			{ L"Effect\\circle04.png",		  L"GREEN_CIRCLE" },

			{ L"Effect\\efect01.png",		  L"RED_CUBE" },
			{ L"Effect\\efect02.png",		  L"GREEN_CUBE" },
			{ L"Effect\\efect03.png",		  L"PURPLE_CUBE" },
			{ L"Effect\\efect04.png",		  L"BLUE_CUBE" },
			{ L"Effect\\Ring01.png",		  L"RING" },

			{ L"BackGround\\gias.png", L"BACKGROUND_TX" },
			{ L"TimeSprite\\number_orange.png", L"NUMBER_TX" },
			{ L"TimeSprite\\Time.png", L"PLAYTIME_TX" },
			{ L"TimeSprite\\Second.png", L"SECOND_TX" },
			{ L"TimeSprite\\YButtonSelect.png", L"YBUTTONSELECT_TX" },
			{ L"TimeSprite\\AButtonTitle.png", L"ABUTTONTITLE_TX" },
			{ L"TimeSprite\\PutNumber.png", L"PUTNUMBER_TX" },
			{ L"TimeSprite\\AButtonRiselt.png", L"ABUTTONRISELT_TX" },
			{ L"TimeSprite\\Time.png", L"TIME_TX" },
			{ L"TimeSprite\\TotalScore.png", L"TOTALSCORE_TX" },
			{ L"TimeSprite\\NextStage.png", L"NEXTSTAGE_TX" },
			{ L"TimeSprite\\scorepoint.png",L"SCOREPOINT_TX"},
			{ L"TimeSprite\\batu.png", L"BATU_TX" },
			{ L"TimeSprite\\Mainasu.png", L"MAINASU_TX" },
			{ L"Fade\\Black.png", L"BLOCKBLACK_TX" },
			{ L"ColorCUBEIcon\\BlueBox.png", L"BLUEICON_NOT" },
			{ L"ColorCUBEIcon\\GreenBox.png", L"GREENICON_NOT" },
			{ L"ColorCUBEIcon\\PurpleBox.png", L"PURPLEICON_NOT" },
			{ L"ColorCUBEIcon\\RedBox.png", L"REDICON_NOT" },
			//スコア関連
			{ L"ScoreSprite\\Plus100.png",		 L"SCORE100" },
			{ L"ScoreSprite\\Plus200.png",		 L"SCORE200" },
			{ L"ScoreSprite\\Plus400.png",		 L"SCORE400" },
			//白色エフェクト
			{ L"Effect\\bannou.png",			 L"BANNOU" },
			{ L"Effect\\starstar.png",			 L"STARBAN" },
			//チュートリアル関連
			{ L"GameStateUI\\Tutorial01.png",	 L"TUTORIAL01" },
			{ L"GameStateUI\\Tutorial02.png", L"TUTORIAL02" },
			{ L"GameStateUI\\Tutorial03.png", L"TUTORIAL03" },
			{ L"GameStateUI\\Tutorial04.png", L"TUTORIAL04" },

			//RB LBのボタン
			{ L"GameStateUI\\LB_Button.png", L"LB_D" },
			{ L"GameStateUI\\LB_Light.png", L"LB_L" },
			{ L"GameStateUI\\RB_Button.png", L"RB_D" },
			{ L"GameStateUI\\RB_Light.png", L"RB_L" },

			//ステージセレクト関連
			{ L"UI\\waku.png", L"FRAME" },
			{ L"UI\\mark.png", L"MARKMAT" },
			{ L"UI\\HyScore.png", L"HYSCORE" },
			{ L"UI\\UsedMat.png", L"USEDMAT" },
			{ L"UI\\Stage.png", L"STAGESELECT_TX" },
			{ L"UI\\Ready.png", L"READY" },
			{ L"UI\\Go.png", L"GO" },

			//スピードアップ、ダウン関連の画像
			{ L"UI\\SpeedUp.png", L"ROLLUP" },
			{ L"UI\\speeddown.png", L"ROLLDOWN" },
			{ L"UI\\SpeedUp_Not.png", L"ROLLUP_NOT" },
			{ L"UI\\speeddown_not.png", L"ROLLDOWN_NOT" },

			//リザルト関連
			{ L"GameStateUI\\back.png", L"BACKSELECT" },
			{ L"GameStateUI\\next.png", L"NEXTSTAGE" },

			//32ビットの数字
			{ L"TimeSprite\\number1.png", L"SCORENUMBER" },
			{ L"UI\\ScoreMoji.png", L"SCOREMOJI" },


		};

		for (auto& nt : nameTable){
			wsTextureTable.push_back({ nt.texture, nt.resource });
		}

		//	: テクスチャを登録.
		for (auto& wsTexture : wsTextureTable) {
			const wstring wsTexturePath = App::GetApp()->m_wstrRelativeDataPath + wsTexture.at(0);
			const wstring wsKeyName = wsTexture.at(1);
			App::GetApp()->RegisterTexture(wsKeyName, wsTexturePath);
		}

		//ワーク用配列
		vector< vector<wstring> > wsWavTable;

		struct WavTable
		{
			wstring music;
			wstring resource;
			WavTable(const wstring& str1, const wstring& str2) :
				music(str1),
				resource(str2){}
		};

		//ネームテーブルの作成	BGM SEの追加はここで
		vector<WavTable> WavTable = {
			{ L"Sound\\cursor.wav", L"Cursor" },
			{ L"Sound\\MatDelete.wav", L"MatDelete" },
			{ L"Sound\\Move.wav", L"Move" },
			{ L"Sound\\Goal.wav", L"Goal" },
			{ L"Sound\\bubu.wav", L"BUBU" },
			{ L"Sound\\MatPut.wav", L"MatPut" },
			{ L"Sound\\MatHit.wav", L"MatHit" },
			{ L"Sound\\GameOver.wav", L"GameOver" },
			{ L"Sound\\EnamyHit.wav", L"EnamyHit" },
			{ L"Sound\\CUBEGoal.wav", L"CUBEGoal" },
			{ L"Sound\\CUBEALLGOAL.wav", L"CUBEALLGOAL" },
			{ L"Sound\\BGM.wav", L"BGM" },
			{ L"Sound\\MatChange.wav", L"MatChange" },
			{ L"Sound\\ScoreCount.wav", L"SCORECOUNT" },
			{ L"Sound\\Start.wav", L"START" },
			{ L"Sound\\Score.wav", L"SCOREUP"},	
			{ L"Sound\\ModeSelect.wav", L"STAGESELECT"},
			{ L"Sound\\PauseSE.wav",		L"PAUSESE" },
			{ L"Sound\\OK.wav",				L"PAUSEOK" },
			{ L"Sound\\KETTEI.wav",			L"KETTEI" },
			{ L"Sound\\ColorSE.wav",		L"COLORSE" },
			{ L"Sound\\REVERSE.wav",		L"REVERSE" }

		};

		for (auto& nt : WavTable){
			wsWavTable.push_back({ nt.music, nt.resource });
		}

		//BGM SEの登録.
		for (auto& wsWav : wsWavTable) {
			const wstring wsWavPath = App::GetApp()->m_wstrRelativeDataPath + wsWav.at(0);
			const wstring wsKeyName = wsWav.at(1);
			App::GetApp()->RegisterWav(wsKeyName, wsWavPath);
		}


		auto DataDir = App::GetApp()->m_wstrRelativeDataPath + L"Model\\";
		auto PtrFbxScene = App::GetApp()->RegisterFbxScene(L"HUMAN_FBX", DataDir, L"Human_master.fbx");
		auto PtrFbxMesh = App::GetApp()->RegisterFbxMesh(L"HUMAN_MESH", PtrFbxScene, 0);
		PtrFbxMesh->AddAnimation("walk", 0, 24, true);

		DataDir = App::GetApp()->m_wstrRelativeDataPath + L"Model\\";
		PtrFbxScene = App::GetApp()->RegisterFbxScene(L"CUBERED_FBX", DataDir, L"RedCube.fbx");
		PtrFbxMesh = App::GetApp()->RegisterFbxMesh(L"CUBERED_MESH", PtrFbxScene, 0);

		DataDir = App::GetApp()->m_wstrRelativeDataPath + L"Model\\";
		PtrFbxScene = App::GetApp()->RegisterFbxScene(L"CUBEPURPLE_FBX", DataDir, L"PurpleCube.fbx");
		PtrFbxMesh = App::GetApp()->RegisterFbxMesh(L"CUBEPURPLE_MESH", PtrFbxScene, 0);

		DataDir = App::GetApp()->m_wstrRelativeDataPath + L"Model\\";
		PtrFbxScene = App::GetApp()->RegisterFbxScene(L"CUBEGREEN_FBX", DataDir, L"GreenCube.fbx");
		PtrFbxMesh = App::GetApp()->RegisterFbxMesh(L"CUBEGREEN_MESH", PtrFbxScene, 0);

		DataDir = App::GetApp()->m_wstrRelativeDataPath + L"Model\\";
		PtrFbxScene = App::GetApp()->RegisterFbxScene(L"CUBEBLUE_FBX", DataDir, L"BlueCube.fbx");
		PtrFbxMesh = App::GetApp()->RegisterFbxMesh(L"CUBEBLUE_MESH", PtrFbxScene, 0);

		DataDir = App::GetApp()->m_wstrRelativeDataPath + L"Model\\";
		PtrFbxScene = App::GetApp()->RegisterFbxScene(L"SCOREUP_BLUE_FBX", DataDir, L"ScoreUpCube_Blue.fbx");
		PtrFbxMesh = App::GetApp()->RegisterFbxMesh(L"BLUE", PtrFbxScene, 0);

		DataDir = App::GetApp()->m_wstrRelativeDataPath + L"Model\\";
		PtrFbxScene = App::GetApp()->RegisterFbxScene(L"SCOREUP_RED_FBX", DataDir, L"ScoreUpCube_Red.fbx");
		PtrFbxMesh = App::GetApp()->RegisterFbxMesh(L"RED", PtrFbxScene, 0);

		DataDir = App::GetApp()->m_wstrRelativeDataPath + L"Model\\";
		PtrFbxScene = App::GetApp()->RegisterFbxScene(L"SCOREUP_PURPLE_FBX", DataDir, L"ScoreUpCube_Purple.fbx");
		PtrFbxMesh = App::GetApp()->RegisterFbxMesh(L"PURPLE", PtrFbxScene, 0);

		DataDir = App::GetApp()->m_wstrRelativeDataPath + L"Model\\";
		PtrFbxScene = App::GetApp()->RegisterFbxScene(L"SCOREUP_GREEN_FBX", DataDir, L"ScoreUpCube_Green.fbx");
		PtrFbxMesh = App::GetApp()->RegisterFbxMesh(L"GREEN", PtrFbxScene, 0);

	}

	//--------------------------------------------------------------------------------------
	//	class Scene : public SceneBase;
	//	用途: シーンクラス
	//--------------------------------------------------------------------------------------
	void Scene::Create(){
		try{
			//画像リソースをメモリに保存する,サウンドリソースをメモリに保存するステージを最初に読み込む
			CreateResorce();
			

			//セレクト関連のBGM
			m_MultiAudio = Object::CreateObject<MultiAudioObject>();
			m_MultiAudio->AddAudioResource(L"STAGESELECT");

			//最初のアクティブステージの設定
			ResetActiveStage<StartStage>();
		}
		catch (...){
			throw;
		}
	}
	//スタート
	void Scene::StartBGM(){
		m_MultiAudio->Start(L"STAGESELECT", XAUDIO2_LOOP_INFINITE, 0.2f);
	}

	//ストップ
	void Scene::StopBGM(){
		m_MultiAudio->Stop(L"STAGESELECT");
	}

	void Scene::OnEvent(const shared_ptr<Event>& event)
	{
		//ゲーム画面
		if (event->m_MsgStr == L"ToGame"){
			m_Usemat = 0.0f;
			ResetActiveStage<GameStage>();
		}
		//タイトル
		if (event->m_MsgStr == L"ToMenu"){
			m_Usemat = 0.0f;
			ResetActiveStage<StartStage>();
		}
		//リザルト
		if (event->m_MsgStr == L"ToGameClear"){
			ResetActiveStage<GameClearStage>();
		}
		//ステージセレクト
		if (event->m_MsgStr == L"ToStageSelect"){
			ResetActiveStage<ModeSelectStage>();
		}
		//ワールドセレクト	//現在使っていない
		if (event->m_MsgStr == L"WorldSelect"){
			ResetActiveStage<WorldSelectScene>();
		}

	}
}
//end basedx11
