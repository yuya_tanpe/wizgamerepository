#pragma once
#include "stdafx.h"

namespace basedx11{

	//--------------------------------------------------------------------------------------
	//	class Scene : public SceneBase;
	//	用途: シーンクラス
	//--------------------------------------------------------------------------------------
	class Scene : public SceneBase{
	private:
		//ステージの番号を格納
		size_t m_Stagenumber;

		//ステージのゴール数を格納
		size_t m_StageGoalSize;

		Vector3 m_HarfSize;

		//過ぎた時間を格納
		float m_Timer;
		//使用したマットの数
		float m_Usemat;

		//ステージセレクト時にスティック情報を格納するもの
		bool m_Stickbrief;

		//ワールド選択
		wstring m_WorldName;

		//難易度２種類　 false : Normal	 true : Hard
		bool m_GameMode;

		//スコア　位など
		size_t m_100KURAI;
		size_t m_1000KURAI;

		//リソースの作成
		void CreateResorce();

		//BGM
		shared_ptr<MultiAudioObject> m_MultiAudio;

	public:
		//構築と破棄 正直これはCppに書いたほうがいい気がする
		Scene():
			m_Stagenumber(0),
			m_StageGoalSize(0),
			m_HarfSize(0,0,0),
			m_Timer(0.0f),
			m_Usemat(0.0f),
			m_Stickbrief(false),
			m_WorldName(L"Easy"),
			m_GameMode(false),				//初期はゲーム難易度はNormal		当たり判定がない
			m_100KURAI(0),
			m_1000KURAI(0)
		{}
		~Scene(){}
		//アクセサ
		//操作
		virtual void Create()override;
		virtual void OnEvent(const shared_ptr<Event>& event)override;
			
		void StartBGM();	//スタート
		void StopBGM();		//ストップ
		//ステージ番号を取得
		const size_t GetStageNumber()const {
			return m_Stagenumber;
		}

		//ステージ番号を設定
		void SetStageNumber(size_t num) {
			m_Stagenumber = num;
		}


		//ステージのゴールサイズを取得
		const size_t GetStageGoalSize() const{
			return m_StageGoalSize;
		}

		//ステージのゴールサイズを設定
		void SetStageGoalSize(size_t GoalSize){
			m_StageGoalSize = GoalSize;
		}

		//ステージの半分の取得
		const Vector3 GetHarfStageSize() const {
			return m_HarfSize;
		}

		//ステージの半分のスケールを格納
		void SetHarfStageSize(Vector3 HarfSize){
			m_HarfSize = HarfSize;
		}
		//過ぎた時間を習得
		const float GetPastTime() const{
			return m_Timer;
		}
		//過ぎた時間を格納
		void SetPastTime(float time){
			m_Timer = time;
		}
		//コメント
		const float GetUsemat(){
			return m_Usemat;
		}
		//コメント
		void PlusUsemat(){
			m_Usemat += 1.0f;
		}
		//コメント
		void SetUsemat(){
			m_Usemat = 0;
		}

		//	@author yuyu sike
		//	@date 2015 / 12 / 19 Start

		//セレクトステージでのスティック操作を格納する
		void Set_Stickbrief(bool Now_Stick){
			m_Stickbrief = Now_Stick;
		}

		//セレクトステージでのスティック操作を取得する
		bool Get_Stickbrief(){
			return m_Stickbrief;
		}

		//ワールド選択の名前を設定
		void Set_WorldSelect(wstring Texturename){
			m_WorldName = Texturename;
		}

		//ワールド選択の取得
		wstring Get_WorldSelect(){
			return m_WorldName;
		}

		//ステージセレクトでの選ばれたゲームモードを設定する
		void Set_GameMode(bool i_GameMode){
			m_GameMode = i_GameMode;
		}

		//ゲームモードの取得
		bool Get_GameMode(){
			return m_GameMode;
		}

		//TODO : この以下ぜ絶対書き直しする

		//１００の位
		void Set_100(size_t score){
			m_100KURAI = score;
		}

		const size_t Get_100(){
			return m_100KURAI;
		}
		//１０００の位
		void Set_1000(size_t score){
			m_1000KURAI = score;
		}
		const size_t Get_1000(){
			return m_1000KURAI;
		}
	};
}
//end basedx11
