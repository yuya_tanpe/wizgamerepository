#include "stdafx.h"
#include "Project.h"

namespace basedx11{

	//構築と破棄
	ScoreUp_Item::ScoreUp_Item(const shared_ptr<Stage>& StagePtr, const Vector3& Pos, const wstring& MeshName) :
		GameObject(StagePtr),
		m_Pos(Pos),
		m_Rotate(0,0,0),
		m_MeshName(MeshName)
	{
	}

	//構築
	void ScoreUp_Item::Create(){

		auto Trans = GetComponent<Transform>();
		m_Pos += Vector3(0.0f, 0.4f, 0.0f);
		Trans->SetPosition(m_Pos);
		Trans->SetRotation(m_Rotate);
		Trans->SetScale(0.3f, 0.4f, 0.3f);
		
		//map<wstring , ColorCube>　EnumClassで関連付けしてる
		m_CubeMap[L"RED"] = CubeColor::RED;
		m_CubeMap[L"BLUE"] = CubeColor::BLUE;
		m_CubeMap[L"GREEN"] = CubeColor::GREEN;
		m_CubeMap[L"PURPLE"] = CubeColor::PURPLE;
		m_CubeMap[L"NONE"] = CubeColor::NONE;

		//モデルのデフォルトサイズの設定
		Matrix4X4 CharaScale;
		CharaScale.DefTransformation(
			Vector3(0.055f, 0.055f, 0.055f),
			Vector3(0.0f, 0.0f, 0.0f),
			Vector3(0.0f, -0.5f, 0.0f)
			);


		//描画コンポーネントの設定
		auto PtrDraw = AddComponent<BasicFbxPNTDraw>();
		PtrDraw->SetMeshToTransform(CharaScale);
		PtrDraw->SetFbxMeshResource(m_MeshName);
		PtrDraw->SetTextureOnlyNoLight(true);

		//当たり判定
		auto PtrColl = AddComponent<CollisionObb>();
		PtrColl->SetFixed(false);
		PtrColl->SetDrawActive(false);

		auto PtrSE = AddComponent<MultiSoundEffect>();
		PtrSE->AddAudioResource(L"SCOREUP");
	

	}

	//変化
	void ScoreUp_Item::Update(){
		RotateObj();
	}

	//反応
	void ScoreUp_Item::Update2(){
		auto PtrColl = GetComponent<CollisionObb>();
		if (PtrColl->GetHitObject()){
			//当たったのがプレイヤーだった場合
			//auto GameStagePtr = dynamic_pointer_cast<GameStage>(GetStage());
			//Cubeの親のグループを取得
			//auto Parent_Group_Vec = GameStagePtr->GetSharedObjectGroup(L"ParentGroup")->GetGroupVector();
			//当たった判定を取る対象
			auto PtrCube = dynamic_pointer_cast<ChildBox>(PtrColl->GetHitObject());
			auto PtrPlayer = dynamic_pointer_cast<Player>(PtrColl->GetHitObject());
			/*
			auto Parent_Group_Vec = GameStagePtr->GetSharedObjectGroup(L"ParentGroup")->GetGroupVector();
			for (auto wpParent : Parent_Group_Vec){
				//なかったら早期リターン 
				if (!wpParent)	return;
				//Keyを親と子供から取得して同じCUBE同士で変化させる
				if (wpParent->Parent_to_Chiled_Connect() == PtrCube->Chiled_to_Parent_Connect()){
					//メッシュを変更
					auto pd = pChiled->GetComponent<BasicFbxPNTDraw>();
					pd->SetFbxMeshResource(MeshKey);
					//変えた色にあうようにColorCubeのゴールする色も変更
					Set_goal_value(goalID);
					//SEを鳴らす
					auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
					pMultiSoundEffect->Start(L"COLORSE", 0, 0.3f);
				}
			}
			*/

			if (PtrCube){
				//ここが修正箇所
				wstring ColorWord = PtrCube->GetMeshName(); //返却されるのはwstring型の色単語
				wstring ScoreWord = m_MeshName;		//自分の色が分かる
				auto a = 0;

				switch (m_CubeMap[Color_Color(ColorWord, ScoreWord)])
				{
					
				case CubeColor::BLUE:
					staging_etc(1,400);
					ScoreUp(4);
					break;
				case CubeColor::RED:
					staging_etc(0,400);
					ScoreUp(4);
					break;
				case CubeColor::PURPLE:
					staging_etc(3,400);
					ScoreUp(4);
					break;
				case CubeColor::GREEN:
					//第１引数　エフェクトの色　第２引数　スコアの点数
					staging_etc(2,400);
					ScoreUp(4);
					break;
				case CubeColor::NONE:
					//色が違う場合
					//TODO : エフェクト変える
					staging_etc(4,200);
					ScoreUp(2);
					break;
				default:
					assert(!"不正な値が入りました。Switch文の可能性が高いです");
					break;
				}
			}
			
			if (PtrPlayer){
				//TODO : エフェクト変える
				staging_etc(4, 100);
				ScoreUp(1);
				//非表示にする	
				SetDrawActive(false);
				SetUpdateActive(false);


				
			}
		}
	}

	//スコアあげる関数 
	void ScoreUp_Item::ScoreUp(size_t score){
		//ゲームステージの取得
		auto GamePtr = dynamic_pointer_cast<GameStage>(GetStage());
		//スコアの変更
		GamePtr->ScoreUp(score);
	}

	//色									メッシュの名前		スコアの色
	wstring ScoreUp_Item::Color_Color(wstring Cube, wstring Score){
		//色が同じだったらそのまま返す
		wstring CubeName = L"";

		//Cubeのメッシュで仕分けを行うことにした
		//day : 2016/02/06
		//TODO : ここ当てつけ工事したからリファクタリング必ずやる
		if (Cube == L"CUBERED_MESH"){
			CubeName = L"RED";
		}
		if (Cube == L"CUBEBLUE_MESH"){
			CubeName = L"BLUE";
		}
		if (Cube == L"CUBEPURPLE_MESH"){
			CubeName = L"PURPLE";
		}
		if (Cube == L"CUBEGREEN_MESH"){
			CubeName = L"GREEN";
		}

		if (CubeName == Score){
			//色が一緒なら色単語を返す
			return Score;
		}
		else
			//色が異なっていたらスコアを変える
			return L"NONE";
	}

	//演出まとめる
	void ScoreUp_Item::staging_etc(const size_t TexNum , const int Point){
		//ColorCubeの仮実装
		//ゲームステージの取得
		auto GamePtr = dynamic_pointer_cast<GameStage>(GetStage());
		//サウンド
		auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
		pMultiSoundEffect->Start(L"SCOREUP", 0, 0.1f);

		//エフェクト
		m_wpGameEffectManager = GetStage()->GetSharedGameObject<GameEffectManager>(L"GameEffectManager", false);
		auto pGameEffectManeger = m_wpGameEffectManager.lock();
		Vector3 Pos{ GetComponent<Transform>()->GetPosition() };
		pGameEffectManeger->InsertScoreUpSquareEffect(Pos, TexNum);
		auto a = 0;
		//スコアの画像の表示
		GamePtr->AddGameObject<ScoreUp_Square>(Pos, Point);
		//スコアアップアイテムを取ったらItemCaseを書き換えする
		auto ItemPos = GetComponent<Transform>()->GetPosition();
		int Row = static_cast<int>(ItemPos.x);
		int Col = static_cast<int>(ItemPos.z);
		//Emptyに書き換えて、マット配置が出来るようにする
		GamePtr->SetItemObjsize_t(Col, Row, ItemCase::Empty);

		//非表示にする	
		SetDrawActive(false);
		SetUpdateActive(false);

	}

	//回転
	void ScoreUp_Item::RotateObj(){
		auto elp = App::GetApp()->GetElapsedTime();
		m_Rotate.y += elp / 1.2f;
		auto Trans = GetComponent<Transform>();
		Trans->SetRotation(m_Rotate);
	}

	/*
	* @class ScoreUp_Sprite
	* @brief スコアを通知するスクエア.
	* @author yuyu sike
	* @date 2016/01/18 Start
	*/

	//構築と破棄
	ScoreUp_Square::ScoreUp_Square(const shared_ptr<Stage>& StagePtr, const Vector3& Pos, const int& GetPoint):
		GameObject(StagePtr),
		m_Position(Pos),
		m_Point(GetPoint),
		m_TextureName(L"")
	{}

	//引数で受け取ったm_Pointによりテクスチャのリソースを変更する
	void ScoreUp_Square::SetTextureName(wstring& SetTextureName, const int GetPoint){
		switch (GetPoint)
		{
		case 100:
			//プレイヤーと衝突した場合
			SetTextureName = L"SCORE100";
			break;
		case 200:
			//色は違うがCubeと衝突した場合
			SetTextureName = L"SCORE200";
			break;
		case 400:
			//色が一緒でCubeと衝突した場合
			SetTextureName = L"SCORE400";
			break;
		default:
			assert(!"当たったオブジェクトが登録されてません。Switch文とかとか確認してください");
			break;
		}
	}

	//構築
	void ScoreUp_Square::Create(){
		auto Trans = GetComponent<Transform>();
		Trans->SetScale(2.0f, 1.0f, 1.0f);
		Trans->SetRotation(0.0f, 0.0f, 0.0f);
		Trans->SetPosition(m_Position);

		auto PtrMove = AddComponent<MoveTo>();
		Vector3 NextPos = m_Position + Vector3(0.0f, 1.0f, 0.0f);
		PtrMove->SetParams(1.0f, NextPos);
		//アクション実行開始
		PtrMove->Run();

		//変更できるスクエアリソースを作成
		m_SquareMeshResource = CommonMeshResource::CreateSquare(1.0f, true);
		//描画周り
		auto DrawComp = AddComponent<BasicPNTDraw>();
		DrawComp->SetMeshResource(m_SquareMeshResource);
		SetTextureName(m_TextureName, m_Point);
		DrawComp->SetTextureResource(m_TextureName);
		DrawComp->SetTextureOnlyNoLight(true);
		SetAlphaActive(true);
	}

	//変化
	void ScoreUp_Square::Update(){
		auto Trans = GetComponent<Transform>();

		//カメラの取得
		auto PtrCamera = GetStage()->GetCamera(0);
		Quaternion Qt;
		//向きをビルボードにする
		Qt.Billboard(PtrCamera->GetAt() - PtrCamera->GetEye());
		Trans->SetQuaternion(Qt);


	}

	//反応
	void ScoreUp_Square::Update2(){
		auto PtrMove = GetComponent<MoveTo>();
		if (PtrMove->IsArrived()){
			//アクションが終わったら動作、描画の終了
			SetUpdateActive(false);
			SetDrawActive(false);

			//TODO : なにか消えたらエフェクトの表示
			m_wpGameEffectManager = GetStage()->GetSharedGameObject<GameEffectManager>(L"GameEffectManager", false);
			auto pGameEffectManeger = m_wpGameEffectManager.lock();
			////エフェクト
			Vector3 Pos{ GetComponent<Transform>()->GetPosition() };
			//pGameEffectManeger->InsertScoreUpSquareEffect(Pos);
		}

	}



}
//endof  basedx11
