#pragma once

#include "stdafx.h"

namespace basedx11{

	/*
	* @class ScoreUp_Item
	* @brief スコアを上げるアイテム.
	* @author yuyu sike
	* @date 2016/01/06 Start
	*/
	class ScoreUp_Item : public GameObject {
	private:
		Vector3 m_Pos;
		Vector3 m_Rotate;
		wstring m_MeshName;

		 void RotateObj();
		 void staging_etc(const size_t TexNum, const int Point);
		 void ScoreUp(size_t score);
		 wstring Color_Color(wstring Cube, wstring Score);
		 weak_ptr< GameEffectManager > m_wpGameEffectManager;	//エフェクト管理システム.
		 //スコア関連で結び付けて切り替えるため
		 enum class CubeColor{
			 BLUE,
			 RED,
			 GREEN,
			 PURPLE,
			 NONE,
		 };

		 map<wstring, CubeColor> m_CubeMap;

	public:
		ScoreUp_Item(const shared_ptr<Stage>& StagePtr, const Vector3& Pos , const wstring& MeshName
			);
		~ScoreUp_Item(){}
		//構築
		virtual void Create() override;
		//変化
		virtual void Update() override;
		//反応
		virtual void Update2() override;

		//	: エフェクト管理システムを取得する.
		const weak_ptr< GameEffectManager >GetGameEffectManager() const
		{
			return m_wpGameEffectManager;
		}

		//	: エフェクト管理システムを設定する.
		void SetGameEffectManager(const weak_ptr< GameEffectManager > i_wpGameEffectManager)
		{
			m_wpGameEffectManager = i_wpGameEffectManager;
		}

		
	};


	/*
	* @class ScoreUp_Sprite
	* @brief スコアを通知するスクエア.
	* @author yuyu sike
	* @date 2016/01/18 Start
	*/
	class ScoreUp_Square : public GameObject {
	private:
		// ---------- member ----------.
		//スクエアのMeshResorceを格納
		shared_ptr<CommonMeshResource> m_SquareMeshResource;
		//作成する位置
		Vector3 m_Position;
		//テクスチャの名前を格納
		wstring m_TextureName;
		//当たり判定の相手によってポイントの変化があるため
		int m_Point;
		//エフェクト管理システム.
		weak_ptr< GameEffectManager > m_wpGameEffectManager;

		//	---------- Method ----------.
		//引数で受け取ったm_Pointによりテクスチャのリソースを変更する
		void SetTextureName(wstring& SetTextureName, const int GetPoint);


	public:
		//構築と破棄
		ScoreUp_Square(const shared_ptr<Stage>& StagePtr, const Vector3& Pos, const int& GetPoint);
		~ScoreUp_Square(){}
		//	---------- Method ----------.
		virtual void Create();
		virtual void Update();
		virtual void Update2();

		// ---------- Accessor ----------.
		//	: エフェクト管理システムを取得する.
		const weak_ptr< GameEffectManager >GetGameEffectManager() const
		{
			return m_wpGameEffectManager;
		}

		//	: エフェクト管理システムを設定する.
		void SetGameEffectManager(const weak_ptr< GameEffectManager > i_wpGameEffectManager)
		{
			m_wpGameEffectManager = i_wpGameEffectManager;
		}

	};

}
//endof  basedx11
