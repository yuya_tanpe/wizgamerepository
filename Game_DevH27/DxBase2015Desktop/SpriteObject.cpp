#include "stdafx.h"
#include "Project.h"
/**
* @class SpriteObject
* @brief Sprite表示　作成
* @author 菅野 翔吾
*/


namespace basedx11{
	//--------------------------------------------------------------------------------------
	//	class SpriteObject : public GameObject;
	//	用途: テキスト（スプライト）のクラス。画像のリソース名を渡すと、その画像を表示します。
	//--------------------------------------------------------------------------------------
	void SpriteObject::Create(){
		auto PtrTransform = GetComponent<Transform>();
		PtrTransform->SetPosition(m_StartPos);
		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(0.0f, 0.0f, 0.0f);
		//スプライトをつける
		auto PtrSprite = AddComponent<Sprite>(Color4(1.0f, 1.0f, 1.0f, 1.0f));

		//テクスチャのアドレス名が hoge （名無しの時のコンストラクタで入る適当な名前）のときは、テクスチャを設定しません。
		if (m_TextureName != L"hoge"){
			PtrSprite->SetTextureResource(m_TextureName);
		}

		//1メートル当たりのピクセル数
		//以下は640*480ピクセルの場合。横幅20,縦15メートルということ
		PtrSprite->SetPixelParMeter(32.0f);
		SetAlphaActive(true);
		SetAlphaExActive(true);
		//中心原点
		PtrSprite->SetCoordinate(Sprite::Coordinate::m_CenterZeroPlusUpY);
	}
	void SpriteObject::Update(){


		auto elp = App::GetApp()->GetElapsedTime();
		m_c += elp * 2;

		if (m_c >= 1.0f){
			m_ct++;
			m_c = 0.0f;
			if (m_ct % 1){
				auto PtrSprite = AddComponent<Sprite>(Color4(1.0f, 1.0f, 1.0f, 1.0f));
				PtrSprite->SetDrawActive(false);
			}
			else{
				auto PtrSprite = AddComponent<Sprite>(Color4(1.0f, 1.0f, 1.0f, 1.0f));
				PtrSprite->SetDrawActive(true);
			}
		}

	}


}
//end basedx11
