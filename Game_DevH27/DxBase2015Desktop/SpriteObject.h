#pragma once

#include "stdafx.h"

namespace basedx11{
	/**
	* @class　SpriteObject
	* @brief スプライトを作る基底クラス.
	* @author syougo kanno
	* @date 2015/10/08 Start
	*/

	//--------------------------------------------------------------------------------------
	//	class UISpriteObject : public GameObject;
	//	用途: テキスト（スプライト）の基底クラス。
	//--------------------------------------------------------------------------------------
	class SpriteObject : public GameObject{
	private:
		float m_c;
		size_t m_ct;
	protected:
		Vector3 m_StartPos;
		Vector3 m_Scale;
		wstring m_TextureName;
		

	public:
		SpriteObject(shared_ptr<Stage>& StagePtr,
			const Vector3& StartPos,
			const Vector3& Scale,
			const wstring& TextureName
			) :
			GameObject(StagePtr),
			m_StartPos(StartPos),
			m_Scale(Scale),
			m_TextureName(TextureName),
			m_c(0.0f),
			m_ct(0)
		{}

		SpriteObject(shared_ptr<Stage>& StagePtr,
			const Vector3& StartPos,
			const Vector3& Scale
			) :
			GameObject(StagePtr),
			m_StartPos(StartPos),
			m_Scale(Scale),
			m_TextureName(L"hoge")
		{}

		SpriteObject(shared_ptr<Stage>& StagePtr,
			const Vector3& StartPos
			) :
			GameObject(StagePtr),
			m_StartPos(StartPos),
			m_Scale(Vector3(1, 1, 1)),
			m_TextureName(L"hoge")
		{}

		virtual ~SpriteObject(){}

		virtual void Create();
		virtual void Update();
	};
}