#include "stdafx.h"
#include "Project.h"

namespace basedx11{

	//--------------------------------------------------------------------------------------
	//	class StageManager : public GameObject;
	//	用途: ステージ変更するイベントオブジェクト
	//--------------------------------------------------------------------------------------
	StageManager::StageManager(const shared_ptr<Stage>& StagePtr, const size_t i_GoalPoint) :
		GameObject(StagePtr),
		m_GoalPoint(i_GoalPoint),			//ステージのゴール数
		m_NowPoint(0)						//現在のゴール数
	{}

	//初期化
	void StageManager::Create(){
		//イベントマシンの構築
		m_EventMachine = make_shared< EventMachine<StageManager>>(GetThis<StageManager>());
		//イベントキーとイベントステートを結び付ける
		m_EventMachine->AddEventState(L"MathToClear", MathToGameClearEvent::Instance());
		m_EventMachine->AddEventState(L"GoToGameOver", GoToGameOverEvent::Instance());



	}


	//イベントのハンドラ
	void StageManager::OnEvent(const shared_ptr<Event>& event){
		//ハンドラ関数呼び出し
		//これでイベントが振り分けられる
		m_EventMachine->HandleEvent(event);
	}

	//引数でゴールするまでを受け取り、ゴールしたらインクリメントを行う
	void StageManager::Event_GameClear(const size_t i_Point){

		m_NowPoint += i_Point;

		//通ることの確認
		if (m_NowPoint == m_GoalPoint){
			//ステージ切り替え
			//PostEvent(2.0f, GetThis<StageManager>(), App::GetApp()->GetSceneBase(), L"ToGameClear");
			auto GamePtr = dynamic_pointer_cast<GameStage>(GetStage());
			GamePtr->GetStateMachine()->ChangeState(GameClearState::Instance());

		}
	
	}

	//引数でゴールするまでを受け取り、ゴールしたらインクリメントを行う
	void StageManager::Event_GameOver(){

		//完成はGameStageのステート切り替え
		//ステージ切り替え
		//PostEvent(2.0f, GetThis<StageManager>(), App::GetApp()->GetSceneBase(), L"ToGameClear");
		
		auto GamePtr = dynamic_pointer_cast<GameStage>(GetStage());
		GamePtr->GetStateMachine()->ChangeState(GameOverState::Instance());

	}
		
	//--------------------------------------------------------------------------------------
	//	class MathToGameClearEvent : public EventState<StageManager>;
	//	用途: ステージのゴール数まで足してクリア画面に移行するイベント
	//--------------------------------------------------------------------------------------
	//イベントステートのインスタンスを得る
	shared_ptr<MathToGameClearEvent> MathToGameClearEvent::Instance(){
		static shared_ptr<MathToGameClearEvent> instance;
		if (!instance){
			instance = shared_ptr<MathToGameClearEvent>(new MathToGameClearEvent);
		}
		return instance;
	}
	//このイベントが発生したときに呼ばれる
	void MathToGameClearEvent::Enter(const shared_ptr<StageManager>& Obj, const shared_ptr<Event>& event){
		Obj->Event_GameClear(1);
	}


	//--------------------------------------------------------------------------------------
	//	class GoToGameOverEvent : public EventState<StageManager>;
	//	用途: 呼ばれたらゲームオーバーに移行するイベント
	//--------------------------------------------------------------------------------------
	//イベントステートのインスタンスを得る
	shared_ptr<GoToGameOverEvent> GoToGameOverEvent::Instance(){
		static shared_ptr<GoToGameOverEvent> instance;
		if (!instance){
			instance = shared_ptr<GoToGameOverEvent>(new GoToGameOverEvent);
		}
		return instance;
	}
	//このイベントが発生したときに呼ばれる
	void GoToGameOverEvent::Enter(const shared_ptr<StageManager>& Obj, const shared_ptr<Event>& event){
		Obj->Event_GameOver();
	}

}
//endof  basedx11
