#pragma once
#include "stdafx.h"

namespace basedx11{
	/**
	* @class　StageManager
	* @brief　ステージ管理するマネージャークラス
	* @author yuyu sike
	* @date 2015/11/05 Start
	*/
	class StageManager : public GameObject{
	public:
		//コンストデスト
		StageManager(const shared_ptr<Stage>& StagePtr, const size_t i_GoalPoint);
		virtual ~StageManager(){}

		virtual void Create() override;

		//イベントのハンドラ
		virtual void OnEvent(const shared_ptr<Event>& event)override;

		// ----------Event----------.
		void Event_GameClear(const size_t i_Point);	//ゴールにいくまでの関数

		void Event_GameOver();							//ゲームオーバーの関数

	private:
		shared_ptr< EventMachine<StageManager> > m_EventMachine;	//イベントマシン
		size_t m_GoalPoint;											//選んだステージのゴール数
		size_t m_NowPoint;											//現在のゴール数

	};

	/**
	* @class　MathToGameClearEvent : public EventState<StageManager>;
	* @brief　ステージのゴール数まで足してクリア画面に移行するイベント
	* @author yuyu sike
	* @date 2015/11/05 Start
	*/
	class MathToGameClearEvent : public EventState<StageManager>
	{
		MathToGameClearEvent(){}
	public:
		//イベントステートのインスタンスを得る
		static shared_ptr<MathToGameClearEvent> Instance();
		//このイベントが発生したときに呼ばれる
		virtual void Enter(const shared_ptr<StageManager>& Obj, const shared_ptr<Event>& event)override;
	};

	/**
	* @class　GoToGameOverEvent : public EventState<StageManager>;
	* @brief　呼ばれたらゲームオーバーに移行するイベント
	* @author yuyu sike
	* @date 2015/11/05 Start
	*/
	class GoToGameOverEvent : public EventState<StageManager> 
	{
		GoToGameOverEvent(){}
	public:
		//イベントステートのインスタンスを得る
		static shared_ptr <GoToGameOverEvent> Instance();
		//このイベントが発生したときに呼ばれる
		virtual void Enter(const shared_ptr<StageManager>& Obj, const shared_ptr<Event>& event)override;
	};





}
//endof  basedx11
