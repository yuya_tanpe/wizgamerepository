#include "stdafx.h"
#include "Project.h"

namespace basedx11{

	//--------------------------------------------------------------------------------------
	//	class Star_Object : public GameObject;
	//	用途: ワールド選択時のオブジェクト
	//--------------------------------------------------------------------------------------
	//構築と破棄
	Star_Object::Star_Object(const shared_ptr<Stage>& StagePtr, const int& point, const wstring& texture
		) :
		GameObject(StagePtr),
		point(point),
		m_Rotate(0,0,0),
		m_tex(texture),
		m_Position(3),
		m_Scale(3)
	{
	}

	void Star_Object::firstvector(){
		m_Position[0] = Vector3(0, 0, 0);
		m_Position[1] = Vector3(-4, 2, 0);
		m_Position[2] = Vector3(4, 2, 0);

		m_Scale[0] = Vector3(5.0f,5.0f,5.0f);
		m_Scale[1] = Vector3(1, 1, 1);
		m_Scale[2] = Vector3(1, 1, 1);


	}
	//構築
	void Star_Object::Create(){
		firstvector();
		auto PtrTrans = GetComponent<Transform>();
		PtrTrans->SetScale(m_Scale[point]);
		PtrTrans->SetPosition(m_Position[point]);
		PtrTrans->SetRotation(Vector3(m_Rotate));

		//描画の追加
		auto PtrDraw = AddComponent<BasicPNTDraw>();
		PtrDraw->SetMeshResource(L"DEFAULT_SPHERE");
		PtrDraw->SetTextureResource(m_tex);

	}

	//変化
	void Star_Object::Update(){
		auto elp = App::GetApp()->GetElapsedTime();

		m_Rotate.y += elp;
		auto Trans = GetComponent<Transform>();

		Trans->SetRotation(m_Rotate);

		if (point > 2){
			point = 0;
			
		}
		else if(point < 0 ){
			point = 2;
		}
	}

	//イベントの受け取り 位置替え
	void Star_Object::OnEvent(const shared_ptr<Event>& event){
		//ポーズ画面のときGameStageからL"Pause"送られてくる
		auto Trans = GetComponent<Transform>();

		if (event->m_MsgStr == L"MoveLeft"){
			//右に移動
			point -= 1;
			if (point == -1){
				point = 2;
			}
			Trans->SetPosition(m_Position[point]);
			Trans->SetScale(m_Scale[point]);
		}

		else if (event->m_MsgStr == L"MoveRight"){
			//左に移動
			point += 1;
			if (point == 3){
				point = 0;
			}
			
			Trans->SetPosition(m_Position[point]);
			Trans->SetScale(m_Scale[point]);
		}

		else if (event->m_MsgStr == L"SelectStar"){
			//Sceneにテクスチャ名前を格納して、あっちで選択
			App::GetApp()->GetScene<Scene>()->Set_WorldSelect(m_tex);
			//ボタン入力があったらStageの再読み込み
			PostEvent(0.0f, GetThis<Star_Object>(), App::GetApp()->GetSceneBase(), L"ToStageSelect");
		}
	}

}
//endof  basedx11
