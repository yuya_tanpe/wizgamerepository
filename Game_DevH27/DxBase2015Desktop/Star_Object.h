#pragma once

#include "stdafx.h"

namespace basedx11{
	/*
	* @class　Star_Object
	* @brief　ワールド選択時のオブジェクト
	* @author yuyu sike
	* @date 2016/01/02
	*/

	class Star_Object : public GameObject {
		// ---------- member ----------.
		int point;			
		Vector3 m_Rotate;		//角度
		wstring m_tex;

		vector<Vector3> m_Position;
		vector<Vector3> m_Scale;
		void firstvector();
	public:
		Star_Object(const shared_ptr<Stage>& StagePtr, const int& point, const wstring& texture
			);
		virtual ~Star_Object(){}

		//初期化
		virtual void Create() override;
		//操作
		virtual void Update() override;
		//virtual void Update2() override;
		//virtual void Update3() override;		//デバッグ用として使用
		//イベント
		virtual void OnEvent(const shared_ptr<Event>& event)override;

		const int Getpoint(){
			return point;
		}
	};


}
//endof  basedx11
