#include "stdafx.h"
#include "Project.h"

namespace basedx11{
	/**
	* @class StartStage
	* @brief スタートステージ作成.
	* @author yuyu sike
	* @date 2015/10/06 Start
	*/
	//--------------------------------------------------------------------------------------
	//	class StartStage : public Stage;
	//	用途: メニューステージクラス
	//--------------------------------------------------------------------------------------
	//リソースの作成
	void StartStage::CreateResourses(){
	
		wstring strTexture = App::GetApp()->m_wstrRelativeDataPath + L"Cube\\Blue.png";
		App::GetApp()->RegisterTexture(L"BLUECUBE_TX", strTexture);
		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"Scene\\Start.png";
		App::GetApp()->RegisterTexture(L"START_TX", strTexture);

		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"Scene\\PlayGame.png";
		App::GetApp()->RegisterTexture(L"PLAYGAME_TX", strTexture);

		strTexture = App::GetApp()->m_wstrRelativeDataPath + L"Fade\\Black.png";
		App::GetApp()->RegisterTexture(L"BLOCKBLACK_TX", strTexture);


		//サウンド
		wstring CursorWav = App::GetApp()->m_wstrRelativeDataPath + L"Sound\\Start.wav";
		App::GetApp()->RegisterWav(L"Start", CursorWav);
		CursorWav = App::GetApp()->m_wstrRelativeDataPath + L"Sound\\Title.wav";
		App::GetApp()->RegisterWav(L"TITLE", CursorWav);

		
	}

	void StartStage::CreateViews(){

		//最初にデフォルトのレンダリングターゲット類を作成する
		CreateDefaultRenderTargets();
		//マルチビューコンポーネントの取得
		auto PtrMultiView = GetComponent<MultiView>();
		//マルチビューにビューの追加
		auto PtrView = PtrMultiView->AddView();
		//ビューの矩形を設定（ゲームサイズ全体）
		Rect2D<float> rect(0, 0, (float)App::GetApp()->GetGameWidth(), (float)App::GetApp()->GetGameHeight());
		//最初のビューにパラメータの設定
		PtrView->ResetParamaters<Camera, MultiLight>(rect, Color4(0.0f, 0.001f, 0.01f, 1.0f), 1, 0.0f, 1.0f);

		//0番目のビューのカメラを得る
		auto PtrCamera = GetCamera(0);
		PtrCamera->SetEye(Vector3(0.0f, 10.0f, -15.0f));
		PtrCamera->SetAt(Vector3(0.0f, 0.0f, 0.0f));

	}


	//void StartStage::CreateSprite(){
	//	auto PtrTransform = GetComponent<Transform>();
	//	PtrTransform->SetPosition(0.0f, 0.0f, 0.0f);
	//	PtrTransform->SetScale(11.0f, 4.5f, 1.0f);
	//	PtrTransform->SetRotation(0.0f, 0.0f, 0.0f);
	//	auto Ptr = AddComponent<Sprite>(Color4(1.0f, 1.0f, 1.0f, 1.0f));
	//	Ptr->SetTextureResource(L"START_TX");
	//	//1メートル当たりのピクセル数
	//	//以下は640*480ピクセルの場合。横幅20,縦15メートルということ
	//	Ptr->SetPixelParMeter(32.0f);
	//	//透明処理
	//	SetAlphaActive(false);
	//	//中心原点
	//	Ptr->SetCoordinate(Sprite::Coordinate::m_CenterZeroPlusUpY);
	//	//各数字ごとにUV値を含む頂点データを配列化しておく
	//	for (size_t i = 0; i < 10; i++){
	//		float from = ((float)i) / 10.0f;
	//		float to = from + (1.0f / 10.0f);
	//		vector<VertexPositionColorTexture> NumVirtex =
	//		{
	//			//左上頂点
	//			VertexPositionColorTexture(
	//			Vector3(-0.5f, 0.5f, 0),
	//			Color4(1.0f, 1.0f, 1.0f, 1.0f),
	//			Vector2(from, 0)
	//			),
	//			//右上頂点
	//			VertexPositionColorTexture(
	//			Vector3(0.5f, 0.5f, 0),
	//			Color4(1.0f, 1.0f, 1.0f, 1.0f),
	//			Vector2(to, 0)
	//			),
	//			//左下頂点
	//			VertexPositionColorTexture(
	//			Vector3(-0.5f, -0.5f, 0),
	//			Color4(1.0f, 1.0f, 1.0f, 1.0f),
	//			Vector2(from, 1.0f)
	//			),
	//			//右下頂点
	//			VertexPositionColorTexture(
	//			Vector3(0.5f, -0.5f, 0),
	//			Color4(1.0f, 1.0f, 1.0f, 1.0f),
	//			Vector2(to, 1.0f)
	//			),
	//		};
	//		m_NumberBurtexVec.push_back(NumVirtex);
	//	}
	//}

	void StartStage::TitleSound(){
		auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
		pMultiSoundEffect->AddAudioResource(L"TITLE");
		pMultiSoundEffect->Start(L"TITLE", XAUDIO2_LOOP_INFINITE, 0.3f);

	}

	void StartStage::CreateStartBox(){
		 AddGameObject<StartBox>(
			 Vector3(10.0f, 10.0f, 10.0f),
			 Vector3(0.0f, XM_PIDIV2, 0.0f),
			 Vector3(0.0f, 0.0f, 0.0f)
			 );

			 
		//SetSharedGameObject(L"BOX", PtrBox);

	}

	//文字列の作成
	void StartStage::CreateString(){
		////文字列をつける
		//auto PtrString = AddComponent<StringSprite>();
		//PtrString->SetText(L"Aボタンでスタート！！");
		//PtrString->SetFont(L"DWRITE_FONT_WEIGHT_LIGHT", 40.0f);
		//auto strHeight = App::GetApp()->GetGameHeight();		//画面の高さの取得
		//auto strWidth = App::GetApp()->GetGameWidth();		//画面の幅の取得
		//Rect2D<float> rect(0, 0, 400, 200);
		//rect += Point2D<float>(strWidth / 2.0f - 100 , strHeight / 2.0f + 150 );
		//PtrString->SetTextRect(rect);
	}
	//タイトル作成
	void StartStage::CreateTitle(){

		auto TitleFade = AddGameObject<FadePanel>(Vector3(0.0f, -5.0f, 0.0f), Vector3(20.0f, 5.0f, 1.0f), L"PLAYGAME_TX");
		//TitleFade->SetColor(Color4(1, 1, 1, 0));
		SetSharedGameObject(L"TitleFadePanel", TitleFade);
	}

	void StartStage::CreateFade(){

		//画面のフェードイン用のオブジェクト作成
		auto FadePtr = AddGameObject<FadePanel>();
		SetSharedGameObject(L"FadePanel", FadePtr);

	}

	//フェードアウトする
	void StartStage::FadeOut(){

		auto FadePtr = GetSharedGameObject<FadePanel>(L"FadePanel");

		if (!FadePtr->IsFadeInActive() && !FadePtr->IsFadeOutActive()){

			auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
			pMultiSoundEffect->Start(L"Start", 0, 0.2f);

			//ボタンを押したら点滅が速くなる
			auto FlashingTitle = GetSharedGameObject<FadePanel>(L"TitleFadePanel");
			FlashingTitle->SetFadeSpeed(0.2f);

			//フェードアウトを開始する
			PostEvent(0.0f, GetThis<StartStage>(), FadePtr, L"FadeOut");
		}

	}
	//フェードアウトしたらシーン移動
	void StartStage::ChangeScene(){
		auto FadePtr = GetSharedGameObject<FadePanel>(L"FadePanel");
		if (FadePtr->IsFadeOutActive() == false && FadePtr->IsFadeOutBeforeActive() == true){
			PostEvent(0.0f, GetThis<StartStage>(), App::GetApp()->GetSceneBase(), L"ToStageSelect");
			//PostEvent(0.0f, GetThis<StartStage>(), App::GetApp()->GetSceneBase(), L"ToStageSelect");
			auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
			pMultiSoundEffect->Stop(L"TITLE");
			//BGMのスタート		
			App::GetApp()->GetScene<Scene>()->StartBGM();
		}
	}

	//タイトル点滅させる
	void StartStage::TitleFlashing(){
		auto FadePtr = GetSharedGameObject<FadePanel>(L"FadePanel");

		if (FadePtr->IsFadeInActive() && !FadePtr->IsFadeOutActive()){

			auto Ptr = GetSharedGameObject<FadePanel>(L"TitleFadePanel");
			PostEvent(0.0f, GetThis<StartStage>(), Ptr, L"FadeOut");
			PostEvent(1.0f, GetThis<StartStage>(), Ptr, L"Flashing");
		}
	}
	bool StartStage::FadeMoution(){
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		if (CntlVec[0].bConnected){
			if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_A){
				return true;
			}
		}
		return false;

	}
	//初期化
	void StartStage::Create(){

		//リソースの作成
		CreateResourses();
		//ビューの作成
		CreateViews();
		//回ってるオブジェクト
		CreateStartBox();
		//文字列の作成
		CreateString();
		//フェードアウト作成
		CreateFade();
		//タイトル作成
		CreateTitle();
		//スプライトの作成
		//CreateSprite();

		//画像の作成	 引数で宣言	//     Position                 Scale                TextureName
		auto ptr = AddGameObject<SpriteObject>(Vector3(0.0f, 5.0f, 0.0f), Vector3(40.0f, 20.0f, 1.0f), L"START_TX");
		//AddGameObject<SpriteObject>(Vector3(0.0f, -5.0f, 0.0f), Vector3(20.0f, 5.0f, 1.0f), L"PLAYGAME_TX");
		//ptr->SetDrawActive(false);

		//サウンドを登録.
		auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
		pMultiSoundEffect->AddAudioResource(L"Start");

		m_StateMachine = make_shared< StateMachine<StartStage> >(GetThis<StartStage>());
		m_StateMachine->SetCurrentState(FadeState::Instance());
		m_StateMachine->GetCurrentState()->Enter(GetThis<StartStage>());

	}

	//操作
	void StartStage::Update(){
		//ステートマシンの更新
		m_StateMachine->Update();
	}


	shared_ptr<FadeState> FadeState::Instance(){
		static shared_ptr<FadeState> instance;
		if (!instance){
			instance = shared_ptr<FadeState>(new FadeState);
		}
		return instance;
	}
	//ステートに入ったときに呼ばれる関数
	void FadeState::Enter(const shared_ptr<StartStage>& Obj){
		Obj->TitleSound();

	}
	//ステート実行中に毎ターン呼ばれる関数
	void FadeState::Execute(const shared_ptr<StartStage>& Obj){
		if (Obj->FadeMoution()){
			Obj->FadeOut();
		}
		Obj->ChangeScene();
		Obj->TitleFlashing();

	}
	//ステートにから抜けるときに呼ばれる関数
	void FadeState::Exit(const shared_ptr<StartStage>& Obj){
		//何もしない
		
	}

}
//end basedx11
