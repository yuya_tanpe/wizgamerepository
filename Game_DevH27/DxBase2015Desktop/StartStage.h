#pragma once
#include "stdafx.h"

namespace basedx11{

	/**
	* @class StartStage 
	* @brief スタートステージの分離化.
	* @author yuyu sike
	* @date 2015/10/06 Start
	*/
	class StartStage : public Stage{
		//ビューの作成
		void CreateViews();
		//リソースの作成
		void CreateResourses();
		//回ってるオブジェクト
		void CreateStartBox();
		//文字列の作成
		void CreateString();
		void CreateFade();
		void CreateTitle();
		//スプライトの作成
		//void CreateSprite();


	public:
		//構築と破棄
		StartStage() :Stage(){}
		virtual ~StartStage(){}
		//初期化
		virtual void Create()override;
		//更新
		virtual void Update() override;

		void FadeOut();
		void TitleFlashing();
		void ChangeScene();
		void TitleSound();

		bool FadeMoution();




		shared_ptr<StateMachine<StartStage>> GetStateMachine() const{
			return m_StateMachine;
		}


		shared_ptr<StateMachine<StartStage>>  m_StateMachine;	//ステートマシーン

		vector< vector<VertexPositionColorTexture> > m_NumberBurtexVec;
		bool m_flag = false;

	};

	class FadeState : public ObjState<StartStage>
	{
		FadeState(){}
	public:
		//ステートのインスタンス取得
		static shared_ptr<FadeState> Instance();
		//ステートに入ったときに呼ばれる関数
		virtual void Enter(const shared_ptr<StartStage>& Obj)override;
		//ステート実行中に毎ターン呼ばれる関数
		virtual void Execute(const shared_ptr<StartStage>& Obj)override;
		//ステートにから抜けるときに呼ばれる関数
		virtual void Exit(const shared_ptr<StartStage>& Obj)override;
	};



}
//end basedx11
