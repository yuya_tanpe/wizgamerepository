#include "stdafx.h"
#include "Project.h"

namespace basedx11{

	
	StartBox::StartBox(shared_ptr<Stage>& StagePtr,
		const Vector3& Scale,
		const Vector3& Rotation,
		const Vector3& Position
		) :
		GameObject(StagePtr),
			m_Scale(Scale),
			m_Rotation(Rotation),
			m_Position(Position),
			m_c(0.0f),
			m_ct(0)
	{}
	
	StartBox::~StartBox(){}

	//初期化
	void StartBox::Create(){
		auto PtrTransform = GetComponent<Transform>();

		PtrTransform->SetScale(m_Scale);
		PtrTransform->SetRotation(m_Rotation);
		PtrTransform->SetPosition(m_Position);

		////操舵系のコンポーネントをつける場合はRigidbodyをつける
		//auto PtrRegid = AddComponent<Rigidbody>();
		//auto PtrObb = AddComponent<CollisionObb>();
		//PtrObb->SetFixed(true);


		Matrix4X4 CharaScale;
		CharaScale.DefTransformation(
			Vector3(0.06f, 0.06f, 0.06f),
			Vector3(0.0f, 0.0f, 0.0f),
			Vector3(0.0f, -0.5f, 0.0f)
			);

		//描画コンポーネント
		auto PtrDraw = AddComponent<BasicFbxPNTDraw>();
		PtrDraw->SetMeshToTransform(CharaScale);
		PtrDraw->SetFbxMeshResource(L"CUBEBLUE_MESH");
		PtrDraw->SetTextureOnlyNoLight(true);
		//PtrDraw->SetOwnShadowActive(true);
		//auto PtrDraw = AddComponent<BasicPNTDraw>();
		//PtrDraw->SetMeshResource(L"DEFAULT_CUBE");
		//PtrDraw->SetTextureResource(m_TextureName);
		PtrDraw->SetDrawActive(true);



		//auto PtrDraw = AddComponent<BasicPNTDraw>();
		//PtrDraw->SetMeshResource(L"DEFAULT_CUBE");
		//PtrDraw->SetTextureResource(L"PURPLECUBE_TX");
		//PtrDraw->SetOwnShadowActive(true);
	


		auto PtrAction = AddComponent<Action>();
		PtrAction->AddRotateBy(20.0f, Vector3(0, XM_PI, 0));
		//ループする
		PtrAction->SetLooped(true);

		PtrAction->Run();




	}

	void StartBox::Update(){
		
	
		//auto PtrTransform = GetComponent<Transform>();
		//auto Rot = PtrTransform->GetRotation();

		//Rot.y += 1.0f;

		//PtrTransform->SetRotation(Rot);
	}
	


}
//end basedx11
