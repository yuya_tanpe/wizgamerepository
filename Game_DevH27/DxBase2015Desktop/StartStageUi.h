#pragma once
#include "stdafx.h"

namespace basedx11{
	//--------------------------------------------------------------------------------------
	//	class FixedBox : public GameObject;
	//	用途: 固定のボックス
	//--------------------------------------------------------------------------------------
	class StartBox : public GameObject{
		Vector3 m_Scale;
		Vector3 m_Rotation;
		Vector3 m_Position;
		float m_c;
		size_t m_ct;
	public:
		//構築と破棄
		StartBox(shared_ptr<Stage>& StagePtr,
			const Vector3& Scale,
			const Vector3& Rotation,
			const Vector3& Position
			);

		//構築と破棄
		virtual ~StartBox();
		//初期化
		virtual void Create() override;
		virtual void Update() override;

		//操作
	};

	/**
	* @class　StateStageUi
	* @brief スプライトを作るクラス.
	* @author syougo kanno
	* @date 2015/10/08 Start
	*/

	class StartStageUi : public GameObject{
		//ビューの作成
		void Createview();
		//スプライトの表示
		void CreateSprite();



	public :
		StartStageUi(shared_ptr<Stage>& StagePtr);
		virtual ~StartStageUi();
		//初期化
		virtual void Create() override;
		virtual void Update() override;
	};




}
//end basedx11
