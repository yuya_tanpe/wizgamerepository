#include "stdafx.h"
#include "Project.h"

namespace basedx11{
	/**
	* @class　Item
	* @brief　アイテム関係の親クラス
	* @author yuyu sike
	* @date 2015/11/30 Start
	*/
	Item::Item(const shared_ptr<Stage>& StagePtr, const Vector3& i_Start_Pos, const wstring& MeshResource, const wstring& TextureResource) :
		GameObject(StagePtr),
		m_Start_Pos(i_Start_Pos),				//初期位置
		m_MeshResource(MeshResource),			//メッシュリソース
		m_TextureResource(TextureResource)		//画像リソース
	{
	}

	void Item::Create(){
		auto PtrTrans = GetComponent<Transform>();
		PtrTrans->SetPosition(m_Start_Pos);
		PtrTrans->SetRotation(0, 0, 0);

		//衝突判定
		auto PtrObb = AddComponent<CollisionObb>();
		PtrObb->SetFixed(false);
		PtrObb->SetDrawActive(false);

		//描画の追加
		auto PtrDraw = AddComponent<BasicPNTDraw>();
		PtrDraw->SetMeshResource(m_MeshResource);				//引数で渡されたMesh、Textureで生成
		PtrDraw->SetTextureResource(m_TextureResource);
		
	}

	//ScoreUp_Item::ScoreUp_Item(const shared_ptr<Stage>& StagePtr, const Vector3& i_Start_Pos, const wstring& MeshResource, const wstring& TextureResource) :
	//	Item(StagePtr, i_Start_Pos, MeshResource, TextureResource)
	//{

	//}

	////変化
	//void ScoreUp_Item::Update(){


	//}

	//void ScoreUp_Item::Update2(){


	//}






}
//endof  basedx11
