#pragma once

#include "stdafx.h"

namespace basedx11{
	/**
	* @class　Item
	* @brief　アイテム関係の親クラス
	* @author yuyu sike
	* @date 2015/11/30 Start
	*/

	class Item : public GameObject {
	private:
		// ---------- member ----------.
		Vector3 m_Start_Pos;				//プレイヤーの位置
		wstring m_MeshResource;				//メッシュリソース
		wstring m_TextureResource;			//画像リソース

	public:
		Item(const shared_ptr<Stage>& StagePtr, const Vector3& i_Start_Pos, const wstring& MeshResource, const wstring& TextureResource);
		virtual ~Item(){}
		//初期化
		virtual void Create() override;
		
	};

	/**
	* @class　ScoreUp_Item
	* @brief　スコアアップするアイテム
	* @author yuyu sike
	* @date 2015/11/30 Start
	*/

	//class ScoreUp_Item : Item {
	//private:

	//public:
	//	ScoreUp_Item(const shared_ptr<Stage>& StagePtr, const Vector3& i_Start_Pos, const wstring& MeshResource, const wstring& TextureResource);
	//	virtual ~ScoreUp_Item(){}
	//	//変化
	//	virtual void Update() override;
	//	virtual void Update2() override;




	//};
}
//endof  basedx11
