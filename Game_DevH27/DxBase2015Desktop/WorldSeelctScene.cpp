#include "stdafx.h"
#include "Project.h"

namespace basedx11{

	//ビューの作成
	void WorldSelectScene::CreateViews(){
		//最初にデフォルトのレンダリングターゲット類を作成する
		CreateDefaultRenderTargets();
		//マルチビューコンポーネントの取得
		auto PtrMultiView = GetComponent<MultiView>();
		//マルチビューにビューの追加
		auto PtrView = PtrMultiView->AddView();
		//ビューの矩形を設定（ゲームサイズ全体）
		Rect2D<float> rect(0, 0, (float)App::GetApp()->GetGameWidth(), (float)App::GetApp()->GetGameHeight());
		//最初のビューにパラメータの設定
		PtrView->ResetParamaters<Camera, MultiLight>(rect, Color4(0.1f, 0.1f, 0.2f, 0.0f), 1, 0.0f, 1.0f);
		//0番目のビューのカメラを得る
		PtrView->SetCamera(dynamic_pointer_cast<Camera>(GetCamera(0)));
		auto PtrCamera = GetCamera(0);
		PtrCamera->SetEye(Vector3(0, 4.0f,-10.0f));			//最初のカメラ位置
		PtrCamera->SetAt(Vector3(0, 0, 0));					//注視ポジション 
	}

	//星の作成
	void WorldSelectScene::CreateWorld_Object(){
		auto Tutorial = AddGameObject<Star_Object>(0,L"Easy");
		auto Ice = AddGameObject<Star_Object>(1,L"Normal");
		auto Volcano = AddGameObject<Star_Object>(2,L"Hard");

		//移動などで使うグループの取得
		auto Star_Group = GetSharedObjectGroup(L"StarGroup");

		//StarGloupにゲームオブジェクトを登録
		Star_Group->IntoGroup(Tutorial);
		Star_Group->IntoGroup(Ice);
		Star_Group->IntoGroup(Volcano);

		//auto sprite = AddGameObject<WorldSelect_UI>();
		//SetSharedGameObject(L"Sprite", sprite);
	}

	//構築
	void WorldSelectScene::Create(){

		//BGMのスタート		
		App::GetApp()->GetScene<Scene>()->StartBGM();

		//エフェクトの追加
		auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
		pMultiSoundEffect->AddAudioResource(L"KETTEI");
		pMultiSoundEffect->AddAudioResource(L"PAUSEOK");

		//グループ
		auto pStarGroup = CreateSharedObjectGroup(L"StarGroup");

		CreateViews();
		CreateWorld_Object();
	}

	//引数のイベントの名前でイベントを変える
	void WorldSelectScene::SomeEvent_Select(wstring EventName , wstring GroupName){
		auto Pause_Group_Vec = GetSharedObjectGroup(GroupName)->GetGroupVector();
		for (auto wpObj : Pause_Group_Vec){
			auto pObj = dynamic_pointer_cast<Star_Object>(wpObj.lock());
			auto Sh_point = pObj->Getpoint();
			//
			////TODO : うんこーど　あとでリファクタリング　必ず
			//if (Sh_point == 0){
			//	PostEvent(0.0f, GetThis<WorldSelectScene>(), pObj, EventName);		//ゲームオブジェクトグループに送る
			//}
			//else if(Sh_point == 1){
			//	PostEvent(0.0f, GetThis<WorldSelectScene>(), pObj, EventName);		//ゲームオブジェクトグループに送る
			//}
			//else if (Sh_point == 2){
			//	PostEvent(0.0f, GetThis<WorldSelectScene>(), pObj, EventName);		//ゲームオブジェクトグループに送る

			//}
			PostEvent(0.0f, GetThis<WorldSelectScene>(), pObj, EventName);		//ゲームオブジェクトグループに送る

		}
	}

	void WorldSelectScene::EventSprite(wstring EventName){
	//	auto Sh = GetSharedGameObject<WorldSelect_UI>(L"Sprite", false);
		//PostEvent(0.0f, GetThis<WorldSelectScene>(), Sh, EventName);
	}
	//コントローラーでの選択
	void WorldSelectScene::StickSelect(){
		//コントローラーの取得
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		if (CntlVec[0].bConnected){

			//スティックがあまり傾いていない時など
			if (CntlVec[0].fThumbLX == 0.0f && CntlVec[0].fThumbLY == 0.0f){
				m_Stick = false;
			}

			else if (m_Stick == false){

				//スティックが左に傾いたとき　
				if (CntlVec[0].fThumbLX < -0.85f){
					SomeEvent_Select(L"MoveLeft", L"StarGroup");
					EventSprite(L"Change_Left");
					m_Stick = true;
				}

				//スティックが右に傾いたとき
				if (CntlVec[0].fThumbLX > 0.85f){
					SomeEvent_Select(L"MoveRight", L"StarGroup");
					EventSprite(L"Change_Right");
					m_Stick = true;
				}
			}
		}
	}

	//Aボタンで選択
	void WorldSelectScene::Is_Abutton(){
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		if (CntlVec[0].bConnected){

			if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_A){
				auto Pause_Group_Vec = GetSharedObjectGroup(L"StarGroup")->GetGroupVector();
				for (auto wpObj : Pause_Group_Vec){
					auto pObj = dynamic_pointer_cast<Star_Object>(wpObj.lock());
					auto Sh_point = pObj->Getpoint();

					//SEを鳴らす
					auto pMultiSoundEffect = AddComponent<MultiSoundEffect>();
					pMultiSoundEffect->Start(L"KETTEI", 0, 0.5f);

					//TODO : うんこーど　あとでリファクタリング　必ず
					if (Sh_point == 0){
						PostEvent(0.2f, GetThis<WorldSelectScene>(), pObj, L"SelectStar");		//ゲームオブジェクトグループに送る
					}
				}
			}
		}
	}

	//Bボタンを押したらタイトルに移動する
	void WorldSelectScene::Is_Bbutton(){
		auto CntlVec = App::GetApp()->GetInputDevice().GetControlerVec();
		if (CntlVec[0].bConnected){
			//Bボタンを押したら
			if (CntlVec[0].wPressedButtons & XINPUT_GAMEPAD_B){
				//タイトル画面に戻る

				//TODO : シーン移動する際にフェード効果をつける
				PostEvent(0.0f, GetThis<WorldSelectScene>(), App::GetApp()->GetSceneBase(), L"ToMenu");
			}
		}
	}

	//変化
	void WorldSelectScene::Update(){
		
		//スティック操作で選択
		StickSelect();

		//Aボタンで真ん中の星に移動
		Is_Abutton();

		//Bボタンでタイトル画面に戻る
		Is_Bbutton();
		


	}

}
//endof  basedx11
