#pragma once

#include "stdafx.h"

namespace basedx11{

	//--------------------------------------------------------------------------------------
	//	class WorldSelectScene : public Stage;
	//	用途: ワールド選択シーン
	//--------------------------------------------------------------------------------------
	class WorldSelectScene : public Stage{
	public:
		//構築と破棄
		WorldSelectScene() :Stage(),
			m_Stick(false)
		{}
		virtual ~WorldSelectScene(){}

		//初期化
		virtual void Create()override;
		//ステート切り替えでUpdate使用中
		virtual void Update()override;

		void SomeEvent_Select(wstring EventName , wstring GroupName);	//引数のイベントの名前でイベントを変える
		void EventSprite(wstring EventName);


	private:
		//ビューの作成
		void CreateViews();
		//星の作成
		void CreateWorld_Object();
		void StickSelect();
		void Is_Abutton();
		//Bボタンでタイトルに移動する
		void Is_Bbutton();
		//スティック操作をしたかしてないか
		bool m_Stick;
	};

	


}
//endof  basedx11
